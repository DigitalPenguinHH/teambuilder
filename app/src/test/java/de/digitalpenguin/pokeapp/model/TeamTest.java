package de.digitalpenguin.pokeapp.model;

import org.junit.Test;

import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TeamTest {

    @Test
    public void testContains() {
        Team team = new Team();
        team.add(new TeamMember(0, 562, 0));
        team.add(new TeamMember(1, 3));
        assertThat(team.contains(4, -1), is(false));
        assertThat(team.contains(4, 0), is(false));
        assertThat(team.contains(4, 1), is(false));
        assertThat(team.contains(3, -1), is(true));
        assertThat(team.contains(3, 0), is(false));
        assertThat(team.contains(3, 1), is(false));
        assertThat(team.contains(562, -1), is(true));
        assertThat(team.contains(562, 0), is(true));
        assertThat(team.contains(562, 1), is(false));
    }

}
