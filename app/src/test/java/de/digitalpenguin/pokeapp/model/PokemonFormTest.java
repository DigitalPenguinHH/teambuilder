package de.digitalpenguin.pokeapp.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.VariantType;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PokemonFormTest {

    @Test
    public void testCompare() {
        List<PokemonForm> list = new ArrayList<>(5);
        list.add(newForm(1, VariantType.GALARIAN));
        list.add(newForm(3, VariantType.DEFAULT));
        list.add(newForm(2, VariantType.DEFAULT));
        list.add(newForm(1, VariantType.ALOLAN));
        list.add(newForm(1, VariantType.DEFAULT));
        Collections.sort(list, PokemonForm.compartor);
        assertThat(list.get(0).getPokemon().getId(), is(1));
        assertThat(list.get(0).getType(), is(VariantType.DEFAULT));
        assertThat(list.get(1).getPokemon().getId(), is(1));
        assertThat(list.get(1).getType(), is(VariantType.ALOLAN));
        assertThat(list.get(2).getPokemon().getId(), is(1));
        assertThat(list.get(2).getType(), is(VariantType.GALARIAN));
        assertThat(list.get(3).getPokemon().getId(), is(2));
        assertThat(list.get(4).getPokemon().getId(), is(3));
    }

    private PokemonForm newForm(int pid, VariantType type) {
        PokemonForm form = new PokemonForm();
        Pokemon pokemon = new Pokemon();
        pokemon.setId(pid);
        form.setPokemonId(pid);
        form.setPokemon(pokemon);
        form.setType(type);
        return form;
    }

}
