package de.digitalpenguin.pokeapp.persistence;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.MachineProperty;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokedex;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.SQLiteDataProvider;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;

import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class SQLiteDataProviderTest extends AbstractPersistenceManagerTest {

    private SQLiteDataProvider dataProvider;

    @Override
    @Before
    public void setUp() {
        Locale.setDefault(Locale.GERMAN);
        super.setUp();
        dataProvider = new SQLiteDataProvider(getPersistenceManager());
    }

    @Test
    public void testFind() {
        List<Object> result = dataProvider.find("was");
        assertThat(result, notNullValue());
        assertThat(result.size(), is(notNullValue()));
    }

    @Test
    public void testGetById() {
        Pokemon p = dataProvider.getById(6);
        System.out.println(p);
        assertThat(p, notNullValue());
        assertThat(p.getNames().getText(Locale.GERMAN), is("Glurak"));
        assertThat(p.getNames().getText(Locale.ENGLISH), is("Charizard"));
        assertThat(p.getProperties(), arrayContaining(PProperty.FIRE, PProperty.FLYING));
    }

    @Test
    public void testGetRandomPokemon() {
        Pokemon p = dataProvider.getRandomPokemon();
        System.out.println(p);
        assertThat(p, notNullValue());
    }

    @Test
    public void testGetRandomPokemonByGeneration() {
        Pokemon p = dataProvider.getRandomPokemon(8);
        System.out.println(p);
        assertThat(p, notNullValue());
    }

    @Test
    public void testGetPokemonDataById() {
        PokemonData p = dataProvider.getPokemonData(6);
        System.out.println(p);
        assertThat(p, notNullValue());
        assertThat(p.getNames().getText(Locale.GERMAN), is("Glurak"));
        assertThat(p.getGenderRatio().getMalePercent(), is(87.5f));
        assertThat(p.getGenderRatio().getFemalePercent(), is(12.5f));
        assertThat(p.getHeight(), is(1.7f));
        assertThat(p.getWeight(), is(90.5f));
        assertThat(p.getDexEntries().getText(Locale.ENGLISH), containsString("It breathes fire"));
        assertThat(p.getBaseStats().getKp(), is(78));
        assertThat(p.getBaseStats().getAtk(), is(84));
        assertThat(p.getBaseStats().getDef(), is(78));
        assertThat(p.getBaseStats().getSpcAtk(), is(109));
        assertThat(p.getBaseStats().getSpcDef(), is(85));
        assertThat(p.getBaseStats().getInit(), is(100));
        assertThat(p.getProperties(), arrayContaining(PProperty.FIRE, PProperty.FLYING));
        assertThat(p.getForms(), notNullValue());
        assertThat(p.getForms().size(), is(4));
        assertThat(p.getForms().get(1).getSuffix(), is("x"));
        assertThat(p.getForms().get(1).getName(Locale.ENGLISH), is("Mega Charizard X"));
        assertThat(p.getForms().get(1).getProperties(), Matchers.arrayContaining(PProperty.FIRE, PProperty.DRAGON));
        assertThat(p.getForms().get(3).getType(), is(VariantType.GIGANTAMAX));
    }

    @Test
    public void testGetPokemonEntries() {
        List<Pokemon> pokemonEntries = dataProvider.getPokemonEntries();
        assertThat(pokemonEntries, notNullValue());
        assertThat(pokemonEntries.size(), is(890));
        assertThat(pokemonEntries.get(0).getId(), is(1));
        assertThat(pokemonEntries.get(889).getId(), is(890));
    }

    @Test
    public void testFindPokemonByFilterName() {
        DataFilter dataFilter = DataFilter.nameFilter("man");
        List<Pokemon> pokemonEntries = dataProvider.findPokemon(dataFilter);
        assertThat(pokemonEntries, notNullValue());
        assertThat(pokemonEntries.size(), is(6));
    }

    @Test
    public void testFindPokemonByFilterRegion() {
        Region region = new Region();
        region.setId(3);
        region.setGeneration(3);
        DataFilter dataFilter = DataFilter.regionFilter(region);
        List<Pokemon> pokemonEntries = dataProvider.findPokemon(dataFilter);
        assertThat(pokemonEntries, notNullValue());
        assertThat(pokemonEntries.size(), is(202));
    }

    @Test
    public void testFindPokemonByFilterNameAndRegion() {
        Region region = new Region();
        region.setId(4);
        region.setGeneration(4);
        DataFilter dataFilter = DataFilter.regionFilter(region);
        dataFilter.setName("man");
        List<Pokemon> pokemonEntries = dataProvider.findPokemon(dataFilter);
        assertThat(pokemonEntries, notNullValue());
        assertThat(pokemonEntries.size(), is(3));
    }

    @Test
    public void testFindPokemonByFilterPropertyAndRegion() {
        Region region = new Region();
        region.setId(4);
        region.setGeneration(4);
        DataFilter dataFilter = DataFilter.propertyFilter(PProperty.DARK);
        dataFilter.setRegion(region);
        List<Pokemon> pokemonEntries = dataProvider.findPokemon(dataFilter);
        assertThat(pokemonEntries, notNullValue());
        assertThat(pokemonEntries.size(), is(12));
    }

    @Test
    public void testGetForms() {
        List<Variant> forms = dataProvider.getForms(6);
        assertThat(forms, notNullValue());
        assertThat(forms.size(), is(4));
        assertThat(forms.get(1).getSuffix(), is("x"));
        assertThat(forms.get(1).getName(Locale.ENGLISH), is("Mega Charizard X"));
        assertThat(forms.get(1).getProperties(), Matchers.arrayContaining(PProperty.FIRE, PProperty.DRAGON));
        assertThat(forms.get(3).getType(), is(VariantType.GIGANTAMAX));
    }

    @Test
    public void testFindPokemonByName() {
        List<Pokemon> results = dataProvider.findPokemon("pu");
        assertThat(results, notNullValue());
        assertThat(results.size(), is(7));
    }

    @Test
    public void testFindPokemonFormsByName() {
        List<PokemonForm> results = dataProvider.findPokemonForms("mau");
        assertThat(results, notNullValue());
        assertThat(results.size(), is(2));
        assertThat(results.get(0).getPokemon(), notNullValue());
        assertThat(results.get(0).getPokemon().getId(), is(52));
        assertThat(results.get(0).getType(), is(VariantType.ALOLAN));
        assertThat(results.get(1).getPokemon(), notNullValue());
        assertThat(results.get(1).getPokemon().getId(), is(52));
        assertThat(results.get(1).getType(), is(VariantType.GALARIAN));
    }

    @Test
    public void testGetMove() {
        Move move = dataProvider.getMove(352);
        assertThat(move, notNullValue());
        assertThat(move.getName(Locale.GERMAN), is("Aquawelle"));
        assertThat(move.getType(), is(MoveType.SPECIAL));
        assertThat(move.getStrength(), is(60));
    }

    @Test
    public void testGetMoveByName() {
        Move move = dataProvider.getMoveByName("Misty Terrain", Locale.ENGLISH);
        assertThat(move, notNullValue());
        assertThat(move.getId(), is(581));
        assertThat(move.getType(), is(MoveType.STATUS));
        assertThat(move.getProperty(), is(PProperty.FAIRY));
        assertThat(move.getGeneration(), is(6));
        assertThat(move.getAp(), is(10));
    }

    @Test
    public void testGetMoves() {
        List<Move> moves = dataProvider.getMoves();
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(796));
    }

    @Test
    public void testListMovesNoCriteria() {
        List<Move> moves = dataProvider.listMoves(null, null);
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(796));
    }

    @Test
    public void testListMovesByType() {
        List<Move> moves = dataProvider.listMoves(MoveType.SPECIAL, null);
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(214));
    }

    @Test
    public void testListMovesByProperty() {
        List<Move> moves = dataProvider.listMoves(null, PProperty.DRAGON);
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(25));
    }

    @Test
    public void testListMovesByPropertyAndType() {
        List<Move> moves = dataProvider.listMoves(MoveType.SPECIAL, PProperty.DRAGON);
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(13));
    }

    @Test
    public void testFindMovesByName() {
        List<Move> moves = dataProvider.findMoves("nebelf");
        assertThat(moves, notNullValue());
        assertThat(moves.size(), is(1));
        assertThat(moves.get(0).getId(), is(581));
        assertThat(moves.get(0).getType(), is(MoveType.STATUS));
        assertThat(moves.get(0).getProperty(), is(PProperty.FAIRY));
        assertThat(moves.get(0).getGeneration(), is(6));
        assertThat(moves.get(0).getAp(), is(10));
    }

    @Test
    public void testGetMoveForMachine() {
        Move move = dataProvider.getMoveForMachine("TM89");
        assertThat(move, notNullValue());
        assertThat(move.getId(), is(581));
    }

    @Test
    public void testHasMovesTrue() {
        PokemonData p = new PokemonData();
        p.setId(25);
        assertThat(dataProvider.hasMoves(PokemonForm.newDefaultForm(p), 7, Game.GAMES_KEY_LETSGO_PIKACHU), is(true));
    }

    @Test
    public void testHasMovesFalse() {
        PokemonData p = new PokemonData();
        p.setId(514);
        assertThat(dataProvider.hasMoves(PokemonForm.newDefaultForm(p), 7, Game.GAMES_KEY_LETSGO_PIKACHU), is(false));
    }

    @Test
    public void testHasMovesTrueWithoutGame() {
        PokemonData p = new PokemonData();
        p.setId(25);
        assertThat(dataProvider.hasMoves(PokemonForm.newDefaultForm(p), 8, null), is(true));
    }

    @Test
    public void testListPokemonMovesLearned() {
        List<MoveLearned> moveLearned = dataProvider.listPokemonMovesLearned(106);
        assertThat(moveLearned, notNullValue());
        assertThat(moveLearned.size(), is(82));
        assertThat(moveLearned.get(0).getPokemonId(), is(11));
        assertThat(moveLearned.get(1).getPokemonId(), is(12));
        assertThat(moveLearned.get(2).getPokemonId(), is(14));
    }

    @Test
    public void testGetGenerationsMoveSet() {
        for (int i = 1; i <= Pokedex.MAX_POKEMON_ID; i++) {
            Pokemon p = dataProvider.getById(i);
            List<Variant> forms = dataProvider.getForms(i);
            if (forms == null || forms.isEmpty()) {
                forms.add(PokemonForm.newDefaultForm(p));
            }
            for (Variant v: forms) {
                if (v.getType() == VariantType.GIGANTAMAX || v.getType() == VariantType.MEGA || v.getType() == VariantType.OTHER)
                    continue;
                Map<Integer, List<MoveLearned>> moveset = dataProvider.getGenerationsMoveSet(PokemonForm.newForm(v, p));
                assertThat(moveset, notNullValue());
                assertThat(moveset.isEmpty(), is(false));
            }
        }
    }

    @Test
    public void testGetEvolutionChainByPokemon() {
        EvolutionChain chain = dataProvider.getEvolutionChain(94);
        assertThat(chain, notNullValue());
        assertThat(chain.getChainLinks().size(), is(2));
        List<EvolutionCondition<?>> firstTrigger = chain.getChainLinks().get(0).getTrigger();
        assertThat(firstTrigger, notNullValue());
        assertThat(firstTrigger.size(), is(1));
        assertThat(firstTrigger.get(0).getObject(), is((Object) Integer.valueOf(25)));
        assertThat(firstTrigger.get(0).getCondition(), is(EvolutionCondition.Type.LEVELUP));
        List<EvolutionCondition<?>> secondTrigger = chain.getChainLinks().get(1).getTrigger();
        assertThat(secondTrigger, notNullValue());
        assertThat(secondTrigger.size(), is(1));
        assertThat(secondTrigger.get(0).getObject(), nullValue());
        assertThat(secondTrigger.get(0).getCondition(), is(EvolutionCondition.Type.TRADE));
    }

    @Test
    public void testListEvolutionChains() {
        List<EvolutionChain> chains = dataProvider.getEvolutionChains();
        assertThat(chains, notNullValue());
        assertThat(chains.size(), is(309));
    }

    @Test
    public void testListEvolutionChainForPokemon() {
        EvolutionChain chain = dataProvider.getEvolutionChain(133);
        assertThat(chain, notNullValue());
        assertThat(chain.getChainLinks().size(), is(8));
    }

    @Test
    public void testGetItems() {
        List<Item> items = dataProvider.getItems();
        assertThat(items, notNullValue());
        assertThat(items.size(), is(820));
    }

    @Test
    public void testListItems() {
        List<Item> items = dataProvider.listItems(ItemCategory.BATTLE_ITEMS);
        assertThat(items, notNullValue());
        assertThat(items.size(), is(35));
    }

    @Test
    public void testFindItemsByName() {
        List<Item> items = dataProvider.findItems("gal");
        assertThat(items, notNullValue());
        assertThat(items.size(), is(3));
    }

    @Ignore //TODO Gen7+8 HMs insert
    @Test
    public void testGetGenerations() {
        List<Generation> generations = dataProvider.getGenerations();
        assertThat(generations, notNullValue());
        assertThat(generations.size(), is(8));
    }

    @Test
    public void testGetRegions() {
        List<Region> regions = dataProvider.getRegions();
        assertThat(regions, notNullValue());
        assertThat(regions.size(), is(8));
    }

    @Test
    public void testGetRegionById() {
        Region region = dataProvider.getRegion(8);
        assertThat(region, notNullValue());
        assertThat(region.getName(Locale.GERMAN), is("Galar"));
    }

    @Test
    public void testGetGames() {
        List<Game> games = dataProvider.getGames();
        assertThat(games, notNullValue());
        assertThat(games.size(), is(32));
    }

    @Test
    public void testGetPokemonFormByIdx() {
        PokemonForm form = dataProvider.getPokemonForm(52, 2);
        assertThat(form.getPokemon(), notNullValue());
        assertThat(form.getType(), is(VariantType.GALARIAN));
    }

    @Test
    public void testGetPokemonFormByNegativeIdx() {
        PokemonForm form = dataProvider.getPokemonForm(1, -1);
        assertThat(form.getPokemon(), notNullValue());
        assertThat(form.getPokemon().getName(Locale.GERMAN), is("Bisasam"));
        assertThat(form.getType(), is(VariantType.DEFAULT));
    }

    @Test
    public void testGetPokemonFormByType() {
        PokemonForm form = dataProvider.getPokemonForm(52, VariantType.ALOLAN);
        assertThat(form.getPokemon(), notNullValue());
        assertThat(form.getType(), is(VariantType.ALOLAN));
    }

    @Test
    public void testGetPokemonFormByDefaultType() {
        PokemonForm form = dataProvider.getPokemonForm(1, VariantType.DEFAULT);
        assertThat(form.getPokemon(), notNullValue());
        assertThat(form.getPokemon().getName(Locale.GERMAN), is("Bisasam"));
        assertThat(form.getType(), is(VariantType.DEFAULT));
    }

    @Test
    public void testListPokemonHavingAbility() {
        List<PokemonAbility> result = dataProvider.listPokemonWithAbility(63);
        assertThat(result.size(), is(3));
        assertThat(result.get(0).getPokemon(), notNullValue());
        assertThat(result.get(0).getAbility(), notNullValue());
        assertThat(result.get(1).getPokemon(), notNullValue());
        assertThat(result.get(1).getAbility(), notNullValue());
        assertThat(result.get(2).getPokemon(), notNullValue());
        assertThat(result.get(2).getAbility(), notNullValue());
    }

    @Test
    public void testListPokemonAbilitiesWithForm() {
        List<PokemonAbility> result = dataProvider.listAbilities(89, VariantType.ALOLAN);
        assertThat(result.size(), is(3));
        assertThat(result.get(0).getPokemon(), notNullValue());
        assertThat(result.get(0).getAbility(), notNullValue());
        assertThat(result.get(0).getAbility().getName("de"), is("Giftgriff"));
        assertThat(result.get(0).isHidden(), is(false));
        assertThat(result.get(1).getPokemon(), notNullValue());
        assertThat(result.get(1).getAbility(), notNullValue());
        assertThat(result.get(1).getAbility().getName("de"), is("Völlerei"));
        assertThat(result.get(1).isHidden(), is(false));
        assertThat(result.get(2).getPokemon(), notNullValue());
        assertThat(result.get(2).getAbility(), notNullValue());
        assertThat(result.get(2).getAbility().getName("de"), is("Chemiekraft"));
        assertThat(result.get(2).isHidden(), is(true));
    }

    @Test
    public void testListPokemonAbilities() {
        List<PokemonAbility> result = dataProvider.listAbilities(89, VariantType.DEFAULT);
        assertThat(result.size(), is(3));
        assertThat(result.get(0).getPokemon(), notNullValue());
        assertThat(result.get(0).getAbility(), notNullValue());
        assertThat(result.get(0).getAbility().getName("de"), is("Duftnote"));
        assertThat(result.get(0).isHidden(), is(false));
        assertThat(result.get(1).getPokemon(), notNullValue());
        assertThat(result.get(1).getAbility(), notNullValue());
        assertThat(result.get(1).getAbility().getName("de"), is("Klebekörper"));
        assertThat(result.get(1).isHidden(), is(false));
        assertThat(result.get(2).getPokemon(), notNullValue());
        assertThat(result.get(2).getAbility(), notNullValue());
        assertThat(result.get(2).getAbility().getName("de"), is("Giftgriff"));
        assertThat(result.get(2).isHidden(), is(true));
    }

    @Test
    public void testListMachineProperties() {
        List<MachineProperty> result = dataProvider.listMachineProperties();
        assertThat(result.size() > 0, is(true));
        for (MachineProperty mp : result) {
            assertThat(mp.getMove(), notNullValue());
        }
    }

}
