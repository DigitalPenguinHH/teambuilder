package de.digitalpenguin.pokeapp.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokedex;
import de.digitalpenguin.pokeapp.data.PokedexEntry;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Region;

import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class PersistenceManagerTest extends AbstractPersistenceManagerTest {

    @Test
    public void testLoadAbility() {
        Ability a = persistenceManager.load(Ability.class, 167);
        System.out.println(a);
        assertThat(a, notNullValue());
        assertThat(a.getNames().getText(Locale.GERMAN), is("Backentaschen"));
        assertThat(a.getDescriptions().getText(Locale.ENGLISH), containsString("Restores HP"));
        assertThat(a.getSinceGen(), is(6));
    }

    @Test
    public void testInsertAbility() {
        Ability a = new Ability();
        a.setId(9999);
        LocalizedText names = new LocalizedText();
        names.addText("de", "Stampfer");
        names.addText("en", "Stomp");
        a.setSinceGen(5);
        a.setNames(names);
        persistenceManager.persist(a);
        assertThat(persistenceManager.load(Ability.class, a.getId()), notNullValue());
        persistenceManager.delete(a);
        assertThat(persistenceManager.load(Ability.class, a.getId()), nullValue());
    }

    @Test
    public void testLoadItem() {
        Item item = getPersistenceManager().load(Item.class,154);
        System.out.println(item);
        assertThat(item, notNullValue());
        assertThat(item.getNames().getText(Locale.GERMAN), is("Heilball"));
        assertThat(item.getDescriptions().getText(Locale.ENGLISH), containsString("A remedial Poké Ball that restores"));
        assertThat(item.getCategory(), is(ItemCategory.POKEBALLS));
    }

    @Test
    public void testLoadMove() {
        Move m = getPersistenceManager().load(Move.class,450);
        System.out.println(m);
        assertThat(m, notNullValue());
        assertThat(m.getNames().getText(Locale.GERMAN), is("Käferbiss"));
        assertThat(m.getProperty(), is(PProperty.BUG));
        assertThat(m.getType(), is(MoveType.PHYSICAL));
        assertThat(m.isDamageFix(), is(false));
        assertThat(m.isDamageVariable(), is(false));
        assertThat(m.getStrength(), is(60));
        assertThat(m.getAccuracy(), is(100));
        assertThat(m.getAp(), is(20));
        assertThat(m.getContestCategory(), is(ContestCategory.CUTENESS));
        assertThat(m.getGeneration(), is(4));
        assertThat(m.getDescriptions().getText(Locale.ENGLISH), containsString("The user bites the target"));
    }

    @Test
    public void testInsertRegion() {
        Region r = new Region();
        r.setId(9999);
        LocalizedText names = new LocalizedText();
        names.addText("de", "Hoenn");
        names.addText("en", "Unova");
        r.setNames(names);
        r.setGeneration(3);
        Pokedex pokedex = new Pokedex();
        pokedex.setId(998);
        List<PokedexEntry> entries = Arrays.asList(
                new PokedexEntry(100001, 1, 495, 998),
                new PokedexEntry(100002, 2, 496, 998),
                new PokedexEntry(100003, 3, 497, 998)
        );
        pokedex.setEntries(entries);
        pokedex.setRegion(r);
        r.setPokedex(pokedex);
        persistenceManager.persist(r);
        assertThat(persistenceManager.load(Region.class, r.getId()), notNullValue());
        persistenceManager.delete(r);
        assertThat(persistenceManager.load(Region.class, r.getId()), nullValue());
    }

    @Test
    public void testLoadRegion() {
        Region r = getPersistenceManager().load(Region.class, 8);
        System.out.println(r);
        assertThat(r, notNullValue());
        assertThat(r.getNames().getText(Locale.GERMAN), is("Galar"));
        assertThat(r.getGeneration(), is(8));
        assertThat(r.getPokedex(), notNullValue());
        assertThat(r.getPokedex().getEntries(), notNullValue());
        assertThat(r.getPokedex().getEntries().size(), is(not(0)));
    }

    @Test
    public void testLoadPokemon() {
        PokemonData p = getPersistenceManager().load(PokemonData.class,6);
        System.out.println(p);
        assertThat(p, notNullValue());
        assertThat(p.getNames().getText(Locale.GERMAN), is("Glurak"));
        assertThat(p.getGenderRatio().getMalePercent(), is(87.5f));
        assertThat(p.getGenderRatio().getFemalePercent(), is(12.5f));
        assertThat(p.getHeight(), is(1.7f));
        assertThat(p.getWeight(), is(90.5f));
        assertThat(p.getDexEntries().getText(Locale.ENGLISH), containsString("It breathes fire"));
        assertThat(p.getBaseStats().getKp(), is(78));
        assertThat(p.getBaseStats().getAtk(), is(84));
        assertThat(p.getBaseStats().getDef(), is(78));
        assertThat(p.getBaseStats().getSpcAtk(), is(109));
        assertThat(p.getBaseStats().getSpcDef(), is(85));
        assertThat(p.getBaseStats().getInit(), is(100));
        assertThat(p.getProperties(), arrayContaining(PProperty.FIRE, PProperty.FLYING));
        assertThat(p.getForms(), notNullValue());
        assertThat(p.getForms().size(), is(4));
    }

    @Test
    public void testListAllPokemon() {
        List<Pokemon> pokemons = getPersistenceManager().loadList(Pokemon.class, QueryBuilder.forType(Pokemon.class));
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(890));
    }

    @Test
    public void testListPokemonByPropertiesExact() {
        List<Pokemon> pokemons = getPersistenceManager().loadList(Pokemon.class, QueryBuilder.forType(Pokemon.class).eq("properties", "PSYCHIC,FAIRY"));
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(7));
    }

    @Test
    public void testListPokemonByPropertiesLike() {
        List<Pokemon> pokemons = getPersistenceManager().loadList(Pokemon.class, QueryBuilder.forType(Pokemon.class).like("properties", PProperty.FAIRY));
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(54));
    }

    @Test
    public void testListPokemonByName() {
        List<Pokemon> pokemons = getPersistenceManager().loadList(Pokemon.class, QueryBuilder.forType(Pokemon.class).likeIgnoreCase("names", "de=Glurak"));
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(1));
    }

    @Test
    public void testListPokemonByNamePart() {
        List<Pokemon> pokemons = getPersistenceManager().loadList(Pokemon.class, QueryBuilder.forType(Pokemon.class).likeIgnoreCase("names", "en=Cha"));
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(7));
        printResult(pokemons);
    }

    @Test
    public void testListNatures() {
        List<Nature> natures = getPersistenceManager().loadList(Nature.class, QueryBuilder.forType(Nature.class));
        assertThat(natures, notNullValue());
        assertThat(natures.size(), is(46));
    }

    @Test
    public void testListCharacteristics() {
        List<Characteristic> characteristics = getPersistenceManager().loadList(Characteristic.class, QueryBuilder.forType(Characteristic.class));
        assertThat(characteristics, notNullValue());
        assertThat(characteristics.size(), is(30));
    }

    @Test
    public void testLoadHiddenMachines() {
        List<HiddenMachine> machines = getPersistenceManager().loadList(HiddenMachine.class, QueryBuilder.forType(HiddenMachine.class));
        assertThat(machines, notNullValue());
        assertThat(machines.size(), is(40));
        assertThat(machines.iterator().next().getMove(), notNullValue());
    }

    private void printResult(List<Pokemon> list) {
        for (Serializable o : list) {
            System.out.println(o.toString());
        }
    }
}
