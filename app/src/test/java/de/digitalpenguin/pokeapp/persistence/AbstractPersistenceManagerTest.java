package de.digitalpenguin.pokeapp.persistence;

import android.database.sqlite.SQLiteDatabase;

import org.junit.After;
import org.junit.Before;

import java.io.File;

import de.digitalpenguin.pokeapp.util.ILogger;

public abstract class AbstractPersistenceManagerTest {

    public static final String ASSETS_DIR = "..\\app\\src\\main\\assets";
    public static final String RES_DIR = "..\\app\\src\\main\\res";
    public static final String DB_FILE = "teambuilder";

    protected SQLiteDatabase db;
    protected PersistenceManager persistenceManager;

    private ILogger logger = new ILogger() {
        @Override
        public void log(String message) {
            System.out.println(message);
        }
        @Override
        public void error(String message) {
            System.err.println(message);
        }
    };

    @Before
    public void setUp() {
        db = SQLiteDatabase.openDatabase(new File(ASSETS_DIR, DB_FILE), new SQLiteDatabase.OpenParams.Builder().build());
        persistenceManager = new PersistenceManager(db, logger);
    }

    @After
    public void tearDown() {
        if (persistenceManager != null) {
            try {
                persistenceManager.getDatabase().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void withTransaction(Runnable r) {
        persistenceManager.getDatabase().beginTransaction();
        try {
            r.run();
            persistenceManager.getDatabase().setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            persistenceManager.getDatabase().endTransaction();
        }

    }

    public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }
}
