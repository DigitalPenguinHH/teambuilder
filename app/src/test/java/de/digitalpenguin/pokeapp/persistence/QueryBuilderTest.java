package de.digitalpenguin.pokeapp.persistence;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import de.digitalpenguin.pokeapp.data.Pokemon;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Unit Tests for the {@link QueryBuilder}.
 * 
 * @author meyer38
 */
public class QueryBuilderTest {
	
	private QueryBuilder<Pokemon> builder;
	
	@Before
	public void setUp() {
		builder = QueryBuilder.forType(Pokemon.class);
	}

	@Test
	public void testSimple() {
		assertThat(builder
				.getQueryString(), is(defaultResult()));
	}

	@Test
	public void testSimpleToString() {
		assertThat(builder
				.toString(), is(defaultResult()));
	}
	
	@Test
	public void testAttributeNull() {
		assertThat(builder
				.eq("id", null)
				.getQueryString(), is(defaultResult() + " AND o.id IS null"));
	}
	
	@Test
	public void testAttributeIsNull() {
		assertThat(builder
				.isNull("id")
				.getQueryString(), is(defaultResult() + " AND o.id IS null"));
	}
	
	@Test
	public void testAttributeIsNotNull() {
		assertThat(builder
				.isNotNull("name")
				.getQueryString(), is(defaultResult() + " AND o.name IS NOT null"));
	}
	
	@Test
	public void testAttributeEquals() {
		assertThat(builder
				.eq("name", 1)
				.getQueryString(), is(defaultResult() + " AND o.name = ?"));
	}
	
	@Test
	public void testAttributeEqualsMultiple() {
		assertThat(builder
				.eq("id", 1)
				.like("name", "de=Bisasam")
				.getQueryString(), is(defaultResult() + " AND o.id = ? AND o.name LIKE '%de=Bisasam%'"));
	}
	
	@Test
	public void testWithId() {
		assertThat(builder
				.withId(99)
				.getQueryString(), is(defaultResult() + " AND o.id = ?"));
	}
	
	@Test
	public void testDistinct() {
		assertThat(builder
				.distinct()
				.getQueryString(), is("SELECT DISTINCT o.* FROM POKEMON AS o WHERE 1=1"));
	}
	
	@Test
	public void testAttributeGreaterThan() {
		assertThat(builder
				.gt("id", 1) 
				.getQueryString(), is(defaultResult() + " AND o.id > ?"));
	}
	
	@Test
	public void testAttributeGreaterThanEquals() {
		assertThat(builder
				.gte("id", 1) 
				.getQueryString(), is(defaultResult() + " AND o.id >= ?"));
	}
	
	@Test
	public void testAttributeLessThan() {
		assertThat(builder
				.lt("id", 1) 
				.getQueryString(), is(defaultResult() + " AND o.id < ?"));
	}
	
	@Test
	public void testAttributeLessThanEquals() {
		assertThat(builder
				.lte("id", 1) 
				.getQueryString(), is(defaultResult() + " AND o.id <= ?"));
	}
	
	@Test
	public void testAttributeInNotEmpty() {
		assertThat(builder
				.in("id", Arrays.asList(1, 10, 22))
				.getQueryString(), is(defaultResult() + " AND o.id IN (?)"));
	}
	
	@Test
	public void testAttributeInEmptyOmit() {
		assertThat(builder
				.in("id", Collections.emptyList())
				.getQueryString(), is(defaultResult()));
	}
	
	@Test
	public void testStringAttributeLike() {
		assertThat(builder
				.like("updateUser", "Test")
				.getQueryString(), is(defaultResult() + " AND o.updateUser LIKE '%Test%'"));
	}
	
	@Test
	public void testStringAttributeLikeIgnoreCase() {
		assertThat(builder
				.likeIgnoreCase("updateUser", "Test")
				.getQueryString(), is(defaultResult() + " AND lower(o.updateUser) LIKE '%test%'"));
	}
	
	@Test
	public void testStringAttributeEndsWith() {
		assertThat(builder
				.endsWith("updateUser", "123")
				.getQueryString(), is(defaultResult() + " AND o.updateUser LIKE '%123'"));
	}
	
	@Test
	public void testStringAttributeStartsWith() {
		assertThat(builder
				.startsWith("updateUser", "123")
				.getQueryString(), is(defaultResult() + " AND o.updateUser LIKE '123%'"));
	}
	
	@Test
	public void testStringAttributeEndsWithIgnoreCase() {
		assertThat(builder
				.endsWithIgnoreCase("updateUser", "TESTER")
				.getQueryString(), is(defaultResult() + " AND lower(o.updateUser) LIKE '%tester'"));
	}
	
	@Test
	public void testStringAttributeStartsWithIgnoreCase() {
		assertThat(builder
				.startsWithIgnoreCase("updateUser", "TESTER")
				.getQueryString(), is(defaultResult() + " AND lower(o.updateUser) LIKE 'tester%'"));
	}
	
	@Test
	public void testLimit() {
		assertThat(builder
				.eq("id", 1)
				.limit(1000)
				.getQueryString(), is(defaultResult() + " AND o.id = ? LIMIT 1000"));
	}
	
	@Test
	public void testOrderBy() {
		assertThat(builder
				.eq("id", 1)
				.orderBy("updateTime")
				.getQueryString(), is(defaultResult() + " AND o.id = ? ORDER BY o.updateTime ASC"));
		// Make sure multiple invocations
		assertThat(builder.getQueryString(), is(defaultResult() + " AND o.id = ? ORDER BY o.updateTime ASC"));
	}
	
	@Test
	public void testOrderByDescending() {
		assertThat(builder
				.eq("id", 1)
				.orderByDescending("updateTime")
				.getQueryString(), is(defaultResult() + " AND o.id = ? ORDER BY o.updateTime DESC"));
	}
	
	@Test
	public void testOrderByDescendingLimit() {
		assertThat(builder
				.eq("id", 1)
				.orderByDescending("updateTime")
				.limit(1000)
				.getQueryString(), is(defaultResult() + " AND o.id = ? LIMIT 1000 ORDER BY o.updateTime DESC"));
	}
	
	@Test
	public void testOrderByMultiple() {
		assertThat(builder
				.orderBy("updateTime", "updateUser")
				.getQueryString(), is(defaultResult() + " ORDER BY o.updateTime, o.updateUser ASC"));
	}
	
	@Test
	public void testOrderByMultipleDescending() {
		assertThat(builder
				.orderByDescending("updateTime", "updateUser")
				.getQueryString(), is(defaultResult() + " ORDER BY o.updateTime, o.updateUser DESC"));
	}
	
	@Test
	public void testOrderByName() {
		assertThat(builder
				.orderByName()
				.getQueryString(), is(defaultResult() + " ORDER BY o.name ASC"));
	}
	
	@Test
	public void testOrderByNameCaseInsensitive() {
		assertThat(builder
				.orderByName(false)
				.getQueryString(), is(defaultResult() + " ORDER BY lower(o.name) ASC"));
	}
	
	@Test
	public void testOrAttributesEquals() {
		assertThat(builder
				.orEq(1, "id", "userId", "version")
				.getQueryString(), is(defaultResult() + " AND (o.id = ? OR o.userId = ? OR o.version = ?)"));
	}

	private String defaultResult() {
		return "SELECT o.* FROM POKEMON AS o WHERE 1=1";
	}
	
}
