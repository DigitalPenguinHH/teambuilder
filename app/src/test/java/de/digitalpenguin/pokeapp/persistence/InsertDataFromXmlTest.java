package de.digitalpenguin.pokeapp.persistence;

import com.thoughtworks.xstream.XStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.AbilitiesDB;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.CharacteristicsDB;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainDB;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemsDB;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.NaturesDB;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.SQLiteDataProvider;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.util.XStreamHelper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class InsertDataFromXmlTest extends PersistenceManagerTest {

//    @Test
    public void testInsertAbilities() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamPokemon(new SQLiteDataProvider(getPersistenceManager()));
        // Load Abilities
        final AbilitiesDB db = (AbilitiesDB) xstream.fromXML(new File(RES_DIR + "\\raw\\abilities.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Ability a: db.getEntries()) {
                getPersistenceManager().persist(a);
            }
            }
        });
    }

//    @Test
    public void testInsertMoves() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamPokemon(new SQLiteDataProvider(getPersistenceManager()));
        // Load Abilities
        final MovesDB db = (MovesDB) xstream.fromXML(new File(RES_DIR + "\\raw\\moves.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getMoves(), not(hasSize(0)));

        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Move m: db.getMoves()) {
                getPersistenceManager().persist(m);
            }
            }
        });
    }

//    @Test
    public void testInsertMovesLearned() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamPokemon(new SQLiteDataProvider(getPersistenceManager()));
        // Load Abilities
        final PokemonDB db = (PokemonDB) xstream.fromXML(new File(RES_DIR + "\\raw\\pokemon.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (PokemonData p: db.getEntries()) {
                for (MoveLearned ml: p.getMovesLearned()) {
                    ml.setId(idx.incrementAndGet());
                    getPersistenceManager().persist(ml);
                }
                System.out.println("MovesLearned inserted for " + p.getId());
            }
            }
        });
    }

//    @Test
    public void testInsertPokemon() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamPokemon(new SQLiteDataProvider(getPersistenceManager()));
        // Load Abilities
        final PokemonDB db = (PokemonDB) xstream.fromXML(new File(RES_DIR + "\\raw\\pokemon.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Pokemon p: db.getEntries()) {
                getPersistenceManager().persist(p);
            }
            }
        });
    }

//    @Test
    public void testInsertVariants() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamPokemon(new SQLiteDataProvider(getPersistenceManager()));
        // Load Abilities
        final PokemonDB db = (PokemonDB) xstream.fromXML(new File(RES_DIR + "\\raw\\pokemon.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (PokemonData p : db.getEntries()) {
                if (p.getForms() == null)
                    continue;
                for (Variant v: p.getForms()) {
                    v.setId(idx.incrementAndGet());
                    getPersistenceManager().persist(v);
                }
            }
            }
        });
    }

//    @Test
    public void testInsertItems() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamItems();
        // Load Items
        final ItemsDB db = (ItemsDB) xstream.fromXML(new File(RES_DIR + "\\raw\\items.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Item i: db.getEntries()) {
                i.setId(idx.incrementAndGet());
                getPersistenceManager().persist(i);
            }
            }
        });
    }

//    @Test
    public void testInsertGames() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamGamesAndRegions();
        // Load Items
        final GamesDB db = (GamesDB) xstream.fromXML(new File(RES_DIR + "\\raw\\games.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getGames(), not(hasSize(0)));

        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Game g: db.getGames()) {
                getPersistenceManager().persist(g);
            }
            }
        });
    }

//    @Test
    public void testInsertNatures() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamNatures();
        // Load Items
        final NaturesDB db = (NaturesDB) xstream.fromXML(new File(RES_DIR + "\\raw\\natures.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Nature n : db.getEntries()) {
                n.setId(idx.incrementAndGet());
                getPersistenceManager().persist(n);
            }
            }
        });
    }

//    @Test
    public void testInsertCharacteristics() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamCharacteristics();
        // Load Items
        final CharacteristicsDB db = (CharacteristicsDB) xstream.fromXML(new File(RES_DIR + "\\raw\\characteristics.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getEntries(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Characteristic c : db.getEntries()) {
                c.setId(idx.incrementAndGet());
                getPersistenceManager().persist(c);
            }
            }
        });
    }

//    @Test
    public void testInsertGenerations() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamGamesAndRegions();
        // Load Items
        final GamesDB db = (GamesDB) xstream.fromXML(new File(RES_DIR + "\\raw\\games.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getGenerations(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        final AtomicInteger idxHm = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
            for (Generation g : db.getGenerations()) {
                g.setId(idx.incrementAndGet());
                for (HiddenMachine hm : g.getHms()) {
                    hm.setId(idxHm.getAndIncrement());
                }
                getPersistenceManager().persist(g);
            }
            }
        });
    }

    @Test
    public void testInsertEvoChains() {

        // Set Up XStream
        XStream xstream = XStreamHelper.initXStreamEvochains(new SQLiteDataProvider(getPersistenceManager()));
        // Load Items
        final EvolutionChainDB db = (EvolutionChainDB) xstream.fromXML(new File(RES_DIR + "\\raw\\evochains.xml"));
        assertThat(db, notNullValue());
        assertThat(db.getChains(), not(hasSize(0)));

        final AtomicInteger idx = new AtomicInteger(0);
        final AtomicInteger idxLink = new AtomicInteger(0);
        final AtomicInteger idxCond = new AtomicInteger(0);
        // Insert each
        withTransaction(new Runnable() {
            @Override
            public void run() {
                for (EvolutionChain chain : db.getChains()) {
                    chain.setId(idx.incrementAndGet());
                    for (EvolutionChainLink link : chain.getChainLinks()) {
                        link.setId(idxLink.incrementAndGet());
                        for (EvolutionCondition<?> cond : link.getTrigger()) {
                            cond.setId(idxCond.incrementAndGet());
                            cond.setChainLink(link.getId());
                        }
                        link.setChainId(chain.getId());
                    }
                    getPersistenceManager().persist(chain);
                }
            }
        });
    }

}
