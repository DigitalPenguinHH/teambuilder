package de.digitalpenguin.pokeapp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.comparator.NamedLexicalComparator;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.Pokemon;

public class NamedLexicalComparatorTest {

    @Test
    public void testCompare() {

        Locale.setDefault(Locale.ENGLISH);

        List list = new LinkedList();

        Item i1 = new Item("Trank");
        list.add(i1);

        Pokemon p2 = new Pokemon();
        p2.addName("en", "Weepinbell");
        list.add(p2);

        Pokemon p1 = new Pokemon();
        p1.addName("en", "Beedle");
        list.add(p1);

        Pokemon p3 = new Pokemon();
        p3.addName("en", "Volbeat");
        list.add(p3);

        Item i2 = new Item("Beleber");
        list.add(i2);

        Collections.sort(list, new NamedLexicalComparator("Be"));

        Assert.assertTrue(list.get(0) == p1);
        Assert.assertTrue(list.get(1) == i2);
        Assert.assertTrue(list.get(2) == p3);
        Assert.assertTrue(list.get(3) == p2);
        Assert.assertTrue(list.get(3) == i1);

    }


}
