package de.digitalpenguin.pokeapp;

import junit.framework.Assert;

import org.apache.commons.lang3.Range;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.util.PokemonSearchResultLexicalInfo;

public class PokemonSearchResultLexicalInfoTest {

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(new Locale("de", "DE"));
    }

    @Test
    public void testRange() {
        Pokemon p = new Pokemon();
        p.addName("de", "Smettbo");

        PokemonSearchResultLexicalInfo info1 = new PokemonSearchResultLexicalInfo("me", p.getName());
        Assert.assertEquals(3, info1.getOccurrences().size());
        Assert.assertEquals(Range.between(0,1), info1.getOccurrences().get(0).getRange());
        Assert.assertEquals("S", info1.getOccurrences().get(0).getText());
        Assert.assertEquals(false, info1.getOccurrences().get(0).isMatchText());
        Assert.assertEquals(Range.between(1,3), info1.getOccurrences().get(1).getRange());
        Assert.assertEquals("me", info1.getOccurrences().get(1).getText());
        Assert.assertEquals(true, info1.getOccurrences().get(1).isMatchText());
        Assert.assertEquals(Range.between(3,7), info1.getOccurrences().get(2).getRange());
        Assert.assertEquals("ttbo", info1.getOccurrences().get(2).getText());
        Assert.assertEquals(false, info1.getOccurrences().get(2).isMatchText());

        PokemonSearchResultLexicalInfo info2 = new PokemonSearchResultLexicalInfo("Smett", p.getName());
        Assert.assertEquals(2, info2.getOccurrences().size());
        Assert.assertEquals(Range.between(0,5), info2.getOccurrences().get(0).getRange());
        Assert.assertEquals("Smett", info2.getOccurrences().get(0).getText());
        Assert.assertEquals(true, info2.getOccurrences().get(0).isMatchText());
        Assert.assertEquals(Range.between(5,7), info2.getOccurrences().get(1).getRange());
        Assert.assertEquals("bo", info2.getOccurrences().get(1).getText());
        Assert.assertEquals(false, info2.getOccurrences().get(1).isMatchText());

        PokemonSearchResultLexicalInfo info3 = new PokemonSearchResultLexicalInfo("bo", p.getName());
        Assert.assertEquals(2, info3.getOccurrences().size());
        Assert.assertEquals(Range.between(0,5), info3.getOccurrences().get(0).getRange());
        Assert.assertEquals("Smett", info3.getOccurrences().get(0).getText());
        Assert.assertEquals(false, info3.getOccurrences().get(0).isMatchText());
        Assert.assertEquals(Range.between(5,7), info3.getOccurrences().get(1).getRange());
        Assert.assertEquals("bo", info3.getOccurrences().get(1).getText());
        Assert.assertEquals(true, info3.getOccurrences().get(1).isMatchText());
    }

}
