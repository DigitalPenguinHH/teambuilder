package de.digitalpenguin.pokeapp.xml;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.xml.XPathDataProvider;

import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class XPathDataProviderTest {

    private static final String RAW_RES_PATH = "..\\app\\src\\main\\res\\raw\\";

    private XPathDataProvider dataProvider;

    @Before
    public void setUp() {
        try {
            this.dataProvider = new XPathDataProvider();
            this.dataProvider.addSupport(Pokemon.class, new FileInputStream(RAW_RES_PATH.concat("pokemon.xml")));
            this.dataProvider.addSupport(PokemonData.class, new FileInputStream(RAW_RES_PATH.concat("pokemon.xml")));
            this.dataProvider.addSupport(Variant.class, new FileInputStream(RAW_RES_PATH.concat("pokemon.xml")));
        } catch (FileNotFoundException e) {
            Assert.fail("Unable to create DataXPathDataProvider. " + e.getMessage());
        }
    }

    @Test
    public void testLoadPokemonById() {
        Pokemon p = dataProvider.getById(6);
        System.out.println(p);
        assertThat(p, notNullValue());
        assertThat(p.getNames().getText(Locale.GERMAN), is("Glurak"));
        assertThat(p.getProperties(), arrayContaining(PProperty.FIRE, PProperty.FLYING));
    }

    @Test
    public void testListAllPokemon() {
        List<Pokemon> pokemons = dataProvider.getPokemonEntries();
        printResult(pokemons);
        assertThat(pokemons, notNullValue());
        assertThat(pokemons.size(), is(890));
    }

    @Test
    public void testLoadPokemonData() {
        PokemonData p = dataProvider.getPokemonData(6);
        System.out.println(p);
        assertThat(p, notNullValue());
        assertThat(p.getNames().getText(Locale.GERMAN), is("Glurak"));
        assertThat(p.getProperties(), arrayContaining(PProperty.FIRE, PProperty.FLYING));
        assertThat(p.getForms(), notNullValue());
        assertThat(p.getForms().size(), is(4));
        assertThat(p.getForms().get(1).getSuffix(), is("x"));
        assertThat(p.getForms().get(1).getName(Locale.ENGLISH), is("Mega Charizard X"));
        assertThat(p.getForms().get(1).getProperties(), Matchers.arrayContaining(PProperty.FIRE, PProperty.DRAGON));
        assertThat(p.getForms().get(3).getType(), is(VariantType.GIGANTAMAX));
    }

    private void printResult(List<Pokemon> list) {
        for (Serializable o : list) {
            System.out.println(o.toString());
        }
    }

}
