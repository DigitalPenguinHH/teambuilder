package de.digitalpenguin.pokeapp.listener;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

public class OnClickToaster implements View.OnClickListener {

    private final Context context;

    private final String text;

    private int length = Toast.LENGTH_LONG;

    public OnClickToaster(Context context, String text) {
        this.context = context;
        this.text = text;
    }

    public OnClickToaster setShort() {
        this.length = Toast.LENGTH_SHORT;
        return this;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(context, text, length).show();
    }
}
