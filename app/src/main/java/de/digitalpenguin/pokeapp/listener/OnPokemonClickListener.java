package de.digitalpenguin.pokeapp.listener;

import androidx.fragment.app.FragmentManager;
import android.view.View;

import de.digitalpenguin.pokeapp.fragments.PokemonDetailFragment;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class OnPokemonClickListener implements View.OnClickListener {

    private final FragmentManager fragmentManager;

    /* Params */
    private final int formIdx;
    private final int pokemonId;
    private final Boolean animate;

    public OnPokemonClickListener(FragmentManager fm, int pokemonId, int formIdx, Boolean animate) {
        this.fragmentManager = fm;
        this.pokemonId = pokemonId;
        this.formIdx = formIdx;
        this.animate = animate;
    }

    @Override
    public void onClick(View view) {
        UIHelper.swapFragment(fragmentManager, PokemonDetailFragment.newInstance(pokemonId, formIdx, true), animate == null || animate);
    }

}
