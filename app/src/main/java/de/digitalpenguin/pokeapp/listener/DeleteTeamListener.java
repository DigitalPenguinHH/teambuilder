package de.digitalpenguin.pokeapp.listener;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ArrayAdapter;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class DeleteTeamListener implements View.OnClickListener {

    private final ArrayAdapter listAdapter;
    private final Context context;
    private Team team;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public DeleteTeamListener(Context context, Team team, ArrayAdapter adapter) {
        this.context = context;
        this.team = team;
        this.listAdapter = adapter;
    }

    @Override
    public void onClick(View view) {
        if (TeamsStore.getInstance().deleteTeam(team)) {
            XMLHelper.persistTeams(context);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    listAdapter.notifyDataSetChanged();
                }
            });
        }
    }
}
