package de.digitalpenguin.pokeapp.listener;

import android.content.Context;
import android.content.DialogInterface;
import android.view.MenuItem;
import android.view.View;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.TeamListAdapter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.dialog.ChangeTeamIconDialog;
import de.digitalpenguin.pokeapp.dialog.TeamRenameDialog;
import de.digitalpenguin.pokeapp.dialog.TeamSizeDialog;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class TeamOptionsOnClickListener implements androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener {

    private final Context context;
    private final TeamListAdapter listAdapter;
    private final View source;

    public TeamOptionsOnClickListener(Context context, TeamListAdapter listAdapter, View finalConvertView) {
        this.context = context;
        this.listAdapter = listAdapter;
        this.source = finalConvertView;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        final Team t = (Team) source.getTag();
        if (item.getItemId() == R.id.menu_item_changeicon) {
            ChangeTeamIconDialog dialog = new ChangeTeamIconDialog(context, t);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    listAdapter.notifyDataSetChanged();
                }
            });
            dialog.show();
            return true;
        } else if (item.getItemId() == R.id.menu_item_rename) {
            new TeamRenameDialog(context, t, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listAdapter.notifyDataSetChanged();
                }
            }).show();
            return true;
        } else if (item.getItemId() == R.id.menu_item_changesize) {
            new TeamSizeDialog(context, t, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listAdapter.notifyDataSetChanged();
                }
            }).show();
            return true;
        } else if (item.getItemId() == R.id.menu_item_delete) {
            if (TeamsStore.getInstance().deleteTeam(t)) {
                XMLHelper.persistTeams(context);
                listAdapter.getDataEntries().remove(t);
                listAdapter.notifyDataSetChanged();
            }
            return true;
        }
        return false;
    }
}
