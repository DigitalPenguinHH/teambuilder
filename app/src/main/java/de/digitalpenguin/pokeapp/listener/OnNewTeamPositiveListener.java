package de.digitalpenguin.pokeapp.listener;

import android.content.Context;
import android.content.DialogInterface;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public abstract class OnNewTeamPositiveListener implements DialogInterface.OnClickListener {

    private Context context;

    private Team team;

    public void setTeam(Context context, Team team) {
        this.context = context;
        this.team = team;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        // store team
        TeamsStore.getInstance().getTeamEntries().add(team);
        // persist teams
        XMLHelper.persistTeams(context);
        // close
        dialogInterface.dismiss();
        // after save work
        onTeamSaved(dialogInterface, team);
    }

    public abstract void onTeamSaved(DialogInterface dialogInterface, Team team);

}
