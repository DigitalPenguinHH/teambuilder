package de.digitalpenguin.pokeapp.listener;

import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.SearchView;

import org.apache.commons.lang3.StringUtils;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.fragments.SearchResultsFragment;

public class SearchInputListener implements SearchView.OnQueryTextListener {

    private static final String FRAGMENT_TAG_RESULTS = "SEARCH_RESULTS";

    private Handler mHandler = new Handler();

    // stores text for delayed submit on key-up
    private String mQueryString;

    private FragmentManager fragmentManager;

    public SearchInputListener(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doSearch(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQueryString = newText;
        mHandler.removeCallbacksAndMessages(null);
        Fragment searchResultsFragment = findSearchResultsFragment();
        if (StringUtils.isEmpty(newText) && (searchResultsFragment == null || !searchResultsFragment.isVisible()))
            return true;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                doSearch(mQueryString);
            }
        }, 500);
        return true;
    }

    private void doSearch(String text) {
        Fragment current = findSearchResultsFragment();
        SearchResultsFragment searchResultsFragment;
        if (current == null || !current.isVisible()) {
            if (text == null || text.length() < 2)
                return;
            searchResultsFragment = new SearchResultsFragment();
            addSearchStringToBundleParams(text, searchResultsFragment);
            fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.content_frame, searchResultsFragment, FRAGMENT_TAG_RESULTS).commit();
        } else {
            searchResultsFragment = (SearchResultsFragment) current;
            addSearchStringToBundleParams(text, searchResultsFragment);
            searchResultsFragment.setQuery(text);
        }
    }

    private Fragment findSearchResultsFragment() {
        return fragmentManager.findFragmentByTag(FRAGMENT_TAG_RESULTS);
    }

    private void addSearchStringToBundleParams(String text, SearchResultsFragment searchResultsFragment) {
        Bundle args = searchResultsFragment.getArguments();
        if (args == null) {
            args = new Bundle();
            args.putString(SearchResultsFragment.PARAM_QUERY_STR, text);
            searchResultsFragment.setArguments(args);
        } else {
            args.putString(SearchResultsFragment.PARAM_QUERY_STR, text);
        }
    }

}
