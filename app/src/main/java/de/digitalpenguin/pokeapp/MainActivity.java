package de.digitalpenguin.pokeapp;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.fragments.AboutFragment;
import de.digitalpenguin.pokeapp.fragments.BrowseCharacteristicsAndNaturesFragment;
import de.digitalpenguin.pokeapp.fragments.BrowseFragment;
import de.digitalpenguin.pokeapp.fragments.HomeFragment;
import de.digitalpenguin.pokeapp.fragments.SettingsFragment;
import de.digitalpenguin.pokeapp.fragments.TeamsFragment;
import de.digitalpenguin.pokeapp.fragments.TypesFragment;
import de.digitalpenguin.pokeapp.listener.SearchInputListener;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class MainActivity extends PersistenceActivityBase implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment fragment;

    private DrawerLayout mDrawerLayout;

    private SearchView searchView;

    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        // only start from Home if new Activity
        if (getSupportFragmentManager().getFragments().isEmpty()) {
            startHome(navigationView);
        }

        Log.i("Entity: ", getDataProvider().getById(25).toString());
    }

    private void startHome(NavigationView navigationView) {
        this.fragment = new HomeFragment();
        MenuItem menuItem = navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setCheckedItem(R.id.nav_home);
        setTitle(menuItem.getTitle());
        activateCurrentFragment();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        //if (!searchView.isIconified()) {
        //    searchView.setIconified(true);
        //}
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            if (fm.getBackStackEntryCount() == 1 && containsHomeFragment(fm.getFragments())) {
                // don't go back any further
                return;
            }
            fm.popBackStackImmediate();
        }
    }

    private boolean containsHomeFragment(List<Fragment> fragments) {
        for (Fragment f : fragments) {
            if (f instanceof HomeFragment)
                return  true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.mMenu = menu;

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        SearchInputListener searchInputListener = new SearchInputListener(getSupportFragmentManager());
        searchView.setOnQueryTextListener(searchInputListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();
        this.fragment = null;
        switch (id) {
            case R.id.nav_home:
                this.fragment = new HomeFragment();
                break;
            case R.id.nav_teams:
                this.fragment = new TeamsFragment();
                break;
            case R.id.nav_browse:
                this.fragment = new BrowseFragment();
                break;
            case R.id.nav_types:
                this.fragment = new TypesFragment();
                break;
            case R.id.nav_dvnatures:
                this.fragment = new BrowseCharacteristicsAndNaturesFragment();
                break;
            case R.id.nav_settings:
                this.fragment = new SettingsFragment();
                break;
            case R.id.nav_about:
                this.fragment = new AboutFragment();
                break;
            default:
                break;
        }

        if (this.fragment == null)
            return false;

        activateCurrentFragment();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);

        // Set action bar title
        setTitle(menuItem.getTitle());

        // Close the navigation drawer
        mDrawerLayout.closeDrawers();

        return true;
    }

    private void activateCurrentFragment() {
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.content_frame, this.fragment, this.fragment.getClass().getSimpleName()).commit();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void collapseSearchBar() {
        if (mMenu != null) {
            (mMenu.findItem(R.id.search)).collapseActionView();
        }
    }

    public void restoreSearchBar(final CharSequence query) {
        MenuItemCompat.expandActionView(mMenu.findItem(R.id.search));
        searchView.setQuery(query, false);
        UIHelper.hideKeyboardFrom(this, searchView);
    }

}
