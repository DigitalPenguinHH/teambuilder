package de.digitalpenguin.pokeapp.fragments;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.TeamDetailsViewAdapter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class TeamDetailFragment extends AbstractAppFragment implements ViewPager.OnPageChangeListener {

    public static final String TAG = TeamDetailFragment.class.getName();

    private View view;

    private Team team;

    private TeamDetailsViewAdapter pagerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.team_detail_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getParameter();
        initContent();
        initTabs();
    }

    private void getParameter() {
        // fetch team
        String teamId = getArguments().getString(Team.PARAM_ID);
        this.team = TeamsStore.getInstance().getTeam(teamId);
    }

    private void initTabs() {

        TabLayout tabLayout = this.view.findViewById(R.id.teamDetailsTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.common_team));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.team_performance));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = this.view.findViewById(R.id.teamDetailsViewPager);
        pagerAdapter = new TeamDetailsViewAdapter(getChildFragmentManager(), this.team, this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(this);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initContent() {
        if (team == null)
            return;
        // set team values
        TextView teamName = view.findViewById(R.id.tileTeamName);
        teamName.setText(team.getName());
        // team capacity (colored optional)
        updateTeamCapacity();
        // team icon
        ImageView teamIcon = view.findViewById(R.id.tileTeamIcon);
        if (team.getIcon() != null) {
            int resId = UIHelper.getDrawableResourceId(getContext(), team.getIcon());
            Drawable drawable = getContext().getResources().getDrawable(resId);
            teamIcon.setImageDrawable(drawable);
        } else {
            int resIdIcon = UIHelper.getResourceId(getContext(), team.getGame());
            teamIcon.setImageResource(resIdIcon);
        }
        // Options
        final ImageView viewOptions = view.findViewById(R.id.tileTeamOptions);
        removeFromParent(viewOptions);
    }

    public void updateTeamCapacity() {
        String teamCapacityStr = String.format(Locale.getDefault(), "%d/%d", team.getMembers().size(), team.getTeamSizeMax());
        Spannable spannable = new SpannableString(teamCapacityStr);
        spannable.setSpan(new ForegroundColorSpan(team.isFull() ? Color.BLUE : Color.RED), 0, 1, 0);
        TextView capacityText = view.findViewById(R.id.tileTeamCapacity);
        capacityText.setText(spannable);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.team_details));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1) {
            pagerAdapter.tabSelected(1);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
