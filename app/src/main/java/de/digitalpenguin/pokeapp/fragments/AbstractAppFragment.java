package de.digitalpenguin.pokeapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.digitalpenguin.pokeapp.MainActivity;
import de.digitalpenguin.pokeapp.PersistenceActivityBase;
import de.digitalpenguin.pokeapp.StartupActivity;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public abstract class AbstractAppFragment extends Fragment {

    /*
     * Animation Workaround for disappearing "from"-child fragment in animation
     * Taken from: https://stackoverflow.com/a/23276145
     */

    private static final int DEFAULT_CHILD_ANIMATION_DURATION = 750;

    private static final String DEF_TYPE_STRING = "string";

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final Fragment parent = getParentFragment();
        // Apply the workaround only if this is a child fragment, and the parent
        // is being removed.
        if (!enter && parent != null && parent.isRemoving()) {
            // This is a workaround for the bug where child fragments disappear when
            // the parent is removed (as all children are first removed from the parent)
            // See https://code.google.com/p/android/issues/detail?id=55228
            Animation doNothingAnim = new AlphaAnimation(1, 1);
            doNothingAnim.setDuration(getNextAnimationDuration(parent, DEFAULT_CHILD_ANIMATION_DURATION));
            return doNothingAnim;
        } else {
            return super.onCreateAnimation(transit, enter, nextAnim);
        }
    }

    private static long getNextAnimationDuration(Fragment fragment, long defValue) {
        try {
            // Attempt to get the resource ID of the next animation that
            // will be applied to the given fragment.
            Field nextAnimField = Fragment.class.getDeclaredField("mNextAnim");
            nextAnimField.setAccessible(true);
            int nextAnimResource = nextAnimField.getInt(fragment);
            Animation nextAnim = AnimationUtils.loadAnimation(fragment.getActivity(), nextAnimResource);

            // ...and if it can be loaded, return that animation's duration
            return (nextAnim == null) ? defValue : nextAnim.getDuration();
        } catch (NoSuchFieldException|IllegalAccessException|Resources.NotFoundException ex) {
            return defValue;
        }
    }

    protected void updateActivityAppBarTitle (String title) {
        // Set title bar
        ((MainActivity) getActivity()).setActionBarTitle(title);
    }

    protected void removeFromParent(View v) {
        if (v == null || v.getParent() == null)
            return;
        ((ViewGroup) v.getParent()).removeView(v);
    }

    protected int getDisplayWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    protected void restartApp() {
        Intent intent = new Intent(getActivity(), StartupActivity.class);
        getActivity().startActivity(intent);
    }

    protected String getStringResource(Context context, String key) {
        int resIdName = context.getResources().getIdentifier(key, DEF_TYPE_STRING, context.getPackageName());
        return context.getString(resIdName);
    }

    protected void backPressed() {
        getActivity().onBackPressed();
    }

    protected Bundle createBundleArgs(String key1, Object o1) {
        Bundle b = new Bundle();
        putBundleArg(b, key1, o1);
        return b;
    }

    protected Bundle createBundleArgs(String key1, Object o1, String key2, Object o2) {
        Bundle b = new Bundle();
        putBundleArg(b, key1, o1);
        putBundleArg(b, key2, o2);
        return b;
    }

    private void putBundleArg(Bundle b, String key, Object o) {
        if (StringUtils.isEmpty(key) || o == null)
            return;
        if (String.class.isInstance(o)) {
            b.putString(key, (String) o);
        } else if (Integer.class.isInstance(o)) {
            b.putInt(key, (Integer) o);
        } else if (Serializable.class.isInstance(o)) {
            b.putSerializable(key, (Serializable) o);
        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(getClass().getName(), "onSaveInstanceState");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(getClass().getName(), "onViewCreated");
    }

    protected DataProvider getDataProvider() {
        return ((PersistenceActivityBase) getActivity()).getDataProvider();
    }
}
