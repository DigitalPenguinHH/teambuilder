package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.TeamListAdapter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class TeamsFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    private ViewGroup rootView;

    private TeamListAdapter listAdapter;

    private ListView teamsListView;

    private List<Team> allTeams = Collections.emptyList();

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.rootView = (ViewGroup) inflater.inflate(R.layout.teams_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initListView();
    }

    private void initListView() {
        // ListView
        this.allTeams = loadTeams();
        this.listAdapter = new TeamListAdapter(this.rootView.getContext(), this.allTeams);
        this.teamsListView = this.rootView.findViewById(R.id.teamsListView);
        this.teamsListView.setOnItemClickListener(this);
        this.teamsListView.setAdapter(listAdapter);
    }

    private List<Team> loadTeams() {
        // load persistent teams
        List<Team> teamEntries = new ArrayList<>(6);
        if (!CollectionUtils.isEmpty(TeamsStore.getInstance().getTeamEntries())) {
            teamEntries.addAll(TeamsStore.getInstance().getTeamEntries());
        }
        return teamEntries;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        // Button "New Team"
        FloatingActionButton buttonAdd = rootView.findViewById(R.id.teamsAddFab);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // new team fragment
                CreateNewTeamFragment fragment = new CreateNewTeamFragment();
                fragment.setArguments(new Bundle()); // avoid NPE in fragment
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Team item = (Team) teamsListView.getItemAtPosition(pos);
        // start fragment team Details
        TeamDetailFragment f = new TeamDetailFragment();
        Bundle params = new Bundle();
        params.putString(Team.PARAM_ID, item.getId());
        f.setArguments(params);
        UIHelper.swapFragment(getActivity().getSupportFragmentManager(), f, TeamDetailFragment.TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.my_teams));
    }

}
