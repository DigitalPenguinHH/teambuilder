package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.dialog.PropertyCounterInfoDialog;

public class TeamPerformaceDetailFragment extends TeamMembersDetailFragment {

    private View view;

    private Team team;

    private CheckBox toggleStance;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.team_performance_detail_fragment, container, false);
        getParameter();
        initContent();
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (team != null)
            outState.putString(Team.PARAM_ID, team.getId());
    }

    private void getParameter() {
        // fetch team
        String teamId = getArguments().getString(Team.PARAM_ID);
        this.team = TeamsStore.getInstance().getTeam(teamId);
    }

    private void initContent() {

        if (this.team == null) {
            // something went horribly wrong
            restartApp();
            return;
        }

        toggleStance = view.findViewById(R.id.teamPerformanceToggleStanceButton);
        toggleStance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateTiles();
            }
        });

        updateTiles();
    }

    private void updateTiles() {
        if (toggleStance.isChecked()) {
            updateTilesDefence();
        } else {
            updateTilesOffence();
        }
    }

    private void updateTilesOffence() {
        for (PProperty prop : PProperty.values()) {
            // identify counters
            final List<TeamMember> counters = team.listCounteringTeamMembers(prop, getDataProvider());
            // get wrapper tile
            String tileName = "propertyPerformance" + prop.name().toUpperCase();
            LinearLayout tileWrapper = view.findViewById(getResources().getIdentifier(tileName, "id", getActivity().getPackageName()));
            // Set Disabled if not countered
            ImageView imgView = (ImageView) tileWrapper.getChildAt(0);
            String imgName = "@drawable/prop_" + prop.name().toLowerCase() + (counters.isEmpty() ? "_disabled" : "");
            imgView.setImageResource(getResources().getIdentifier(imgName, "drawable", getActivity().getPackageName()));
            // Set Background Color
            int resId = R.color.defaultBackground;
            if (counters.size() == 1) {
                resId = R.color.propertyCountered2;
            } else if (counters.size() >= 2) {
                resId = R.color.propertyCountered3;
            }
            int color = ContextCompat.getColor(getActivity().getApplicationContext(), resId);
            tileWrapper.setBackgroundColor(color);
            if (!counters.isEmpty()) {
                addOnClickCounterInfoDialogListener(tileWrapper, prop, counters);
            }
        }
    }

    private void updateTilesDefence() {
        for (PProperty prop : PProperty.values()) {
            // identify counters
            final List<TeamMember> disadvantaged = team.listTeamMembersWeakAgainst(prop, getDataProvider());
            // get wrapper tile
            String tileName = "propertyPerformance" + prop.name().toUpperCase();
            LinearLayout tileWrapper = view.findViewById(getResources().getIdentifier(tileName, "id", getActivity().getPackageName()));
            // Set Disabled if not countered
            ImageView imgView = (ImageView) tileWrapper.getChildAt(0);
            String imgName = "@drawable/prop_" + prop.name().toLowerCase() + (disadvantaged.isEmpty() ? "_disabled" : "");
            imgView.setImageResource(getResources().getIdentifier(imgName, "drawable", getActivity().getPackageName()));
            // Set Background Color
            int resId = R.color.defaultBackground;
            if (disadvantaged.size() == 1) {
                resId = R.color.propertyCounteredDefensive1;
            } else if (disadvantaged.size() == 2) {
                resId = R.color.propertyCounteredDefensive2;
            } else if (disadvantaged.size() >= 3) {
                resId = R.color.propertyCounteredDefensive3;
            }
            int color = ContextCompat.getColor(getActivity().getApplicationContext(), resId);
            tileWrapper.setBackgroundColor(color);
            if (!disadvantaged.isEmpty()) {
                addOnClickCounterInfoDialogListener(tileWrapper, prop, disadvantaged);
            }
        }
    }

    private void addOnClickCounterInfoDialogListener(LinearLayout tileWrapper, final PProperty prop, final List<TeamMember> counters) {
        tileWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PropertyCounterInfoDialog dialog = new PropertyCounterInfoDialog(getContext(), getDataProvider(), prop, counters, toggleStance.isChecked());
                dialog.show();
            }
        });
    }

    @Override
    public void updateContent() {
        initContent();
    }

}
