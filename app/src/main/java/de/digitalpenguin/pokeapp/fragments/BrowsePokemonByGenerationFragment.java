package de.digitalpenguin.pokeapp.fragments;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.NamedItem;
import de.digitalpenguin.pokeapp.adapter.RegionSpinnerAdapter;
import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Region;

public class BrowsePokemonByGenerationFragment extends BrowsePokemonAllFragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;

    @Override
    protected int getLayoutId() {
        return R.layout.browse_pokemon_bygeneration_fragment;
    }

    @Override
    protected List<Pokemon> getInitialResults() {
        return new ArrayList<>(0);
    }

    @Override
    protected void initViews() {
        // spinner
        spinner = this.view.findViewById(R.id.browsePokemonRegionSpinner);
        try {
            spinner.setAdapter(new RegionSpinnerAdapter(this.view.getContext(), getDataProvider()));
        } catch (IllegalArgumentException e) {
            restartApp();
            return;
        }
        spinner.setOnItemSelectedListener(this);
        if (getGeneration() == null) {
            // select newest
            spinner.setSelection(0);
        } else {
            // select resion by generaiton param
            for (int i = 0 ; i < spinner.getCount() ; i++) {
                NamedItem<Region> item = (NamedItem<Region>) spinner.getItemAtPosition(i);
                if (item.getObject().getGeneration() == getGeneration().intValue()) {
                    spinner.setSelection(i);
                    break;
                }
            }
            spinner.setEnabled(false);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        NamedItem<Region> item = (NamedItem<Region>) spinner.getItemAtPosition(pos);
        Region r = item.getObject();
        getCurrentResult().clear();
        if (r != null) {
            List<Pokemon> result = getDataProvider().find(DataFilter.regionFilter(r));
            Collections.sort(result, new PokemonComparator(r.getPokedex()));
            getCurrentResult().addAll(result);
        }
        getListAdapter().setRawEntries(getCurrentResult());
        getListAdapter().notifyDataSetChanged();
        //getResultsListView().setSelectionAfterHeaderView();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    protected void asyncLoadData() {
        // no op
    }
}
