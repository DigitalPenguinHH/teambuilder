package de.digitalpenguin.pokeapp.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.data.ListDataProvider;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class MoveDetailFragment extends AbstractAppFragment implements View.OnClickListener {

    private View view;

    private Move move;

    private List<MoveLearned> pokemonMoveLearnedList;
    private GenericListAdapter<MoveLearned> listAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.move_detail_fragment, container, false);
        getParameter();
        initContent();
        return view;
    }

    private void getParameter() {
        int moveId = getArguments().getInt(Move.TAG_ID);
        this.move = getDataProvider().getMove(moveId);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getString(R.string.move_detail_actinobar_title));
    }

    private void initContent() {
        if (move == null)
            return;
        //
        final ImageView toggleFolderIcon = view.findViewById(R.id.folderToggleIcon);
        final ViewGroup header = view.findViewById(R.id.moveDetailHeader2);
        toggleFolderIcon.setImageDrawable(getResources().getDrawable(R.drawable.add_boxed));
        final ViewGroup foldableContent = view.findViewById(R.id.moveDetailLearnedByWrapper);
        foldableContent.setVisibility(View.GONE);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean visible = foldableContent.getVisibility() == View.VISIBLE;
                foldableContent.setVisibility(visible ? View.GONE : View.VISIBLE);
                toggleFolderIcon.setImageDrawable(getResources().getDrawable(visible ? R.drawable.add_boxed : R.drawable.remove_boxed));
                if (!visible)
                    asyncLoadData();
            }
        });
        // set move values
        TextView moveNameBig = view.findViewById(R.id.moveDetailNameTop);
        moveNameBig.setText(move.getName());
        TextView moveDesc = view.findViewById(R.id.moveDetailsDescriptionText);
        moveDesc.setText(move.getDescriptions().getText());
        // Attributes
        TextView attrTextCategory = view.findViewById(R.id.moveDetailsAttibuteTypeTxt);
        attrTextCategory.setText(UIHelper.getString(getContext(), "move_type_" + move.getType().name().toLowerCase()));
        // Icons
        ImageView propertyIcon = view.findViewById(R.id.moveDetailsAttibutePropertyImg);
        UIHelper.makePropertyIcon(getContext(), propertyIcon, move.getProperty());
        ImageView typeIcon = view.findViewById(R.id.moveDetailsAttibuteTypeImg);
        String uri = "drawable://" + getContext().getResources().getIdentifier("move_type_" + move.getType().name().toLowerCase(),"drawable", getContext().getPackageName());
        ImageLoader.getInstance().displayImage(uri, typeIcon);
        TextView attrTextProperty = view.findViewById(R.id.moveDetailsAttibutePropertyTxt);
        attrTextProperty.setText(UIHelper.getString(getContext(), "prop_" + move.getProperty().name().toLowerCase()));
        // Values
        TextView attrTextStr = view.findViewById(R.id.moveDetailsAttibuteStrengthTxt);
        attrTextStr.setText(move.getDisplayStrength());
        TextView attrTextAccu = view.findViewById(R.id.moveDetailsAttibuteAccuracyTxt);
        attrTextAccu.setText(move.getDisplayAccuracy());
        TextView attrTextAp = view.findViewById(R.id.moveDetailsAttibuteAPTxt);
        attrTextAp.setText(move.getAp() == null ? "-" : Integer.toString(move.getAp()));
        // Erlernbarkeit
        pokemonMoveLearnedList = new LinkedList<>();
        ListView listViewLearnedBy = view.findViewById(R.id.moveDetailLearnedByList);
        listAdapter = new GenericListAdapter<MoveLearned>(getContext(), pokemonMoveLearnedList, R.layout.movelearned_pokemon_tile) {
            @Override
            protected void initView(MoveLearned ml, View view) {
                ImageView pkmnIcon = view.findViewById(R.id.moveLearnedByPokemonIcon);
                // Pokemon
                PokemonForm form = getDataProvider().getPokemonForm(ml.getPokemonId(), VariantType.DEFAULT);
                Pokemon p = form.getPokemon();
                pkmnIcon.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(p, form.getType()), null, getContext().getPackageName()));
                TextView pNameText = view.findViewById(R.id.moveLearnedByPokemonName);
                pNameText.setText(p.getName());
                // LvL <> Machine
                TextView sourceText = view.findViewById(R.id.moveLearnedByPokemonSource);
                sourceText.setText("");
                // TODO: handle generations
                    /*
                    if (ml.getTrigger() == MoveLearnTrigger.LEVELUP) {
                        sourceText.setText(getResources().getText(R.string.moves_header_lv) + " " + Integer.toString(ml.getDisplayLevel()));
                    } else if (ml.getTrigger() == MoveLearnTrigger.MACHINE) {
                        String displayName = "";
                        if (StringUtils.isNotEmpty(ml.getMachine())) {
                            if (ml.getMachine().startsWith("HM") || ml.getMachine().startsWith("TR")) {
                                displayName = String.format("%s%s", getResources().getString(ml.getGeneration() == 8 ? R.string.hm_gen8 : R.string.hm), ml.getMachine().substring(2));
                            } else {
                                displayName = ml.getMachine();
                            }
                        }
                        sourceText.setText(displayName);
                    }
                    */
                view.setOnClickListener(MoveDetailFragment.this);
                view.setTag(ml);
            }
        };
        listViewLearnedBy.setAdapter(listAdapter);
        UIHelper.setListViewHeightBasedOnChildren(listViewLearnedBy);
    }

    private static class LoadPokemonWithMovesAsyncTask extends AsyncTask<Void, Integer, List<MoveLearned>> {

        private WeakReference<View> refView;
        private ListDataProvider<MoveLearned> dataProvider;
        private WeakReference<GenericListAdapter> adapterRef;

        LoadPokemonWithMovesAsyncTask(View viewRoot, GenericListAdapter adapter, ListDataProvider dataProvider) {
            this.refView = new WeakReference<>(viewRoot);
            this.dataProvider = dataProvider;
            this.adapterRef = new WeakReference<>(adapter);
        }

        @Override
        protected List<MoveLearned> doInBackground(Void... voids) {
            return dataProvider.getData();
        }

        @Override
        protected void onPostExecute(List<MoveLearned> data) {
            // update progress finished
            View view = refView.get();
            if (view == null)
                return;
            refView.get().findViewById(R.id.loadingProgress).setVisibility(View.GONE);
            // set list entries in adapter
            GenericListAdapter resultsListViewAdapter = adapterRef.get();
            if (resultsListViewAdapter == null)
                return;
            resultsListViewAdapter.clear();

            ListView listView = view.findViewById(R.id.moveDetailLearnedByList);
            if (data.isEmpty()) {
                ((ViewGroup) listView.getParent()).removeView(listView);
            } else {
                View noneFound = view.findViewById(R.id.moveDetailLearnedByPlaceholderNone);
                if (noneFound != null && noneFound.getParent() != null) {
                    ((ViewGroup) noneFound.getParent()).removeView(noneFound);
                }
                resultsListViewAdapter.addAll(data);
                resultsListViewAdapter.notifyDataSetChanged();
                UIHelper.setListViewHeightBasedOnChildren(listView);
            }
        }

        @Override
        protected void onPreExecute() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void asyncLoadData() {
        new LoadPokemonWithMovesAsyncTask(view, listAdapter, new ListDataProvider() {
            @Override
            public List getData() {
                return getDataProvider().listPokemonMovesLearned(move.getId());
            }
        }).execute();
    }

    @Override
    public void onClick(View v) {
        MoveLearned ml = (MoveLearned) v.getTag();
        PokemonDetailFragment fragment = new PokemonDetailFragment();
        fragment.setArguments(createBundleArgs(Pokemon.PARAM_ID, ml.getPokemonId()));
        UIHelper.swapFragment(getFragmentManager(), fragment);
    }

}
