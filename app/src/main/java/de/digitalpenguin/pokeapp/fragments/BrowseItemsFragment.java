package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.adapter.GenericRecyclerViewAdapter;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.MachineProperty;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.util.ItemPopup;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowseItemsFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    private View rootView;

    private List<Item> itemDataSet;

    private GenericRecyclerViewAdapter<Item> itemListAdapter;

    private ItemCategory category = ItemCategory.POKEBALLS;

    private Map<String, String> itemResIdsCache = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // fragment root view
        this.rootView = inflater.inflate(R.layout.browse_items_fragment, container, false);
        initListView();
        return rootView;
    }

    private void initListView() {

        Spinner itemCategorySpinner = rootView.findViewById(R.id.itemCategoriesSpinner);
        List<ItemCategory> itemCategories = new ArrayList<>(ItemCategory.values().length);
        itemCategories.addAll(Arrays.asList(ItemCategory.values()));
        final GenericListAdapter<ItemCategory> spinnerAdapter = new GenericListAdapter<ItemCategory>(getContext(), itemCategories, R.layout.spinner_layout_default) {
            @Override
            protected void initView(ItemCategory cat, View view) {
                String name = UIHelper.getDisplayName(getContext(), cat);
                TextView textView = view.findViewById(R.id.txt);
                textView.setText(name);
                int resId = 0;
                switch (cat) {
                    case BERRIES:
                        resId = R.drawable.item_sitrus_berry;
                        break;
                    case BATTLE_ITEMS:
                        resId = R.drawable.item_x_attack;
                        break;
                    case GENERAL:
                        resId = R.drawable.item_town_map;
                        break;
                    case HOLD_ITEMS:
                        resId = R.drawable.item_exp_share;
                        break;
                    case MACHINES:
                        resId = R.drawable.item_tm_normal;
                        break;
                    case MEDICINE:
                        resId = R.drawable.item_potion;
                        break;
                    case POKEBALLS:
                        resId = R.drawable.item_ultra_ball;
                        break;
                    default:break;
                }
                String uri = "drawable://" + resId;
                ImageView imgViewIcon = view.findViewById(R.id.img);
                ImageLoader.getInstance().displayImage(uri, imgViewIcon);
            }
        };
        itemCategorySpinner.setAdapter(spinnerAdapter);
        itemCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = spinnerAdapter.getItemAtPosition(position);
                if (category == ItemCategory.MACHINES) {
                    // init cache i.e.: VM04 -> item_tm_flying
                    if (itemResIdsCache.isEmpty()) {
                        for (MachineProperty mp : getDataProvider().listMachineProperties()) {
                            String propName = PProperty.NORMAL.name();
                            Move move = mp.getMove();
                            if (move != null && move.getProperty() != null) {
                                propName = move.getProperty().getName();
                                String iconName = "item_tm_" + propName.toLowerCase();
                                itemResIdsCache.put(mp.getId(), iconName);
                            }
                        }
                    }
                }
                itemDataSet.clear();
                itemDataSet.addAll(getDataProvider().listItems(category));
                Collections.sort(itemDataSet);
                itemListAdapter.notifyDataSetChanged();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }
        });
        itemCategorySpinner.setSelection(itemCategories.indexOf(ItemCategory.POKEBALLS));

        itemDataSet = getDataProvider().listItems(category);
        Collections.sort(itemDataSet);

        this.itemListAdapter = new GenericRecyclerViewAdapter<Item>(getContext(), itemDataSet, R.layout.item_tile, this){
            @Override
            public void onBindViewHolder(MyViewHolder holder, int position) {
                Item item = (Item) getItem(position);
                ImageView imgView = holder.view.findViewById(R.id.itemTileIcon);
                int iconResource;
                if (item.getCategory() == ItemCategory.MACHINES) {
                    String resName = itemResIdsCache.get(item.getName(Locale.GERMAN));
                    iconResource = getContext().getResources().getIdentifier("@drawable/" + resName, null, getContext().getPackageName());
                } else {
                    iconResource = UIHelper.getItemResourceId(getContext(), getDataProvider(), item);
                }
                imgView.setImageResource(iconResource);
                TextView txtView = holder.view.findViewById(R.id.itemTileText);
                String itemName = getDataProvider().getDisplayName(item);
                txtView.setText(itemName);
                removeFromParent(holder.view.findViewById(R.id.itemTileCategoryText));
                UIHelper.setBackGroundColorStriped(getResources(), position, holder.view);
            }
        };
        RecyclerView resultsListView = rootView.findViewById(R.id.itemsListRecylcerView);
        resultsListView.setHasFixedSize(true);
        resultsListView.setAdapter(this.itemListAdapter);
        resultsListView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Item item = (Item) itemListAdapter.getItem(pos);
        ItemPopup.show(getContext(), getDataProvider(), (ViewGroup) this.rootView, item);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(UIHelper.getDisplayName(getContext(), category));
    }

}
