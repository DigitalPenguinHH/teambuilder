package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.TeamListAdapter;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class AddToTeamFragment extends AbstractAppFragment {

    private ViewGroup rootView;

    // Params
    private Pokemon addPokemon;
    private int addForm = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // fragment root view
        this.rootView = (ViewGroup) inflater.inflate(R.layout.select_team_fragment, container, false);
        // get parameter from bundle
        getParameter();
        if (addPokemon == null) {
            // something went horribly wrong
            restartApp();
        }
        // init view content
        initViews();
        return rootView;
    }

    private void getParameter() {
        // fetch team
        this.addPokemon = getDataProvider().getById(getArguments().getInt(Pokemon.PARAM_ID));
        this.addForm = getArguments().getInt(PokemonData.PARAM_FORM_IDX);
    }

    private void initViews() {
        // list
        List<Team> allTeams = TeamsStore.getInstance().getTeamEntries();
        final ListView selectTeamListView = rootView.findViewById(R.id.selectTeamDialogTeamsListView);
        ViewGroup parent = (ViewGroup) selectTeamListView.getParent();
        if (allTeams.isEmpty()) {
            // no teams
            parent.removeView(selectTeamListView);
        } else {
            // configure adapter
            TeamListAdapter listAdapter = new TeamListAdapter(getContext(), allTeams, false, true);
            selectTeamListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    Team team = (Team) selectTeamListView.getItemAtPosition(pos);
                    if (team.isFull())
                        return;
                    handleTeamSelected(team);
                }
            });
            selectTeamListView.setAdapter(listAdapter);
            // remove placeholder
            parent.removeView(rootView.findViewById(R.id.selectTeamDialogNoTeamsPlaceholder));
        }
    }

    private void handleTeamSelected(Team team) {
        // Add Pokemon and save
        team.add(addPokemon, addForm);
        XMLHelper.persistTeams(getContext());
        // Show Details Fragment
        showTeamDetailsFragment(team);
    }

    private void showTeamDetailsFragment(Team team) {
        // show
        TeamDetailFragment fragment = new TeamDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Team.PARAM_ID, team.getId());
        fragment.setArguments(bundle);
        UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.team_add_member_header));
    }

}
