package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.MovesLearnedListAdapter;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.VariantType;

public class MovesLearnedCategoryFragment extends AbstractAppFragment {

    private View rootView;

    /* Params */
    private PokemonForm form;
    private MoveLearnTrigger trigger;
    private String gamesKey;

    /* Data */
    private List<MoveLearned> movesLearnedList;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.moves_learned_category_fragment, container, false);
        getParameter();
        initListView(inflater);
        return rootView;
    }

    private void initListView(LayoutInflater inflater) {
        ListView resultsListView = this.rootView.findViewById(R.id.movesLearnedListView);
        if (resultsListView.getHeaderViewsCount() == 0) {
            MovesLearnedListAdapter listAdapter = new MovesLearnedListAdapter(getActivity().getSupportFragmentManager(),
                    this.rootView.getContext(), getDataProvider(), this.form.getPokemon(), this.movesLearnedList, this.trigger);
            int tileResId = R.layout.movelearned_lvlup_header_tile;
            boolean removeHeader = false;
            switch (trigger) {
                case MACHINE:
                    tileResId = R.layout.movelearned_machine_header_tile;
                    break;
                case BREEDING:
                case TUTOR:
                    tileResId = R.layout.movelearned_edition_header_tile;
                    removeHeader = !hasEditions(this.movesLearnedList);
                    break;
                default:
                    break;
            }
            ViewGroup header = (ViewGroup) inflater.inflate(tileResId, resultsListView, false);
            if (removeHeader) {
                View headerCol = header.findViewById(R.id.moveLearnedHeaderTileEditionCol);
                headerCol.setVisibility(View.GONE);
            }
            resultsListView.addHeaderView(header);
            resultsListView.setAdapter(listAdapter);
        }
    }

    private boolean hasEditions(List<MoveLearned> movesLearnedList) {
        return CollectionUtils.exists(movesLearnedList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                return ((MoveLearned) object).getGames() != null;
            }
        });
    }

    private void getParameter() {
        Bundle b = getArguments();
        Pokemon pokemon = getDataProvider().getById(b.getInt(Pokemon.PARAM_ID));
        String variantTypeStr = b.getString(PokemonData.PARAM_FORM);
        this.form = getDataProvider().getPokemonForm(pokemon.getId(), StringUtils.isEmpty(variantTypeStr) ? VariantType.DEFAULT : VariantType.valueOf(variantTypeStr));
        Integer generation = b.getInt("generation");
        this.trigger = MoveLearnTrigger.valueOf(b.getString("category"));
        this.gamesKey = b.getString("gamesKey");
        this.movesLearnedList = getDataProvider().getGenerationsMoveList(this.form, generation, gamesKey, trigger);
        Collections.sort(this.movesLearnedList, new Comparator<MoveLearned>() {
            @Override
            public int compare(MoveLearned m1, MoveLearned m2) {
                if (m1.getTrigger() == MoveLearnTrigger.MACHINE) {
                    if (m1.getMachine().startsWith("H") && m2.getMachine().startsWith("T")) {
                        return 1;
                    } else if (m1.getMachine().startsWith("T") && m2.getMachine().startsWith("H")) {
                        return -1;
                    } else if (m1.getMachine().startsWith("TP") && m2.getMachine().startsWith("TM")) {
                        return 1;
                    } else if (m1.getMachine().startsWith("TM") && m2.getMachine().startsWith("TP")) {
                        return -1;
                    }
                    Integer mId1 = Integer.valueOf(m1.getMachine().substring(2));
                    Integer mId2 = Integer.valueOf(m2.getMachine().substring(2));
                    return mId1.compareTo(mId2);
                } else if (m1.getTrigger() == MoveLearnTrigger.LEVELUP) {
                    return m1.getDisplayLevel().compareTo(m2.getDisplayLevel());
                } else {
                    return getDataProvider().getMove(m1.getMoveId()).getName().compareTo(getDataProvider().getMove(m2.getMoveId()).getName());
                }
            }
        });
    }
}
