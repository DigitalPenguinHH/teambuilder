package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class AbilityDetailFragment extends AbstractAppFragment implements View.OnClickListener {

    private View view;

    private Ability ability;

    private List<PokemonAbility> pokemonMoveLearnedList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.ability_detail_fragment, container, false);
        getParameter();
        initContent();
        return view;
    }

    private void getParameter() {
        int abilityId = getArguments().getInt(Ability.TAG_ID);
        this.ability = getDataProvider().getAbility(abilityId);
    }

    private void initContent() {
        if (ability == null)
            return;
        // set move values
        TextView abilityNameBig = view.findViewById(R.id.abilityDetailNameTop);
        abilityNameBig.setText(ability.getName());
        TextView abilityDesc = view.findViewById(R.id.abilityDetailsDescriptionText);
        abilityDesc.setText(ability.getDescriptions().getText());
        // Pokemon mit Fähigkeit
        pokemonMoveLearnedList = getDataProvider().listPokemonWithAbility(ability.getId());
        if (pokemonMoveLearnedList.isEmpty()) {
            removeFromParent(view.findViewById(R.id.abilityDetailLearnedByListWrapper));
        } else {
            ListView listViewLearnedBy = view.findViewById(R.id.abilityDetailLearnedByList);
            removeFromParent(view.findViewById(R.id.abilityDetailLearnedByPlaceholderNone));
            listViewLearnedBy.setAdapter(new GenericListAdapter<PokemonAbility>(getContext(), pokemonMoveLearnedList, R.layout.movelearned_pokemon_tile) {
                @Override
                protected void initView(PokemonAbility pa, View view) {
                    ImageView pkmnIcon = view.findViewById(R.id.moveLearnedByPokemonIcon);
                    // Pokemon
                    Pokemon p = pa.getPokemon();
                    VariantType variantType = pa.getVariant() != null ? pa.getVariant().getType() : VariantType.DEFAULT;
                    final PokemonForm form = getDataProvider().getPokemonForm(pa.getPokemonId(), variantType);
                    pkmnIcon.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(p, variantType), null, getContext().getPackageName()));
                    TextView pNameText = view.findViewById(R.id.moveLearnedByPokemonName);
                    pNameText.setText(UIHelper.getPokemoFormDisplayName(form, getContext(), true));
                    // redundant
                    TextView sourceText = view.findViewById(R.id.moveLearnedByPokemonSource);
                    sourceText.setVisibility(View.INVISIBLE);
                    // Listener
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), PokemonDetailFragment.newInstance(form.getPokemonId(), form, true));
                        }
                    });
                }
            });
            UIHelper.setListViewHeightBasedOnChildren(listViewLearnedBy);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getString(R.string.ability_detail_actionbar_title));
    }

    @Override
    public void onClick(View v) {
        // TODO:
//        MoveLearned ml = (MoveLearned) v.getTag();
//        PokemonDetailFragment fragment = new PokemonDetailFragment();
//        fragment.setArguments(createBundleArgs(Pokemon.PARAM_ID, ml.getPokemonId()));
//        UIHelper.swapFragment(getFragmentManager(), fragment);
    }

}
