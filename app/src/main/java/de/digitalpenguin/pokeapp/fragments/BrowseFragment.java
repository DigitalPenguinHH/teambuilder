package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowseFragment extends AbstractAppFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.browse_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        getView().findViewById(R.id.browseCategoriesPokemon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new BrowsePokemonFragment());
            }
        });

        getView().findViewById(R.id.browseCategoriesItems).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new BrowseItemsFragment());
            }
        });

        getView().findViewById(R.id.browseCategoriesMoves).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new BrowseMovesFragment());
            }
        });

        getView().findViewById(R.id.browseCategoriesAbilities).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new BrowseAbilitiesFragment());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.nav_menu_browse));
    }
}
