package de.digitalpenguin.pokeapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterViewFlipper;
import android.widget.ListView;

import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.RandomPokemonViewFlipperAdapter;
import de.digitalpenguin.pokeapp.adapter.TeamListAdapter;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class HomeFragment extends AbstractAppFragment {

    private View canvas;

    private ListView recentTeamsListView;

    private AdapterViewFlipper mViewFlipper;
    private GestureDetector mGestureDetector;
    private RandomPokemonViewFlipperAdapter mFlipperAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        canvas = inflater.inflate(R.layout.home_fragment, container, false);
        initContent();
        return canvas;
    }

    private void initContent() {
        initRecentTeamsList();
        initRandomPokemomFlipper();
    }

    private void initRecentTeamsList() {
        recentTeamsListView = canvas.findViewById(R.id.homeRecentTeamsListView);
        List<Team> recentTeams = TeamsStore.getInstance().getRecentTeams(3);
        if (recentTeams.isEmpty()) {
            removeFromParent(recentTeamsListView);
            View buttonNew = canvas.findViewById(R.id.homeRecentTeamsCreate);
            PushDownAnim.setPushDownAnimTo(buttonNew).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new CreateNewTeamFragment());
                }
            });
            return;
        }
        // remove placeholder
        removeFromParent(canvas.findViewById(R.id.homeRecentTeamsNone));
        // configure adapter
        TeamListAdapter listAdapter = new TeamListAdapter(this.canvas.getContext(), recentTeams, false);
        recentTeamsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Team item = (Team) recentTeamsListView.getItemAtPosition(pos);
                // start fragment team Details
                TeamDetailFragment f = new TeamDetailFragment();
                Bundle params = new Bundle();
                params.putString(Team.PARAM_ID, item.getId());
                f.setArguments(params);
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), f, TeamDetailFragment.TAG);
            }
        });
        recentTeamsListView.setAdapter(listAdapter);
        // adjust height according to teams
        ViewGroup.LayoutParams lp = recentTeamsListView.getLayoutParams();
        lp.height = UIHelper.dpToPx(getContext(), recentTeams.size() * 70);
        recentTeamsListView.setLayoutParams(lp);
        // Header Click Listener -> Teams Overview
        canvas.findViewById(R.id.homeRecentTeamsHeaeder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.swapFragment(getActivity().getSupportFragmentManager(), new TeamsFragment());
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initRandomPokemomFlipper() {
        CustomGestureDetector customGestureDetector = new CustomGestureDetector();
        mGestureDetector = new GestureDetector(getContext(), customGestureDetector);
        mViewFlipper = canvas.findViewById(R.id.randomPokemonViewFlipper);
        if (mFlipperAdapter == null)
            mFlipperAdapter = new RandomPokemonViewFlipperAdapter(getContext(), getActivity().getSupportFragmentManager(), getDataProvider());
        try {
            mViewFlipper.setAdapter(mFlipperAdapter);
        } catch (Exception e) {
            restartApp();
        }
        mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mGestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
        initFlipperAnimation();
        //
        final View buttonAgain = canvas.findViewById(R.id.randomPokemonButton);
        PushDownAnim.setPushDownAnimTo(buttonAgain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mViewFlipper.showNext();
                } catch (Exception e) {
                    restartApp();
                }
            }
        });
    }

    private void initFlipperAnimation() {
        mViewFlipper.setInAnimation(getContext(), R.animator.in_from_right);
        mViewFlipper.setOutAnimation(getContext(), R.animator.out_from_left);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.nav_menu_home));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            outState.putInt(Pokemon.PARAM_ID, mFlipperAdapter.getCurrentId());
        } catch (Exception e) {
            outState.putInt(Pokemon.PARAM_ID, Math.max(1, new Random().nextInt(PokemonDB.MAX_POKEMON_ID)));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            int pId = savedInstanceState.getInt(Pokemon.PARAM_ID);
            mFlipperAdapter.setSaved(pId);
        }
    }

    private class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            // Swipe left (next)
            if (e1.getX() > e2.getX()) {
                mViewFlipper.showNext();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
