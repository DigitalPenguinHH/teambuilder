package de.digitalpenguin.pokeapp.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.comparator.NamedComparator;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.CharacteristicOrNature;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.StatType;
import de.digitalpenguin.pokeapp.data.util.Named;

public class BrowseCharacteristicsFragment extends AbstractAppFragment {

    private View view;

    private GenericListAdapter<CharacteristicOrNature> listAdapter;

    private ListView resultsListView;

    private List<CharacteristicOrNature> currentResult = Collections.emptyList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_characteristics_or_natures_result_list, container, false);
        initListView();
        return view;
    }

    private void initListView() {
        this.currentResult = getInitialResults();
        this.listAdapter = new GenericListAdapter<CharacteristicOrNature>(view.getContext(), this.currentResult, R.layout.characteristic_tile) {

            @Override
            protected void initView(CharacteristicOrNature item, View view) {
                TextView text = view.findViewById(R.id.charactersisticText);
                Context ctx = view.getContext();
                text.setText(item.getCharacteristic().getName());
                TextView textValue = view.findViewById(R.id.charactersisticValue);
                textValue.setText(getStatName(ctx, item.getCharacteristic().getHighest()));
            }
        };
        this.resultsListView = this.view.findViewById(R.id.genericBrowseListView);
        this.resultsListView.setAdapter(listAdapter);
    }

    @NonNull
    private String getStatName(Context context, StatType statType) {
        return getStringResource(context, "stat_" + statType.name().toLowerCase());
    }

    private List<CharacteristicOrNature> getInitialResults() {
        List<CharacteristicOrNature> list = new LinkedList<>();
        for (Characteristic c: getDataProvider().getCharacteristics()) {
            list.add(new CharacteristicOrNature(c));
        }
        Collections.sort(list, new CharacteristicNameComparator());
        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.nav_menu_dvnatures));
    }

    private static class CharacteristicNameComparator extends NamedComparator
    {
        @Override
        public int compare(Named o1, Named o2) {
            return super.compare(((CharacteristicOrNature) o1).getCharacteristic(), ((CharacteristicOrNature) o2).getCharacteristic());
        }
    }

}
