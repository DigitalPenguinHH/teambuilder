package de.digitalpenguin.pokeapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import org.apache.commons.lang3.StringUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GameSpinnerAdapter;
import de.digitalpenguin.pokeapp.adapter.NamedItem;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.Callback;
import de.digitalpenguin.pokeapp.util.TeamNameValidator;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class CreateNewTeamFragment extends AbstractAppFragment {

    private ViewGroup rootView;

    private Team team;

    private Button buttonConfirm;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // fragment root view
        this.rootView = (ViewGroup) inflater.inflate(R.layout.new_team_fragment, container, false);
        // instantiate new team
        String teamId = getArguments() == null ? null : getArguments().getString(Team.PARAM_ID);
        if (StringUtils.isNotEmpty(teamId)) {
            this.team = TeamsStore.getInstance().getTeam(teamId);
        } else {
            this.team = Team.newTeam();
        }
        // init view content
        initContent();
        return rootView;
    }

    @SuppressWarnings("unchecked")
    private void initContent() {
        final View spinnerParent = this.rootView.findViewById(R.id.newTeamSelectGameSpinnerWrapper);
        // team edit
        final EditText nameInput = this.rootView.findViewById(R.id.newTeamNameInput);
        nameInput.addTextChangedListener(new TeamNameValidator(getResources(), nameInput, team, new Callback() {
            @Override
            public void notify(Object arg) {
                buttonConfirm.setEnabled((Boolean) arg);
            }
        }));
        // spinner game version
        final Spinner spinner = this.rootView.findViewById(R.id.newTeamSelectGameSpinner);
        try {
            spinner.setAdapter(new GameSpinnerAdapter(getContext(), getDataProvider()));
        } catch (IllegalArgumentException e) {
           restartApp();
           return;
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                NamedItem<Game> item = (NamedItem<Game>) spinner.getItemAtPosition(pos);
                if (spinnerParent.getVisibility() == View.VISIBLE) {
                    team.setGame(item.getObject());
                    if (item.getObject() != null)
                        setDefaultMember(item.getObject(), team);
                    updateTeamName(getContext(), team, nameInput, item);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner.setSelection(0);
        // Buttons
        Button buttonCancel = rootView.findViewById(R.id.newTeamButtonCancel);
        this.buttonConfirm = rootView.findViewById(R.id.newTeamButtonOk);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPressed();
            }
        });
        this.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update teams model
                TeamsStore.getInstance().getTeamEntries().add(team);
                // pre-fill?
                CheckBox preFillCheckbox = rootView.findViewById(R.id.newTeamPrefillCheckbox);
                if (preFillCheckbox.isChecked()) {
                    while(!team.isFull()) {
                        Pokemon p = getDataProvider().getRandomPokemon(team.getGame() != null ? team.getGame().getGeneration() : null);
                        if (team.contains(p.getId(), -1))
                            continue;
                        team.add(p);
                    }
                }
                XMLHelper.persistTeams(getContext());
                // show team
                TeamDetailFragment fragment = new TeamDetailFragment();
                fragment.setArguments(createBundleArgs(Team.PARAM_ID, team.getId()));
                UIHelper.swapFragment(getFragmentManager(), fragment);
            }
        });
    }

    private void setDefaultMember(Game game, Team team) {
        team.removeAllMembers();
        Integer defaultMember = game.getTeamMemberDefault();
        if (defaultMember != null && defaultMember > 0)
            team.add(defaultMember);
    }

    private static void updateTeamName(Context context, Team team, EditText nameInput, NamedItem<?> item) {
        // bei Standard-Name
        String input = nameInput.getText().toString().trim();
        if (input.isEmpty() || input.matches("^Team [\\w\' ]+$")) {
            // neuen Namen generieren
            String name = context.getResources().getString(R.string.common_team) + " " + item.getName();
            team.setName(name);
            nameInput.setText(name);
            nameInput.setSelection(name.length());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.new_team));
    }

}
