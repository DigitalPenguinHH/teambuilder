package de.digitalpenguin.pokeapp.fragments;

public abstract class TeamDetailChildFragment extends AbstractAppFragment {


    private TeamDetailFragment teamDetailFragment;

    public TeamDetailFragment getTeamDetailFragment() {
        return teamDetailFragment;
    }

    public void setTeamDetailFragment(TeamDetailFragment teamDetailFragment) {
        this.teamDetailFragment = teamDetailFragment;
    }
}
