package de.digitalpenguin.pokeapp.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericRecyclerViewAdapter;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class TeamMemberAbilitiesFragment extends AbstractAppFragment {

    /* canvas */
    private View view;

    /* Params */
    private Integer generation;
    private TeamMember teamMember;

    /* Widgets */
    private ScrollView mScrollView;
    private AnimatorSet scrollAnimator;
    private RecyclerView selectedAbilitiesListView;
    private GenericRecyclerViewAdapter<MoveLearned> selectedAbilitiesListViewAdapter;
    private RecyclerView selectableAbilitiesListView;
    private GenericRecyclerViewAdapter<MoveLearned> selectableAbilitiesListViewAdapter;

    /* State */
    private List<MoveLearned> movesLearned;
    private List<MoveLearned> movesLearnedPool = new LinkedList<>();

    /* constants*/
    private static final Comparator<MoveLearned> moveLearnedLvlComparator = new Comparator<MoveLearned>() {
        @Override
        public int compare(MoveLearned o1, MoveLearned o2) {
            return o1.getMachine().compareTo(o2.getMachine());
        }
    };

    private static final Comparator<MoveLearned> moveLearnedMachineNameComparator = new Comparator<MoveLearned>() {
        @Override
        public int compare(MoveLearned o1, MoveLearned o2) {
            if (o1.getLevel() == null)
                return o2.getLevel() != null ? -1 : 0;
            if (o2.getLevel() == null)
                return o1.getLevel() != null ? 1 : 0;
            return o1.getLevel().compareTo(o2.getLevel());
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.team_member_abilities_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getParameter();
        initContent();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (teamMember != null)
            outState.putSerializable(TeamMember.PARAM_ID, teamMember);
        outState.putInt(Region.PARAM_GENERATION, generation);
    }

    private void getParameter() {
        // fetch team
        teamMember = (TeamMember) getArguments().getSerializable(TeamMember.PARAM_ID);
        if (teamMember == null) {
            // something went horribly wrong
            restartApp();
        }
        this.generation = getArguments().getInt(Region.PARAM_GENERATION);
    }

    private void initContent() {
        if (teamMember == null)
            return;
        // Scrollview
        mScrollView = view.findViewById(R.id.teamMemberAbilitiesScrollView);
        ObjectAnimator xTranslate = ObjectAnimator.ofInt(mScrollView, "scrollX", 0);
        ObjectAnimator yTranslate = ObjectAnimator.ofInt(mScrollView, "scrollY", 0);
        scrollAnimator = new AnimatorSet();
        scrollAnimator.setDuration(1000L);
        scrollAnimator.playTogether(xTranslate, yTranslate);
        // initial moves learned
        movesLearned = teamMember.getMoves() == null ? new ArrayList<MoveLearned>(4) : teamMember.getMoves();
        // moves learned list view
        selectedAbilitiesListView = view.findViewById(R.id.teamMemberAbilitiesLearnedList);
        selectedAbilitiesListView.setLayoutManager(new LinearLayoutManager(getContext()));
        selectedAbilitiesListViewAdapter = new GenericRecyclerViewAdapter<MoveLearned>(getContext(), movesLearned, R.layout.movelearned_common_tile, null) {
            @Override
            public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
                MoveLearned ml = getDataset().get(position);
                Pokemon pokemon = getDataProvider().getById(teamMember.getPid());
                UIHelper.updateListTileMove(getContext(), getDataProvider(), holder.view, getResources(), ml, pokemon.getProperties(), true);
                holder.view.setTag(ml);
            }
        };
        selectedAbilitiesListView.setAdapter(selectedAbilitiesListViewAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                movesLearned.remove(viewHolder.itemView.getTag());
                // persist
                teamMember.setMoves(movesLearned);
                XMLHelper.persistTeams(getContext());
                // update UI
                selectedAbilitiesListViewAdapter.notifyDataSetChanged();
                updateSelectorComponentsVisibility(true);
            }
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
        });
        itemTouchHelper.attachToRecyclerView(selectedAbilitiesListView);
        // available moves list view
        selectableAbilitiesListView = view.findViewById(R.id.teamMemberAbilitiesMovesList);
        selectableAbilitiesListView.setLayoutManager(new LinearLayoutManager(getContext()));
        selectableAbilitiesListViewAdapter = new GenericRecyclerViewAdapter<MoveLearned>(getContext(), movesLearnedPool, 0, null) {
            @Override
            public int getItemViewType(int position) {
                return getDataset().get(position).getTrigger().ordinal();
            }

            @NonNull
            @Override
            public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View v = layoutInflater.inflate(R.layout.movelearned_common_tile, parent, false);
                final MyViewHolder vh = new MyViewHolder(v);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addLearnedAbility((MoveLearned) v.getTag());
                    }
                });
                return vh;
            }

            @Override
            public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
                MoveLearned ml = getDataset().get(position);
                Pokemon pokemon = getDataProvider().getById(teamMember.getPid());
                UIHelper.updateListTileMove(getContext(), getDataProvider(), holder.view, getResources(), ml, pokemon.getProperties(), true);
                holder.view.setTag(ml);
            }
        };
        selectableAbilitiesListView.setAdapter(selectableAbilitiesListViewAdapter);
        updateMovesLearnedPool(MoveLearnTrigger.LEVELUP);
        //selectableAbilitiesListView.setVisibility(View.INVISIBLE);
        // Buttons
        MultiStateToggleButton toggle = view.findViewById(R.id.mstb_multi_id);
        toggle.setValue(0);
        toggle.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int position) {
                switch (position) {
                    case 0:
                        toggleMoveLearnedType(MoveLearnTrigger.LEVELUP);
                        break;
                    case 1:
                        toggleMoveLearnedType(MoveLearnTrigger.MACHINE);
                        break;
                    case 2:
                        toggleMoveLearnedType(MoveLearnTrigger.BREEDING);
                        break;
                    case 3:
                        toggleMoveLearnedType(MoveLearnTrigger.TUTOR);
                        break;
                    default:
                        break;
                }
            }
        });
        updateSelectorComponentsVisibility(false);
    }

    private void toggleMoveLearnedType(MoveLearnTrigger trigger) {
        updateMovesLearnedPool(trigger);
        selectableAbilitiesListViewAdapter.notifyDataSetChanged();
        // update list view
        ViewGroup headerParent = view.findViewById(R.id.teamMemberAbilitiesMovesListHeader);
        headerParent.removeAllViews();
        if (trigger == MoveLearnTrigger.LEVELUP) {
            LayoutInflater.from(getContext()).inflate(R.layout.movelearned_lvlup_header_tile, headerParent);
        } else {
            LayoutInflater.from(getContext()).inflate(R.layout.movelearned_machine_header_tile, headerParent);
        }
        selectableAbilitiesListView.setVisibility(View.VISIBLE);
    }

    private void updateMovesLearnedPool(final MoveLearnTrigger trigger) {
        movesLearnedPool.clear();
        movesLearnedPool.addAll(getDataProvider().getGenerationsMoveList(
                getDataProvider().getPokemonForm(teamMember.getPid(), teamMember.getVariantIdx()),
                generation,
                trigger));
        if (trigger == MoveLearnTrigger.LEVELUP || trigger == MoveLearnTrigger.MACHINE)
            Collections.sort(movesLearnedPool, trigger == MoveLearnTrigger.LEVELUP ? moveLearnedMachineNameComparator : moveLearnedLvlComparator);
    }

    @SuppressLint("StringFormatInvalid")
    private void addLearnedAbility(MoveLearned ml) {
        movesLearned.add(ml);
        // persist
        teamMember.setMoves(movesLearned);
        XMLHelper.persistTeams(getContext());
        // update UI to new state
        selectedAbilitiesListViewAdapter.notifyDataSetChanged();
        updateSelectorComponentsVisibility(true);
        String toastMsg = String.format(getResources().getString(R.string.team_member_ability_learned_toast),
                getDataProvider().getById(teamMember.getPid()).getName(),
                getDataProvider().getMove(ml.getMoveId()).getName());
        // quick info in UI for user
        Toast.makeText(getContext(), toastMsg, Toast.LENGTH_SHORT).show();
    }

    private void updateSelectorComponentsVisibility(boolean animate) {
        if (movesLearned.size() >= TeamMember.MAX_MOVES) {
            if (animate) {
                scrollAnimator.start();
                UIHelper.fadeOut(view.findViewById(R.id.teamMemberAbilitiesMovesWrapper));
            } else {
                mScrollView.scrollTo(0,0);
            }
            view.findViewById(R.id.teamMemberAbilitiesMovesWrapper).setVisibility(View.INVISIBLE);
        } else {
            view.findViewById(R.id.teamMemberAbilitiesMovesWrapper).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.team_member_abilities));
    }
}