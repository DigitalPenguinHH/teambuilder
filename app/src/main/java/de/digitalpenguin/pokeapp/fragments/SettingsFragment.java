package de.digitalpenguin.pokeapp.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreferenceCompat;

import java.util.Locale;

import de.digitalpenguin.pokeapp.MainActivity;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.util.LocaleManager;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {

    public static final String PREF_LANGUAGE = "language";

    static final String PREF_SEARCH_RESULT_INCLUDE_FORMS = "search.result.include.forms";
    static final String PREF_SEARCH_RESULT_INCLUDE_CHARACTERISTICS = "search.result.include.characteristics";
    static final String PREF_SEARCH_RESULT_INCLUDE_NATURES = "search.result.include.natures";
    static final String PREF_SEARCH_RESULT_INCLUDE_ITEMS = "search.result.include.items";
    static final String PREF_SEARCH_RESULT_INCLUDE_MOVES = "search.result.include.moves";
    static final String PREF_SEARCH_RESULT_INCLUDE_ABILITIES = "search.result.include.abilities";

    private ListPreference mListPreferenceLang;

    private SwitchPreferenceCompat switchPreferenceForms;
    private SwitchPreferenceCompat switchPreferenceCharacteristics;
    private SwitchPreferenceCompat switchPreferenceNatures;
    private SwitchPreferenceCompat switchPreferenceItems;
    private SwitchPreferenceCompat switchPreferenceMoves;
    private SwitchPreferenceCompat switchPreferenceAbilities;

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_fragment);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        initPreferences();
    }

    private void initPreferences() {
        // Language
        mListPreferenceLang = (ListPreference)  getPreferenceManager().findPreference(PREF_LANGUAGE);
        mListPreferenceLang.setOnPreferenceChangeListener(this);
        String langCode = sharedPreferences.getString(PREF_LANGUAGE, Locale.getDefault().getLanguage());
        mListPreferenceLang.setValue(langCode);
        mListPreferenceLang.setIcon(UIHelper.getDrawableResourceId(getContext(), "flag_" + langCode));
        mListPreferenceLang.setSummary(new Locale(langCode).getDisplayLanguage());
        // Search Results
        switchPreferenceForms = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_FORMS);
        switchPreferenceForms.setOnPreferenceChangeListener(this);
        boolean prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_FORMS, true);
        switchPreferenceForms.setChecked(prefYesNo);
        switchPreferenceForms.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
        //
        switchPreferenceCharacteristics = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_CHARACTERISTICS);
        switchPreferenceCharacteristics.setOnPreferenceChangeListener(this);
        prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_CHARACTERISTICS, true);
        switchPreferenceCharacteristics.setChecked(prefYesNo);
        switchPreferenceCharacteristics.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
        //
        switchPreferenceNatures = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_NATURES);
        switchPreferenceNatures.setOnPreferenceChangeListener(this);
        prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_NATURES, true);
        switchPreferenceNatures.setChecked(prefYesNo);
        switchPreferenceNatures.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
        //
        switchPreferenceItems = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_ITEMS);
        switchPreferenceItems.setOnPreferenceChangeListener(this);
        prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_ITEMS, true);
        switchPreferenceItems.setChecked(prefYesNo);
        switchPreferenceItems.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
        //
        switchPreferenceMoves = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_MOVES);
        switchPreferenceMoves.setOnPreferenceChangeListener(this);
        prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_MOVES, true);
        switchPreferenceMoves.setChecked(prefYesNo);
        switchPreferenceMoves.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
        //
        switchPreferenceAbilities = (SwitchPreferenceCompat) getPreferenceManager().findPreference(PREF_SEARCH_RESULT_INCLUDE_ABILITIES);
        switchPreferenceAbilities.setOnPreferenceChangeListener(this);
        prefYesNo = sharedPreferences.getBoolean(PREF_SEARCH_RESULT_INCLUDE_ABILITIES, true);
        switchPreferenceAbilities.setChecked(prefYesNo);
        switchPreferenceAbilities.setSummary(prefYesNo ? R.string.common_yes : R.string.common_no);
    }

    private void persistPreferenceValue(String key, Object value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (value instanceof  String) {
            edit.putString(key, (String) value);
        } else if (value instanceof  Integer) {
            edit.putInt(key, (Integer) value);
        }
        edit.apply();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        switch (preference.getKey()) {
            case PREF_LANGUAGE:
                persistPreferenceValue(preference.getKey(), newValue);
                LocaleManager.setNewLocale(getContext(), (String) newValue);
                getActivity().recreate();
                break;
            case PREF_SEARCH_RESULT_INCLUDE_FORMS:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceForms.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            case PREF_SEARCH_RESULT_INCLUDE_CHARACTERISTICS:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceCharacteristics.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            case PREF_SEARCH_RESULT_INCLUDE_NATURES:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceNatures.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            case PREF_SEARCH_RESULT_INCLUDE_ITEMS:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceItems.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            case PREF_SEARCH_RESULT_INCLUDE_MOVES:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceMoves.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            case PREF_SEARCH_RESULT_INCLUDE_ABILITIES:
                persistPreferenceValue(preference.getKey(), newValue);
                switchPreferenceAbilities.setSummary(((Boolean) newValue) ? R.string.common_yes : R.string.common_no);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set title bar
        ((MainActivity) getActivity()).setActionBarTitle(getResources().getString(R.string.nav_menu_settings));
    }

}
