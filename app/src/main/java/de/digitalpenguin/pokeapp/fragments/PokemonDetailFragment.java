package de.digitalpenguin.pokeapp.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thekhaeng.pushdownanim.PushDownAnim;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.MainActivity;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.Location;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.listener.OnClickToaster;
import de.digitalpenguin.pokeapp.listener.OnPokemonClickListener;
import de.digitalpenguin.pokeapp.util.EvolutionChainMatrixBuilder;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

import static de.digitalpenguin.pokeapp.util.UIHelper.dpToPx;

public class PokemonDetailFragment extends AbstractAppFragment {

    private static final List<PProperty> TEXT_WHITE_PROPERTIES = Arrays.asList(PProperty.DARK, PProperty.GROUND, PProperty.GHOST);

    private static final int POKEMON_GENERATIONS = 8;
    private static final int STAT_MIN_BAR_WIDTH_DP = 30;
    private static final int MAX_FORMS = 6;

    private static final String PARAM_ADDABLE = "Pokemon.isAddable";

    private View view;

    private PokemonData p;

    private Team team;

    private Variant selectedForm = null;

    private Context ctx;

    private Boolean addable = Boolean.TRUE;

    static PokemonDetailFragment newInstance(int pokemonId) {
        return newInstance(pokemonId, -1, true);
    }

    public static PokemonDetailFragment newInstance(int pokemonId, Variant form, boolean addable) {
        PokemonDetailFragment f = new PokemonDetailFragment();
        Bundle b = new Bundle();
        b.putInt(Pokemon.PARAM_ID, pokemonId);
        if (form != null)
            b.putSerializable(PokemonData.PARAM_FORM, form);
        b.putBoolean(PARAM_ADDABLE, addable);
        f.setArguments(b);
        return f;
    }

    public static PokemonDetailFragment newInstance(int pokemonId, int formIdx, boolean addable) {
        PokemonDetailFragment f = new PokemonDetailFragment();
        Bundle b = new Bundle();
        b.putInt(Pokemon.PARAM_ID, pokemonId);
        if (formIdx >= 0)
            b.putInt(PokemonData.PARAM_FORM_IDX, formIdx);
        b.putBoolean(PARAM_ADDABLE, addable);
        f.setArguments(b);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pokemon_detail_fragment, container, false);
        ctx = getContext();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFromParams();
        initContent();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getPokemon() != null) {
            outState.putInt(Pokemon.PARAM_ID, p.getId());
            if (selectedForm != null)
                outState.putInt(PokemonData.PARAM_FORM_IDX, p.getForms().indexOf(selectedForm));
            if (team != null)
                outState.putString(Team.PARAM_ID, team.getId());
        } else {
            restartApp();
        }
    }

    private void initFromParams() {
        Bundle args = getArguments();
        if (args == null)
            return;
        // fetch current pokemon
        p = getDataProvider().getPokemonData(args.getInt(Pokemon.PARAM_ID, 0));
        if (p == null) {
            // something went horribly wrong
            restartApp();
            return;
        }
        int formIdx = args.getInt(PokemonData.PARAM_FORM_IDX);
        if (formIdx >= 0 && p.getForms().size() > formIdx) {
            selectedForm = getPokemon().getForms().get(formIdx);
        }
        final Variant v = (Variant) args.getSerializable(PokemonData.PARAM_FORM);
        if (v != null) {
            selectedForm = v;
        }
        // current Team
        String teamId = args.getString(Team.PARAM_ID);
        this.team = StringUtils.isNotEmpty(teamId) ? TeamsStore.getInstance().getTeam(teamId) : null;
        // moves
//        moves = getDataProvider().getGenerationsMoveSet(getPokemonForm());
        // can be added to to team via FAB?
        addable = args.getBoolean(PARAM_ADDABLE);
    }

    private Map<Object, View.OnClickListener> listenerCache = new HashMap<>(10);

    private void initContent() {
        final PokemonData pokemon = getPokemon();
        if (pokemon == null) {
            restartApp();
            return;
        }
        initPokemonInformation();
        initAbilities();
        initEvolutionChain();
//        initMoveByGenerrationButtons(data);
        initStats();
        // BG-Color and Foldable Listener
        initHeader(R.id.pokemonDetailHeader1, R.id.pokemonDetailContentArtwork);
        initHeader(R.id.pokemonDetailHeader2, R.id.pokemonDetailDataTable);
        initHeader(R.id.pokemonDetailHeader4, R.id.pokemonDetailEvolutionContent);
        initHeader(R.id.pokemonDetailHeaderAbilities, R.id.pokemonDetailAbilitiesContent);
        initHeader(R.id.pokemonDetailHeader3, R.id.pokemonDetailContentMoves);
        initHeader(R.id.pokemonDetailHeader5, R.id.pokemonDetailContentStats);
        // evtl. Text-Color
        if (TEXT_WHITE_PROPERTIES.contains(pokemon.getPrimaryProperty())) {
            TextView v1 = view.findViewById(R.id.pokemonDetailHeaderName);
            v1.setTextColor(getResources().getColor(R.color.colorWhite));
            TextView v2 = view.findViewById(R.id.pokemonDetailHeaderInfo);
            v2.setTextColor(getResources().getColor(R.color.colorWhite));
        }
        // Floating ActionButton
        FloatingActionButton fab = view.findViewById(R.id.pokemonJoinTeamFab);
        if (Boolean.TRUE.equals(addable)) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Variant matchForm = (Variant) CollectionUtils.find(pokemon.getForms(), new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            return ((Variant) object).getId() == selectedForm.getId();
                        }
                    });
                    if (team == null) {
                        AddToTeamFragment fragment = new AddToTeamFragment();
                        fragment.setArguments(createBundleArgs(Pokemon.PARAM_ID, pokemon.getId(),
                                PokemonData.PARAM_FORM_IDX, matchForm != null ? pokemon.getForms().indexOf(matchForm) : -1));
                        UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
                    } else {
                        team.add(pokemon, pokemon.getForms().indexOf(matchForm));
                        XMLHelper.persistTeams(getContext());
                        showTeamDetailsFragment(team);
                    }
                }
            });
            fab.setVisibility(TeamsStore.getInstance().getTeamEntries().isEmpty() ? View.INVISIBLE : View.VISIBLE);
        } else {
            removeFromParent(fab);
        }
        // Force Focus away from Search
        view.findViewById(R.id.pokemonDetailRootLayout).requestFocus();
    }

    private void initHeader(final int headerId, final int closableId) {
        setBackgroundColor(headerId);
        final View header = view.findViewById(headerId);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup content = view.findViewById(closableId);
                boolean visible = content.getVisibility() == View.VISIBLE;
                content.setVisibility(visible ? View.GONE : View.VISIBLE);
                ImageView icon = header.findViewById(R.id.folderToggleIcon);
                icon.setImageDrawable(getResources().getDrawable(visible ? R.drawable.add_boxed : R.drawable.remove_boxed));
            }
        });
    }

    private class LoadMovesLearnedAsyncTask extends AsyncTask<Void, Integer, Map<Integer, List<MoveLearned>>> {

        private final WeakReference<View> refProgress;

        LoadMovesLearnedAsyncTask(View progressBar) {
            this.refProgress = new WeakReference<>(progressBar);
        }

        @Override
        protected void onPreExecute() {
            if (refProgress.get() == null)
                return;
            refProgress.get().setVisibility(View.VISIBLE);
        }

        @Override
        protected Map<Integer, List<MoveLearned>> doInBackground(Void... voids) {
            return getDataProvider().getGenerationsMoveSet(getPokemonForm());
        }

        @Override
        protected void onPostExecute(Map<Integer, List<MoveLearned>> data) {
            if (refProgress.get() == null)
                return;
            refProgress.get().findViewById(R.id.loadingProgress).setVisibility(View.GONE);
            initMoveByGenerationButtons(data);
        }
    }

    private void asyncLoadData() {
        new LoadMovesLearnedAsyncTask(view.findViewById(R.id.loadingProgress)).execute();
    }

    private void initMoveByGenerationButtons(Map<Integer, List<MoveLearned>> data) {
        // Move Button listeners
        for (int i = 1; i <= POKEMON_GENERATIONS; i++) {
            String buttonId = String.format("pokemonDetailsMovesButtonGen%d", i);
            Button b = view.findViewById(getResources().getIdentifier(buttonId, "id", getActivity().getPackageName()));
            final int genId = i;
            enableButton(b, data.get(genId) != null);
            View.OnClickListener listener = listenerCache.get(i);
            if (listener == null) {
                listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UIHelper.swapFragment(getFragmentManager(), getMovesLearnedFragment(genId, ""));
                    }
                };
                listenerCache.put(i, listener);
            }
            b.setOnClickListener(listener);
            PushDownAnim.setPushDownAnimTo(b).setOnClickListener(listener);
        }
        // Let's Go extra Button
        Button ib = view.findViewById(getResources().getIdentifier("pokemonDetailsMovesButtonGen7LG", "id", getActivity().getPackageName()));
        if (getDataProvider().hasGameSpecificMoves(getPokemon())) {
            final String key = Game.GAMES_KEY_LETSGO_PIKACHU;
            View.OnClickListener listener = listenerCache.get(key);
            if (listener == null) {
                listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UIHelper.swapFragment(getFragmentManager(), getMovesLearnedFragment(7, key));
                    }
                };
                listenerCache.put(key, listener);
            }
            ib.setOnClickListener(listener);
            PushDownAnim.setPushDownAnimTo(ib).setOnClickListener(listener);
            enableButton(ib, getDataProvider().hasMoves(getPokemonForm(), 7, key));
        } else {
            removeFromParent(ib);
        }
    }

    private void enableButton(Button b, boolean enabled) {
        if (enabled) {
            b.setTextColor(getResources().getColor(R.color.colorBlack));
            b.setTypeface(b.getTypeface(), Typeface.BOLD);
            b.setEnabled(true);
        } else {
            b.setTextColor(getResources().getColor(R.color.lightGrey));
            b.setTypeface(b.getTypeface(), Typeface.NORMAL);
            b.setEnabled(false);
        }
    }

    private PokemonData getPokemon() {
        if (p == null)
            initFromParams();
        if (p == null) {
            restartApp();
        }
        return p;
    }

    private Fragment getMovesLearnedFragment(int generation, String gamesKey) {

        PokemonMovesLearnedFragment fragment = new PokemonMovesLearnedFragment();
        Bundle args = new Bundle();
        args.putInt(Pokemon.PARAM_ID, (int) getPokemon().getId());
        if (selectedForm != null)
            args.putString(PokemonData.PARAM_FORM,  selectedForm.getType().name());
        args.putInt("generation", generation);
        args.putString("gamesKey", gamesKey);
        fragment.setArguments(args);

        return fragment;
    }

    private void initPokemonInformation() {
        TextView headerName = view.findViewById(R.id.pokemonDetailHeaderName);
        headerName.setText(getPokemon().getName());
        // Icon
        ImageView headerIcon = view.findViewById(R.id.pokemonDetailHeaderIcon);
        headerIcon.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(getPokemon(), VariantType.DEFAULT),null, getActivity().getPackageName()));
        // NationalDex-#
        TextView dexNo = view.findViewById(R.id.pokemonDetailHeaderNationalDexNr);
        dexNo.setText(String.format("#%03d", getPokemon().getId()));
        // Artwork und Formen
        initFormsViews();
        // Properties
        updateProperties();
        // Pokedex Daten
        DecimalFormat decimalFormat = new DecimalFormat("#0.0", DecimalFormatSymbols.getInstance(Locale.getDefault()));
        TextView textHeight = view.findViewById(R.id.pokemonDetailHeight);
        textHeight.setText(getPokemon().getHeight() != null ? decimalFormat.format(getPokemon().getHeight()) + " m" : "???");
        TextView textWeight = view.findViewById(R.id.pokemonDetailWeight);
        textWeight.setText(getPokemon().getWeight() != null ? decimalFormat.format(getPokemon().getWeight()) + " kg" : "???");
        TextView textGender = view.findViewById(R.id.pokemonDetailGender);
        if (getPokemon().getMalePercent() >= 0 && getPokemon().getFemalePercent() >= 0) {
            textGender.setText(String.format("♂ %.1f%% , ♀ %.1f%%", getPokemon().getMalePercent(), getPokemon().getFemalePercent()));
        } else {
            textGender.setText("-");
        }
        if (getPokemon().getMalePercent() < 0 && getPokemon().getFemalePercent() < 0)
            textGender.setText(R.string.common_unknown);
        TextView textDesc = view.findViewById(R.id.pokemonDetailDescription);
        textDesc.setText(getPokemon().getDexEntry());
    }

    private void updateProperties() {
        PProperty[] props = getPokemon().getProperties();
        if (selectedForm != null && ArrayUtils.isNotEmpty(selectedForm.getProperties())) {
            props = selectedForm.getProperties();
        }
        ImageView propertyIcon1 = view.findViewById(R.id.pokemonDetailProperty1);
        UIHelper.makePropertyIcon(getActivity(), propertyIcon1, props[0]);
        ImageView propertyIcon2 = view.findViewById(R.id.pokemonDetailProperty2);
        if (ArrayUtils.getLength(props) > 1 && props[1] != null) {
            UIHelper.makePropertyIcon(getActivity(), propertyIcon2, props[1]);
            propertyIcon2.setVisibility(View.VISIBLE);
        } else {
            propertyIcon2.setVisibility(View.INVISIBLE);
        }
    }

    private void updateArtwork() {
        ImageView artWorkImg = view.findViewById(R.id.pokemonDetailArtwork);
        // determine Variant
        Variant v = this.selectedForm;
        if (v == null && CollectionUtils.isNotEmpty(getPokemon().getForms())) {
            v = getPokemon().getForms().get(0);
        }
        // set Image
        int artworkResId = UIHelper.getArtworkResource(getContext(), getPokemon(), v);
        String uri = "drawable://" + artworkResId;
        ImageLoader.getInstance().displayImage(uri, artWorkImg);
    }

    private void initFormsViews() {
        List<Variant> forms = getPokemon().getForms();
        boolean hasVariants = CollectionUtils.isNotEmpty(forms);
        if (selectedForm == null)
            selectedForm = hasVariants ? forms.get(0) : null;
        int i = 1;
        while (i <= MAX_FORMS) {
            Variant v = PokemonData.getForm(forms, i - 1);
            initFormButton(i, v);
            i++;
        }
        // Artwork
        updateArtwork();
    }

    private Map<Integer, WeakReference<TextView>> abilitiesescriptionsMap = new HashMap<>(3);

    private void initAbilities() {
        abilitiesescriptionsMap.clear();
        PokemonForm form = getPokemonForm();
        ViewGroup wrapper = view.findViewById(R.id.pokemonDetailsAbiltiesWrapper);
        List<PokemonAbility> abilities = getDataProvider().listAbilities(getPokemon().getId(), form.getType());
        for (int i = 0; i < 3; i++) {
            ViewGroup accordionCompAbility = (ViewGroup) wrapper.getChildAt(i);
            if (i < abilities.size()) {
                final PokemonAbility pa = abilities.get(i);
                TextView tv = accordionCompAbility.findViewById(R.id.abilityListItemName);
                tv.setText(pa.getAbility().getName());
                // set description text and hide (initially)
                TextView abilityDesc = (TextView) accordionCompAbility.getChildAt(accordionCompAbility.getChildCount() - 1);
                abilityDesc.setText(pa.getAbility().getDescriptions().getText());
                abilityDesc.setVisibility(View.GONE);
                // "hidden" hint
                ImageView iv = accordionCompAbility.findViewById(R.id.abilityListItemHidden);
                if (!pa.isHidden()) {
                    iv.setVisibility(View.INVISIBLE);
                } else {
                    iv.setOnClickListener(new OnClickToaster(getContext(), getString(R.string.ability_hidden)));
                }
                accordionCompAbility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (Map.Entry<Integer, WeakReference<TextView>> e : abilitiesescriptionsMap.entrySet()) {
                            TextView textView = e.getValue().get();
                            if (textView == null)
                                continue;
                            if (e.getKey().equals(v.getTag())) {
                                textView.setVisibility(textView.getVisibility() == View.GONE ? View.VISIBLE :  View.GONE);
                            } else {
                                textView.setVisibility(View.GONE);
                            }
                        }
                    }
                });
                accordionCompAbility.setTag(pa.getAbilityId());
                accordionCompAbility.setVisibility(View.VISIBLE);
                abilitiesescriptionsMap.put(pa.getAbilityId(), new WeakReference<>(abilityDesc));
            } else {
                accordionCompAbility.setVisibility(View.GONE);
            }
        }
    }

    private PokemonForm getPokemonForm() {
        PokemonForm form;
        if (selectedForm != null) {
            form = PokemonForm.newForm(selectedForm, getPokemon());
        } else {
            form = PokemonForm.newDefaultForm(getPokemon());
        }
        return form;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initFormButton(int i, final Variant v) {
        final LinearLayout ll = view.findViewById(getResources().getIdentifier("pokemonDetailForm" + Integer.toString(i), "id", ctx.getPackageName()));
        if (v == null) {
            removeFromParent(ll);
            return;
        }
        ll.setTag(v.getId());
        PushDownAnim.setPushDownAnimTo(ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SwitchFormListener(v).onClick(ll);
            }
        });
        TextView tv = (TextView) ll.getChildAt(1);
        tv.setText(UIHelper.getFullPokemonName(getPokemon(), v, getResources()));
        if (v.getId() == selectedForm.getId()) {
            setFormSelected(ll);
        }
    }

    private void initEvolutionChain() {
        final LinearLayout container = view.findViewById(R.id.pokemonDetailsEvolutionsContainer);

        EvolutionChain chain = getDataProvider().getEvolutionChain(getPokemon().getId());
        // check no evo chains
        if (chain == null) {
            // placeholder view
            TextView tvPlaceholder = view.findViewById(R.id.pokemonDetailsEvolutionsNoContent);
            LayoutParams lpPlaceHolder = tvPlaceholder.getLayoutParams();
            lpPlaceHolder.width = getDisplayWidth() - 10;
            tvPlaceholder.setLayoutParams(lpPlaceHolder);
            // remove evo hint
            removeFromParent(this.view.findViewById(R.id.pokemonDetailsEvolutionDescription));
            return;
        }

        // clear all
        container.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(ctx);

        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();

        for (int rowIdx = 0 ; rowIdx < rows.size() ; rowIdx ++) {
            List<EvolutionChainLink> links = rows.get(rowIdx);
            // evolution row
            LinearLayout ll = new LinearLayout(ctx);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            if (rowIdx != rows.size() - 1) {
                ll.setBackgroundResource(R.drawable.shape_bg_stroke_thin_bottom);
                lp.setMargins(0, UIHelper.dpToPx(ctx, 5), 0, 0);
            }
            ll.setPadding(0, UIHelper.dpToPx(ctx, 5), 0,UIHelper.dpToPx(ctx, 5));
            ll.setLayoutParams(lp);
            // remember
            List<String> chainPokemonIds = new ArrayList<>(3);
            for (EvolutionChainLink link: links) {
                int idFrom = link.getPokemonIdFrom();
                // avoid duplicate links
                String keyFrom = Integer.toString(idFrom).concat("#").concat(StringUtils.defaultString(link.getVariantFrom()));
                if (!chainPokemonIds.contains(keyFrom)) {
                    newEvolutionForm(ll, inflater, idFrom, link.getVariantFrom());
                    chainPokemonIds.add(keyFrom);
                }
                // connector
                View connector = inflater.inflate(link.getTrigger().size() > 3 ? R.layout.pokemon_evolution_link_tall: R.layout.pokemon_evolution_link, ll, false);
                LinearLayout conditionContainer = connector.findViewById(R.id.evolutionLinkConditionsWrapper);
                conditionContainer.removeAllViews();
                int idx = 0;
                for (EvolutionCondition<?> condition : link.getTrigger()) {
                    addConditionRow(conditionContainer, condition, idx);
                    idx++;
                }
                lp = (LinearLayout.LayoutParams) conditionContainer.getLayoutParams();
                lp.height = dpToPx(ctx, (idx * 32) + ((idx-1) * 5) + 10);
                conditionContainer.setLayoutParams(lp);
                ll.addView(connector);
                newEvolutionForm(ll, inflater, link.getPokemonIdTo(), link.getVariantTo());
                String keyTo = Integer.toString(link.getPokemonIdTo()).concat("#").concat(StringUtils.defaultString(link.getVariantTo()));
                chainPokemonIds.add(keyTo);
            } // end loop links
            container.addView(ll);
        }

        if (rows.isEmpty() || !CollectionUtils.exists(chain.getChainLinks(), new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                return CollectionUtils.exists(((EvolutionChainLink) object).getTrigger(), new Predicate() {
                    @Override
                    public boolean evaluate(Object object) {
                        return ((EvolutionCondition<?>) object).getCondition() == EvolutionCondition.Type.OTHER;
                    }
                });
            }
        })) {
            removeFromParent(this.view.findViewById(R.id.pokemonDetailsEvolutionDescription));
        }
    }

    private void addConditionRow(LinearLayout conditionContainer, EvolutionCondition<?> condition, int idx) {
        // Declare views
        ConstraintLayout row = (ConstraintLayout) getLayoutInflater().inflate(R.layout.pokemon_evolution_link_condition_simple, conditionContainer, false);
        if (idx > 0) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) row.getLayoutParams();
            lp.setMargins(0, dpToPx(ctx, 7), 0,0);
            row.setLayoutParams(lp);
        }
        ImageView linkIconBefore = row.findViewById(R.id.evolutionLinkIconBeforeText);
        TextView condText = row.findViewById(R.id.evolutionConditionText);
        ImageView linkIconAfter = row.findViewById(R.id.evolutionLinkIconAfterText);
        // Game Version Info
        addGameInfoOptional(row, condition);
        // Type of Condition
        Spannable spannable = null;
        String str = "";
        switch (condition.getCondition()) {
            case LEVELUP:
                Integer lvl = (Integer) condition.getObject();
                if (lvl != null && lvl > 0)
                    str = Integer.toString(lvl);
                condText.setTypeface(Typeface.DEFAULT_BOLD);
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                linkIconBefore.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.evolution_condition_lvlup)));
                removeFromParent(linkIconAfter);
                break;
            case ITEM:
                Item item = (Item) condition.getReferenceObject(getDataProvider());
                String iconName = String.format("item_%s", item.getDrawableResourceName());
                int iconResource = getResources().getIdentifier("@drawable/" + iconName, null, getActivity().getPackageName());
                linkIconBefore.setImageResource(iconResource);
                linkIconBefore.setOnClickListener(new OnClickToaster(ctx, item.getName()));
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) linkIconBefore.getLayoutParams();
                lp.width = dpToPx(ctx, 28);
                lp.height = dpToPx(ctx, 28);
                linkIconBefore.setLayoutParams(lp);
                removeFromParent(linkIconAfter);
                break;
            case FRIENDSHIP:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                linkIconAfter.setImageResource(R.drawable.icon_heart_red);
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.evolution_condition_affection)));
                break;
            case CANDIES:
                removeFromParent(linkIconBefore);
                str = Integer.toString((Integer) condition.getReferenceObject(getDataProvider()));
                linkIconAfter.setImageResource(R.drawable.candy_meltan);
                break;
            case DAY:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                linkIconAfter.setImageResource(R.drawable.icons8_sun_96);
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.evolution_condition_day)));
                break;
            case NIGHT:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                linkIconAfter.setImageResource(R.drawable.icons8_moon_and_stars_96);
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.evolution_condition_night)));
                break;
            case BEAUTY:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                linkIconBefore.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.evolution_condition_lvlup)));
                str = ctx.getString(R.string.evolution_condition_beauty);
                removeFromParent(linkIconAfter);
                break;
            case TRADE:
                linkIconBefore.setImageResource(R.drawable.icons8_trade_pokmemon);
                str = getResources().getString(R.string.evolution_condition_trade);
                removeFromParent(linkIconAfter);
                break;
            case TRADE_AGAINST:
                linkIconBefore.setImageResource(R.drawable.icons8_trade_pokmemon);
                Integer pokemonId = (Integer) condition.getReferenceObject(getDataProvider());
                Pokemon pTrade = getDataProvider().getById(pokemonId);
                str = getResources().getString(R.string.evolution_condition_tradeagainst);
                linkIconAfter.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(pTrade, VariantType.DEFAULT),null, getActivity().getPackageName()));
                linkIconAfter.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, pTrade.getName()));
                break;
            case TRADE_WITH_ITEM:
                linkIconBefore.setImageResource(R.drawable.icons8_trade_pokmemon);
                str = ctx.getString(R.string.evolution_condition_trade_with_item);
                Item tradeItem = (Item) condition.getReferenceObject(getDataProvider());
                String tradeIconName = String.format("item_%s", tradeItem.getDrawableResourceName());
                int tradeIconResId = getResources().getIdentifier("@drawable/" + tradeIconName, null, getActivity().getPackageName());
                linkIconAfter.setImageResource(tradeIconResId);
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, tradeItem.getName()));
                break;
            case ABILITY:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                Move ability = (Move) condition.getReferenceObject(getDataProvider());
                String moveName = ability.getName();
                str = String.format(getResources().getString(R.string.evolution_condition_ability), moveName);
                spannable = new SpannableString(str);
                spannable.setSpan(new ForegroundColorSpan(Color.BLUE), str.indexOf(moveName), str.indexOf(moveName) + moveName.length(), 0);
                removeFromParent(linkIconAfter);
                break;
            case DS_UPSIDE_DOWN:
                str = ctx.getString(R.string.evolution_condition_ds_upside_down);
                removeFromParent(linkIconBefore);
                removeFromParent(linkIconAfter);
                break;
            case FREE_TEAMSLOT:
                str = ctx.getString(R.string.evolution_condition_free_teamslot);
                linkIconAfter.setImageResource(R.drawable.item_poke_ball);
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, ctx.getString(R.string.evolution_condition_pokeball)));
                removeFromParent(linkIconBefore);
                break;
            case WEATHER:
                if ("Rainy".equals(condition.getReferenceObject(getDataProvider()))) {
                    linkIconBefore.setImageResource(R.drawable.icons8_rain_48);
                    str = ctx.getString(R.string.evolution_condition_weather_rainy);
                } else {
                    removeFromParent(linkIconBefore);
                }
                removeFromParent(linkIconAfter);
                break;
            case PROPERTY_IN_TEAM:
                PProperty prop = PProperty.valueOf((String) condition.getReferenceObject(getDataProvider()));
                UIHelper.makePropertyIcon(ctx, linkIconBefore, prop);
                str = getResources().getString(R.string.evolution_condition_prop_in_team);
                removeFromParent(linkIconAfter);
                break;
            case GENDER:
                String genderId = (String) condition.getReferenceObject(getDataProvider());
                str = getResources().getString(R.string.evolution_condition_gender, genderId.equals("M") ? "♂" : "♀");
                removeFromParent(linkIconBefore);
                removeFromParent(linkIconAfter);
                break;
            case LOCATION:
                linkIconBefore.setImageResource(R.drawable.ic_location);
                Location loc = (Location) condition.getReferenceObject(getDataProvider());
                str = loc.getName();
                removeFromParent(linkIconAfter);
                break;
            case RANDOM:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                str = getResources().getString(R.string.evolution_condition_random);
                removeFromParent(linkIconAfter);
                break;
            case STAT_RELATION:
                String[] parts = ((String) condition.getReferenceObject(getDataProvider())).split(" ");
                StringBuilder sbRel = new StringBuilder();
                for (int i = 0; i < parts.length; i++) {
                    if (i%2==0) {
                        sbRel.append(UIHelper.getString(ctx, "stat_" + parts[i].toLowerCase()));
                    } else {
                        switch (parts[1]) {
                            case "gt":
                                sbRel.append(" > ");
                                break;
                            case "lt":
                                sbRel.append(" < ");
                                break;
                            case "eq":
                                sbRel.append(" = ");
                                break;
                            default:
                                break;
                        }
                    }
                }
                str = sbRel.toString();
                break;
            case POKEMON_IN_TEAM:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                str = getResources().getString(R.string.evolution_condition_pokemon_in_team);
                Pokemon pInTeam = (Pokemon) condition.getReferenceObject(getDataProvider());
                linkIconAfter.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(pInTeam, VariantType.DEFAULT),null, getActivity().getPackageName()));
                linkIconAfter.setOnClickListener(new OnClickToaster(ctx, pInTeam.getName()));
                break;
            case TIME:
                linkIconBefore.setImageResource(R.drawable.icons8_watch_48);
                str = (String) condition.getReferenceObject(getDataProvider());
                if (Locale.getDefault().getLanguage().equals("en")) {
                    if (str.contains("17")) {
                        str = str.replaceAll("17", "05");
                        str = str.replaceAll("-", "PM - ");
                    } else {
                        str = str.replaceAll("-", "AM - ");
                    }
                }
                removeFromParent(linkIconAfter);
                break;
            case PROPERTY_ABILITY:
                linkIconBefore.setImageResource(R.drawable.icon_lvlup);
                str = getResources().getString(R.string.evolution_condition_ability_learned);
                PProperty propLearned = (PProperty) condition.getReferenceObject(getDataProvider());
                UIHelper.makePropertyIcon(ctx, linkIconAfter, propLearned);
                break;
            case HEARTS_POKEMONAMI:
                str = getResources().getString(R.string.evolution_condition_hearts_in_pokemonami, (Integer) condition.getReferenceObject(getDataProvider()));
                removeFromParent(linkIconBefore);
                removeFromParent(linkIconAfter);
                break;
            case HEARTS_POKEREFRESH:
                str = getResources().getString(R.string.evolution_condition_hearts_in_pokepause, (Integer) condition.getReferenceObject(getDataProvider()));
                removeFromParent(linkIconBefore);
                removeFromParent(linkIconAfter);
                break;
            case OTHER:
                linkIconBefore.setImageResource(R.drawable.ic_questionmark);
                linkIconBefore.setOnClickListener(new OnClickToaster(ctx, getResources().getString(R.string.common_unknown)));
                TextView descText = this.view.findViewById(R.id.pokemonDetailsEvolutionDescriptionText);
                descText.setText(getResources().getIdentifier((String) condition.getReferenceObject(getDataProvider()), "string", getActivity().getPackageName()));
                removeFromParent(linkIconAfter);
                break;
            default:
                break;
        }
        // hint "From generation ..."
        TextView textGenHint = row.findViewById(R.id.evolutionLinkGenerationHint);
        if (condition.getFromGeneration() != null) {
            String genText = ctx.getString(R.string.evolution_condition_from_generation, condition.getFromGeneration());
            textGenHint.setText(genText);
        } else {
            removeFromParent((View) textGenHint.getParent());
        }
        // Text
        if (spannable != null) {
            condText.setText(spannable, TextView.BufferType.SPANNABLE);
        } else if (StringUtils.isNotEmpty(str)) {
            condText.setText(str);
        } else {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) linkIconAfter.getLayoutParams();
            lp.setMargins(dpToPx(ctx, 5), 0, 0,0);
            removeFromParent(condText);
        }
        conditionContainer.addView(row);
    }

    private void addGameInfoOptional(View container, EvolutionCondition<?> condition) {
        List<Game> games = condition.getGames();
        ViewGroup wrapper = container.findViewById(R.id.evolutionLinkGameIcons);
        if (wrapper == null)
            return;
        wrapper.removeAllViews();
        if (games != null) {
            for (Game g : games) {
                ImageView iv = new ImageView(ctx);
                String iconName = "game_icon_" + g.getResourceName() + "_square";
                int iconResource = getResources().getIdentifier("@drawable/" + iconName, null, getActivity().getPackageName());
                iv.setImageResource(iconResource);
                LinearLayout.LayoutParams lpGameIcon = new LinearLayout.LayoutParams(dpToPx(ctx, 20), LayoutParams.MATCH_PARENT);
                lpGameIcon.setMargins(0, dpToPx(ctx, 5), 0, dpToPx(ctx, 5));
                wrapper.addView(iv, lpGameIcon);
                iv.setOnClickListener(new OnClickToaster(ctx, g.getName()));
            }
        } else {
            removeFromParent(wrapper);
        }
    }

    private void newEvolutionForm(LinearLayout container, LayoutInflater inflater, int pId, String variantName) {
        // get layout
        View form = inflater.inflate(R.layout.pokemon_evolution_form, null);
        // get pokemon
        Pokemon pkmn = getDataProvider().getById(pId);
        // determine Variant
        Variant var = null;
        List<Variant> forms = getDataProvider().getForms(pId);
        if (variantName != null) {
            for (Variant v : forms) {
                if (v.getType().name().equals(variantName.toUpperCase())) {
                    var = v;
                    break;
                }
            }
        } else if (selectedForm != null && CollectionUtils.isNotEmpty(forms)) {
            var = selectedForm;
        }
        // name
        TextView textName = form.findViewById(R.id.evolutionFormName);
        textName.setText(UIHelper.getFullPokemonName(pkmn, var, getResources()));
        if (getPokemon().getId() == pId) {
            textName.setTypeface(null, Typeface.BOLD);
        }
        // artwork
        ImageView viewArt = form.findViewById(R.id.evolutionFormImg);
        int artworkResId = UIHelper.getArtworkResource(ctx, pkmn, var);
        if (artworkResId == 0 && !forms.isEmpty())
            artworkResId = UIHelper.getArtworkResource(ctx, pkmn, forms.iterator().next());
        String uri = "drawable://" + artworkResId;
        ImageLoader.getInstance().displayImage(uri, viewArt);
        // listener
        if (getPokemon().getId() != pId) {
            OnPokemonClickListener listener = new OnPokemonClickListener(getActivity().getSupportFragmentManager(), pId,
                    getPokemon().getForms().indexOf(var), false);
            viewArt.setOnClickListener(listener);
            textName.setOnClickListener(listener);
        }
        // add
        container.addView(form);
    }

    private void initStats() {
        if (getPokemon().getBaseStats() != null) {
            Integer max = Collections.max(Arrays.asList(
                    getPokemon().getBaseStats().getKp(),
                    getPokemon().getBaseStats().getAtk(),
                    getPokemon().getBaseStats().getDef(),
                    getPokemon().getBaseStats().getSpcAtk(),
                    getPokemon().getBaseStats().getSpcDef(),
                    getPokemon().getBaseStats().getInit()
            ));
            max = Math.max(180, max);
            initStat(R.id.pokemonDetailStatsBarHp, getPokemon().getBaseStats().getKp(), max);
            initStat(R.id.pokemonDetailStatsBarAtk, getPokemon().getBaseStats().getAtk(), max);
            initStat(R.id.pokemonDetailStatsBarDef, getPokemon().getBaseStats().getDef(), max);
            initStat(R.id.pokemonDetailStatsBarSpAtk, getPokemon().getBaseStats().getSpcAtk(), max);
            initStat(R.id.pokemonDetailStatsBarSpDef, getPokemon().getBaseStats().getSpcDef(), max);
            initStat(R.id.pokemonDetailStatsBarInit, getPokemon().getBaseStats().getInit(), max);
        } else {
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarHp));
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarAtk));
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarDef));
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarSpAtk));
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarSpDef));
            removeFromParent(this.view.findViewById(R.id.pokemonDetailStatsBarInit));
        }
    }

    private void initStat(int resIdBar, int value, int max) {
        LinearLayout bar = view.findViewById(resIdBar);
        float relation = ((float) max / (float) value);
        int widthRemainingPx = getDisplayWidth()
                - dpToPx(ctx, 20) // paddings
                - dpToPx(ctx, 85); // label
        int widthPx = (int) (widthRemainingPx / relation);
        int minWidthPx = dpToPx(ctx, STAT_MIN_BAR_WIDTH_DP);
        if (value < 10) {
            // Mindestbreite
            widthPx = minWidthPx;
        } else {
            widthPx = Math.max(minWidthPx, widthPx);
        }
        bar.setLayoutParams(new LinearLayout.LayoutParams(widthPx, LayoutParams.MATCH_PARENT));
        TextView text = (TextView) bar.getChildAt(0);
        text.setText(Integer.toString(value));
    }

    private void setBackgroundColor(int layoutResId) {
        LinearLayout layout = view.findViewById(layoutResId);
        if (layout == null)
            return;
        PProperty prop = null;
        if (selectedForm != null && selectedForm.getProperties() != null) {
            prop = selectedForm.getPrimaryProperty();
            if (PProperty.NORMAL == prop && selectedForm.getProperties().length > 1) {
                prop = selectedForm.getProperties()[1];
            }
        } else {
            prop = getPokemon().getPrimaryProperty();
            if (PProperty.NORMAL == prop && getPokemon().isMultiProperty())
                prop = getPokemon().getProperties()[1];
        }
        String colorName = "colorPropertyBg_" + prop.name();
        int colorResId = getResources().getIdentifier("@color/" + colorName,null, getActivity().getPackageName());
        GradientDrawable bgDrawable = (GradientDrawable) layout.getBackground();
        bgDrawable.setColor(getResources().getColor(colorResId));
    }

    @Override
    public void onStart() {
        super.onStart();
        asyncLoadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        //
        ((MainActivity) getActivity()).collapseSearchBar();
        if (getPokemon() != null)
            updateActivityAppBarTitle(getPokemon().getName());
    }

    private void setFormSelected(ViewGroup v) {
        updateArtwork();
        updateProperties();
        initAbilities();
        // bg
        for (int i = 1; i <= MAX_FORMS; i++) {
            if (i > getPokemon().getForms().size())
                continue;
            int resId = getResources().getIdentifier("pokemonDetailForm" + Integer.toString(i), "id", ctx.getPackageName());
            ViewGroup formBtn = view.findViewById(resId);
            AppCompatRadioButton btnRadio = (AppCompatRadioButton) formBtn.getChildAt(0);
            btnRadio.setChecked(v == formBtn);
        }
    }

    private void showTeamDetailsFragment(Team team) {
        // show
        TeamDetailFragment fragment = new TeamDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Team.PARAM_ID, team.getId());
        fragment.setArguments(bundle);
        UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
    }

    private class SwitchFormListener implements View.OnClickListener
    {
        private final Variant variant;

        private SwitchFormListener(Variant variant) {
            this.variant = variant;
        }

        @Override
        public void onClick(View source) {
            PokemonDetailFragment.this.selectedForm = this.variant;
            setFormSelected((ViewGroup) source);
        }
    }
}
