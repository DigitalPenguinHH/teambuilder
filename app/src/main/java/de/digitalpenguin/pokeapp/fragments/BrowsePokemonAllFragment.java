package de.digitalpenguin.pokeapp.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericSearchResultListAdapter;
import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.ListDataProvider;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class BrowsePokemonAllFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    private RecyclerView resultsListView;
    private GenericSearchResultListAdapter resultsListViewAdapter;

    private List<Pokemon> currentResult = Collections.emptyList();

    protected View view;

    /* from Arguments */
    private Team team;
    private TeamMember editMember;
    private Game game;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(getLayoutId(), container, false);
        getParameter();
        initListView();
        initViews();
        return view;
    }

    private void getParameter() {
        if (getArguments() == null)
            return;
        // fetch team
        String teamId = getArguments().getString(Team.PARAM_ID);
        if (teamId == null)
            return;
        this.team = TeamsStore.getInstance().getTeam(teamId);
        this.editMember = (TeamMember) getArguments().getSerializable(TeamMember.PARAM_ID);
        String gameKey = getArguments().getString(Team.PARAM_GAME);
        if (gameKey != null)
            this.game = getDataProvider().getGameByName(gameKey);
    }

    protected Integer getGeneration() {
        return game != null ? game.getGeneration() : null;
    }

    protected void initViews() {
        // no op
    }

    protected int getLayoutId() {
        return R.layout.browse_pokemon_all_fragment;
    }

    private void initListView() {

        this.currentResult = getInitialResults();

        this.resultsListView = this.view.findViewById(R.id.genericBrowseListView);

        this.resultsListView = view.findViewById(R.id.searchResultsListView);
        this.resultsListView.setHasFixedSize(true);

        this.resultsListViewAdapter = new GenericSearchResultListAdapter(getContext(), getDataProvider(), getCurrentResult(), this);
        this.resultsListView.setAdapter(this.resultsListViewAdapter);

        RecyclerView.LayoutManager resultsListViewLayoutManager = new LinearLayoutManager(getContext());
        this.resultsListView.setLayoutManager(resultsListViewLayoutManager);
    }

    protected List<Pokemon> getInitialResults() {
        return new LinkedList<>();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        if (team == null) {
            UIHelper.swapFragment(fragmentManager, getPokemonDetailFragment(position));
        } else {
            // add to team
            Pokemon p = (Pokemon) resultsListViewAdapter.getItem(position);
            if (editMember == null) {
                team.add(p);
            } else {
                team.substitute(editMember.getSlot(), p.getId());
            }
            // hide keyboard
            UIHelper.hideKeyboardFrom(getContext(), view);
            // save changes
            XMLHelper.persistTeams(getActivity().getApplicationContext());
            // return to previous
            fragmentManager.popBackStack(TeamDetailFragment.TAG, 0);
        }
    }

    @NonNull
    private PokemonDetailFragment getPokemonDetailFragment(int position) {
        Pokemon p = (Pokemon) resultsListViewAdapter.getItem(position);
        return PokemonDetailFragment.newInstance(p.getId());
    }

    public GenericSearchResultListAdapter getListAdapter() {
        return resultsListViewAdapter;
    }

    public RecyclerView getResultsListView() {
        return resultsListView;
    }

    List<Pokemon> getCurrentResult() {
        return currentResult;
    }

    @Override
    public void onStart() {
        super.onStart();
        asyncLoadData();
    }

    private static class LoadAsyncTask<T> extends AsyncTask<Void, Integer, List<T>> {

        private ListDataProvider<T> dataProvider;
        private WeakReference<View> progressBarRef;
        private WeakReference<GenericSearchResultListAdapter> adapterRef;

        LoadAsyncTask(View progressBar, GenericSearchResultListAdapter adapter, ListDataProvider dataProvider) {
            this.dataProvider = dataProvider;
            this.adapterRef = new WeakReference<>(adapter);
            this.progressBarRef = new WeakReference<>(progressBar);
        }

        @Override
        protected List<T> doInBackground(Void... voids) {
            return dataProvider.getData();
        }

        @Override
        protected void onPostExecute(List<T> data) {
            // update progress finished
            View progressBar = progressBarRef.get();
            if (progressBar == null)
                return;
            progressBar.setVisibility(View.GONE);
            // set list entries in adapter
            GenericSearchResultListAdapter resultsListViewAdapter = adapterRef.get();
            if (resultsListViewAdapter == null)
                return;
            resultsListViewAdapter.setEntries(data);
        }
    }

    protected void asyncLoadData() {
        View progressBar = view.findViewById(R.id.loadingProgress);
        if (game != null) {
            new LoadAsyncTask<Pokemon>(progressBar, resultsListViewAdapter, new ListDataProvider<Pokemon>() {
                @Override
                public List<Pokemon> getData() {
                    Region region = getDataProvider().getRegion(game.getGeneration());
                    List<Pokemon> allEntries = getDataProvider().find(DataFilter.regionFilter(region));
                    Collections.sort(allEntries, new PokemonComparator(getDataProvider().getRegion(game.getGeneration()).getPokedex()));
                    return allEntries;
                }
            }).execute();
        } else {
            new LoadAsyncTask<Pokemon>(progressBar, resultsListViewAdapter, new ListDataProvider<Pokemon>() {
                @Override
                public List<Pokemon> getData() {
                    List<Pokemon> allEntries = getDataProvider().getPokemonEntries();
                    Collections.sort(allEntries, new PokemonComparator(PokemonComparator.SORT_ID));
                    return allEntries;
                }
            }).execute();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(team != null ? R.string.team_add_member_header : R.string.nav_menu_browse));
    }
}
