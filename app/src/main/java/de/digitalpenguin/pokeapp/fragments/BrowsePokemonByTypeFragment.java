package de.digitalpenguin.pokeapp.fragments;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.NamedItem;
import de.digitalpenguin.pokeapp.adapter.PropertySpinnerAdapter;
import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PProperty;

public class BrowsePokemonByTypeFragment extends BrowsePokemonAllFragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;

    @Override
    protected int getLayoutId() {
        return R.layout.browse_pokemon_bytype_fragment;
    }

    @Override
    protected List<Pokemon> getInitialResults() {
        return new ArrayList<>(0);
    }

    @Override
    protected void initViews() {
        // spinner
        spinner = this.view.findViewById(R.id.browsePokemonPropertySpinner);
        spinner.setAdapter(new PropertySpinnerAdapter(getActivity()));
        spinner.setOnItemSelectedListener(this);
        spinner.setSelection(0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        NamedItem<PProperty> item = (NamedItem<PProperty>) spinner.getItemAtPosition(pos);
        PProperty p = item.getObject();
        getCurrentResult().clear();
        if (p != null) {
            DataFilter dataFilter = DataFilter.propertyFilter(p);
            if (getGeneration() != null)
                dataFilter.setRegion(getDataProvider().getRegion(getGeneration()));
            List<Pokemon> result = getDataProvider().find(dataFilter);
            Collections.sort(result, new PokemonComparator(PokemonComparator.SORT_ID));
            getCurrentResult().addAll(result);
        }
        getListAdapter().setRawEntries(getCurrentResult());
        getListAdapter().notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    protected void asyncLoadData() {
        // no op
    }
}
