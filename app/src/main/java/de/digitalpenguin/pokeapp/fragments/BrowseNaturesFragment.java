package de.digitalpenguin.pokeapp.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.comparator.NamedComparator;
import de.digitalpenguin.pokeapp.comparator.NamedLexicalComparator;
import de.digitalpenguin.pokeapp.data.CharacteristicOrNature;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.StatType;
import de.digitalpenguin.pokeapp.data.util.Named;

public class BrowseNaturesFragment extends AbstractAppFragment {

    private View view;

    private GenericListAdapter<CharacteristicOrNature> listAdapter;

    private ListView resultsListView;

    private List<CharacteristicOrNature> currentResult = Collections.emptyList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_characteristics_or_natures_result_list, container, false);
        initListView();
        return view;
    }

    private void initListView() {
        this.currentResult = getInitialResults();
        this.listAdapter = new GenericListAdapter<CharacteristicOrNature>(view.getContext(), this.currentResult, R.layout.nature_tile) {

            @Override
            protected void initView(CharacteristicOrNature item, View view) {
                TextView text = view.findViewById(R.id.natureText);
                Context ctx = view.getContext();
                text.setText(item.getNature().getName());
                if (item.getNature().getDecreased() == null || item.getNature().getIncreased() == null) {
                    LinearLayout parent = view.findViewById(R.id.natureValueWrapper);
                    parent.removeAllViews();
                    TextView neutralText = new TextView(ctx);
                    neutralText.setText(R.string.common_none);
                    neutralText.setTextColor(getResources().getColor(R.color.lightGrey));
                    parent.addView(neutralText, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    parent.setGravity(Gravity.CENTER_HORIZONTAL);
                } else {
                    TextView textInc = view.findViewById(R.id.natureIncreaseValue);
                    textInc.setText(getStatName(ctx, item.getNature().getIncreased()));
                    TextView textDec = view.findViewById(R.id.natureDecreaseValue);
                    textDec.setText(getStatName(ctx, item.getNature().getDecreased()));
                }
            }
        };
        this.resultsListView = this.view.findViewById(R.id.genericBrowseListView);
        this.resultsListView.setAdapter(listAdapter);
    }

    @NonNull
    private String getStatName(Context context, StatType statType) {
        return getStringResource(context, "stat_" + statType.name().toLowerCase());
    }

    private List<CharacteristicOrNature> getInitialResults() {
        List<CharacteristicOrNature> list = new LinkedList<>();
        for (Nature n: getDataProvider().getNatures()) {
            list.add(new CharacteristicOrNature(n));
        }
        Collections.sort(list, new NatureNameComparator());
        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.nav_menu_dvnatures));
    }

    private static class NatureNameComparator extends NamedComparator
    {
        @Override
        public int compare(Named o1, Named o2) {
            return super.compare(((CharacteristicOrNature) o1).getNature(), ((CharacteristicOrNature) o2).getNature());
        }
    }

}
