package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowseCharacteristicsAndNaturesFragment extends Fragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_pokemon_fragment, container, false);
        initTabs();
        return this.view;
    }

    private void initTabs() {

        TabLayout tabLayout = this.view.findViewById(R.id.browsePokemonTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.nature)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.characteristic)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = this.view.findViewById(R.id.browsePokemonCategoryPager);
        viewPager.setAdapter(new BrowserCharacteristicsAndNaturesPagerAdapter(getChildFragmentManager()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // hide keyboard
                UIHelper.hideKeyboardFrom(getContext(), view);
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    class BrowserCharacteristicsAndNaturesPagerAdapter extends FragmentStatePagerAdapter
    {

        BrowserCharacteristicsAndNaturesPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new BrowseNaturesFragment();
                case 1:
                    return new BrowseCharacteristicsFragment();
                default:
                    break;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
