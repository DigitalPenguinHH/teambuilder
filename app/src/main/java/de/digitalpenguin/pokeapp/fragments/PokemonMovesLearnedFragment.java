package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.PokemonMoveCategoryPagerAdapter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;

public class PokemonMovesLearnedFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    private Integer generation;

    private Pokemon pokemon;
    private String variantTypeStr;

    private String gamesKey;

    protected View view;

    private View root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.moves_learned_fragment, container, false);
        this.root = view.findViewById(R.id.movesLearnedFragmentRoot);
        return view;
    }

    private void getParameter() {
        Bundle b = getArguments();
        this.pokemon = getDataProvider().getById(b.getInt(Pokemon.PARAM_ID));
        this.variantTypeStr = b.getString(PokemonData.PARAM_FORM);
        this.generation = b.getInt("generation");
        this.gamesKey = b.getString("gamesKey");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getParameter();
        initTabs();
        initViews();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Pokemon.PARAM_ID, pokemon.getId());
        outState.putInt("generation", generation);
        outState.putString("gamesKey", gamesKey);
        outState.putString(PokemonData.PARAM_FORM, variantTypeStr);
    }

    private void initTabs() {

        TabLayout tabLayout = this.view.findViewById(R.id.pokemonmMovesLearnedTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.move_category_lvlup));
        tabLayout.addTab(tabLayout.newTab().setText(Integer.valueOf(Generation.GEN_8_ID).equals(this.generation) ? R.string.move_category_machine_gen8 : R.string.move_category_machine));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.move_category_eggmoves));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.move_category_tutored));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = this.view.findViewById(R.id.pokemonmMovesLearnedCategoryPager);
        viewPager.setAdapter(new PokemonMoveCategoryPagerAdapter(getChildFragmentManager(), this.pokemon, this.variantTypeStr, this.generation, this.gamesKey));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    protected void initViews() {
        // no op
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(String.format(getResources().getString(R.string.moves_header_gen), generation));
        root.requestFocus();
    }

}
