package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.BrowsePokemonFragmentPagerAdapter;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowsePokemonFragment extends AbstractAppFragment {

    private View view;

    /* Args from Bundle */
    private Team team;
    private TeamMember editMember;
    private Game game;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_pokemon_fragment, container, false);
        getParameter();
        initTabs();
        return this.view;
    }

    private void getParameter() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            // fetch team
            String teamId = getArguments().getString(Team.PARAM_ID);
            this.team = TeamsStore.getInstance().getTeam(teamId);
            TeamMember member = (TeamMember) getArguments().getSerializable(TeamMember.PARAM_ID);
            if (member != null) {
                this.editMember = member;
            }
            String gameKey = getArguments().getString(Team.PARAM_GAME);
            if (gameKey != null)
                this.game = getDataProvider().getGameByName(gameKey);
        }
    }

    private void initTabs() {

        TabLayout tabLayout = this.view.findViewById(R.id.browsePokemonTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.pokedex));
        if (game == null) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.common_region));
        }
        tabLayout.addTab(tabLayout.newTab().setText(R.string.common_type));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = this.view.findViewById(R.id.browsePokemonCategoryPager);
        viewPager.setAdapter(new BrowsePokemonFragmentPagerAdapter(getChildFragmentManager(), this.team, this.editMember));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // hide keyboard
                UIHelper.hideKeyboardFrom(getContext(), view);
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
