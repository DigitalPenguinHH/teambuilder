package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.NamedItem;
import de.digitalpenguin.pokeapp.adapter.PropertySpinnerAdapter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.PropertyRelation;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class TypesFragment extends AbstractAppFragment implements AdapterView.OnItemSelectedListener {

    private View rootView;

    private Spinner spinner;

    private int selectedTypePos = 0;
    private int selectedStanceId = R.id.typeStanceButtonDefensive;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // fragment root view
        rootView = inflater.inflate(R.layout.types_fragment, container, false);
        // spinner
        spinner = rootView.findViewById(R.id.browsePokemonPropertySpinner);
        spinner.setAdapter(new PropertySpinnerAdapter(getActivity()));
        spinner.setOnItemSelectedListener(this);
        spinner.setSelection(0);
        // switch
        RadioGroup buttonGroupStance = rootView.findViewById(R.id.typeStanceButtonGroup);
        buttonGroupStance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedStanceId = checkedId;
                onItemSelected(null, null, selectedTypePos, 0L);
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        selectedTypePos = pos;
        NamedItem<PProperty> item = (NamedItem<PProperty>) spinner.getItemAtPosition(pos);
        PProperty p = item.getObject();
        LinearLayout layout1 = rootView.findViewById(R.id.propertyStrengths);
        LinearLayout layout2 = rootView.findViewById(R.id.propertyWeaknessesContent);
        LinearLayout layout3 = rootView.findViewById(R.id.propertyNoEffectAgainst);
        if (p == null) {
            layout1.setVisibility(View.INVISIBLE);
            layout2.setVisibility(View.INVISIBLE);
            layout3.setVisibility(View.INVISIBLE);
        } else {
            layout1.setVisibility(View.VISIBLE);
            layout1.removeViews(1, layout1.getChildCount()-1);
            layout2.setVisibility(View.VISIBLE);
            layout2.removeAllViews();
            layout3.setVisibility(View.VISIBLE);
            layout3.removeViews(1, layout3.getChildCount()-1);
            for (PropertyRelation pr : getDataProvider().listPropertyRelations(p)) {
                if (pr.getRelationType() == PropertyRelation.RelationType.STRONG_AGAINST) {
                    newLine(pr.getRelationTo(), layout1, R.color.propertyRelationStrength);
                } else if (selectedStanceId == R.id.typeStanceButtonOffensive && pr.getRelationType() == PropertyRelation.RelationType.WEAK_AGAINST) {
                    newLine(pr.getRelationTo(), layout2, R.color.propertyRelationWeakness);
                } else if (selectedStanceId == R.id.typeStanceButtonDefensive && pr.getRelationType() == PropertyRelation.RelationType.BEAT_UP_BY) {
                    newLine(pr.getRelationTo(), layout2, R.color.propertyRelationWeakness);
                } else if (pr.getRelationType() == PropertyRelation.RelationType.NO_EFFECT_ON) {
                    newLine(pr.getRelationTo(), layout3, R.color.propertyRelationNoEffect);
                }
            }
        }
    }

    private void newLine(PProperty p, LinearLayout layout1, int colorId) {
        View tile = LayoutInflater.from(getContext()).inflate(R.layout.property_relation_tile, layout1, false);
        ImageView iv = tile.findViewById(R.id.propertyTileIcon);
        iv.setImageResource(getResources().getIdentifier(UIHelper.getImageUriPropertyIcon(p), null, getContext().getPackageName()));
        TextView tv = tile.findViewById(R.id.propertyTileText);
        tv.setText(UIHelper.getString(getContext(), String.format("prop_%s", p.name().toLowerCase())));
        tile.setBackgroundColor(getResources().getColor(colorId));
        layout1.addView(tile);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle(getResources().getString(R.string.nav_menu_properties));
    }
}
