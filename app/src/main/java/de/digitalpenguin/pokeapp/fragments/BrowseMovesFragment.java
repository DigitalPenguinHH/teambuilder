package de.digitalpenguin.pokeapp.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.adapter.GenericSearchResultListAdapter;
import de.digitalpenguin.pokeapp.adapter.NamedItem;
import de.digitalpenguin.pokeapp.adapter.PropertySpinnerAdapter;
import de.digitalpenguin.pokeapp.comparator.NamedComparator;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.ListDataProvider;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowseMovesFragment extends AbstractAppFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private RecyclerView.Adapter<GenericSearchResultListAdapter.MyViewHolder> resultsListViewAdapter;

    private List<Move> currentResult = Collections.emptyList();

    private View view;

    private Spinner propertySpinner;
    private PProperty selectedProperty;

    private Spinner categorySpinner;
    private MoveType selectedMoveType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_moves_fragment, container, false);
        initViews();
        initListView(inflater);
        return this.view;
    }

    private void initViews() {
        // property spinner
        propertySpinner = this.view.findViewById(R.id.browseMovesPropertySpinner);
        propertySpinner.setAdapter(new PropertySpinnerAdapter(getActivity(), true));
        propertySpinner.setOnItemSelectedListener(this);
        propertySpinner.setSelection(0);
        // category spinner
        categorySpinner = this.view.findViewById(R.id.browseMovesCategorySpinner);
        categorySpinner.setAdapter(new GenericListAdapter<MoveType>(getContext(),
                Arrays.asList(null, MoveType.PHYSICAL, MoveType.SPECIAL, MoveType.STATUS), R.layout.spinner_layout_default) {
            @Override
            protected void initView(MoveType item, View view) {
                ImageView imageView = view.findViewById(R.id.img);
                TextView textView = view.findViewById(R.id.txt);
                if (item == null) {
                    imageView.setImageDrawable(null);
                    textView.setText(getResources().getString(R.string.common_any));
                } else {
                    String key = "move_type_" + item.name().toLowerCase();
                    int resIdIcon = UIHelper.getDrawableResourceId(getContext(), key);
                    String name = UIHelper.getString(getContext(), key);
                    imageView.setImageResource(resIdIcon);
                    textView.setText(name);
                }
            }
        });
        categorySpinner.setOnItemSelectedListener(this);
        categorySpinner.setSelection(0);
    }


    private void initListView(final LayoutInflater inflater) {

        this.currentResult = getInitialResults();

        removeFromParent(this.view.findViewById(R.id.moveLearnedTileHeaderLvl));

        RecyclerView resultsListView = view.findViewById(R.id.searchResultsListView);
        resultsListView.setHasFixedSize(true);

        this.resultsListViewAdapter = new RecyclerView.Adapter<GenericSearchResultListAdapter.MyViewHolder>(){
            @NonNull
            @Override
            public GenericSearchResultListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View v = layoutInflater.inflate(R.layout.movelearned_lvlup_tile, parent, false);
                v.setOnClickListener(BrowseMovesFragment.this);
                return new GenericSearchResultListAdapter.MyViewHolder(v);
            }
            @Override
            public void onBindViewHolder(@NonNull GenericSearchResultListAdapter.MyViewHolder holder, int position) {
                Move move = currentResult.get(position);
                holder.view.setTag(move.getId());
                UIHelper.updateListTileMove(getContext(), holder.view, getResources(), move, null);
                UIHelper.setBackGroundColorStriped(getResources(), position, holder.view);
            }
            @Override
            public int getItemCount() {
                return currentResult.size();
            }
        };
        resultsListView.setAdapter(this.resultsListViewAdapter);

        RecyclerView.LayoutManager resultsListViewLayoutManager = new LinearLayoutManager(getContext());
        resultsListView.setLayoutManager(resultsListViewLayoutManager);
    }

    private List<Move> getInitialResults() {
        return new LinkedList<>();
    }

    @Override
    public void onStart() {
        super.onStart();
        asyncLoadData();
    }

    static class LoadMovesAsyncTask extends AsyncTask<Void, Integer, List>{

        private final List<Move> currentResult;
        private final ListDataProvider<Move> dataProvider;
        private final WeakReference<View> refProgress;
        private final WeakReference<RecyclerView.Adapter<GenericSearchResultListAdapter.MyViewHolder>> refAdapter;

        LoadMovesAsyncTask(List<Move> currentResult, View prorgessBar, ListDataProvider<Move> dataProvider, RecyclerView.Adapter<GenericSearchResultListAdapter.MyViewHolder> adapter) {
            this.currentResult = currentResult;
            this.refProgress = new WeakReference<>(prorgessBar);
            this.refAdapter = new WeakReference<>(adapter);
            this.dataProvider = dataProvider;
        }

        @Override
        protected void onPreExecute() {
            if (refProgress.get() == null)
                return;
            refProgress.get().setVisibility(View.VISIBLE);
        }

        @Override
        protected List doInBackground(Void... voids) {
            return dataProvider.getData();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(List data) {
            if (refProgress.get() == null)
                return;
            refProgress.get().findViewById(R.id.loadingProgress).setVisibility(View.GONE);
            currentResult.clear();
            currentResult.addAll(data);
            if (refAdapter.get() == null)
                return;
            refAdapter.get().notifyDataSetChanged();
        }
    }

    private static class MoveDataProvider implements ListDataProvider<Move> {

        private final DataProvider dataProvider;
        private final PProperty selectedProperty;
        private final MoveType selectedMoveType;

        MoveDataProvider(DataProvider dataProvider, PProperty selectedProperty, MoveType selectedMoveType) {
            this.dataProvider = dataProvider;
            this.selectedProperty = selectedProperty;
            this.selectedMoveType = selectedMoveType;
        }

        @Override
        public List<Move> getData() {
            List<Move> data = dataProvider.listMoves(selectedMoveType, selectedProperty);
            Collections.sort(data, new NamedComparator());
            return data;
        }
    }

    private void asyncLoadData() {
        new LoadMovesAsyncTask(currentResult, view.findViewById(R.id.loadingProgress), new MoveDataProvider(getDataProvider(), selectedProperty, selectedMoveType), resultsListViewAdapter).execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == propertySpinner) {
            PProperty selected = ((NamedItem<PProperty>) propertySpinner.getItemAtPosition(position)).getObject();
            if (selected != this.selectedProperty) {
                this.selectedProperty = selected;
                asyncLoadData();
            }
        } else if (parent == categorySpinner) {
            MoveType selected = (MoveType) categorySpinner.getItemAtPosition(position);
            if (selected != this.selectedMoveType) {
                this.selectedMoveType = selected;
                asyncLoadData();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // no op
    }

    @Override
    public void onClick(View v) {
        int moveId = (int) v.getTag();
        if (moveId > 0) {
            MoveDetailFragment fragment = new MoveDetailFragment();
            fragment.setArguments(createBundleArgs(Move.TAG_ID, moveId));
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    }

}
