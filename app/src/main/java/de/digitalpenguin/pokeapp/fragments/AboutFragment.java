package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.digitalpenguin.pokeapp.R;

public class AboutFragment extends AbstractAppFragment {

    private ViewGroup rootView;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.aboutapp_fragment, container, false);
        initContent();
        return rootView;
    }

    private void initContent() {

    }
}
