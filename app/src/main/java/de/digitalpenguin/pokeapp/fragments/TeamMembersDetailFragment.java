package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.ListItem;
import de.digitalpenguin.pokeapp.adapter.TeamMemberExpandableListAdapter;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.util.TeamMemberOptionsHandler;
import de.digitalpenguin.pokeapp.util.XMLHelper;
import de.digitalpenguin.pokeapp.widgets.AnimatedExpandableListView;

public class TeamMembersDetailFragment extends TeamDetailChildFragment {

    private View view;

    private Team team;

    private List<ListItem> listData;
    private AnimatedExpandableListView membersListView;
    private TeamMemberExpandableListAdapter listAdapter;

    private int selectedGroupPos = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.team_members_detail_fragment, container, false);
        getParameter();
        initContent();
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (team != null)
            outState.putString(Team.PARAM_ID, team.getId());
    }

    private void getParameter() {
        // fetch team
        String teamId = getArguments().getString(Team.PARAM_ID);
        this.team = TeamsStore.getInstance().getTeam(teamId);
        if (team == null) {
            // something went horribly wrong
            restartApp();
        }
    }

    private void initContent() {
        if (team == null)
            return;
        membersListView = view.findViewById(R.id.teamMembersListView);
        listData = new ArrayList<>(team.getTeamSizeMax());
        updateListData();
        listAdapter = new TeamMemberExpandableListAdapter(getContext(), getActivity().getSupportFragmentManager(), getDataProvider(), team, listData,
                new TeamMemberOptionsHandler(getActivity().getSupportFragmentManager(), getDataProvider(), team, this));
        membersListView.setAdapter(listAdapter);
        membersListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                    if (i == groupPosition) {
                        if (membersListView.isGroupExpanded(groupPosition)) {
                            membersListView.collapseGroupWithAnimation(i);
                        } else {
                            membersListView.expandGroupWithAnimation(groupPosition);
                            selectedGroupPos = groupPosition;
                        }
                    } else {
                        membersListView.collapseGroup(i);
                    }
                }
                return true;
            }
        });
    }

    private void updateListData() {
        listData.clear();
        for (int i = 1; i <= team.getTeamSizeMax() ; i++) {
            final TeamMember teamMember = team.get(i);
            if (teamMember == null) {
                listData.add(ListItem.EMPTY_ITEM);
            } else {
                listData.add(new ListItem<>(teamMember));
            }
        }
    }

    public void saveAndRefresh() {
        XMLHelper.persistTeams(getActivity().getApplicationContext());
        refreshListView();
    }

    public void refreshListView() {
        updateListData();
        listAdapter.notifyDataSetChanged();
    }

    public TeamMember getSelectedTeamMember() {
        return (TeamMember) listData.get(selectedGroupPos).getData();
    }

    public void updateContent() {

    }
}
