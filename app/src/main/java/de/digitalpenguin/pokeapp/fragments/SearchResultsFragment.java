package de.digitalpenguin.pokeapp.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.MainActivity;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericSearchResultListAdapter;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.util.ItemPopup;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class SearchResultsFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    public static final String PARAM_QUERY_STR = "QUERY";

    private View view;

    private RecyclerView resultsListView;
    private GenericSearchResultListAdapter resultsListViewAdapter;
    private RecyclerView.LayoutManager resultsListViewLayoutManager;

    private CharSequence queryString;
    private List<Object> currentResult = Collections.emptyList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.search_results_fragment, container, false);

        this.resultsListView = view.findViewById(R.id.searchResultsListView);
        this.resultsListView.setHasFixedSize(true);

        this.resultsListViewLayoutManager = new LinearLayoutManager(getContext());
        this.resultsListView.setLayoutManager(resultsListViewLayoutManager);

        this.resultsListViewAdapter = new GenericSearchResultListAdapter(getContext(), getDataProvider(), new ArrayList<Object>(0), this);
        this.resultsListView.setAdapter(this.resultsListViewAdapter);

        this.resultsListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                UIHelper.hideKeyboardFrom(getContext(), view);
            }
        });

        if (StringUtils.isNotEmpty(queryString)) {
            ((MainActivity) getActivity()).restoreSearchBar(queryString);
        }

        return this.view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // perform search
        String query = getArguments().getString(PARAM_QUERY_STR);
        // searchView.setQuery(query, false);
        if (StringUtils.isNotEmpty(query)) {
            setQuery(query);
            ((MainActivity) getActivity()).restoreSearchBar(query);
        }
    }

    public void setQuery(String query) {
        List<Object> results;
        if (query == null || query.length() < 2) {
            if (currentResult.isEmpty())
                return;
            // clear list
            results = new ArrayList<>();
        } else {
            query = query.toLowerCase();
            // obtain results based on query and settings
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            results = getDataProvider().find(query,
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_FORMS, true),
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_CHARACTERISTICS, true),
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_NATURES, true),
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_ITEMS, true),
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_MOVES, true),
                    sharedPreferences.getBoolean(SettingsFragment.PREF_SEARCH_RESULT_INCLUDE_ABILITIES, true));
            results = addGroups(results);
        }
        resultsListViewAdapter = new GenericSearchResultListAdapter(this.view.getContext(), getDataProvider(), results, this);
        resultsListViewAdapter.setQueryString(query);
        resultsListView.setAdapter(resultsListViewAdapter);
        currentResult = results;
        queryString = query;
    }

    private List<Object> addGroups(List<Object> origList) {
        List<Object> result = new LinkedList<>();
        Class<?> lastType = null;
        for (Object o : origList) {
            if (lastType == null || !lastType.equals(o.getClass())) {
                result.add(getGroupName(o.getClass()));
            }
            lastType = o.getClass();
            result.add(o);
        }
        return result;
    }

    private String getGroupName(Class<?> type) {
        if (type.equals(Pokemon.class)) {
            return this.view.getContext().getString(R.string.pokemon);
        } else if (type.equals(PokemonForm.class)) {
            return this.view.getContext().getString(R.string.common_forms);
        } else if (type.equals(Characteristic.class)) {
            return this.view.getContext().getString(R.string.characteristic);
        } else if (type.equals(Nature.class)) {
            return this.view.getContext().getString(R.string.nature);
        } else if (type.equals(Item.class)) {
            return this.view.getContext().getString(R.string.items);
        } else if (type.equals(Move.class)) {
            return this.view.getContext().getString(R.string.common_moves);
        } else if (type.equals(Ability.class)) {
            return this.view.getContext().getString(R.string.common_abilities);
        }
        return this.view.getContext().getString(R.string.common_unknown);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (resultsListViewAdapter != null && StringUtils.isNotEmpty(resultsListViewAdapter.getQuery()))
            outState.putString(PARAM_QUERY_STR, resultsListViewAdapter.getQuery());
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        // Hide Keyboard
        UIHelper.hideKeyboardFrom(getContext(), view);
        // Show Detail Fragment
        Object item = resultsListViewAdapter.getItem(position);
        if (item instanceof Pokemon) {
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), getPokemonDetailFragment((Pokemon) item));
        } else if (item instanceof PokemonForm) {
            PokemonForm form = (PokemonForm) item;
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), PokemonDetailFragment.newInstance(form.getPokemonId(), form, true));
        } else if (item instanceof Item) {
            ItemPopup.show(getContext(), getDataProvider(), (ViewGroup) this.view, (Item) item);
        } else if (item instanceof Move) {
            MoveDetailFragment fragment = new MoveDetailFragment();
            fragment.setArguments(createBundleArgs(Move.TAG_ID, ((Move) item).getId()));
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
        } else if (item instanceof Ability) {
            AbilityDetailFragment fragment = new AbilityDetailFragment();
            fragment.setArguments(createBundleArgs(Ability.TAG_ID, ((Ability) item).getId()));
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    }

    @NonNull
    private PokemonDetailFragment getPokemonDetailFragment(Pokemon p) {
        return PokemonDetailFragment.newInstance(p.getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        updateActivityAppBarTitle("");
    }

}
