package de.digitalpenguin.pokeapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericSearchResultListAdapter;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class BrowseAbilitiesFragment extends AbstractAppFragment implements AdapterView.OnItemClickListener {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.browse_abilities_fragment, container, false);
        initListView();
        return this.view;
    }

    private void initListView() {
        List<Ability> currentResult = getInitialResults();
        GenericSearchResultListAdapter listAdapter = new GenericSearchResultListAdapter(getContext(), getDataProvider(), (List) currentResult, this);
        RecyclerView listView = this.view.findViewById(R.id.browseAbilitiesListView);
        listView.setAdapter(listAdapter);
        RecyclerView.LayoutManager resultsListViewLayoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(resultsListViewLayoutManager);
    }

    private List<Ability> getInitialResults() {
        return getDataProvider().getAbilities();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int abilityId = (int) view.getTag();
        if (abilityId > 0) {
            AbilityDetailFragment fragment = new AbilityDetailFragment();
            fragment.setArguments(createBundleArgs(Ability.TAG_ID, abilityId));
            UIHelper.swapFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    }
}
