package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.fragments.MoveDetailFragment;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class MovesLearnedListAdapter extends DataProviderArrayAdapter<MoveLearned> implements View.OnClickListener {

    private final Pokemon pokemon;

    private final MoveLearnTrigger trigger;

    private final LayoutInflater inflater;

    private final FragmentManager fragmentManager;

    public MovesLearnedListAdapter(FragmentManager fragmentManager, Context context, DataProvider dataProvider, Pokemon pokemon, List<MoveLearned> entries, MoveLearnTrigger trigger) {
        super(context, R.layout.movelearned_lvlup_tile, entries, dataProvider);
        this.fragmentManager = fragmentManager;
        this.pokemon = pokemon;
        this.trigger = trigger;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        MoveLearned ml = (MoveLearned) getItem(position);
        int resIdLayoutTile = 0;
        switch (trigger) {
            case LEVELUP:
                resIdLayoutTile = R.layout.movelearned_lvlup_tile;
                break;
            case MACHINE:
                resIdLayoutTile = R.layout.movelearned_machine_tile;
                break;
            case BREEDING:
            case TUTOR:
                resIdLayoutTile = R.layout.movelearned_edition_tile;
                break;
            default:
                break;

        }
        convertView = inflater.inflate(resIdLayoutTile, parent, false);
        UIHelper.updateListTileMove(getContext(), getDataProvider(), convertView, getContext().getResources(), ml, pokemon.getProperties(), false);
        PushDownAnim.setPushDownAnimTo(convertView).setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        MoveLearned ml = (MoveLearned) v.getTag();
        MoveDetailFragment fragment = new MoveDetailFragment();
        Bundle b = new Bundle();
        b.putInt(Move.TAG_ID, ml.getMoveId());
        fragment.setArguments(b);
        UIHelper.swapFragment(fragmentManager, fragment);
    }
}
