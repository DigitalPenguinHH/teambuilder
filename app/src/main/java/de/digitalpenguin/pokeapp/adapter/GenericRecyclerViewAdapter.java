package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.List;

public abstract class GenericRecyclerViewAdapter<T> extends RecyclerView.Adapter<GenericRecyclerViewAdapter.MyViewHolder> {

    private static final String DEF_TYPE_STRING = "string";

    private final Context context;

    private final AdapterView.OnItemClickListener listener;

    private final int layoutResId;

    private List<T> dataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public int type;
        public MyViewHolder(View v) {
            super(v);
            this.view = v;
            this.type = 0;
        }
        public MyViewHolder(View v, int type) {
            super(v);
            this.view = v;
            this.type = type;
        }
    }

    public GenericRecyclerViewAdapter(Context context, List<T> entries, int layoutResId, AdapterView.OnItemClickListener listener) {
        this.context = context;
        this.dataset = entries;
        this.layoutResId = layoutResId;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public Object getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        //Object o = dataset.get(position);
        //ViewItemType type = ViewItemType.valueOf(o.getClass().getSimpleName().toUpperCase());
        return 0;
    }

    @NonNull
    @Override
    public GenericRecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(layoutResId, parent, false);
        final GenericRecyclerViewAdapter.MyViewHolder vh = new GenericRecyclerViewAdapter.MyViewHolder(v);
        if (listener != null) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(null, v, vh.getLayoutPosition(), 0L);
                }
            });
        }
        return vh;
    }

    public void setEntries(List<T> entries) {
        dataset.clear();
        dataset.addAll(entries);
        notifyDataSetChanged();
    }

    protected String getStringResource(Resources resources, String key) {
        int resIdName = resources.getIdentifier(key, DEF_TYPE_STRING, context.getPackageName());
        return context.getString(resIdName);
    }

    public Context getContext() {
        return context;
    }

    public AdapterView.OnItemClickListener getListener() {
        return listener;
    }

    public int getLayoutResId() {
        return layoutResId;
    }

    public List<T> getDataset() {
        return dataset;
    }
}
