package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class GameSpinnerAdapter extends DataProviderArrayAdapter<NamedItem<Game>> {

    private Context context;
    private LayoutInflater inflater;

    public GameSpinnerAdapter(Context context, DataProvider dp) {
        super(context, 0, initItems(context, dp), dp);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    private static List<NamedItem<Game>> initItems(Context context, DataProvider dp) {
        List<NamedItem<Game>> items = new LinkedList<>();
        List<Game> allGames = dp.getGames();
        if (allGames == null) {
            throw new IllegalArgumentException("Games returned from Datastore must not be empty.");
        }
        Collections.sort(allGames, new Comparator<Game>() {
            @Override
            public int compare(Game o1, Game o2) {
                if (o1.hasPriority() && !o2.hasPriority())
                    return -1;
                if (!o1.hasPriority() && o2.hasPriority())
                    return 1;
                return Integer.valueOf(o2.getGeneration()).compareTo(Integer.valueOf(o1.getGeneration()));
            }
        });
        items.add(new NamedItem<Game>(context.getResources().getString(R.string.common_any)));
        for (Game v: allGames) {
            items.add(new NamedItem<>(v));
        }
        return items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NamedItem<Game> item = (NamedItem<Game>) getItem(position);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_layout_default, parent, false);
        ImageView imageView = convertView.findViewById(R.id.img);
        if (item.getObject() != null) {
            String resName = UIHelper.getImageUriGameIcon(item.getObject());
            int resIdIcon = context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
            String uri = "drawable://" + resIdIcon;
            ImageLoader.getInstance().displayImage(uri, imageView);
        } else {
            imageView.setImageResource(R.drawable.prop_empty);
        }
        TextView textView = convertView.findViewById(R.id.txt);
        textView.setText(item.getObject() == null ? item.getName() : item.getObject().getName());
        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

}
