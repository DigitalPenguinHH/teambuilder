package de.digitalpenguin.pokeapp.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.fragments.TeamDetailChildFragment;
import de.digitalpenguin.pokeapp.fragments.TeamDetailFragment;
import de.digitalpenguin.pokeapp.fragments.TeamMembersDetailFragment;
import de.digitalpenguin.pokeapp.fragments.TeamPerformaceDetailFragment;

public class TeamDetailsViewAdapter extends FragmentStatePagerAdapter {

    private final Team team;

    private final TeamDetailFragment teamDetailFragment;

    public TeamDetailsViewAdapter(FragmentManager fm, Team team, TeamDetailFragment teamDetailFragment) {
        super(fm);
        this.team = team;
        this.teamDetailFragment = teamDetailFragment;
    }

    private Fragment fragment;

    @Override
    public Fragment getItem(int position) {

        fragment = null;
        Bundle args = new Bundle();
        args.putString(Team.PARAM_ID, team.getId());
        switch(position) {
            case 0:
                fragment = new TeamMembersDetailFragment();
                ((TeamDetailChildFragment) fragment).setTeamDetailFragment(teamDetailFragment);
                break;
            case 1:
                fragment = new TeamPerformaceDetailFragment();
                break;
        }
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void tabSelected(int position) {
        if (fragment instanceof  TeamDetailChildFragment) {
            ((TeamMembersDetailFragment) fragment).updateContent();
        }
    }
}
