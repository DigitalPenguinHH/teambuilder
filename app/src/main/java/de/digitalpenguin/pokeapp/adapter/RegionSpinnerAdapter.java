package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class RegionSpinnerAdapter extends DataProviderArrayAdapter<NamedItem<Region>> {

    private Context context;

    private LayoutInflater inflater;

    public RegionSpinnerAdapter(Context context, DataProvider dataProvider) {
        super(context, 0, initItems(context, dataProvider), dataProvider);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    private static List<NamedItem<Region>> initItems(Context context, DataProvider dataProvider) {
        List<NamedItem<Region>> items = new LinkedList<>();
        List<Region> regions = dataProvider.getRegions();
        if (regions == null) {
            throw new IllegalArgumentException("Regions returned from Datastore must not be empty.");
        }
        Collections.sort(regions, new Comparator<Region>() {
            @Override
            public int compare(Region o1, Region o2) {
                return Integer.valueOf(o2.getGeneration()).compareTo(Integer.valueOf(o1.getGeneration()));
            }
        });
        for (Region v: regions) {
            items.add(new NamedItem<>(v));
        }
        return items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NamedItem<Region> item = (NamedItem<Region>) getItem(position);
        View itemView = convertView;
        if (convertView == null)
            itemView = inflater.inflate(R.layout.spinner_layout_default, parent, false);
        ImageView imageView = itemView.findViewById(R.id.img);
        if (item.getObject() != null) {
            int resIdIcon = context.getResources().getIdentifier(UIHelper.getImageUriGenerationIcon(item.getObject().getGeneration()), null, context.getPackageName());
            String uri = "drawable://" + resIdIcon;
            ImageLoader.getInstance().displayImage(uri, imageView);
        }
        TextView textView = itemView.findViewById(R.id.txt);
        textView.setText(item.getObject() == null ? item.getName() : item.getObject().getName());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

}
