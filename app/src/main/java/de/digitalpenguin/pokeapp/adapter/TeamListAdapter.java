package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.listener.TeamOptionsOnClickListener;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class TeamListAdapter extends ArrayAdapter<Team> {

    private final List<Team> dataEntries;

    private final boolean withOptions;

    private final boolean addMode;

    public TeamListAdapter(Context context, List<Team> entries) {
        this(context, entries, true);
    }

    public TeamListAdapter(Context context, List<Team> entries, boolean withOptions) {
        this(context, entries, withOptions, false);
    }

    public TeamListAdapter(Context context, List<Team> entries, boolean withOptions, boolean addMode) {
        super(context, R.layout.search_result_tile_pokemon, entries);
        this.dataEntries = entries;
        this.withOptions = withOptions;
        this.addMode = addMode;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Team team = getItem(position);
        // new child view from layout
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.team_tile, parent, false);
        // set team values
        TextView teamName = convertView.findViewById(R.id.tileTeamName);
        teamName.setText(team.getName());
        if (addMode && team.isFull())
            teamName.setTextColor(ContextCompat.getColor(getContext(), R.color.lightGrey));
        // team capacity (colored optional)
        TextView capacityText = convertView.findViewById(R.id.tileTeamCapacity);
        String teamCapacityStr = String.format(Locale.getDefault(), "%d/%d", team.getMembers().size(), team.getTeamSizeMax());
        Spannable spannable = new SpannableString(teamCapacityStr);
        spannable.setSpan(new ForegroundColorSpan(team.isFull() ? Color.BLUE : Color.RED), 0, 1, 0);
        capacityText.setText(spannable);
        if (addMode && team.isFull()) {
            capacityText.setText(R.string.team_full);
            capacityText.setTextColor(ContextCompat.getColor(getContext(), R.color.darkerGrey));
        }
        // team icon
        ImageView teamIcon = convertView.findViewById(R.id.tileTeamIcon);
        if (team.getIcon() != null) {
            int resId = UIHelper.getDrawableResourceId(getContext(), team.getIcon());
            Drawable drawable = getContext().getResources().getDrawable(resId);
            teamIcon.setImageDrawable(drawable);
        } else {
            int resIdIcon =  UIHelper.getResourceId(getContext(), team.getGame());
            teamIcon.setImageResource(resIdIcon);
        }
        final ImageView viewOptions = convertView.findViewById(R.id.tileTeamOptions);
        if (withOptions) {
            // Options
            final View finalConvertView = convertView;
            viewOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(getContext(), viewOptions);
                    menu.getMenuInflater().inflate(R.menu.team_context_menu, menu.getMenu());
                    menu.setOnMenuItemClickListener(new TeamOptionsOnClickListener(getContext(), TeamListAdapter.this, finalConvertView));
                    MenuPopupHelper menuHelper = new MenuPopupHelper(getContext(), (MenuBuilder) menu.getMenu(), viewOptions);
                    menuHelper.setForceShowIcon(true);
                    menuHelper.show();
                }
            });
        } else {
            // remove options
            if (viewOptions != null)
                ((ViewGroup) viewOptions.getParent()).removeView(viewOptions);
            // adjust right margin because of missing options
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) capacityText.getLayoutParams();
            params.rightMargin = UIHelper.dpToPx(getContext(),20);
            capacityText.setLayoutParams(params);
        }
        convertView.setTag(team);
        return convertView;
    }

    public List<Team> getDataEntries() {
        return dataEntries;
    }

}
