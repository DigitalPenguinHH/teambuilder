package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;

import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.PropertyRelation;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class PropertyCounterPokemonListAdapter extends DataProviderArrayAdapter<TeamMember> {

    private static final Predicate PREDICATE_STRONG_AGAINST = new Predicate() {
        @Override
        public boolean evaluate(Object object) {
            PropertyRelation rel = (PropertyRelation) object;
            return rel.getRelationType() == PropertyRelation.RelationType.STRONG_AGAINST;
        }
    };
    private final Boolean showId;

    private final PProperty property;

    private final PropertyRelation.RelationType relationType;

    public PropertyCounterPokemonListAdapter(Context context, PProperty property, List<TeamMember> entries, Boolean showId, PropertyRelation.RelationType reltype, DataProvider dataProvider) {
        super(context, R.layout.search_result_tile_pokemon, entries, dataProvider);
        this.showId = showId;
        this.property = property;
        this.relationType = reltype;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TeamMember tm = (TeamMember) getItem(position);
        PokemonForm form = getDataProvider().getPokemonForm(tm.getPid(), tm.getVariantIdx());
        Pokemon p = form.getPokemon();
        // new child view from layout
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_result_tile_pokemon, parent, false);
        }
        convertView.setTag(p);
        // set pokemon values
        TextView idText = convertView.findViewById(R.id.tilePokemonId);
        idText.setText(String.format("%03d", p.getId()));
        if (!Boolean.TRUE.equals(showId)) {
            idText.setLayoutParams(new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0.0f
            ));
        }
        // icon
        ImageView pokemonIcon = convertView.findViewById(R.id.tilePokemonIcon);
        Context context = getContext();
        Resources resources = getContext().getResources();
        pokemonIcon.setImageResource(resources.getIdentifier(UIHelper.getImageUriPokemonIcon(p, form.getType()),null, context.getPackageName()));
        // text result (colored optional)
        Spannable spannable = new SpannableString(UIHelper.getPokemoFormDisplayName(form, context, true));
        TextView pokemonName = convertView.findViewById(R.id.tileMainText);
        pokemonName.setText(spannable, TextView.BufferType.SPANNABLE);
        // property plates (including moves)
        ViewGroup propertiesWrapper = convertView.findViewById(R.id.teamMemberProperties);
        propertiesWrapper.removeAllViews();
        List<PProperty> properties = getDataProvider().getDistinctProperties(tm);
        if (relationType == PropertyRelation.RelationType.STRONG_AGAINST) {
            properties.retainAll(getDataProvider().listCounters(this.property));
        } else if (relationType == PropertyRelation.RelationType.BEAT_UP_BY) {
            List<PropertyRelation> propertyRelations = getDataProvider().listPropertyRelations(property);
            Collection subset = CollectionUtils.select(propertyRelations, PREDICATE_STRONG_AGAINST);
            CollectionUtils.transform(subset, new Transformer() {
                @Override
                public Object transform(Object object) {
                    PropertyRelation rel = (PropertyRelation) object;
                    return rel.getRelationTo();
                }
            });
            properties.retainAll(subset);
        }
        UIHelper.drawPropertyLayers(getContext(), propertiesWrapper, properties);
        // bg color
        if (position%2 == 0) {
            convertView.setBackgroundColor(resources.getColor(R.color.colorSearchResultDarker));
        }
        return convertView;
    }

    public void setEntries(List<Pokemon> entries) {
        clear();
        addAll(entries);
        notifyDataSetChanged();
    }

}
