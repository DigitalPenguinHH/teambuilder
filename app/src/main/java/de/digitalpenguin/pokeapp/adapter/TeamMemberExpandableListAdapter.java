package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentManager;

import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.TeamMemberOptionsHandler;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.widgets.AnimatedExpandableListView;

public class TeamMemberExpandableListAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private static final int[] OPTION_MENU_BUTTON_IDS = {
            R.id.teamMemberOptionsPokedexEntry,
            R.id.teamMemberOptionsRemove,
            R.id.teamMemberOptionsReplace,
            R.id.teamMemberOptionsSwapProps,
            R.id.teamMemberOptionsManageAbilities
    };

    private static final int TEXT_SHRINK_THRESHOLD_CHARS = 18;

    private Context _context;

    private FragmentManager fragmentManager;

    private DataProvider dataProvider;

    private final List<ListItem> teamMembersList;

    private final Team team;

    private final TeamMemberOptionsHandler optionsHandler;

    private Map<TeamMember, View> viewCache = new HashMap<>();

    public TeamMemberExpandableListAdapter(Context context, FragmentManager fragmentManager, DataProvider dataProvider, Team team, List<ListItem> teamMembersList, TeamMemberOptionsHandler optionsListener) {
        this._context = context;
        this.fragmentManager = fragmentManager;
        this.dataProvider = dataProvider;
        this.teamMembersList = teamMembersList;
        this.team = team;
        this.optionsHandler = optionsListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return teamMembersList.get(groupPosition).isEmptyItem() ? null : "Child";
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.team_member_tile_options, null);
            final TeamMember tm = (TeamMember) teamMembersList.get(groupPosition).getData();
            initListeners(convertView, tm);
        }
        return convertView;
    }

    private void initListeners(final View optionsMenu, TeamMember tm) {
        for (final int id: OPTION_MENU_BUTTON_IDS) {
            ViewGroup button = optionsMenu.findViewById(id);
            button.setTag(tm);
            PushDownAnim.setPushDownAnimTo(button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch(v.getId()) {
                        case R.id.teamMemberOptionsPokedexEntry:
                            optionsHandler.openPokedexEntry();
                            break;
                        case R.id.teamMemberOptionsReplace:
                            optionsHandler.replace();
                            break;
                        case R.id.teamMemberOptionsSwapProps:
                            optionsHandler.swapProperties();
                            break;
                        case R.id.teamMemberOptionsRemove:
                            optionsHandler.remove();
                            break;
                        case R.id.teamMemberOptionsManageAbilities:
                            optionsHandler.editMemberAbilities();
                            break;
                    }
                }
            });
        }
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return teamMembersList.get(groupPosition).isEmptyItem() ? 0 : 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.teamMembersList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.teamMembersList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        return getView(groupPosition, convertView, parent);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return teamMembersList == null || teamMembersList.isEmpty();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return !teamMembersList.get(groupPosition).isEmptyItem();
    }

    private void initView(final ListItem item, final View view) {
        if (item.isEmptyItem()) {
            PushDownAnim.setPushDownAnimTo(view).setOnClickListener(new EditTeamMemberListener(fragmentManager, team));
        } else if (item.getData() instanceof TeamMember) {
            final TeamMember teamMember = (TeamMember) item.getData();
            initMemberTile(teamMember, (ViewGroup) view);
        }
    }

    @NonNull
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ListItem item = teamMembersList.get(position);
        if (convertView != null) {
            // Verschobener Index, wegen Kind?
            if (!item.isEmptyItem()) {
                TeamMember tmTag = (TeamMember) convertView.getTag();
                TeamMember tmListData = (TeamMember) item.getData();
                if (tmTag != null && tmTag.getPid() == tmListData.getPid()) {
                    updateBackgroundWithProperties(tmTag, (ViewGroup) convertView);
                    return convertView;
                }
            }
        }
        LayoutInflater inflater = LayoutInflater.from(_context);
        if (item.isEmptyItem()) {
            convertView = inflater.inflate(R.layout.team_member_placeholder, parent, false);
        } else if (item.getData() instanceof TeamMember) {
            TeamMember tmListData = (TeamMember) item.getData();
            // Vermeiden einer neu-Initialisierung (Flackern)
            if (viewCache.get(tmListData) != null)
                return viewCache.get(tmListData);
            convertView = inflater.inflate(R.layout.team_member_tile, parent, false);
        }
        initView(item, convertView);
        if (!item.isEmptyItem())
            viewCache.put((TeamMember) item.getData(), convertView);
        return convertView;
    }

    private void initMemberTile(TeamMember m, ViewGroup tile) {
        PokemonForm form = dataProvider.getPokemonForm(m.getPid(), m.getVariantIdx());
        Pokemon p = form.getPokemon();
        // icon
        ImageView pkmnIcon = (ImageView) tile.getChildAt(0);
        int pIconId = _context.getResources().getIdentifier(UIHelper.getImageUriPokemonIcon(p, form.getType()), "drawable", _context.getPackageName());
        if (pIconId == 0)
            pIconId = _context.getResources().getIdentifier("p000", "drawable", _context.getPackageName());
        pkmnIcon.setImageResource(pIconId);
        // name
        TextView memberName = (TextView) tile.getChildAt(1);
        memberName.setText(UIHelper.getPokemoFormDisplayName(form, _context, true));
        // change font size if text very long
        if (memberName.getText().length() >= TEXT_SHRINK_THRESHOLD_CHARS) {
            memberName.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        }
        // Background according to Properties
        updateBackgroundWithProperties(m, tile);
        tile.setTag(m);
    }

    private void updateBackgroundWithProperties(final TeamMember member, ViewGroup tile) {
        List<PProperty> properties = dataProvider.getDistinctProperties(member);
        final PProperty primaryProperty = properties.get(member.getPrimaryPropertyIndex());
        Collections.sort(properties, new Comparator<PProperty>() {
            @Override
            public int compare(PProperty o1, PProperty o2) {
                return o1 == primaryProperty ? -1 : o2 == primaryProperty ? 1 : 0;
            }
        });

        // Properties stacked
        View propertiesView = tile.findViewById(R.id.teamMemberProperties);
        UIHelper.drawPropertyLayers(_context, propertiesView, properties);

        // Background
        String bgImgName = String.format("team_member_bg_%s", primaryProperty.name().toLowerCase());
        LayerDrawable layerDrawableBg = (LayerDrawable) ContextCompat.getDrawable(_context, R.drawable.shape_team_member_bg);
        layerDrawableBg.mutate();
        int resIdNewDrawable = _context.getResources().getIdentifier(bgImgName, "drawable", _context.getPackageName());
        BitmapDrawable d = (BitmapDrawable) ResourcesCompat.getDrawable(_context.getResources(), resIdNewDrawable == 0 ? R.drawable.team_member_bg_normal : resIdNewDrawable, null);
        layerDrawableBg.setDrawableByLayerId(R.id.shapeTeamMemberBgColorItem, d);
        // set drawables
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tile.setBackground(layerDrawableBg);
        } else {
            tile.setBackgroundDrawable(layerDrawableBg);
        }
    }

    public View getView(TeamMember member) {
        return viewCache.get(member);
    }

}
