package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.fragment.app.FragmentManager;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.listener.OnPokemonClickListener;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class RandomPokemonViewFlipperAdapter extends BaseAdapter {

    private final FragmentManager fragmentManager;
    private final Context mContext;
    private final DataProvider dataProvider;

    private Pokemon mCurrent;
    private Pokemon mSaved;

    private AtomicInteger recentPokemonRead = new AtomicInteger(0);

    public RandomPokemonViewFlipperAdapter(Context context, FragmentManager fragmentManager, DataProvider dataProvider) {
        this.mContext = context;
        this.fragmentManager = fragmentManager;
        this.dataProvider = dataProvider;
    }

    @Override
    public int getCount() {
        return dataProvider.getPokemonEntriesCount();
    }

    @Override
    public Object getItem(int position) {
        mSaved = null;
        mCurrent = dataProvider.getRandomPokemon();
        recentPokemonRead.set(0);
        return mCurrent;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.random_pokemon_tile, parent, false);
        }

        Pokemon p;
        if (mSaved != null) {
            p = mSaved;
            // workaround: after second run through we have to purge "mSaved" or else we're stuck forever with it
            if (recentPokemonRead.getAndIncrement() >= 2) {
                p = (Pokemon) getItem(position);
            }
        } else {
            p = (Pokemon) getItem(position);
        }

        // Avoid NPE in next statement;
        if (p == null) {
            p = (Pokemon) getItem(position);
        }

        // listener for details
        OnPokemonClickListener listener = new OnPokemonClickListener(fragmentManager, p.getId(), -1, true){
            @Override
            public void onClick(View view) {
                mSaved = dataProvider.getById((Integer) view.getTag());
                super.onClick(view);
            }
        };

        ImageView iv = convertView.findViewById(R.id.randomPokemonIcon);
        List<Variant> forms = dataProvider.getForms(p.getId());
        int artworkResId = UIHelper.getArtworkResource(mContext, p, CollectionUtils.isNotEmpty(forms) ? forms.get(0) : null);
        String uri = "drawable://" + artworkResId;
        ImageLoader.getInstance().displayImage(uri, iv);
        iv.setOnClickListener(listener);
        iv.setTag(p.getId());

        // ID
        TextView textId = convertView.findViewById(R.id.randomPokemonNo);
        textId.setText(String.format(Locale.getDefault(), "# %03d", p.getId()));

        // Name
        TextView textName = convertView.findViewById(R.id.randomPokemonName);
        textName.setText(p.getName());
        textName.setOnClickListener(listener);
        textName.setTag(p.getId());

        // Properties
        LinearLayout llprops = convertView.findViewById(R.id.randomPokemonProperties);
        PProperty[] props = p.getProperties();
        ImageView propertyIcon1 = (ImageView) llprops.getChildAt(0);
        ImageView propertyIcon2 = (ImageView) llprops.getChildAt(1);
        UIHelper.makePropertyIcon(mContext, propertyIcon1, props[0]);
        if (ArrayUtils.getLength(props) > 1 && props[1] != null) {
            UIHelper.makePropertyIcon(mContext, propertyIcon2, props[1]);
            propertyIcon2.setVisibility(View.VISIBLE);
        } else {
            propertyIcon2.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    public int getCurrentId() {
        if (mCurrent == null)
            getItem(0);
        return mCurrent.getId();
    }

    public void setSaved(int pId) {
        this.mSaved = dataProvider.getById(pId);
    }
}
