package de.digitalpenguin.pokeapp.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.fragments.BrowsePokemonAllFragment;
import de.digitalpenguin.pokeapp.fragments.BrowsePokemonByGenerationFragment;
import de.digitalpenguin.pokeapp.fragments.BrowsePokemonByTypeFragment;

public class BrowsePokemonFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private int numOfTabs = 3;

    private final Team team;
    private final TeamMember editMember;
    private Game game = null;

    public BrowsePokemonFragmentPagerAdapter(FragmentManager fm, Team team, TeamMember editMember) {
        super(fm);
        this.team = team;
        this.editMember = editMember;
        if (this.team != null && this.team.getGame() != null) {
            this.game = team.getGame();
            this.numOfTabs = 2;
        }
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f;
        if (this.game != null) {
            switch (position) {
                case 0:
                    f = new BrowsePokemonAllFragment();
                    break;
                case 1:
                    f = new BrowsePokemonByTypeFragment();
                    break;
                default:
                    return null;
            }
        } else {
            switch (position) {
                case 0:
                    f = new BrowsePokemonAllFragment();
                    break;
                case 1:
                    f = new BrowsePokemonByGenerationFragment();
                    break;
                case 2:
                    f = new BrowsePokemonByTypeFragment();
                    break;
                default:
                    return null;
            }
        }
        if (team != null) {
            Bundle b = new Bundle();
            b.putString(Team.PARAM_ID, team.getId());
            b.putSerializable(TeamMember.PARAM_ID, editMember);
            if (this.game != null)
                b.putString(Team.PARAM_GAME, this.game.getId());
            f.setArguments(b);
        }
        return f;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
