package de.digitalpenguin.pokeapp.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.fragments.MovesLearnedCategoryFragment;

public class PokemonMoveCategoryPagerAdapter extends FragmentStatePagerAdapter {

    private final Pokemon pokemon;

    private final String vaiantTypeStr;

    private final Integer generation;

    private String gamesKey;

    public PokemonMoveCategoryPagerAdapter(FragmentManager fm, Pokemon p, String variantTypeStr, Integer generation, String gamesKey) {
        super(fm);
        this.pokemon = p;
        this.vaiantTypeStr = variantTypeStr;
        this.generation = generation;
        this.gamesKey = gamesKey;
    }

    @Override
    public Fragment getItem(int position) {

        MovesLearnedCategoryFragment fragment = new MovesLearnedCategoryFragment();
        Bundle args = new Bundle();
        args.putInt(Pokemon.PARAM_ID, pokemon.getId());
        args.putString(PokemonData.PARAM_FORM, vaiantTypeStr);
        args.putInt("generation", generation);
        args.putString("gamesKey", gamesKey);
        String triggerName = null;
        switch(position) {
            case 0:
                triggerName = MoveLearnTrigger.LEVELUP.name();
                break;
            case 1:
                triggerName = MoveLearnTrigger.MACHINE.name();
                break;
            case 2:
                triggerName = MoveLearnTrigger.BREEDING.name();
                break;
            case 3:
                triggerName = MoveLearnTrigger.TUTOR.name();
                break;
        }
        args.putString("category", triggerName);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
