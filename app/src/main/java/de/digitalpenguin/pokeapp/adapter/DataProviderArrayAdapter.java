package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public abstract class DataProviderArrayAdapter<T> extends ArrayAdapter {

    private DataProvider dataProvider;

    public DataProviderArrayAdapter(@NonNull Context context, int resource, List<T> entries, DataProvider dataProvider) {
        super(context, resource, entries);
        this.dataProvider = dataProvider;
    }

    protected DataProvider getDataProvider() {
        return dataProvider;
    }
}
