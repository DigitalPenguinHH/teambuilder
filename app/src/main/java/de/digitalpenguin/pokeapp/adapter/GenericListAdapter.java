package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class GenericListAdapter<T> extends ArrayAdapter<T> {

    protected LayoutInflater inflater;

    private final int layoutId;

    public GenericListAdapter(Context context, List<T> entries, int layoutId) {
        super(context, 0, entries);
        this.inflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
    }

    protected abstract void initView(T item, View view);

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        T item = getItem(position);
        convertView = inflater.inflate(layoutId, parent, false);
        initView(item, convertView);
        return convertView;
    }

    public T getItemAtPosition(int postition) {
        return getItem(postition);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

}
