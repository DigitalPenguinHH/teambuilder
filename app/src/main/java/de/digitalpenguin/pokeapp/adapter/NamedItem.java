package de.digitalpenguin.pokeapp.adapter;

import de.digitalpenguin.pokeapp.data.util.Named;

public class NamedItem<T extends Named> extends ListItem {

    private String name;

    public NamedItem(T object) {
        super(object);
        setName(object.getName());
    }

    public NamedItem(String name, T object) {
        super(object);
        setName(name);
    }

    public NamedItem(String name) {
        super(null);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getObject() {
        return (T) getData();
    }

}
