package de.digitalpenguin.pokeapp.adapter;

public class ListItem<T> {

    public static final ListItem EMPTY_ITEM = new ListItem<Object>((Object) null);

    private final T data;

    public ListItem(T itemData) {
        this.data = itemData;
    }

    public T getData() {
        return data;
    }

    public boolean isEmptyItem() {
        return EMPTY_ITEM == this;
    }

}
