package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.StatType;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.fragments.BrowsePokemonAllFragment;
import de.digitalpenguin.pokeapp.util.LexicalResultRange;
import de.digitalpenguin.pokeapp.util.PokemonSearchResultLexicalInfo;
import de.digitalpenguin.pokeapp.util.TextDecorator;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class GenericSearchResultListAdapter extends RecyclerView.Adapter<GenericSearchResultListAdapter.MyViewHolder> {

    private static final String DEF_TYPE_STRING = "string";

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    private final Context context;

    private final AdapterView.OnItemClickListener listener;

    private final DataProvider dataProvider;

    private String queryString;

    private Boolean showId = Boolean.TRUE;

    private List<Object> dataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public int type;
        public MyViewHolder(View v) {
            super(v);
            this.view = v;
            this.type = TYPE_ITEM;
        }
        public MyViewHolder(View v, int type) {
            super(v);
            this.view = v;
            this.type = type;
        }
    }

    public GenericSearchResultListAdapter(Context context, DataProvider dataProvider, List<Object> entries, AdapterView.OnItemClickListener listener) {
        this.context = context;
        this.dataProvider = dataProvider;
        this.dataset = entries;
        this.listener = listener;
    }

    public GenericSearchResultListAdapter(Context context, DataProvider dataProvider, List<Pokemon> entries, BrowsePokemonAllFragment listener) {
        this.context = context;
        this.dataProvider = dataProvider;
        setRawEntries(entries);
        this.listener = listener;
    }

    public void setQueryString(String str) {
        this.queryString = str;
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    private enum ViewItemType {

        STRING(R.layout.search_result_tile_groupheader),
        POKEMON(R.layout.search_result_tile_pokemon),
        POKEMONFORM(R.layout.search_result_tile_pokemon),
        MOVE(R.layout.movelearned_lvlup_tile),
        ITEM(R.layout.item_tile),
        NATURE(R.layout.nature_tile),
        CHARACTERISTIC(R.layout.characteristic_tile),
        ABILITY(R.layout.ability_list_tile);

        private int layoutResourceId;

        ViewItemType(int layoutResourceId) {
            this.layoutResourceId = layoutResourceId;
        }
    }

    public Object getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        Object o = dataset.get(position);
        ViewItemType type = ViewItemType.valueOf(o.getClass().getSimpleName().toUpperCase());
        return type.ordinal();
    }

    @NonNull
    @Override
    public GenericSearchResultListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewItemType viewTypeEnum = ViewItemType.values()[viewType];
        View v = layoutInflater.inflate(viewTypeEnum.layoutResourceId, parent, false);
        final GenericSearchResultListAdapter.MyViewHolder vh = new GenericSearchResultListAdapter.MyViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(null, v, vh.getLayoutPosition(), 0L);
            }
        });
        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(context.getResources().getColor(R.color.buttonClickedBG));
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        v.setBackgroundColor(context.getResources().getColor(R.color.defaultBackground));
                        break;
                }
                // allow target view to handle click
                return false;
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull GenericSearchResultListAdapter.MyViewHolder holder, int position) {
        Object o = dataset.get(position);
        Resources resources = context.getResources();
        if (o instanceof String) {
            updateForGroupHeader(holder.view, resources, (String) o);
        } else if (o instanceof Pokemon) {
            updateForPokemon(holder.view, resources, (Pokemon) o);
        } else if (o instanceof PokemonForm) {
            updateForPokemon(holder.view, resources, (PokemonForm) o);
        } else if (o instanceof Nature) {
            updateForNature(holder.view, resources, (Nature) o);
        } else if (o instanceof Characteristic) {
            updateForCharacteristic(holder.view, resources, (Characteristic) o);
        } else if (o instanceof Item) {
            updateForItem(holder.view, resources, (Item) o);
        } else if (o instanceof Move) {
            updateForMove(holder.view, resources, (Move) o);
        } else if (o instanceof Ability) {
            updateForAbility(holder.view, resources, (Ability) o);
        }
        // bg color
        if (position%2 == 0) {
            holder.view.setBackgroundColor(resources.getColor(R.color.colorSearchResultDarker));
        } else {
            holder.view.setBackgroundColor(resources.getColor(R.color.defaultBackground));
        }
    }

    private void updateForGroupHeader(View tileView, Resources resources, String headerStr) {
        TextView tv = tileView.findViewById(R.id.groupHeaderText);
        tv.setText(headerStr);
    }

    @NonNull
    private void updateForPokemon(View tileView, Resources resources, Pokemon pokemon) {
        PokemonForm form = new PokemonForm();
        form.setPokemon(pokemon);
        form.setType(VariantType.DEFAULT);
        updateForPokemon(tileView, resources, form);
    }

    @NonNull
    private void updateForPokemon(View tileView, Resources resources, PokemonForm form) {
        tileView.setTag(form);
        Pokemon p = form.getPokemon();
        // set pokemon values
        TextView idText = tileView.findViewById(R.id.tilePokemonId);
        idText.setText(StringUtils.leftPad(Integer.toString(p.getId()), 3, '0'));
        if (!Boolean.TRUE.equals(showId)) {
            idText.setLayoutParams(new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0.0f
            ));
        }
        // icon
        ImageView pokemonIcon = tileView.findViewById(R.id.tilePokemonIcon);
        pokemonIcon.setImageResource(resources.getIdentifier(UIHelper.getImageUriPokemonIcon(p, form.getType()),null, context.getPackageName()));
        // text result (colored optional)
        String displayName = UIHelper.getPokemoFormDisplayName(form, context);
        updateTextHighlighted((TextView) tileView.findViewById(R.id.tileMainText), displayName);
        // property plates
        ImageView propertyIcon1 = tileView.findViewById(R.id.tileProperty1);
        PProperty[] props = form.getProperties() == null ?  p.getProperties() : form.getProperties();
        propertyIcon1.setImageResource(resources.getIdentifier(UIHelper.getImageUriPropertyIcon(props[0]),null, context.getPackageName()));
        ImageView propertyIcon2 = tileView.findViewById(R.id.tileProperty2);
        PProperty p2 = props.length > 1 && props[1] != null ? props[1] : null;
        propertyIcon2.setImageResource(resources.getIdentifier(UIHelper.getImageUriPropertyIcon(p2), null, context.getPackageName()));
    }

    private void updateTextHighlighted(TextView textView, String text) {
        Spannable spannable = new SpannableString(text);
        if (this.queryString != null) {
            PokemonSearchResultLexicalInfo resultInfo = new PokemonSearchResultLexicalInfo(this.queryString, text);
            for (LexicalResultRange range : resultInfo.getOccurrences()) {
                spannable.setSpan(new ForegroundColorSpan(range.isMatchText() ? Color.BLUE : Color.BLACK), range.getRange().getMinimum(), range.getRange().getMaximum(), 0);
            }
        }
        textView.setText(spannable, TextView.BufferType.SPANNABLE);
    }

    private void updateForItem(View tileView, Resources resources, Item i) {
        ImageView imgView = tileView.findViewById(R.id.itemTileIcon);
        int iconResource = UIHelper.getItemResourceId(context, dataProvider, i);
        imgView.setImageResource(iconResource);
        updateTextHighlighted((TextView) tileView.findViewById(R.id.itemTileText), dataProvider.getDisplayName(i));
        TextView textCategory = tileView.findViewById(R.id.itemTileCategoryText);
        if (i.getCategory() == null) {
            textCategory.setText(resources.getString(R.string.item_category_misc));
        } else {
            textCategory.setText(UIHelper.getString(context, "item_category_" + i.getCategory().name().toLowerCase()));
        }
    }

    private void updateForAbility(View tileView, Resources resources, Ability ability) {
        TextView nameView = tileView.findViewById(R.id.abilityListItemName);
        nameView.setText(ability.getName());
        nameView.setTag(ability.getId());
        tileView.setTag(ability.getId());
    }

    private class TextHighlightDecorator implements TextDecorator {

        private final String matchStr;

        private TextHighlightDecorator(String matchStr) {
            this.matchStr = matchStr;
        }

        @Override
        public void decorate(TextView textView) {
            updateTextHighlighted(textView, matchStr);
        }
    }

    private void updateForMove(View tileView, Resources resources, Move m) {
        UIHelper.updateListTileMove(context, tileView, resources, m, new TextHighlightDecorator(m.getName()));
    }

    private void updateForCharacteristic(View tileView, Resources resources, Characteristic c) {
        updateTextHighlighted((TextView) tileView.findViewById(R.id.charactersisticText), c.getName());
        TextView text = tileView.findViewById(R.id.charactersisticValue);
        text.setText(getStatName(resources, c.getHighest()));
    }

    private void updateForNature(View tileView, Resources resources, Nature n) {
        updateTextHighlighted((TextView) tileView.findViewById(R.id.natureText), n.getName());
        if (n.getDecreased() == null || n.getIncreased() == null) {
            LinearLayout viewParent = tileView.findViewById(R.id.natureValueWrapper);
            viewParent.removeAllViews();
            TextView neutralText = new TextView(context);
            neutralText.setText(R.string.nature_neutral);
            neutralText.setTextColor(resources.getColor(R.color.lightGrey));
            viewParent.addView(neutralText, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            viewParent.setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            TextView textInc = tileView.findViewById(R.id.natureIncreaseValue);
            textInc.setText(getStatName(resources, n.getIncreased()));
            TextView textDec = tileView.findViewById(R.id.natureDecreaseValue);
            textDec.setText(getStatName(resources, n.getDecreased()));
        }
    }

    public void setEntries(List entries) {
        dataset.clear();
        dataset.addAll(entries);
        notifyDataSetChanged();
    }

    public void setRawEntries(List<Pokemon> entries) {
        if (dataset != null) {
            this.dataset.clear();
        } else {
            this.dataset = new ArrayList<>(entries.size());
        }
        this.dataset.addAll(entries);
    }

    public String getQuery() {
        return this.queryString;
    }

    @NonNull
    private String getStatName(Resources resources, StatType statType) {
        return getStringResource(resources, "stat_" + statType.name().toLowerCase());
    }

    private String getStringResource(Resources resources, String key) {
        int resIdName = resources.getIdentifier(key, DEF_TYPE_STRING, context.getPackageName());
        return context.getString(resIdName);
    }

}
