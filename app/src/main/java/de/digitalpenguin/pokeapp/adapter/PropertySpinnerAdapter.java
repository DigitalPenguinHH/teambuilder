package de.digitalpenguin.pokeapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.StartupActivity;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class PropertySpinnerAdapter extends ArrayAdapter {

    private Context context;
    private LayoutInflater inflater;

    public PropertySpinnerAdapter(Context context) {
        this(context, false);
    }

    public PropertySpinnerAdapter(Context context, boolean includeAllElement) {
        super(context, 0, initItems(context, includeAllElement));
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    private static List<NamedItem<PProperty>> initItems(Context context, boolean includeAllElement) {
        List<NamedItem<PProperty>> items = new LinkedList<>();
        if (includeAllElement) {
            items.add(new NamedItem<PProperty>(context.getResources().getString(R.string.common_any)));
        }
        for (PProperty p: Arrays.asList(PProperty.values())) {
            String propName = UIHelper.getString(context, String.format("prop_%s", p.name()).toLowerCase());
            items.add(new NamedItem<>(propName, p));
        }
        return items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NamedItem<PProperty> item = (NamedItem<PProperty>) getItem(position);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_layout_default, parent, false);
        ImageView imageView = convertView.findViewById(R.id.img);
        if (item.getObject() != null) {
            int resIdIcon = context.getResources().getIdentifier(UIHelper.getImageUriPropertyIcon(item.getObject()), null, context.getPackageName());
            String uri = "drawable://" + resIdIcon;
            try {
                ImageLoader.getInstance().displayImage(uri, imageView);
            } catch (Exception e) {
                StartupActivity.initImageLoader(context);
            }
        } else {
            imageView.setImageResource(0);
        }
        TextView textView = convertView.findViewById(R.id.txt);
        textView.setText(item.getName());
        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

}
