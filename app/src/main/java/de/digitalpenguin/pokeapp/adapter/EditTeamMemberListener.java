package de.digitalpenguin.pokeapp.adapter;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import android.view.View;

import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.fragments.BrowsePokemonFragment;
import de.digitalpenguin.pokeapp.util.UIHelper;

public class EditTeamMemberListener implements View.OnClickListener {

    private final FragmentManager fragmentManager;
    private final Team team;

    public EditTeamMemberListener(FragmentManager fragmentManager, Team team) {
        this.team = team;
        this.fragmentManager = fragmentManager;
    }

    public void performEditTeamMember(TeamMember member) {
        BrowsePokemonFragment fragment = new BrowsePokemonFragment();
        Bundle args = new Bundle();
        args.putString(Team.PARAM_ID, team.getId());
        if (member != null) {
            args.putSerializable(TeamMember.PARAM_ID, member);
        }
        if (team.getGame() != null) {
            args.putString(Team.PARAM_GAME, team.getGame().getId());
            args.putInt(Region.PARAM_GENERATION, team.getGame().getGeneration());
        }
        fragment.setArguments(args);
        UIHelper.swapFragment(fragmentManager, fragment);
    }

    @Override
    public void onClick(View view) {
        TeamMember member = (TeamMember) view.getTag();
        performEditTeamMember(member);
    }
}
