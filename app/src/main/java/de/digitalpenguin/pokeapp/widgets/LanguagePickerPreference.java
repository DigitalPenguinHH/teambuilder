package de.digitalpenguin.pokeapp.widgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.R;

public class LanguagePickerPreference extends ListPreference {

    private static final String PREF_KEY_LANGUAGE = "language";

    private static final String PREFIX_FLAG_IMG = "flag_";

    private class CustomListPreferenceAdapter extends ArrayAdapter<LanguageItem> {

        private Context context;
        private List<LanguageItem> languageItems;
        private int resource;

        public CustomListPreferenceAdapter(Context context, int resource, List<LanguageItem> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.languageItems = objects;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(resource, parent, false);

                holder = new ViewHolder();
                holder.iconName = (TextView) convertView
                        .findViewById(R.id.iconName);
                holder.iconImage = (ImageView) convertView
                        .findViewById(R.id.iconImage);
                holder.radioButton = (RadioButton) convertView
                        .findViewById(R.id.iconRadio);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.iconName.setText(languageItems.get(position).name);

            int identifier = context.getResources().getIdentifier(
                    "flag_" + languageItems.get(position).locale.getLanguage(), "drawable",
                    context.getPackageName());

            holder.iconImage.setImageResource(identifier);

            holder.radioButton.setChecked(languageItems.get(position).isChecked);

            convertView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    ViewHolder holder = (ViewHolder) v.getTag();
                    for (int i = 0; i < languageItems.size(); i++) {
                        if (i == position)
                            languageItems.get(i).isChecked = true;
                        else
                            languageItems.get(i).isChecked = false;
                    }
                }
            });

            return convertView;
        }

    }

    private static class LanguageItem {

        private Locale locale;
        private boolean isChecked;
        private String name;

        LanguageItem(String name, Locale l, boolean isChecked) {
            this.name = name;
            this.locale = l;
            this.isChecked = isChecked;
        }

    }

    private static class ViewHolder {
        protected ImageView iconImage;
        protected TextView iconName;
        protected RadioButton radioButton;
    }

    private Context context;
    private ImageView icon;

    private CharSequence[] languageNames, languageValues;
    private List<LanguageItem> languageItems;
    private SharedPreferences preferences;
    private Resources resources;
    private String selectedLangCode, defaultLangCode = Locale.getDefault().getLanguage();

    public LanguagePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        resources = context.getResources();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private String getEntry(String value) {
        String[] entries = resources.getStringArray(R.array.pref_language_names);
        String[] values = resources.getStringArray(R.array.pref_language_codes);
        int index = Arrays.asList(values).indexOf(value);
        return entries[index];
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        selectedLangCode = preferences.getString(PREF_KEY_LANGUAGE, defaultLangCode);
        icon = (ImageView) holder.findViewById(R.id.iconImage);
        updateIcon();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable parcelable = super.onSaveInstanceState();
        if (languageValues != null) {
            for (int i = 0; i < languageNames.length; i++) {
                LanguageItem item = languageItems.get(i);
                if (item.isChecked) {

                    Editor editor = preferences.edit();
                    String lang = item.locale.getLanguage();
                    editor.putString(
                            PREF_KEY_LANGUAGE,
                            lang);
                    editor.commit();

                    selectedLangCode = lang;
                    updateIcon();

                    break;
                }
            }
        }
        return parcelable;
    }

    /*
    @Override
    protected void onPrepareDialogBuilder(Builder builder) {

        builder.setNegativeButton("Cancel", null);
        builder.setPositiveButton(null, null);

        languageNames = getNameEntries();
        languageValues = getEntryValues();

        if (languageNames == null || languageValues == null || languageNames.length != languageValues.length) {
            throw new IllegalStateException(
                    "ListPreference requires an entries array and an entryValues array which are both the same length");
        }

        String selectedLangCode = preferences.getStringFirstTag(PREF_KEY_LANGUAGE, Locale.getDefault().getLanguage());

        languageItems = new ArrayList<LanguageItem>();

        for (int i = 0; i < languageNames.length; i++) {
            boolean isSelected = selectedLangCode.equals(languageValues[i]) ? true : false;
            LanguageItem item = new LanguageItem(languageNames[i].toString(), new Locale(languageValues[i].toString()), isSelected);
            languageItems.add(item);
        }

        CustomListPreferenceAdapter customListPreferenceAdapter = new CustomListPreferenceAdapter(
                context, R.layout.language_picker, languageItems);
        builder.setAdapter(customListPreferenceAdapter, null);

    }
    */

    private void updateIcon() {
        int identifier = resources.getIdentifier(PREFIX_FLAG_IMG + selectedLangCode, "drawable", context.getPackageName());
        icon.setImageResource(identifier);
        icon.setTag(selectedLangCode);
    }

}