package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

import de.digitalpenguin.pokeapp.data.util.Named;

public class QueryParam implements Named {

    private String name;

    private String value;

    public static QueryParam of(String name, Integer value) {
        return new QueryParam(name, Integer.toString(value));
    }

    public static QueryParam of(String name, Enum<?> value) {
        return new QueryParam(name, value.name());
    }

    public static QueryParam of(String name, String value) {
        return new QueryParam(name, value);
    }

    public static QueryParam of(String name, Object value) {
        return new QueryParam(name, value.toString());
    }

    private QueryParam(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "QueryParam{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryParam that = (QueryParam) o;
        return new EqualsBuilder()
                .append(name, that.name)
                .append(value, that.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(value)
                .toHashCode();
    }
}
