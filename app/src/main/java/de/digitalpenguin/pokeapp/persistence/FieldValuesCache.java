package de.digitalpenguin.pokeapp.persistence;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.util.Identifiable;

public class FieldValuesCache {

    private Map<String, Object> values = new HashMap<>();

    public void add(Field field, Identifiable value) {
        values.put(keyFor(field.getDeclaringClass(), value), value);
    }

    public void add(Collection<Field> fields, Identifiable value) {
        for (Field field : fields) {
            add(field, value);
        }
    }

    private static String keyFor(Class<?> clazz, Identifiable value) {
        return clazz.getName().concat("#").concat(value.getIdentifier().toString());
    }

    public void reset() {
        this.values.clear();
    }

    public Object getValue(Class<?> clazz, Identifiable id) {
        return this.values.get(keyFor(clazz, id));
    }
}
