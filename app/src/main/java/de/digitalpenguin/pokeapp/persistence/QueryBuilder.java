package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.Named;

/**
 * Builder helps with constructing simple SQLite Queries to be used with an {@link android.database.sqlite.SQLiteDatabase}.
 * 
 * @author ChioStellar
 *
 * @param <T> The root Entity class
 */
public class QueryBuilder<T extends Serializable> {

	private static final String DEFAULT_ALIAS_O = "o";
	private String DEFAULT_ALIAS = "o";
	private String DEFAULT_ALIAS_WITH;

	private static final String PARAM_PREFIX = "p";

	private static final String SQL_SELECT = "SELECT ";
	private static final String SQL_FROM = " FROM ";

	public static final String ANON_PARAM = "?";

	private enum Comparator {
		
		GT(">"), LT("<"), EQ("="), GTE(">="), LTE("<="), IS("IS"), NOT("IS NOT");
		
		private String symbol;

		private Comparator(String symbol) {
			this.symbol = symbol;
		}

		public String symbol() {
			return symbol;
		}		
	}
	
	private enum LikeVariant {		
		BOTH, START, END		
	}
	
	private static class OrderBy {
		
		private final String field;
		
		private final boolean descending;
		
		private final boolean caseSensitive;
		
		private OrderBy(String field, boolean descending) {
			this(field, descending, true);
		}
		
		private OrderBy(String field, boolean descending, boolean caseSensitive) {
			this.field = field;
			this.descending = descending;
			this.caseSensitive = caseSensitive;
		}	
	}

	private Class<T> type;
	
	private final StringBuffer sb;

	private Integer maxResults;
	
	private List<OrderBy> orderBy = new LinkedList<>();
	
	private int paramCounter = 0;
	
	private List<QueryParam> paramsList = new LinkedList<>();
	
	/**
	 * Creates a new Query Builder which is going to return entities of <b>type</b>.
	 * @param <T> thre result Type
	 * @param type the Class which is being queried from
	 * @return {@link List} of entities
	 */
	public static <T extends Serializable> QueryBuilder<T> forType(Class<T> type) {
		return new QueryBuilder<T>(type, DEFAULT_ALIAS_O);
	}

	/**
	 * Creates a new Query Builder which is going to return entities of <b>type</b>.
	 * @param <T> thre result Type
	 * @param type the Class which is being queried from
	 * @return {@link List} of entities
	 */
	public static <T extends Serializable> QueryBuilder<T> forType(Class<T> type, String alias) {
		return new QueryBuilder<T>(type, alias);
	}
	 
	private QueryBuilder(Class<T> type, String alias) {
		this.DEFAULT_ALIAS = alias;
		this.DEFAULT_ALIAS_WITH = this.DEFAULT_ALIAS.concat(".");
		this.type = type;
		sb = new StringBuffer(SQL_SELECT);
		sb.append(DEFAULT_ALIAS_WITH.concat("*"));
		sb.append(SQL_FROM);
		sb.append(type.isAnnotationPresent(Table.class) ? type.getAnnotation(Table.class).name() : type.getSimpleName().toUpperCase());
		sb.append(" AS ");
		sb.append(DEFAULT_ALIAS);
		sb.append(" WHERE 1=1");
	}

	/**
	 * Select for distinct entities.
	 * <p>Results in Query:</p>
	 * <p><code>SELECT DISTINCT o FROM Entity o ...</code>
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> distinct() {
		sb.replace(0, sb.indexOf(SQL_FROM), SQL_SELECT.concat("DISTINCT ").concat(DEFAULT_ALIAS_WITH).concat("*"));
		return this;
	}

	/**
	 * Add an EQUALS clause.
	 * <p>Results in Query:</p>
	 * <p><code>... AND field = :field</code></p>
	 * <p><i>value</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> eq(String field, Object value) {
		return andCompare(field, value, Comparator.EQ);
	}
	
	/**
	 * Add a "Less Than" clause.
	 * <p>Results in Query:</p>
	 * <p><code>... AND field > :field</code></p>
	 * <p><i>value</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> lt(String field, Object value) {
		return andCompare(field, value, Comparator.LT);
	}

	/**
	 * Add a "Less Than or Equals" clause.
	 * <p>Results in Query:</p>
	 * <p><code>... AND field <= :field</code></p>
	 * <p><i>value</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> lte(String field, Object value) {
		return andCompare(field, value, Comparator.LTE);
	}

	/**
	 * Add a "Greater Than" clause.
	 * <p>Results in Query:</p>
	 * <p><code>... AND field > :field</code></p>
	 * <p><i>value</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> gt(String field, Object value) {
		return andCompare(field, value, Comparator.GT);
	}
	
	/**
	 * Add a "Less Than or Equals" clause <b>ONLY IF</b> <i>condition</i> evaluates to <code>true</code>.
	 * <p><code>... AND field >= :field</code></p>
	 * <p><i>value</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> gte(String field, Object value) {
		return andCompare(field, value, Comparator.GTE);
	}
	
	private QueryBuilder<T> andCompare(String field, Object value, Comparator comp) {
		and();
		sb.append(clause(field, value, comp, false));
		return this;
	}
	
	/**
	 * Append a LIKE clause <b>ONLY IF</b> <i>condition</i> evaluates to <code>true</code>.
	 * <p><code>... AND field IN (:elements)</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> in(String field, Collection<?> elements) {
		if (elements == null || elements.isEmpty())
			return this;
		and();
		sb.append(inClause(field, elements, false));
		return this;
	}

	/**
	 * Append a LIKE clause which is case insensitive.
	 * <p>Results in Query:</p>
	 * <p><code>... AND lower(field) LIKE lower('%value%')</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> likeIgnoreCase(String field, String value) {
		return like(field, value, true, LikeVariant.BOTH);
	}

	/**
	 * Append a LIKE clause.
	 * <p><code>... AND field LIKE '%value%'</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> like(String field, String value) {
		return like(field, value, false, LikeVariant.BOTH);
	}

	/**
	 * Append a LIKE clause.
	 * <p><code>... AND field LIKE '%value%'</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 *
	 * @return this Builder
	 */
	public QueryBuilder<T> like(String field, Named named) {
		return like(field, named.getName());
	}
	
	private QueryBuilder<T> like(String field, String value, boolean ignoreCase, LikeVariant v) {
		if (ignoreCase) {
			value = value.toLowerCase();
			sb.append(String.format(" AND lower(%s%s)", DEFAULT_ALIAS_WITH, field));
		} else {
			sb.append(String.format(" AND %s%s", DEFAULT_ALIAS_WITH, field));
		}
		sb.append(" ");
		sb.append("LIKE");
		sb.append(" '");
		String likeVal;
		if (v == LikeVariant.START) {
			likeVal = value.concat("%'");
		} else if (v == LikeVariant.END) {
			likeVal = "%".concat(value).concat("'");
		} else {
			likeVal = "%".concat(value).concat("%'");
		}
		sb.append(likeVal);
		return this; 
	}

	/**
	 * Append a LIKE clause (Open End).
	 * <p>Results in Query:</p>
	 * <p><code>... AND field LIKE 'value%'</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> endsWith(String field, String value) {
		return like(field, value, false, LikeVariant.END);
	}

	/**
	 * Append a LIKE clause (Open End) which is case insensitive.
	 * <p>Results in Query:</p>
	 * <p><code>... AND lower(field) LIKE lower('value%')</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> endsWithIgnoreCase(String field, String value) {
		return like(field, value, true, LikeVariant.END);
	}

	/**
	 * Append a LIKE clause (Open Start).
	 * <p>Results in Query:</p>
	 * <p><code>... AND field LIKE 'value%'</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> startsWith(String field, String value) {
		return like(field, value, false, LikeVariant.START);
	}

	/**
	 * Append a LIKE clause (Open Start) which is case insensitive.
	 * <p>Results in Query:</p>
	 * <p><code>... AND lower(field) LIKE lower('value%')</code></p>
	 * <p><i>elements</i> will be replaced by {@link QueryParam} at runtime.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> startsWithIgnoreCase(String field, String value) {
		return like(field, value, true, LikeVariant.START);
	}
	
	private String op(Comparator comparator) {
		return comparator.symbol();
	}

	/**
	 * Append an IS NULL clause.
	 * <p>
	 * <code>... AND field IS NULL</code>
	 * 
	 * @param field field which must be null
	 * 
	 * @return this Builder instance
	 */
	public QueryBuilder<T> isNull(String field) {
		and();
		sb.append(clause(field, null, Comparator.IS, false));
		return this;
	}

	/**
	 * Append an IS NOT NULL clause.
	 * <p>
	 * <code>... AND field IS NOT NULL</code>
	 * 
	 * @param field field which must not be null
	 * 
	 * @return this Builder instance
	 */
	public QueryBuilder<T> isNotNull(String field) {
		and();
		sb.append(clause(field, null, Comparator.NOT, false));
		return this;
	}
	
	/**
	 * Set a limit of how many entities should be returned
	 * <p>
	 * <code>.. AND LIMIT maxResults</code>
	 * 
	 * @param maxResults the entity limit
	 * 
	 * @return this Builder instance
	 */
	public QueryBuilder<T> limit(int maxResults) {
		this.maxResults = maxResults;
		return this;
	}
	
	/**
	 * Apply ordering of results (Ascending).<br/>
	 * Order is based on <i>fields</i>
	 * <p>
	 * <code>... ORDER BY field1, field2, ... ASC</code>
	 * 
	 * @param fields the field names which decide the order
	 * 
	 * @return this Builder instance
	 */
	public QueryBuilder<T> orderBy(String ... fields) {
		return orderBy(false, fields);
	}
	
	/**
	 * Apply ordering of results (Descending).<br/>
	 * Order is based on <i>fields</i>
	 * <p>
	 * <code>... ORDER BY field1, field2, ... DESC</code>
	 * 
	 * @param fields the field names which decide the order
	 * 
	 * @return this Builder instance
	 */
	public QueryBuilder<T> orderByDescending(String ... fields) {
		return orderBy(true, fields);
	}
	
	private QueryBuilder<T> orderBy(boolean descending, String ... fields) {
		for (String field : fields) {
			this.orderBy.add(new OrderBy(field, descending));
		}
		return this;
	}

	private void and() {
		sb.append(" AND ");
	}
	
	/**
	 * @return the result Query String.
	 */
	public String getQueryString() {
		String queryStr = sb.toString();
		queryStr = appendLimit(queryStr);
		queryStr = appendOrderBy(queryStr);
		return queryStr;
	}

	private String appendLimit(String queryStr) {
		if (maxResults != null)
			queryStr = queryStr.concat(String.format(" LIMIT %d", maxResults));
		return queryStr;
	}

	private String appendOrderBy(String queryStr) {
		if (orderBy.isEmpty())
			return queryStr;
		queryStr = queryStr.concat(" ORDER BY ");
		queryStr = queryStr.concat(StringUtils.join(CollectionUtils.collect(this.orderBy, new Transformer() {
			@Override
			public Object transform(Object input) {
				OrderBy o = (OrderBy) input;
				return String.format(o.caseSensitive ? "%s%s" : "lower(%s%s)", DEFAULT_ALIAS_WITH, o.field);
			}
		}), ", "));
		queryStr = queryStr.concat(this.orderBy.iterator().next().descending ? " DESC" : " ASC");
		return queryStr;
	}

	/**
	 * Append an OR clause with EQUALS compare.
	 * <p>
	 * <code>... AND (field1 = value OR field2 = value OR ...)</code>
	 *
	 * @param value value to compare fields with
	 * @param fields fields which should be included in the OR comparison
	 *
	 * @return this Builder instance
	 */
	public QueryBuilder<T> orEq(final Object value, String ... fields) {
		String OR_clauses = StringUtils.join(CollectionUtils.collect(Arrays.asList(fields), new Transformer() {
			@Override
			public Object transform(Object input) {
				String field = (String) input;
				return clause(field, value, Comparator.EQ, true);
			}
		}), " OR ");
		sb.append(String.format(" AND (%s)", OR_clauses));
		return this;
	}

	/**
	 * Append an EXISTS Clause
	 *
	 * @param clause the clause (as in ... <code>AND EXISTS (<i>clause</i>)</code>)
	 *
	 * @return this Builder instance
	 */
	public QueryBuilder<T> addExists(String clause) {
		and();
		sb.append(String.format("EXISTS (%s)", clause));
		return this;
	}
	
	private String clause(String field, Object value, Comparator c, boolean sameParam) {
		StringBuffer b = new StringBuffer();
		b.append(DEFAULT_ALIAS_WITH);
		b.append(field);
		b.append(" ");
		b.append(value != null || c == Comparator.NOT ? op(c) : op(Comparator.IS));
		b.append(" ");
		b.append(sameParam ? getParam(value) : nextParam(value));
		return b.toString();		
	}
	
	private String inClause(String field, Collection<?> elements, boolean sameParam) {
		StringBuffer b = new StringBuffer();
		b.append(DEFAULT_ALIAS_WITH);
		b.append(field);
		b.append(" IN ");
		b.append("(");
		b.append(sameParam ? getParam(elements) : nextParam(elements));
		b.append(")");
		return b.toString();
	}
	
	private String getParam(Object value) {
		if (value == null)
			return "null";
		paramsList.add(QueryParam.of(ANON_PARAM, value));
		return ANON_PARAM;
	}

	private String nextParam(Object value) {
		String p = getParam(value);
		if (p != "null")
			paramCounter++;
		return p;
	}

	/**
	 * Get current {@link QueryParam}s associated with this builder.
	 * 
	 * @return {@link List} of {@link QueryParam} objects
	 */
	public List<QueryParam> getParameterList() {
		return Collections.unmodifiableList(paramsList);
	}

	public String[] getParametersArray() {
		String[] arr = new String[paramsList.size()];
		int i = 0;
		for (QueryParam p : paramsList) {
			arr[i] = p.getValue();
			i++;
		}
		return arr;
	}
	
	/**
	 * Append a id = :value clause.
	 * 
	 * @param id the id-value
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> withId(Object id) {
		return eq("id", id);
	}
	
	/**
	 * Make the result ordered by "name" field.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> orderByName() {
		return orderByName(true);
	}
	
	/**
	 * Make the result ordered by cas insensitive "name" field.
	 * 
	 * @return this Builder
	 */
	public QueryBuilder<T> orderByName(boolean caseSensitive) {
		this.orderBy.add(new OrderBy("name", false, caseSensitive));
		return this;
	}
	
	/**
	 * Returns the Query String.
	 */
	@Override
	public String toString() {
		return getQueryString();
	}

}
