package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class ClassMetaData {

    public static final String ID = "id";

    private static Map<String, PropertyConverter<?,?>> converterCache = new HashMap<>();

    private Class<?> clazz;

    private Map<String, Field> columnNamesFields;
    private Map<String, Class<?>> columnNameToFieldTypes;

    private String tableName;
    private String idColumnName;
    private String idFieldName;

    public ClassMetaData(Class<?> clazz) {
        this.clazz = clazz;
        this.tableName = this.getTableName();
        this.columnNamesFields = this.getRelevantFieldsWithColumns(this.clazz);
        setIdColumnAnFieldName(this.columnNamesFields.values());
        this.columnNameToFieldTypes = this.getFieldTypesMap();
    }

    public Map<String, Field> getRelevantFieldsWithColumns(Class<?> type) {
        Map<String, Field> fields = new HashMap<>();
        for (Field field : getAllFields(type)) {
            if (isIgnoreField(field))
                continue;
            if (field.isAnnotationPresent(OneToMany.class))
                continue;
            if (field.isAnnotationPresent(OneToOne.class) || field.isAnnotationPresent(ManyToOne.class)) {
                if (field.isAnnotationPresent(JoinColumn.class)) {
                    fields.put(field.getAnnotation(JoinColumn.class).name(), field);
                } else if (isEmpty(getMappedBy(field))) {
                    fields.put(getColumnName(field), field);
                }
            } else {
                fields.put(getColumnName(field), field);
            }
        }
        return fields;
    }

    private boolean isIgnoreField(Field field) {
        if (field.isAnnotationPresent(Transient.class))
            return true;
        if (Modifier.isStatic(field.getModifiers()))
            return true;
        if (field.getName().indexOf('$') >= 0)
            return true;
        return false;
    }

    private Map<String, Class<?>> getFieldTypesMap() {
        Map<String, Class<?>> mapping = new HashMap<>();
        for (Field field : this.columnNamesFields.values()) {
            mapping.put(getColumnName(field), field.getType());
        }
        return mapping;
    }

    private void setIdColumnAnFieldName(Collection<Field> fields) {
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                this.idFieldName = field.getName();
                this.idColumnName = getColumnName(field);
                break;
            }
        }
        this.idFieldName = ID;
        this.idColumnName = ID;
    }

    public static String getIdColumnName(Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Id.class)) {
                return getColumnName(field);
            }
        }
        return null;
    }

    private static String getColumnName(Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            String name = field.getName().toLowerCase();
            return StringUtils.defaultString(StringUtils.trimToNull(field.getAnnotation(Column.class).name()), name);
        } else if (field.isAnnotationPresent(JoinColumn.class)) {
            String name = field.getName().toLowerCase();
            return StringUtils.defaultString(StringUtils.trimToNull(field.getAnnotation(JoinColumn.class).name()), name);
        } else {
            return field.getName();
        }
    }

    public String getTableName() {
        if (this.tableName == null) {
            this.tableName = clazz.getSimpleName().toUpperCase();
            if (clazz.isAnnotationPresent(Table.class))
                this.tableName = clazz.getAnnotation(Table.class).name();
        }
        return tableName;
    }

    public String getMappedBy(Field field) {
        if (field.isAnnotationPresent(OneToMany.class))
            return field.getAnnotation(OneToMany.class).mappedBy();
        if (field.isAnnotationPresent(OneToOne.class))
            return field.getAnnotation(OneToOne.class).mappedBy();
        return null;
    }

    public Collection<Field> getRelatedMappedByFields() {
        Collection<Field> result = new ArrayList<>(3);
        for (Field field : this.clazz.getDeclaredFields()) {
            if (isIgnoreField(field))
                continue;
            String mappedBy = getMappedBy(field);
            if (isEmpty(mappedBy))
                continue;
            result.add(field);
        }
        return result;
    }

    public Iterable<? extends Field> getSimpleOneToOneRelationFields() {
        Collection<Field> result = new ArrayList<>(3);
        for (Field field : this.clazz.getDeclaredFields()) {
            if (isIgnoreField(field))
                continue;
            if (field.isAnnotationPresent(OneToOne.class)) {
                OneToOne annot = field.getAnnotation(OneToOne.class);
                if (isEmpty(annot.mappedBy()))
                    result.add(field);
            }
        }
        return result;
    }

    public static Object extractValue(Field field, Object fromObject) {
        try {
            Object v = PropertyUtils.getProperty(fromObject, field.getName());
            if (field.isAnnotationPresent(Converter.class)) {
                PropertyConverter<?, ?> c = getPropertyConverter(field);
                v = c.convert(v);
            } else if ((field.isAnnotationPresent(OneToOne.class) && isNotEmpty(field.getAnnotation(OneToOne.class).mappedBy()))
                    || (field.isAnnotationPresent(ManyToOne.class))) {
                v = PropertyUtils.getProperty(v, ClassMetaData.getIdColumnName(v.getClass()));
            }
            return v;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PropertyConverter<?, ?> getPropertyConverter(Field field) throws IllegalAccessException, InstantiationException {
        Class<? extends PropertyConverter> converterType = field.getAnnotation(Converter.class).converterClass();
        PropertyConverter<?, ?> c = converterCache.get(converterType.getName());
        if (c == null) {
            c = converterType.newInstance();
            converterCache.put(converterType.getName(), c);
        }
        return c;
    }

    private static List<Field> getAllFields(Class<?> type) {
        return getAllFields(new ArrayList<Field>(12), type);
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    public Class<?> getSubject() {
        return clazz;
    }

    public String getIdColumnName() {
        return idColumnName;
    }

    public String getIdFieldName() {
        return idFieldName;
    }

    public Set<String> getSelectableColumnNames() {
        return this.columnNamesFields.keySet();
    }

    public Field getFieldByColumnName(String columnName) {
        return this.columnNamesFields.get(columnName);
    }

    public Class<?> getFieldTypeByColumnName(String columnName) {
        return this.columnNameToFieldTypes.get(columnName);
    }

    public Map<String, Field> getColumnNamesFields() {
        return columnNamesFields;
    }
}
