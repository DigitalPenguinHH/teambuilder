package de.digitalpenguin.pokeapp.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.lang3.time.StopWatch;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import de.digitalpenguin.pokeapp.data.DatabaseOpenHelper;
import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.util.ILogger;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class PersistenceManager {

    private static final Integer ONE = 1;

    private boolean cacheDisabled = false;

    private Context ctx;

    private SQLiteDatabase db;

    private Map<Class<? extends Identifiable>, Map<Serializable, Object>> cache = new HashMap<>(5);

    private Map<Class<?>, ClassMetaData> metaModelCache = new HashMap<>(20);

    private ILogger logger;

    public PersistenceManager(Context context) {
        this.ctx = context;
        this.db = get();
    }

    public PersistenceManager(SQLiteDatabase db) {
        this.db = db;
        this.cacheDisabled = true;
    }

    public PersistenceManager(Context context, ILogger logger) {
        this.ctx = context;
        this.db = get();
        this.logger = logger;
    }

    public PersistenceManager(SQLiteDatabase db, ILogger logger) {
        this.db = db;
        this.cacheDisabled = true;
        this.logger = logger;
    }

    private SQLiteDatabase get() {
        if (db == null)
            db = new DatabaseOpenHelper(this.ctx, "teambuilder", null).getReadableDatabase();
        return db;
    }

    private SQLiteDatabase getWritable() {
        if (db == null)
            db = new DatabaseOpenHelper(this.ctx, "teambuilder", null).getWritableDatabase();
        return db;
    }

    public void persist(final Object o) {
        if (o == null)
            return;
        withTransaction(new Runnable() {
            @Override
            public void run() {
                try {
                    insertOrUpdate(o, metaModelData(o.getClass()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private ClassMetaData metaModelData(Class<?> clazz) {
        if (metaModelCache.containsKey(clazz))
            return metaModelCache.get(clazz);
        ClassMetaData cmd = new ClassMetaData(clazz);
        metaModelCache.put(clazz, cmd);
        return cmd;
    }

    private void insertOrUpdate(Object o, ClassMetaData classMetaData) throws Exception {
        if (!o.getClass().isAnnotationPresent(Entity.class)) {
            throw new IllegalArgumentException(o.getClass().getName() + " is not a known Entity.");
        }
        ContentValues cv = getInsertValues(o, classMetaData);
        insertForRelations(o, classMetaData);
        db.insertWithOnConflict(classMetaData.getTableName(), null, cv, SQLiteDatabase.CONFLICT_ROLLBACK);
    }

    private void insertForRelations(Object o, ClassMetaData classMetaData) throws Exception {
        Collection<Field> relationFields = classMetaData.getRelatedMappedByFields();
        for (Field field : relationFields) {
            Object value = readProperty(o, field, classMetaData);
            if (value instanceof Collection) {
                if (ArrayUtils.indexOf(field.getAnnotation(OneToMany.class).cascade(), CascadeType.MERGE) >= 0
                        || ArrayUtils.indexOf(field.getAnnotation(OneToMany.class).cascade(), CascadeType.PERSIST) >= 0
                        || ArrayUtils.indexOf(field.getAnnotation(OneToMany.class).cascade(), CascadeType.ALL) >= 0) {
                    Collection<?> col = (Collection<?>) value;
                    for (Object rel : col) {
                        insertOrUpdate(rel, metaModelData(rel.getClass()));
                    }
                }
            } else {
                if (field.isAnnotationPresent(OneToOne.class) && (
                        ArrayUtils.indexOf(field.getAnnotation(OneToOne.class).cascade(), CascadeType.MERGE) >= 0
                        || ArrayUtils.indexOf(field.getAnnotation(OneToOne.class).cascade(), CascadeType.PERSIST) >= 0
                        || ArrayUtils.indexOf(field.getAnnotation(OneToOne.class).cascade(), CascadeType.ALL) >= 0)) {
                    insertOrUpdate(value, metaModelData(value.getClass()));
                }
            }
        }
    }

    private ContentValues getInsertValues(Object o, ClassMetaData metaData) throws Exception {
        ContentValues cv = new ContentValues();
        Map<String, Field> fields = metaData.getColumnNamesFields();
        for (Map.Entry<String, Field> e : fields.entrySet()) {
            Object value = ClassMetaData.extractValue(e.getValue(), o);
            String columnName = e.getKey();
            setContentValue(cv, value, columnName, e.getValue(), metaData);
        }
        return cv;
    }

    private void setContentValue(ContentValues cv, Object value, String columnName, Field field, ClassMetaData metaData) throws Exception {
        if (String.class.equals(field.getType())) {
            cv.put(columnName, ((String) value));
        } else if (field.getType().getSimpleName() == "int" || Integer.class.equals(field.getType())) {
            cv.put(columnName, ((Integer) value));
        } else if (field.getType().getSimpleName() == "long" || Long.class.equals(field.getType())) {
            cv.put(columnName, ((Long) value));
        } else if (field.getType().getSimpleName() == "float" || Float.class.equals(field.getType())) {
            cv.put(columnName, ((Float) value));
        } else if (field.getType().isAnnotationPresent(Entity.class)) {
            if (value != null) {
                ClassMetaData metaDataFk = metaModelData(value.getClass());
                Object id = readProperty(value, metaDataFk.getFieldByColumnName(metaDataFk.getIdColumnName()), metaDataFk);
                setContentValue(cv, id, columnName, metaDataFk.getFieldByColumnName(metaDataFk.getIdColumnName()), metaDataFk);
            } else {
                cv.put(columnName, (Integer) null);
            }
        } else if (field.isAnnotationPresent(Converter.class)) {
            Class<?> dbType = field.getAnnotation(Converter.class).columnType();
            if (String.class.equals(dbType)) {
                cv.put(columnName, (String) value);
            }
        }
    }

    private Object readProperty(Object o, Field field, ClassMetaData metaData) {
        try {
            boolean isBool = field.getType().equals(boolean.class) ;
            Class<?> type = metaData.getSubject();
            Method getter = null;
            do {
                try {
                    getter = type.getDeclaredMethod((isBool ? "is" : "get") + StringUtils.capitalize(field.getName()));
                    Object value = getter.invoke(o);
                    return  value;
                } catch (NoSuchMethodException e) {
                    type = type.getSuperclass();
                }
            } while (getter == null && !type.equals(Object.class));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void writeValue(Field field, Object target, Object value, ClassMetaData metaData) {
        try {
            if (field.isAnnotationPresent(Converter.class)) {
                AbstractStringConverter<?> c = (AbstractStringConverter<?>) ClassMetaData.getPropertyConverter(field);
                value = c.convertToEntityProperty((String) value);
            }
            Class<?> type = metaData.getSubject();
            Method setter = null;
            do {
                try {
                    setter = type.getDeclaredMethod("set" + StringUtils.capitalize(field.getName()), field.getType());
                    setter.invoke(target, value);
                } catch (NoSuchMethodException e) {
                    type = type.getSuperclass();
                }
            } while (setter == null && !type.equals(Object.class));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T load(Class<T> clazz, Serializable id) {
        checkIdentifiable(clazz);
        return load(clazz, id, null, new FieldValuesCache(), metaModelData(clazz));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T> T load(Class<T> clazz, Serializable id, String eqField, FieldValuesCache fieldCache, ClassMetaData metaData) {
        // Caching
        if (!cacheDisabled && cache.containsKey(clazz)) {
            Map<Serializable, Object> typeCache = cache.get(clazz);
            if (typeCache.containsKey(id))
                return (T) typeCache.get(id);
        }
        // Prepare Variables
        String whereClause = (eqField != null ? eqField : metaData.getIdColumnName()).concat( " = ?");
        String[] whereArgs = new String[] { id.toString() };
        String orderBy = null;
        Set<String> colNames = metaData.getSelectableColumnNames();
        // Obtain Cursor and run Query
        Cursor cursor = db.query(
                metaData.getTableName(),
                colNames.toArray(new String[colNames.size()]),
                whereClause, whereArgs, null, null, orderBy);
        T inst = null;
        if (cursor.moveToFirst()) {
            inst = instantiateFromCursor(clazz, fieldCache, metaData, cursor);
        }
        return cache(clazz, id, inst);
    }

    public int count(String sql, String[] whereArgs) {
        // Obtain Cursor and run Query
        Cursor cursor = db.rawQuery(sql, whereArgs);
        if (cursor.moveToFirst()) {
            Integer count = cursor.getInt(0);
            return count;
        }
        return 0;
    }

    private <T> T instantiateFromCursor(Class<T> clazz, FieldValuesCache fieldCache, ClassMetaData metaData, Cursor cursor) {
        try {
            T inst = clazz.newInstance();
            String[] columnNames = cursor.getColumnNames();
            Serializable id = null;
            for (int i = 0; i < columnNames.length; i++) {
                Field field = metaData.getFieldByColumnName(columnNames[i]);
                if (field == null)
                    continue;
                Class<?> fieldType = metaData.getFieldTypeByColumnName(columnNames[i]);
                if (fieldType.equals(Integer.class)) {
                    writeValue(field, inst, cursor.isNull(i) ? null : cursor.getInt(i), metaData);
                } else if (fieldType.getSimpleName().equals("int")) {
                    writeValue(field, inst, cursor.getInt(i), metaData);
                } else if (fieldType.equals(Float.class)) {
                    writeValue(field, inst, cursor.isNull(i) ? null : cursor.getFloat(i), metaData);
                } else if (fieldType.getSimpleName().equals("float")) {
                    writeValue(field, inst, cursor.getFloat(i), metaData);
                } else if (fieldType.equals(Long.class)) {
                    writeValue(field, inst, cursor.isNull(i) ? null : cursor.getLong(i), metaData);
                } else if (fieldType.getSimpleName().equals("long")) {
                    writeValue(field, inst, cursor.getLong(i), metaData);
                } else if (fieldType.getSimpleName().equals("boolean") || fieldType.equals(Boolean.class)) {
                    writeValue(field, inst, ONE.equals(cursor.getInt(i)), metaData);
                } else if (field.getType().isAnnotationPresent(Entity.class)) {
                    Object fkEntity = fieldCache.getValue(field.getType(), new MutableNumberId(cursor.getInt(i)));
                    if (fkEntity == null) {
                        fkEntity = load(field.getType(), new MutableNumberId(cursor.getInt(i)));
                    }
                    writeValue(field, inst, fkEntity, metaData);
                } else {
                    writeValue(field, inst, cursor.getString(i), metaData);
                }
                if (field.isAnnotationPresent(Id.class))
                    id = (Serializable) readProperty(inst, field, metaData);
            }
            fieldCache.add(metaData.getRelatedMappedByFields(), (Identifiable) inst);
            setIndirectRelations((Identifiable) inst, clazz, fieldCache, metaData);
            return cache(clazz, ((Identifiable) inst).getIdentifier(), inst);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class MutableNumberId extends MutableInt implements Identifiable {

        public MutableNumberId(Number value) {
            super(value);
        }

        @Override
        public Serializable getIdentifier() {
            return getValue();
        }
    }

    private class MutableSerializableId extends MutableObject<Serializable> implements Identifiable {

        public MutableSerializableId(Serializable value) {
            super(value);
        }

        @Override
        public Serializable getIdentifier() {
            return getValue();
        }
    }

    @SuppressWarnings("unchecked")
    private <T> void setIndirectRelations(Identifiable id, Class<T> clazz, FieldValuesCache fieldCache, ClassMetaData metaData) throws Exception {
        Collection<Field> relationFields = metaData.getRelatedMappedByFields();
        for (Field field : relationFields) {
            if (field.isAnnotationPresent(OneToOne.class)) {
                String mappedBy = metaData.getMappedBy(field);
                Map<String, Field> fields = metaData.getRelevantFieldsWithColumns(field.getType());
                for (Map.Entry<String, Field> e : fields.entrySet()) {
                    String fkColName = e.getKey();
                    if (e.getValue().getName().equalsIgnoreCase(mappedBy)) {
                        Object rel = load(field.getType(), id.getIdentifier(), fkColName, fieldCache, metaModelData(field.getType()));
                        writeValue(field, id, rel, metaData);
                        break;
                    }
                }
            } else if (field.isAnnotationPresent(OneToMany.class)) {
                String mappedBy = metaData.getMappedBy(field);
                if (isNotEmpty(mappedBy)) {
                    Class targetEntity = field.getAnnotation(OneToMany.class).targetEntity();
                    if (targetEntity == null || targetEntity == void.class)
                        throw new IllegalArgumentException(
                                String.format("@OneToMany Field %s is missing targetEntity configuration.", field.getName()));
                    Map<String, Field> fields = metaData.getRelevantFieldsWithColumns(targetEntity);
                    for (Map.Entry<String, Field> e : fields.entrySet()) {
                        if (e.getValue().getName().equalsIgnoreCase(mappedBy)) {
                            Collection relations = null;
                            if (Set.class.isAssignableFrom(e.getValue().getType())){
                                relations = new HashSet<>();
                            } else {
                                relations = new LinkedList<>();
                            }
                            relations.addAll(
                                    loadList(targetEntity,
                                            new QueryParam[]{QueryParam.of(e.getKey(), id.getIdentifier())},
                                            fieldCache, metaModelData(targetEntity)));
                            writeValue(field, id, relations, metaData);
                            break;
                        }
                    }
                }
            }
        }
        for (Field field : metaData.getSimpleOneToOneRelationFields()) {
            if (field.isAnnotationPresent(Column.class)) {
                Column col = field.getAnnotation(Column.class);
                ClassMetaData relMetaModel = metaModelData(field.getType());
                String rawQuery = "SELECT o.* FROM " + relMetaModel.getTableName() + " AS o WHERE o." + relMetaModel.getIdColumnName() + " = (" +
                        "  SELECT o1." + col.name() + " FROM " + metaData.getTableName() + " AS o1 WHERE o1." + metaData.getIdColumnName() + " = " + id.getIdentifier() +
                        ")";
                List<?> entities = loadList(field.getType(), rawQuery, fieldCache, relMetaModel);
                if (!entities.isEmpty())
                    writeValue(field, id, entities.iterator().next(), metaData);
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> List<T> loadList(Class<T> clazz, QueryParam ... queryParams) {
        StopWatch sw = new StopWatch();
        sw.start();
        // load
        List<T> result = loadList(clazz, queryParams, new FieldValuesCache(), metaModelData(clazz));
        sw.stop();
        log(String.format("%s loadList (QueryParams) [%s] took: %s ms", getClass().getSimpleName(), clazz.getSimpleName(), StringUtils.leftPad(Long.toString(sw.getTime()), 5, ' ')));
        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T> List<T> loadList(Class<T> clazz, QueryParam[] queryParams, FieldValuesCache fieldCache, ClassMetaData metaData) {
        checkIdentifiable(clazz);
        // Prepare Variables
        String whereClause = StringUtils.join(CollectionUtils.collect(Arrays.asList(queryParams), new Transformer() {
            @Override
            public Object transform(Object input) {
                return String.format("%s = ?", ((QueryParam) input).getName());
            }
        }), ", ");
        String[] whereArgs = new String[queryParams.length];
        for (int i = 0; i < queryParams.length; i++) {
            whereArgs[i] = queryParams[i].getValue();
        }
        String orderBy = metaData.getIdColumnName();
        Set<String> colNames = metaData.getSelectableColumnNames();
        // Obtain Cursor and run Query
        Cursor cursor = db.query(
                metaData.getTableName(),
                colNames.toArray(new String[colNames.size()]),
                whereClause, whereArgs, null, null, orderBy);
        return listResultFromCursor(clazz, fieldCache, metaData, cursor);
    }

    private <T> List<T> listResultFromCursor(Class<T> clazz, FieldValuesCache fieldCache, ClassMetaData metaData, Cursor cursor) {
        List<T> result = new LinkedList<>();
        StopWatch sw = new StopWatch();
        sw.start();
        int count = 0;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                T inst = instantiateFromCursor(clazz, fieldCache, metaData, cursor);
                if (inst != null)
                    result.add(inst);
                cursor.moveToNext();
                count++;
            }
        }
        sw.stop();
        log(String.format(Locale.getDefault(), "%s listResultFromCursor [%s] (%d) took: %s ms", getClass().getSimpleName(), clazz.getSimpleName(), count, StringUtils.leftPad(Long.toString(sw.getTime()), 5, ' ')));
        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T extends Serializable> List<T> loadList(Class<T> clazz, QueryBuilder<T> builder) {
        StopWatch sw = new StopWatch();
        sw.start();
        // load
        List<T> result = loadList(clazz, builder, new FieldValuesCache(), metaModelData(clazz));
        sw.stop();
        log(String.format("%s loadList (QueryBuilder) [%s] took: %s ms", getClass().getSimpleName(), clazz.getSimpleName(), StringUtils.leftPad(Long.toString(sw.getTime()), 5, ' ')));
        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T extends Serializable> List<T> loadList(Class<T> clazz, QueryBuilder<T> builder, FieldValuesCache fieldCache, ClassMetaData metaData) {
        checkIdentifiable(clazz);
        // Obtain Cursor and run Query
        Cursor cursor = db.rawQuery(builder.getQueryString(), builder.getParametersArray());
        return listResultFromCursor(clazz, fieldCache, metaData, cursor);
    }

    private void checkIdentifiable(Class<?> clazz) {
        if (!Identifiable.class.isAssignableFrom(clazz))
            throw new IllegalArgumentException("Cannot load entity of type " + clazz.getSimpleName() + " since it does not implement the Identifiable interface.");
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> List<T> loadList(Class<T> clazz, String rawQuery) {
        StopWatch sw = new StopWatch();
        sw.start();
        // load
        List<T> result = loadList(clazz, rawQuery, new FieldValuesCache(), metaModelData(clazz));
        sw.stop();
        log(String.format("%s loadList (raw) [%s] took: %s ms", getClass().getSimpleName(), clazz.getSimpleName(), StringUtils.leftPad(Long.toString(sw.getTime()), 5, ' ')));
        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T> List<T> loadList(Class<T> clazz, String rawQuery, FieldValuesCache fieldCache, ClassMetaData metaData) {
        // load
        checkIdentifiable(clazz);
        // Obtain Cursor and run Query
        Cursor cursor = db.rawQuery(rawQuery, null);
        return listResultFromCursor(clazz, fieldCache, metaData, cursor);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T> T cache(Class<T> clazz, Serializable id, T inst) {
        if (!cacheDisabled && clazz.isAnnotationPresent(Cache.class)) {
            Map<Serializable, Object> typeCache = cache.get(clazz);
            if (typeCache == null) {
                typeCache = new HashMap<>(100);
                cache.put((Class<? extends Identifiable>) clazz, typeCache);
            }
            typeCache.put(id, inst);
        }
        return inst;
    }

    private Field getField(String columnName, Field[] fields) {
        for (Field field: fields) {
            String fieldName = field.getName();
            if (field.isAnnotationPresent(Column.class))
                fieldName = field.getAnnotation(Column.class).name();
            if (columnName.equalsIgnoreCase(fieldName))
                return field;
        }
        return null;
    }

    public SQLiteDatabase getDatabase() {
        return ctx != null ? get() : db;
    }

    public void delete(final Identifiable o) {
        withTransaction(new Runnable() {
            @Override
            public void run() {
            try {
                delete(o, metaModelData(o.getClass()), new HashSet<String>(5));
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
        });
    }

    private void delete(final Identifiable o, ClassMetaData metaData, Set<String> dontFollowFields) throws Exception {
        final String tableName = metaData.getTableName();
        final String whereClause = metaData.getIdFieldName().concat(" = ?");
        final String[] whereArgs = new String[] { o.getIdentifier().toString() };
        deleteForRelations(o, metaData, dontFollowFields);
        deleteInTx(tableName, whereClause, whereArgs);
    }

    private void delete(final Collection<Serializable> ids, ClassMetaData metaData, Set<String> dontFollowFields) throws Exception {
        final String tableName = metaData.getTableName();
        final String sql = "delete from " + tableName + " where " + metaData.getIdColumnName() + " in (" + StringUtils.join(CollectionUtils.collect(ids, new Transformer() {
            @Override
            public Object transform(Object input) {
                return input.toString();
            }
        }), ',') + ")";
        db.execSQL(sql);
    }

    @SuppressWarnings("unchecked")
    private void deleteForRelations(Object o, ClassMetaData metaData, Set<String> dontFollowFields) throws Exception {
        Collection<Field> relationFields = metaData.getRelatedMappedByFields();
        for (Field field : relationFields) {
            if (dontFollowFields.contains(field.getName()))
                continue;
            Object value = readProperty(o, field, metaData);
            if (value instanceof Collection) {
                Collection<?> col = (Collection<?>) value;
                if (ArrayUtils.indexOf(field.getAnnotation(OneToMany.class).cascade(), CascadeType.REMOVE) >= 0
                    || ArrayUtils.indexOf(field.getAnnotation(OneToMany.class).cascade(), CascadeType.ALL) >= 0) {
                    final String mappedBy = metaData.getMappedBy(field);
                    if (isNotEmpty(mappedBy))
                        dontFollowFields.add(mappedBy);
                    // delete using IN Query
                    Class targetEntity = field.getAnnotation(OneToMany.class).targetEntity();
                    delete(CollectionUtils.collect(col, new Transformer() {
                        @Override
                        public Object transform(Object input) {
                            return ((Identifiable) input).getIdentifier();
                        }
                    }), metaModelData(targetEntity), dontFollowFields);
                }
            } else {
                if (field.isAnnotationPresent(OneToOne.class) &&
                        (ArrayUtils.indexOf(field.getAnnotation(OneToOne.class).cascade(), CascadeType.REMOVE) >= 0
                        || ArrayUtils.indexOf(field.getAnnotation(OneToOne.class).cascade(), CascadeType.ALL) >= 0)) {
                    final String mappedBy = metaData.getMappedBy(field);
                    if (isNotEmpty(mappedBy))
                        dontFollowFields.add(mappedBy);
                    // delete single entity
                    if (value instanceof Identifiable)
                        delete((Identifiable) value, metaModelData(field.getType()), dontFollowFields);
                }
            }
        }
    }

    private void deleteInTx(String tableName, String whereClause, String[] whereArgs) {
        getDatabase().delete(tableName, whereClause, whereArgs);
    }

    private void withTransaction(Runnable r) {
        getDatabase().beginTransaction();
        try {
            r.run();
            getDatabase().setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (getDatabase().inTransaction())
                getDatabase().endTransaction();
        }

    }

    private void log(String messages) {
        logger.log(messages);
    }
}
