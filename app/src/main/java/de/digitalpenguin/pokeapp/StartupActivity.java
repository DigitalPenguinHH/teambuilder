package de.digitalpenguin.pokeapp;

import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Fade;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.commons.lang3.time.StopWatch;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.util.LocaleManager;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class StartupActivity extends PersistenceActivityBase {

    public static final boolean USE_LEGACY_DATASTORE = false;

    // Handle progress Events in UI
    private Handler mHandler = new Handler();
    // Progress Bar
    private ProgressBar mProgress;

    // DB Loading Multithreading
    private AtomicInteger mProgressStatus = new AtomicInteger(0);

    private StopWatch stopWatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // declare transitions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            // set an exit transition
            getWindow().setExitTransition(new Fade());
        }
        Log.i(getClass().getSimpleName(), "Starting initilaization.");
        // Update Locale
        LocaleManager.setLocaleFromPreferences(this);
        //
        initContent();
    }

    private void initContent() {

        // activity layout
        setContentView(R.layout.activity_startup);

        // register Handle
        mProgress = findViewById(R.id.startupProgessBar);
        initImageLoader(this);

        // Version Label
        String versionName = BuildConfig.VERSION_NAME;
        TextView versionLabel = findViewById(R.id.appVersionLabel);
        versionLabel.setText("Ver. " + versionName);

        // Background Scroll Bottom
        initScrolView();
    }

    public static void initImageLoader(Context context) {
        // Image Loader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
            .memoryCache(new LRULimitedMemoryCache(5 * 1024 * 1024))
			.build();
        ImageLoader.getInstance().init(config);
    }

    private void initScrolView() {
        final HorizontalScrollView scrollView = findViewById(R.id.startupScrollView);
        scrollView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                Random rand = new Random();
                boolean reverse = rand.nextBoolean();
                scrollView.setSmoothScrollingEnabled(false);
                int widthOfImage = 1920;
                int startX;
                if (reverse) {
                    startX = widthOfImage - rand.nextInt(widthOfImage / 2);
                    scrollView.scrollTo(startX, 0);
                } else {
                    startX = rand.nextInt(widthOfImage / 2);
                    scrollView.scrollTo(startX, 0);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Initalizae DataStore
        initStuff();
    }

    private void initStuff() {
        getDataProvider().init();
        // Teams from XML
        XMLHelper.loadTeams(getApplicationContext(), getDataProvider(), true);
        reportProgress(4);
        //
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgress.setProgress(100);
            }
        });
        onProceed();
    }

    private void reportProgress(int step) {
        mProgressStatus.getAndAdd(step);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgress.setProgress(mProgressStatus.get());
            }
        });
    }

    private String getStopWatchTime() {
        return NumberFormat.getInstance(Locale.GERMANY).format(stopWatch.getTime());
    }

    private void onProceed() {
        if (stopWatch != null && stopWatch.isStarted()) {
            stopWatch.stop();
            Log.i(getClass().getSimpleName(), String.format("DataStore init completed in %s ms.", getStopWatchTime()));
        }

        // after all loaded
        if (USE_LEGACY_DATASTORE) {
            DataStore.getInstance().initMachineDetails();
            reportProgress(5);
        }

        logMemoryUsage();

        long delay = USE_LEGACY_DATASTORE ? 250 : 1000;

        // start
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Context ctx = StartupActivity.this;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(new Intent(ctx, MainActivity.class), ActivityOptions.makeSceneTransitionAnimation(StartupActivity.this).toBundle());
                } else {
                    startActivity(new Intent(ctx, MainActivity.class));
                }
            }
        }, delay);
    }

    private void logMemoryUsage() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);

        long mb = 1024*1024;
        Runtime runtime = Runtime.getRuntime();
        Log.i(getClass().getName(), "Used Memory:  " + (runtime.totalMemory() - runtime.freeMemory()) / mb);
        Log.i(getClass().getName(), "Free Memory:  " + runtime.freeMemory()  / mb);
        Log.i(getClass().getName(), "Total Memory: " + runtime.totalMemory() / mb);
        Log.i(getClass().getName(), "Max Memory:   " + runtime.maxMemory()   / mb);
    }
}
