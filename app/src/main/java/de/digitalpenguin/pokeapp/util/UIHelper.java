package de.digitalpenguin.pokeapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.LocalizedNamed;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.listener.OnClickToaster;

public class UIHelper
{

	private static final String DEF_TYPE_STRING = "string";

	private UIHelper()
	{
		// 
	}

	private static Map<String, String> cache = new HashMap<>();

	public static void swapFragment(FragmentManager fragmentManager, Fragment fragment) {
		swapFragment(fragmentManager, fragment, fragment.getClass().getName(), true, true);
	}

    public static void swapFragment(FragmentManager fragmentManager, Fragment fragment, boolean animate) {
        swapFragment(fragmentManager, fragment, fragment.getClass().getName(), true, animate);
    }

	public static void swapFragment(FragmentManager fragmentManager, Fragment fragment, String name) {
		swapFragment(fragmentManager, fragment, name, true, true);
	}

	public static void swapFragment(FragmentManager fragmentManager, Fragment fragment, String name, boolean addToStack, boolean animate) {
		try {
			FragmentTransaction ftx = fragmentManager.beginTransaction();
			if (animate)
                ftx.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
			if (addToStack)
                ftx.addToBackStack(name);
			ftx.replace(R.id.content_frame, fragment).commit();
		} catch (Exception e) {
			Log.e(UIHelper.class.getSimpleName(), "Swap fragment failed.", e);
		}
	}
	
	public static String getImageUriPokemonIcon(int pokemonId, VariantType variantType)
	{
		String resKey = String.format(Locale.getDefault(), "p%03d", pokemonId);
		if (variantType == VariantType.ALOLAN || variantType == VariantType.GALARIAN) {
			resKey = resKey.concat("_".concat(variantType.name().toLowerCase()));
		}
		return "@drawable/".concat(resKey);
	}

	public static String getImageUriGenerationIcon(int generation)
	{
		return "@drawable/" + String.format("games_icon_gen%d", generation);
	}
	
	public static String getImageUriPokemonIcon(Pokemon p, VariantType variantType)
	{
		return getImageUriPokemonIcon(p.getId(), variantType);
	}

	public static String getImageUriGameIcon(Game game)
	{
		String gameId = game.getName(Locale.ENGLISH).toLowerCase().replaceAll("[\\' ]", "");
		return "@drawable/" + String.format("game_icon_%s", gameId);
	}

	public static int getResourceId(Context context, Game game) {
		if (game == null) {
			return R.drawable.icons8_swatch_48;
		}
		return context.getResources().getIdentifier(getImageUriGameIcon(game), null, context.getPackageName());
	}
	
	public static String getImageUriPropertyIcon(PProperty p)
	{
		return p == null ? "@drawable/prop_empty" : "@drawable/prop_" + p.name().toLowerCase(Locale.GERMAN);
	}

	public static void makePropertyIcon (Context context, ImageView imgView, PProperty prop) {
        int resIdPropDrawable = context.getResources().getIdentifier(getImageUriPropertyIcon(prop), "drawable", context.getPackageName());
		String uri = "drawable://" + resIdPropDrawable;
		ImageLoader.getInstance().displayImage(uri, imgView);
        String propertyStringKey = "prop_" + prop.name().toLowerCase();
        int resIdPropName = context.getResources().getIdentifier(propertyStringKey, "string", context.getPackageName());
        imgView.setOnClickListener(new OnClickToaster(context, context.getString(resIdPropName)));
    }

	public static String getStandardName(LocalizedNamed named, Locale locale) {
		String itemNameEn = named.getName(locale);
		itemNameEn = itemNameEn
				.replaceAll("\\.", "")
				.replaceAll("-", "_")
				.replaceAll("'", "")
				.replaceAll(" ", "")
				.toLowerCase();
		itemNameEn = StringUtils.stripAccents(itemNameEn);
		return itemNameEn;
	}

	@SuppressWarnings("rawtypes")
	private static Class colorResourceClass;

	static {
		colorResourceClass = R.color.class;
	}

	public static String getString(Context context, String resName) {
		int resIdName = context.getResources().getIdentifier(resName, "string", context.getPackageName());
		return context.getString(resIdName);
	}

	public static int dpToPx(Context context, int value) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
        return px;
    }

	public static void hideKeyboardFrom(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static String getDisplayName(Context context, ItemCategory cat) {
        String key = "item_category_" + cat.name().toLowerCase();
        String name = context.getResources().getString(context.getResources().getIdentifier(key,DEF_TYPE_STRING, context.getPackageName()));
        return name;
    }

	public static int getArtworkResource(Context context, Pokemon p, Variant v) {
		String filename = getArtworkFilename(p, v);
		int resId = context.getResources().getIdentifier("@drawable/" + filename,null, context.getPackageName());
		return resId;
	}

	public static String getArtworkFilename(Pokemon p, Variant v) {
		StringBuilder sb = new StringBuilder();
		sb.append("artw");
		sb.append(String.format("%03d", p.getId()));
		String regex = "[^[A-Z][a-z][0-9]]";
		sb.append(StringUtils.stripAccents(p.getNameEn()).toLowerCase().replaceAll(regex, ""));
		if (v != null && !"Normal".equals(v.getName()) && v.getType() != VariantType.DEFAULT) {
			sb.append('_');
			if (v.getType() == VariantType.MEGA) {
				sb.append(v.getType().name().toLowerCase());
				sb.append(v.getSuffix());
			} else if (v.getType() == VariantType.ALOLAN || v.getType() == VariantType.GALARIAN ||v.getType() == VariantType.GIGANTAMAX) {
				sb.append(v.getType().name().toLowerCase());
			} else if (StringUtils.isNoneEmpty(v.getSuffix())) {
				sb.append(v.getSuffix());
			} else {
				sb.append(v.getName(Locale.ENGLISH).replaceAll("[\\- ]", "").toLowerCase());
			}
		}
		return sb.toString();
	}

	public static String getFullPokemonName(Pokemon p, Variant v, Resources res) {
		if (v == null)
			return p.getName();
		if (v.getType() == VariantType.MEGA) {
			String nameStr = "";
			if (StringUtils.isNotEmpty(v.getName())) {
				nameStr = v.getName();
			} else if (StringUtils.isNotEmpty(v.getSuffix())) {
				nameStr = String.format(res.getString(R.string.mega_name) + " " + v.getSuffix().toUpperCase());
			} else {
				nameStr = String.format(res.getString(R.string.mega_name), p.getName());
			}
			return nameStr;
		} else if (v.getType() == VariantType.DEFAULT) {
			return p.getName();
		} else if (v.getType() == VariantType.ALOLAN) {
			return res.getString(R.string.pokemon_name_alolan, p.getName());
		} else if (v.getType() == VariantType.GALARIAN) {
			return res.getString(R.string.pokemon_name_galarian, p.getName());
		} else if (v.getType() == VariantType.GIGANTAMAX) {
			return String.format("%s (%s)", p.getName(), res.getString(R.string.variant_gigantamax));
		} else {
			return v.getName();
		}
	}

	public static int getDrawableResourceId(Context ctx, String name) {
		return ctx.getResources().getIdentifier("@drawable/" + name, "drawable", ctx.getPackageName());
	}

	public static int getItemResourceId(Context ctx, DataProvider dataProvider, Item item) {
		String iconName = String.format("item_%s", item.getDrawableResourceName());
		if (item.getCategory() == ItemCategory.MACHINES) {
			Move move = dataProvider.getMoveForMachine(item.getName(Locale.ENGLISH));
			if (move == null) {
				return R.drawable.item_tm_normal;
			}
			PProperty prop = move.getProperty();
			String propNameLower = prop != null ? prop.name().toLowerCase() : "_normal";
			iconName = "item_tm_" + propNameLower;
		}
		int id = ctx.getResources().getIdentifier("@drawable/" + iconName, null, ctx.getPackageName());
		return id;
	}

	public static String getPokemoFormDisplayName(PokemonForm form, Context context) {
		return getPokemoFormDisplayName(form, context, false);
	}

	public static String getPokemoFormDisplayName(PokemonForm form, Context context, boolean _short) {
		Pokemon p = form.getPokemon();
		String displayName = p.getName();
		if (form.isRegional()) {
			int resId = context.getResources().getIdentifier("@string/pokemon_name_" + form.getType().name().toLowerCase(), "string", context.getPackageName());
			if (resId > 0)
				displayName = context.getResources().getString(resId, displayName);
		} else if (VariantType.OTHER == form.getType()) {
			if (form.getName().contains(displayName)) {
				displayName = form.getName();
			} else if (!_short) {
				displayName = displayName.concat(String.format(" (%s)", form.getName()));
			}
		} else if (VariantType.MEGA == form.getType()) {
			// Mega-...
			if (StringUtils.isNotEmpty(form.getName())) {
				displayName = form.getName();
			} else {
				displayName = String.format(Locale.getDefault(), context.getString(R.string.mega_name), p.getName());
			}
		} else if (StringUtils.isNotEmpty(form.getName()) && !_short) {
			displayName = String.format("%s (%s)", displayName, form.getName());
		}
		return displayName;
	}

	public static void updateListTileMove(Context context, View tileView, Resources resources, Move m, TextDecorator textDecorator) {
		// remove level info
		View moveLevelText = tileView.findViewById(R.id.moveLearnedTileLevel);
		if (moveLevelText != null)
			((ViewGroup) moveLevelText.getParent()).removeView(moveLevelText);
		// set fields
		TextView viewName = tileView.findViewById(R.id.moveLearnedTileName);
		viewName.setText(m.getName());
		if (textDecorator != null)
			textDecorator.decorate(viewName);
		ImageView propertyIcon = tileView.findViewById(R.id.moveLearnedTileProperty);
		UIHelper.makePropertyIcon(context, propertyIcon, m.getProperty());
		ImageView typeIcon = tileView.findViewById(R.id.moveLearnedTileType);
		String uri = "drawable://" + resources.getIdentifier("move_type_" + m.getType().name().toLowerCase(),"drawable", context.getPackageName());
		ImageLoader.getInstance().displayImage(uri, typeIcon);
		typeIcon.setOnClickListener(new OnClickToaster(context, context.getString(resources.getIdentifier("move_type_" + m.getType().name().toLowerCase(), "string", context.getPackageName()))));
		TextView viewStrength = tileView.findViewById(R.id.moveLearnedTileStrength);
		viewStrength.setText(m.getDisplayStrength());
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static void updateListTileMove(Context context, DataProvider dataProvider, View convertView, Resources resources, MoveLearned ml, PProperty[] pokemonProperties, boolean icon) {
		// move
		Move m = dataProvider.getMove(ml.getMoveId());
		// fields
		if (icon) {
			ImageView iconView = convertView.findViewById(R.id.moveLearnedTileIcon);
			String iconName;
			switch (ml.getTrigger()) {
				case LEVELUP:
					iconName = "icon_lvlup";
					break;
				case MACHINE:
					iconName = "item_tm_flying";
					break;
				case BREEDING:
					iconName = "item_mystery_egg";
					break;
				case TUTOR:
					iconName = "item_rule_book";
					break;
				default:
					throw new IllegalStateException("Unexpected value: " + ml.getTrigger());
			}
			TextView viewTxt = convertView.findViewById(R.id.moveLearnedTileTriggerName);
			String lvlOrMachineStr = ml.getMachine();
			if (ml.getTrigger() == MoveLearnTrigger.LEVELUP) {
				lvlOrMachineStr = context.getResources().getText(R.string.moves_header_lv) + " " + ml.getDisplayLevel();
			} else if (ml.getTrigger() == MoveLearnTrigger.MACHINE) {
				//
			} else if (ml.getTrigger() == MoveLearnTrigger.BREEDING || ml.getTrigger() == MoveLearnTrigger.TUTOR) {
//				LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) iconView.getLayoutParams();
////				layoutParams.height = dpToPx(context, 48);
////				iconView.setLayoutParams(layoutParams);
				ViewGroup parent = (ViewGroup) iconView.getParent();
				for (int i = 0; i < parent.getChildCount(); i++) {
					parent.getChildAt(i).setVisibility(View.GONE);
				}
//				convertView.findViewById(R.id.moveLearnedTileTriggerName).setVisibility(View.GONE);
			}
			String uri = "drawable://" + resources.getIdentifier(iconName, "drawable", context.getPackageName());
			ImageLoader.getInstance().displayImage(uri, iconView);
			viewTxt.setText(lvlOrMachineStr);
		} else if (ml.getTrigger() == MoveLearnTrigger.LEVELUP || ml.getTrigger() == MoveLearnTrigger.MACHINE) {
			TextView viewLevel = convertView.findViewById(R.id.moveLearnedTileLevel);
			if (viewLevel != null)
				viewLevel.setText(Integer.toString(ml.getDisplayLevel()));
			TextView textTmVm = convertView.findViewById(R.id.moveLearnedTileMachine);
			if (textTmVm != null) {
				String machineName = ml.getMachine().substring(0, 2);
				String machineNumber = ml.getMachine().substring(2);
				switch (machineName) {
					case "TM":
						machineName = context.getString(R.string.move_category_machine).split("/")[0].concat(machineNumber);
						break;
					case "TP":
						machineName = context.getString(R.string.hm_gen8).concat(machineNumber);
						break;
					case "VM":
						machineName = context.getString(R.string.hm).concat(machineNumber);
						break;
				}
				textTmVm.setText(machineName);
			}
		} else {
			List<Game> games = dataProvider.getGames();
			ViewGroup wrapper = convertView.findViewById(R.id.moveLearnedTileEditionsWrapper);
			if (ml.getGames() != null) {
				for (final String gameId : ml.getGames()) {
					ImageView iconView = new ImageView(context);
					Game game = (Game) CollectionUtils.find(games, new Predicate() {
						@Override
						public boolean evaluate(Object object) {
							return ((Game) object).getId().equalsIgnoreCase(gameId);
						}
					});
					int resIdIcon = context.getResources().getIdentifier("game_icon_" + UIHelper.getStandardName(game, Locale.ENGLISH) + "_square", "drawable", context.getPackageName());
					if (resIdIcon <= 0)
						continue;
					iconView.setImageResource(resIdIcon);
					LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(dpToPx(context, 16), dpToPx(context, 22));
					params.rightMargin = dpToPx(context, 2);
					iconView.setLayoutParams(params);
					iconView.setOnClickListener(new OnClickToaster(context, game.getName()).setShort());
					wrapper.addView(iconView);
				}
				ViewGroup.LayoutParams wrapperParams = wrapper.getLayoutParams();
				wrapperParams.width = dpToPx(context, Math.max(62, 18 * ml.getGames().length) - 2);
				wrapper.setLayoutParams(wrapperParams);
			} else {
				((ViewGroup) wrapper.getParent()).removeView(wrapper);
			}
		}
		TextView viewName = convertView.findViewById(R.id.moveLearnedTileName);
		viewName.setText(m.getName());
		viewName.setTypeface(null, ArrayUtils.contains(pokemonProperties, m.getProperty()) && m.doesDamage() ? Typeface.BOLD : Typeface.NORMAL); // STAB
		ImageView propertyIcon = convertView.findViewById(R.id.moveLearnedTileProperty);
		UIHelper.makePropertyIcon(context, propertyIcon, m.getProperty());
		ImageView typeIcon = convertView.findViewById(R.id.moveLearnedTileType);
		String uri = "drawable://" + resources.getIdentifier("move_type_" + m.getType().name().toLowerCase(),"drawable", context.getPackageName());
		ImageLoader.getInstance().displayImage(uri, typeIcon);
		typeIcon.setOnClickListener(new OnClickToaster(context, context.getString(resources.getIdentifier("move_type_" + m.getType().name().toLowerCase(), "string", context.getPackageName()))));
		TextView viewStrength = convertView.findViewById(R.id.moveLearnedTileStrength);
		viewStrength.setText(m.getDisplayStrength());
		convertView.setTag(ml);
	}

	public static void fadeOut(View view) {
		fadeOut(view, 1000);
	}

	public static void fadeOut(View view, long duration) {
		AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(duration);
		anim.setRepeatCount(0);
		view.startAnimation(anim);
	}

	public static void drawPropertyLayers(Context _context, View propertiesView, List<PProperty> orderedProperties) {

		LayerDrawable layerDrawable = (LayerDrawable) ContextCompat.getDrawable(_context, R.drawable.shape_propreties_stacked);
		layerDrawable.mutate();

		int i;
		for (i = 0; i < 4; i++) {
			BitmapDrawable d;
			try {
				PProperty prop = orderedProperties.get(i);
				d = (BitmapDrawable) ResourcesCompat.getDrawable(_context.getResources(), UIHelper.getDrawableResourceId(_context, "prop_" + prop.name().toLowerCase()), null);
			} catch (IndexOutOfBoundsException e) {
				d = (BitmapDrawable) ResourcesCompat.getDrawable(_context.getResources(), UIHelper.getDrawableResourceId(_context, "prop_empty"), null);
			}
			switch (i) {
				case 0:
					layerDrawable.setDrawableByLayerId(R.id.stackedProperty_4, d);
					break;
				case 1:
					layerDrawable.setDrawableByLayerId(R.id.stackedProperty_3, d);
					break;
				case 2:
					layerDrawable.setDrawableByLayerId(R.id.stackedProperty_2, d);
					break;
				case 3:
					layerDrawable.setDrawableByLayerId(R.id.stackedProperty_1, d);
					break;
				default:
					break;
			}
		}

		// set drawables
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			propertiesView.setBackground(layerDrawable);
		} else {
			propertiesView.setBackgroundDrawable(layerDrawable);
		}

	}

	public static void setBackGroundColorStriped(Resources res, int position, View view) {
		if (position%2 == 0) {
			view.setBackgroundColor(res.getColor(R.color.colorSearchResultDarker));
		} else {
			view.setBackgroundColor(res.getColor(R.color.defaultBackground));
		}
	}
}
