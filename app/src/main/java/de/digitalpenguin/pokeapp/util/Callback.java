package de.digitalpenguin.pokeapp.util;

public interface Callback {

    void notify(Object o);

}
