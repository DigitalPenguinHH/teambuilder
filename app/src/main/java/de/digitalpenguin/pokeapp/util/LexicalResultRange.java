package de.digitalpenguin.pokeapp.util;

import org.apache.commons.lang3.Range;

public class LexicalResultRange {

    private Range<Integer> range;

    private String text;

    private boolean matchText;

    public LexicalResultRange(Range<Integer> range, String fullname, String term) {
        this.range = range;
        this.text = fullname.substring(range.getMinimum(), range.getMaximum());
        this.matchText = this.text.equals(term);
    }

    public Range<Integer> getRange() {
        return range;
    }

    public String getText() {
        return text;
    }

    public boolean isMatchText() {
        return matchText;
    }
}
