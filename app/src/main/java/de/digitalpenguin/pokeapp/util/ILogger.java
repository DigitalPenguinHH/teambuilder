package de.digitalpenguin.pokeapp.util;

public interface ILogger {

    void log(String message);

    void error(String message);

}
