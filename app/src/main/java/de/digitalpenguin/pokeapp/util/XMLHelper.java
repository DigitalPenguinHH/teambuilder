package de.digitalpenguin.pokeapp.util;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.thoughtworks.xstream.XStream;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.converter.AbilityConverter;
import de.digitalpenguin.pokeapp.converter.CharacteristicConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainLinkConverter;
import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.GenerationConverter;
import de.digitalpenguin.pokeapp.converter.HiddenMachineConverter;
import de.digitalpenguin.pokeapp.converter.ItemConverter;
import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.converter.NatureConverter;
import de.digitalpenguin.pokeapp.converter.PokemonConverter;
import de.digitalpenguin.pokeapp.converter.PokemonDBConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.converter.TeamConverter;
import de.digitalpenguin.pokeapp.converter.TeamMemberConverter;
import de.digitalpenguin.pokeapp.converter.VariantConverter;
import de.digitalpenguin.pokeapp.data.AbilitiesDB;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.CharacteristicsDB;
import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainDB;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemsDB;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.NaturesDB;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamDB;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.TeamsStore;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class XMLHelper {

    private static final String TEAM_DB_XML = "teams.xml";

    private static final String TAG = "LoadXML";

    private XMLHelper() {
        // utility
    }

    @NonNull
    private static StopWatch getAndStartStopWatch() {
        StopWatch sw = new StopWatch();
        sw.start();
        return sw;
    }

    public static List<Team> loadTeams(Context context, DataProvider dataProvider, boolean retryOnError) {
        XStream xstream;
        File f = null;
        try {
            xstream = initTeamDBXStream(dataProvider);
            f = new File(context.getFilesDir(), TEAM_DB_XML);
            if (!f.exists()) {
                persistTeams(context, dataProvider);
            }
            FileInputStream inStream = context.openFileInput(TEAM_DB_XML);
            TeamDB db = (TeamDB) xstream.fromXML(inStream);
            TeamsStore.setInstance(new TeamsStore(db.getTeams()));
            inStream.close();
            return TeamsStore.getInstance().getTeamEntries();
        } catch (Exception e) {
            Log.e(TAG, "Unable to load team db. Deleting and starting over.", e);
            if (retryOnError && f != null && f.exists()) {
                f.delete();
                return loadTeams(context, dataProvider, false);
            }
        }
        return new ArrayList<>();
    }

    public static void persistTeams(Context context) {
        persistTeams(context, null);
    }

    public static void persistTeams(Context context, DataProvider dataProvider) {
        XStream xstream;
        try {
            xstream = initTeamDBXStream(dataProvider);
            TeamDB db = new TeamDB();
            if (TeamsStore.getInstance() != null) {
                List<Team> teamEntries = TeamsStore.getInstance().getTeamEntries();
                db.setTeams(teamEntries);
            }
            FileOutputStream outStream = context.openFileOutput(TEAM_DB_XML, Context.MODE_PRIVATE);
            xstream.toXML(db, outStream);
            outStream.close();
        } catch (Exception e) {
            Log.e("persistance", "Unable to store team db.", e);
        }
    }

    @NonNull
    private static XStream initTeamDBXStream(DataProvider dataProvider) {
        XStream xstream = new XStream();
        xstream.setClassLoader(TeamDB.class.getClassLoader());
        xstream.alias("db", TeamDB.class);
        xstream.alias("team", Team.class);
        xstream.alias("member", TeamMember.class);
        xstream.omitField(TeamMember.class, "team");
        xstream.registerConverter(new TeamConverter(dataProvider));
        xstream.registerConverter(new TeamMemberConverter());
        xstream.addImplicitCollection(TeamDB.class, "teams", Team.class);
        xstream.addImplicitCollection(Team.class, "members", TeamMember.class);
        return xstream;
    }

}
