package de.digitalpenguin.pokeapp.util;

import org.apache.commons.lang3.Range;

import java.util.LinkedList;
import java.util.List;

public class PokemonSearchResultLexicalInfo {

    private String term;

    private String text;

    private List<LexicalResultRange> occurrences = new LinkedList<>();

    public PokemonSearchResultLexicalInfo(String term, String text) {
        this.text = text;
        this.term = term;
        doInfo();
    }

    private void doInfo() {
        String name = text == null ? "" : text.toLowerCase();
        int pos = Math.max(0, name.indexOf(term));
        if (pos > 0) {
            occurrences.add(new LexicalResultRange(Range.between(0,pos), name, this.term));
        }
        int endIdx = name.indexOf(term) < 0 ? 0 : pos + term.length();
        occurrences.add(new LexicalResultRange(Range.between(pos, endIdx), name, this.term));
        if (endIdx < name.length()) {
            occurrences.add(new LexicalResultRange(Range.between(endIdx,name.length()), name, this.term));
        }
    }

    public List<LexicalResultRange> getOccurrences() {
        return occurrences;
    }
}
