package de.digitalpenguin.pokeapp.util;

import android.widget.TextView;

public interface TextDecorator {

    void decorate(TextView textView);

}
