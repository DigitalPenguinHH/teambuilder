package de.digitalpenguin.pokeapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import androidx.preference.PreferenceManager;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

import de.digitalpenguin.pokeapp.fragments.SettingsFragment;

public class LocaleManager {

    public static void setLocaleFromPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String lang = prefs.getString(SettingsFragment.PREF_LANGUAGE, "");
        if (StringUtils.isNotEmpty(lang))
            updateResources(context, lang);
    }

    public static void setNewLocale(Context c, String language) {
        updateResources(c, language);
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
        } else {
            config.locale = locale;
        }
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}
