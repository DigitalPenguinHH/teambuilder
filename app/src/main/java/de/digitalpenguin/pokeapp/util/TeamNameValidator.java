package de.digitalpenguin.pokeapp.util;

import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import org.apache.commons.lang3.StringUtils;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamsStore;

public class TeamNameValidator implements TextWatcher {

    private static final int MAX_NAME_LEGTH = 24;

    private final Team team;
    private final EditText nameInput;
    private final Resources resources;
    private Callback callback = null;
    private Button button = null;

    public TeamNameValidator(Resources res, EditText input, Team t) {
        this.nameInput = input;
        this.team = t;
        this.resources = res;
    }

    public TeamNameValidator(Resources res, EditText input, Team t, Callback callback) {
        this.nameInput = input;
        this.team = t;
        this.resources = res;
        this.callback = callback;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        team.setName(charSequence.toString());
        nameInput.setError(null);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        validate();
    }

    public boolean validate() {
        return validate(team.getName());
    }

    public boolean validate(String teamName) {
        boolean ok = true;
        // not empty
        if (ok && StringUtils.isEmpty(teamName))
            ok = false;
        // length
        if (ok && teamName.length() > MAX_NAME_LEGTH) {
            nameInput.setError(resources.getString(R.string.team_name_length_error));
            ok = false;
        }
        // name uniqueness
        Team existing = TeamsStore.getInstance().getTeamByName(teamName);
        if (ok && existing != null && existing != this.team) {
            nameInput.setError(resources.getString(R.string.team_name_unique_error));
            ok = false;
        }
        if (button != null)
            button.setEnabled(ok);
        if (callback != null)
            callback.notify(ok);
        return ok;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}
