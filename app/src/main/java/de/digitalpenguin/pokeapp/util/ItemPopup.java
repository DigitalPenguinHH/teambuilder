package de.digitalpenguin.pokeapp.util;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class ItemPopup {

    public static void show(Context context, DataProvider dataProvider, ViewGroup viewRoot, Item item) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View content = inflater.inflate(R.layout.item_popup_layout, viewRoot, false);
        initPopupContent(context, dataProvider, content, item);
        final PopupWindow popupWindow = new PopupWindow(content, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setAnimationStyle(android.R.anim.fade_in);
        popupWindow.showAtLocation(viewRoot, Gravity.CENTER, 0, 0);
        content.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private static void initPopupContent(Context context, DataProvider dataProvider, View content, Item item) {
        TextView tvHeader = content.findViewById(R.id.itemDetailHeader);
        tvHeader.setText(item.getName());
        ImageView ivIcon = content.findViewById(R.id.itemDetailIcon);
        int iconResource = UIHelper.getItemResourceId(context, dataProvider, item);
        ivIcon.setImageResource(iconResource);
        TextView tvHeaderDesc = content.findViewById(R.id.itemDetailText);
        tvHeaderDesc.setText(item.getDescription());
    }

}
