package de.digitalpenguin.pokeapp.util;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import de.digitalpenguin.pokeapp.adapter.EditTeamMemberListener;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.fragments.PokemonDetailFragment;
import de.digitalpenguin.pokeapp.fragments.TeamMemberAbilitiesFragment;
import de.digitalpenguin.pokeapp.fragments.TeamMembersDetailFragment;

public class TeamMemberOptionsHandler {

    private FragmentManager fragmentManager;

    private Team team;

    private TeamMembersDetailFragment fragment;

    private DataProvider dataProvider;

    public TeamMemberOptionsHandler(FragmentManager fragmentManager, DataProvider dataProvider, Team team, TeamMembersDetailFragment fragment) {
        this.fragmentManager = fragmentManager;
        this.team = team;
        this.fragment = fragment;
        this.dataProvider = dataProvider;
    }

    public void openPokedexEntry() {
        TeamMember tm = fragment.getSelectedTeamMember();
        UIHelper.swapFragment(fragmentManager, PokemonDetailFragment.newInstance(tm.getPid(), tm.getVariantIdx(), false));
    }

    public void replace() {
        TeamMember teamMember = fragment.getSelectedTeamMember();
        new EditTeamMemberListener(fragmentManager, this.team).performEditTeamMember(teamMember);
        fragment.refreshListView();
        fragment.getTeamDetailFragment().updateTeamCapacity();
    }

    public void swapProperties() {
        TeamMember teamMember = fragment.getSelectedTeamMember();
        teamMember.increasePrimaryPropertyIndex(dataProvider.getDistinctProperties(teamMember));
        fragment.saveAndRefresh();
    }

    public void remove() {
        TeamMember teamMember = fragment.getSelectedTeamMember();
        team.remove(teamMember);
        fragment.saveAndRefresh();
        fragment.getTeamDetailFragment().updateTeamCapacity();
    }

    public void editMemberAbilities() {
        TeamMember teamMember = fragment.getSelectedTeamMember();
        TeamMemberAbilitiesFragment fragment = new TeamMemberAbilitiesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TeamMember.PARAM_ID, teamMember);
        if (team.getGame() != null)
            bundle.putInt(Region.PARAM_GENERATION, team.getGame().getGeneration());
        fragment.setArguments(bundle);
        UIHelper.swapFragment(fragmentManager, fragment);
    }
}
