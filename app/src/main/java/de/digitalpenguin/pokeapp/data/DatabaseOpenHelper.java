package de.digitalpenguin.pokeapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabaseOpenHelper extends AbstractDatabaseOpenHelper {

    private Context context;

    private SQLiteDatabase sqliteDatabase;

    private static String DB_PATH;
    private static String DB_NAME;

    public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory, 1);
        this.context = context;
        DB_NAME = name;
        DB_PATH = context.getDatabasePath(DB_NAME).getAbsolutePath();
        try {
            createDataBase();
        } catch (Exception ioe) {
            throw new Error("Unable to create database");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

    /** Open Database for Use */
    public void openDatabase() {
        sqliteDatabase = SQLiteDatabase.openDatabase(DB_PATH, null,
                (SQLiteDatabase.OPEN_READWRITE));
    }

    /** Close Database after use */
    @Override
    public synchronized void close() {
        if ((sqliteDatabase != null) && sqliteDatabase.isOpen()) {
            sqliteDatabase.close();
        }
        super.close();
    }

    /** Get database instance for use */
    public SQLiteDatabase getSqliteDatabase() {
        return sqliteDatabase;
    }

    /** Create new database if not present */
    private void createDataBase() {
        SQLiteDatabase sqliteDatabase = null;

        if (!databaseExists()) {
            copyDataBase();
        } else {
            // TODO: migrate, currently overwrites
            copyDataBase();
        }

        /* Database does not exists create blank database */
        sqliteDatabase = this.getReadableDatabase();
        sqliteDatabase.close();
    }

    /** Check Database if it exists */
    private boolean databaseExists() {
        SQLiteDatabase sqliteDatabase = null;
        try {
            sqliteDatabase = SQLiteDatabase.openDatabase(DB_PATH, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            Log.i(getClass().getSimpleName(), "Database " + DB_PATH + " does not exist.");
        }

        if (sqliteDatabase != null) {
            sqliteDatabase.close();
        }
        return sqliteDatabase != null;
    }

    /**
     * Copy existing database file in system
     */
    private void copyDataBase() {
        try {
            copyFile(DB_NAME);
            copyFile(DB_NAME.concat("-shm"));
            copyFile(DB_NAME.concat("-wal"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyFile(String fileName) throws IOException {
        int length;
        byte[] buffer = new byte[1024];
        InputStream databaseInputFile = this.context.getAssets().open(fileName);
        if(databaseInputFile.available() > 0) {
            OutputStream databaseOutputFile = new FileOutputStream(context.getDatabasePath(DB_NAME).getParent().concat("/" + fileName));
            while ((length = databaseInputFile.read(buffer)) > 0) {
                databaseOutputFile.write(buffer, 0, length);
                databaseOutputFile.flush();
            }
            databaseInputFile.close();
            databaseOutputFile.close();
        }
    }

}