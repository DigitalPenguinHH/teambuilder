package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.comparator.NamedComparator;
import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.persistence.PersistenceManager;
import de.digitalpenguin.pokeapp.persistence.QueryBuilder;
import de.digitalpenguin.pokeapp.util.DataUtil;

public class SQLiteDataProvider extends AbstractDataProvider {

    private static final String NAMES = "names";

    private PersistenceManager persistenceManager;

    public SQLiteDataProvider(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

    private QueryBuilder<Pokemon> pokemonBuilder() {
        return QueryBuilder.forType(Pokemon.class);
    }

    @Override
    public List<PProperty> getDistinctProperties(TeamMember teamMember) {
        return super.getDistinctProperties(teamMember);
    }

    private QueryBuilder<MoveLearned> movesLearnedBuilder() {
        return QueryBuilder.forType(MoveLearned.class);
    }

    @Override
    public Pokemon getById(int pokemonId) {
        return persistenceManager.load(Pokemon.class, pokemonId);
    }

    @Override
    public Pokemon getRandomPokemon(Integer generation) {
        String sql = "SELECT p.id, p.names, p.properties, p.generation \n" +
                "FROM POKEMON AS p WHERE p.id = (" +
                "   SELECT ABS(RANDOM()) % " +
                "   ((SELECT MAX(p2.id) FROM POKEMON AS p2 WHERE p2.generation {0}) - " +
                "    (SELECT MIN(p1.id) FROM POKEMON AS p1 WHERE p1.generation {1})) + " +
                "   (SELECT MIN(p3.id) FROM POKEMON AS p3 WHERE p3.generation {2})" +
                ")";
        String append = generation != null ? ("= " + generation) : "IS NOT NULL";
        sql = MessageFormat.format(sql, append, append, append);
        return persistenceManager.loadList(Pokemon.class, sql).iterator().next();
    }

    @Override
    public PokemonData getPokemonData(int pokemonId) {
        return persistenceManager.load(PokemonData.class, pokemonId);
    }

    @Override
    public PokemonData getPokemonData(Pokemon p) {
        if (p == null)
            return null;
        return getPokemonData(p.getId());
    }

    @Override
    public List<Pokemon> getPokemonEntries() {
        return persistenceManager.loadList(Pokemon.class, pokemonBuilder());
    }

    @Override
    public List<Variant> getForms(int pokemonId) {
        return persistenceManager.loadList(Variant.class,
                QueryBuilder.forType(Variant.class)
                    .eq("pokemon_id", pokemonId)
                    .orderBy("id"));
    }

    @Override
    public PokemonForm getPokemonForm(int pid, int variantIdx) {
        String cacheKey = buildKey("getPokemonForm", pid, variantIdx);
        if (isCached(cacheKey))
            return (PokemonForm) getCached(cacheKey);
        PokemonForm form = PokemonForm.newDefaultForm(getById(pid));
        if (variantIdx >= 0) {
            List<PokemonForm> forms = persistenceManager.loadList(PokemonForm.class, QueryBuilder.forType(PokemonForm.class).eq("pokemon_id", pid).orderBy("id"));
            if (!forms.isEmpty()) {
                form = forms.get(variantIdx);
            }
        }
        return (PokemonForm) cache(cacheKey, form);
    }

    @Override
    public PokemonForm getPokemonForm(int pid, VariantType form) {
        List<PokemonForm> forms = persistenceManager.loadList(PokemonForm.class,
                QueryBuilder.forType(PokemonForm.class)
                        .eq("type", form.name())
                        .eq("pokemon_id", pid));
        if (!forms.isEmpty()) {
            return forms.iterator().next();
        }
        return PokemonForm.newDefaultForm(getById(pid));
    }

    @Override
    public List<Pokemon> findPokemon(String name) {
        if (StringUtils.isNumeric(name)) {
            return persistenceManager.loadList(Pokemon.class,
                    pokemonBuilder().eq("id", Integer.valueOf(name)));
        } else {
            return persistenceManager.loadList(Pokemon.class,
                    pokemonBuilder().likeIgnoreCase(NAMES, getNamesLikeClause(name)));
        }
    }

    @Override
    public List<PokemonForm> findPokemonForms(String name) {
        List<PokemonForm> result = new LinkedList<>();
        String sqlTemplate = "SELECT v.* FROM VARIANT AS v WHERE v.type not in ('DEFAULT','GIGANTAMAX') AND (" +
                "lower(v.names) LIKE '%" + getNamesLikeClause(name.toLowerCase()) + "%' " +
                "OR (v.type in ('ALOLAN','GALARIAN') AND v.type LIKE '" + name.toUpperCase() + "%') " +
                "OR EXISTS (SELECT 1 FROM POKEMON AS p WHERE p.id = v.pokemon_id AND lower(p.names) LIKE '%" + getNamesLikeClause(name).toLowerCase() + "%'))";
        result.addAll(persistenceManager.loadList(PokemonForm.class, sqlTemplate));
        Collections.sort(result, PokemonForm.compartor);
        return result;
    }

    @Override
    public List<Pokemon> findPokemon(DataFilter filter) {
        QueryBuilder<Pokemon> builder = QueryBuilder.forType(Pokemon.class, "p");
        // filter name
        if (StringUtils.isNotEmpty(filter.getName())) {
            builder.likeIgnoreCase("names", getNamesLikeClause(filter.getName()));
        }
        // filter property
        if (filter.getProperty() != null) {
            builder.like("properties", filter.getProperty().getName());
        }
        // filter by region
        if (filter.getRegion() != null) {
            // filter.getRegion().getPokedex().containsNationalDexId(p.getId())
            builder.addExists(String.format(Locale.getDefault(), "SELECT 1 FROM POKEDEX AS pd, POKEDEX_ENTRY AS pe WHERE pd.id = pe.pokedex_id AND pd.region = %d AND pe.national_dex_id = p.id", filter.getRegion().getId()));
        }
        return persistenceManager.loadList(Pokemon.class, builder);
    }

    @Override
    public Move getMove(int moveId) {
        String cacheKey = buildKey("getMove", moveId);
        if (isCached(cacheKey))
            return (Move) getCached(cacheKey);
        return (Move) cache(cacheKey, persistenceManager.load(Move.class, moveId));
    }

    @Override
    public Move getMoveByName(String name, Locale locale) {
        return persistenceManager.loadList(Move.class,
                QueryBuilder.forType(Move.class)
                    .likeIgnoreCase("names", getNamesLikeClause(name, locale))).iterator().next();
    }

    @Override
    public List<Move> getMoves() {
        return persistenceManager.loadList(Move.class, QueryBuilder.forType(Move.class));
    }

    @Override
    public List<Move> findMoves(String name) {
        return persistenceManager.loadList(Move.class,
                QueryBuilder.forType(Move.class)
                        .likeIgnoreCase("names", getNamesLikeClause(name)));
    }

    @Override
    public List<Move> listMoves(MoveType moveType, PProperty property) {
        QueryBuilder<Move> builder = QueryBuilder.forType(Move.class);
        if (moveType != null)
            builder.eq("type", moveType);
        if (property != null)
            builder.eq("property", property);
        return persistenceManager.loadList(Move.class, builder);
    }

    @Override
    public List<MachineProperty> listMachineProperties() {
        return persistenceManager.loadList(MachineProperty.class, QueryBuilder.forType(MachineProperty.class));
    }

    @Override
    public Move getMoveForMachine(String machineName) {
        String sql = "SELECT m.* FROM MOVE AS m WHERE m.id = (" +
                "SELECT DISTINCT move_id FROM MOVE_LEARNED WHERE machine = '" + machineName + "' " +
                "ORDER BY generation DESC LIMIT 1)";
        List<Move> moves = persistenceManager.loadList(Move.class, sql);
        if (moves.isEmpty()) {
            sql = "SELECT m.* FROM MOVE AS m, HIDDEN_MACHINE AS hm " +
                    "WHERE hm.names LIKE '%" + getNamesLikeClause(machineName, Locale.ENGLISH) + "%' " +
                    "AND hm.move_id = m.id " +
                    //"AND m.generation = hm.generation " +
                    "ORDER BY m.generation ASC LIMIT 1";
            moves = persistenceManager.loadList(Move.class, sql);
        }
        return moves.isEmpty() ? null : moves.iterator().next();
    }

    @Override
    public Map<Integer, List<MoveLearned>> getGenerationsMoveSet(PokemonForm form) {
        List<MoveLearned> allMoves = listMovesLearned(form);
        return DataUtil.toMap(allMoves, new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((MoveLearned) input).getGeneration();
            }
        });
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm form, Integer generation, MoveLearnTrigger trigger) {
        return getGenerationsMoveList(form, generation, null, trigger);
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm form, Integer generation, String gamesId) {
        return getGenerationsMoveList(form, generation, gamesId);
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm form, Integer generation, String gameId, MoveLearnTrigger trigger) {
        QueryBuilder<MoveLearned> builder = movesLearnedBuilder()
                .eq("pokemon_id", form.getPokemonId())
                .eq("generation", generation);
        if (form.isRegional()) {
            builder.eq("variant_id", form.getId());
        } else {
            builder.isNull("variant_id");
        }
        if (StringUtils.isNotEmpty(gameId))
            builder.like("games", gameId.toUpperCase());
        if (trigger != null)
            builder.eq("trigger", trigger);
        builder.orderBy("generation", "trigger");
        return persistenceManager.loadList(MoveLearned.class, builder);
    }

    @Override
    public boolean hasGameSpecificMoves(Pokemon p) {
        String sql = String.format(Locale.getDefault(), "SELECT COUNT(1) FROM MOVE_LEARNED where pokemon_id = %d AND games IS NOT NULL", p.getId());
        return persistenceManager.count(sql, new String[0]) > 0;
    }

    @Override
    public List<MoveLearned> listMovesLearned(PokemonForm form) {
        QueryBuilder<MoveLearned> builder = movesLearnedBuilder().eq("pokemon_id", form.getPokemonId());
        if (form.isRegional()) {
            builder.eq("variant_id", form.getId());
        } else {
            builder.isNull("variant_id");
        }
        List<MoveLearned> result = persistenceManager.loadList(MoveLearned.class, builder);
        // Workaround ALOLA Moves = Default Moves
        if (result.isEmpty() && form.getType() == VariantType.ALOLAN)
            result = listMovesLearned(PokemonForm.newDefaultForm(form.getPokemon()));
        return result;
    }

    @Override
    public List<EvolutionChain> getEvolutionChains() {
        return persistenceManager.loadList(EvolutionChain.class, QueryBuilder.forType(EvolutionChain.class));
    }

    @Override
    public EvolutionChain getEvolutionChain(int pId) {
        String sql = String.format(Locale.getDefault(), "SELECT ec.id FROM EVOLUTION_CHAIN AS ec " +
                "WHERE ec.id IN (SELECT ecl.chain FROM EVOLUTION_CHAIN_LINK AS ecl WHERE ecl.pokemon_from = %d OR ecl.pokemon_to = %d)", pId, pId);
        List<EvolutionChain> evolutionChains = persistenceManager.loadList(EvolutionChain.class, sql);
        return evolutionChains.isEmpty() ? null : evolutionChains.iterator().next();
    }

    @Override
    public List<Item> getItems() {
        return persistenceManager.loadList(Item.class, QueryBuilder.forType(Item.class));
    }

    @Override
    public List<Item> listItems(ItemCategory category) {
        return persistenceManager.loadList(Item.class, QueryBuilder.forType(Item.class).eq("category", category));
    }

    @Override
    public List<Item> findItems(final String name) {
        List<Item> result = persistenceManager.loadList(Item.class, QueryBuilder.forType(Item.class).likeIgnoreCase(NAMES, Locale.getDefault().getLanguage().concat("=%").concat(name)));
        CollectionUtils.filter(result, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                Item i = (Item) object;
                return i.getName().toLowerCase().contains(name.toLowerCase());
            }
        });
        return result;
    }

    @Override
    public List<Generation> getGenerations() {
        return persistenceManager.loadList(Generation.class, QueryBuilder.forType(Generation.class));
    }

    @Override
    public List<Region> getRegions() {
        return persistenceManager.loadList(Region.class, QueryBuilder.forType(Region.class));
    }

    @Override
    public Region getRegion(Integer generation) {
        return persistenceManager.loadList(Region.class, QueryBuilder.forType(Region.class).eq("generation", generation)).iterator().next();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Game> getGames() {
        String key = buildKey("getGames");
        if (isCached(key))
            return (List<Game>) getCached(key);
        return (List<Game>) cache(key, persistenceManager.loadList(Game.class, QueryBuilder.forType(Game.class)));
    }

    @Override
    public List<Characteristic> getCharacteristics() {
        return persistenceManager.loadList(Characteristic.class, QueryBuilder.forType(Characteristic.class));
    }

    @Override
    public List<Characteristic> findCharacteristics(String name) {
        return persistenceManager.loadList(Characteristic.class, QueryBuilder.forType(Characteristic.class).likeIgnoreCase(NAMES, getNamesLikeClause(name)));
    }

    @Override
    public List<Nature> getNatures() {
        return persistenceManager.loadList(Nature.class, QueryBuilder.forType(Nature.class));
    }

    @Override
    public List<Nature> findNatures(String name) {
        return persistenceManager.loadList(Nature.class, QueryBuilder.forType(Nature.class).likeIgnoreCase(NAMES, getNamesLikeClause(name)));
    }

    @Override
    public Ability getAbility(int id) {
        return persistenceManager.load(Ability.class, id);
    }

    @Override
    public List<Ability> getAbilities() {
        List<Ability> abilities = persistenceManager.loadList(Ability.class, QueryBuilder.forType(Ability.class));
        Collections.sort(abilities, new NamedComparator());
        return abilities;
    }

    @Override
    public List<Ability> findAbilities(String name) {
        return persistenceManager.loadList(Ability.class,
                QueryBuilder.forType(Ability.class)
                        .likeIgnoreCase("names", getNamesLikeClause(name)));
    }

    @Override
    public List<MoveLearned> listPokemonMovesLearned(int moveId) {
        String sql = MessageFormat.format("SELECT move_id, pokemon_id " +
                "FROM MOVE_LEARNED AS ml WHERE move_id = {0} " +
                "GROUP BY move_id, pokemon_id ORDER BY pokemon_id", moveId);
        return persistenceManager.loadList(MoveLearned.class, sql);
    }

    @Override
    public List<PokemonAbility> listPokemonWithAbility(int abilityId) {
        List<PokemonAbility> result = persistenceManager.loadList(PokemonAbility.class, QueryBuilder.forType(PokemonAbility.class).eq("ability_id", abilityId));
        final Comparator<Pokemon> comp = new PokemonComparator(PokemonComparator.SORT_ID);
        Collections.sort(result, new Comparator<PokemonAbility>() {
            @Override
            public int compare(PokemonAbility o1, PokemonAbility o2) {
                Pokemon p1 = o1.getPokemon();
                Pokemon p2 = o2.getPokemon();
                return comp.compare(p1, p2);
            }
        });
        return result;
    }

    @Override
    public List<PokemonAbility> listAbilities(int pokemonId, VariantType type) {
        String sql = "SELECT pa.* FROM POKEMON_ABILITY AS pa WHERE pokemon_id = " + pokemonId;
        if (type == VariantType.GALARIAN || type == VariantType.ALOLAN || type == VariantType.MEGA) {
            sql = sql + " AND pa.variant_id in (SELECT v.id FROM VARIANT AS v WHERE v.pokemon_id = " + pokemonId + " AND v.type = '" + type.name() + "')";
        } else {
            sql = sql + " AND pa.variant_id IS NULL";
        }
        sql += " ORDER BY pa.hidden, pa.id";
        return persistenceManager.loadList(PokemonAbility.class, sql);
    }

    @Override
    public boolean hasMoves(PokemonForm form, int generation, String gameKey) {
        List<String> params = new LinkedList<>();
        params.add(Integer.toString(form.getPokemonId()));
        params.add(Integer.toString(generation));
        String sql = "SELECT COUNT(1) FROM MOVE_LEARNED WHERE pokemon_id = ? AND generation = ?";
        if (form.isDefault()) {
            sql = sql.concat(" AND variant_id IS NULL");
        } else {
            sql = sql.concat(" AND variant_id = ?");
            params.add(Integer.toString(form.getId()));
        }
        if (StringUtils.isNotEmpty(gameKey)) {
            sql = sql.concat(" AND games LIKE '%" + gameKey + "%'");
        }
        return persistenceManager.count(sql, params.toArray(new String[params.size()])) > 0;
    }

    @Override
    public int getPokemonEntriesCount() {
        return persistenceManager.count("SELECT COUNT(1) FROM POKEMON", new String[0]);
    }

    @Override
    public Item getItemByName(String value, Locale locale) {
        List<Item> items = persistenceManager.loadList(Item.class,
                QueryBuilder.forType(Item.class)
                        .likeIgnoreCase("names", getNamesLikeClause(value, locale)));
        return items.isEmpty() ? null : items.iterator().next();
    }

    private String getNamesLikeClause(String name) {
        return String.format("%s=%s", Locale.getDefault().getLanguage(), name);
    }

    private String getNamesLikeClause(String name, Locale locale) {
        return String.format("%s=%s", locale.getLanguage(), name);
    }
}
