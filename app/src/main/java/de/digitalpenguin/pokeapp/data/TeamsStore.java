package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import de.digitalpenguin.pokeapp.data.util.TeamsProvider;

public class TeamsStore implements TeamsProvider {

    private static TeamsProvider instance;

    private List<Team> teamEntries;

    public TeamsStore(List<Team> teamEntries) {
        this.teamEntries = teamEntries;
    }

    public static TeamsProvider getInstance() {
        return instance;
    }

    public static void setInstance(TeamsProvider provider) {
        instance = provider;
    }

    @Override
    public List<Team> getTeamEntries() {
        if (teamEntries == null)
            teamEntries = new ArrayList<>();
        return teamEntries;
    }

    @Override
    public boolean deleteTeam(Team team) {
        return this.teamEntries.remove(team);
    }

    public void setTeamEntries(List<Team> teamEntries)
    {
        this.teamEntries = teamEntries;
    }

    @Override
    public Team getTeam(final String teamId)
    {
        if (StringUtils.isEmpty(teamId))
            return null;
        return (Team) CollectionUtils.find(getTeamEntries(), new Predicate() {
            @Override
            public boolean evaluate(Object o)
            {
                Team t = (Team) o;
                return teamId.equalsIgnoreCase(t.getId());
            }
        });
    }

    @Override
    public List<Team> getRecentTeams(int max) {
        List<Team> teams = new ArrayList<>(max);
        List<Team> allTeams = getTeamEntries();
        if (!allTeams.isEmpty()) {
            Collections.sort(allTeams, new Comparator<Team>() {
                @Override
                public int compare(Team t1, Team t2) {
                    Date d1 = new Date(t1.getLastModified());
                    Date d2 = new Date(t2.getLastModified());
                    return d1.compareTo(d2);
                }
            });
            Collections.reverse(allTeams);
        }
        Iterator<Team> it = allTeams.iterator();
        while (teams.size() < max && it.hasNext()) {
            teams.add(it.next());
        }
        return teams;
    }

    @Override
    public Team getTeamByName(CharSequence teamName) {
        for (Team t: getTeamEntries()) {
            if (teamName.toString().equals(t.getName())) {
                return t;
            }
        }
        return null;
    }

}
