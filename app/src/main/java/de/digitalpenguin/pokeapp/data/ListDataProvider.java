package de.digitalpenguin.pokeapp.data;

import java.util.List;

public interface ListDataProvider<T> {

    List<T> getData();

}
