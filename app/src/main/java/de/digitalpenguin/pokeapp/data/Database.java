package de.digitalpenguin.pokeapp.data;

import android.database.Cursor;
import android.database.SQLException;

/**
 * Database abstraction used internally by greenDAO.
 */
public interface Database {
    Cursor rawQuery(String sql, String[] selectionArgs);

    void execSQL(String sql) throws SQLException;

    void beginTransaction();

    void endTransaction();

    boolean inTransaction();

    void setTransactionSuccessful();

    void execSQL(String sql, Object[] bindArgs) throws SQLException;

    boolean isDbLockedByCurrentThread();

    boolean isOpen();

    void close();

    Object getRawDatabase();
}
