package de.digitalpenguin.pokeapp.dialog;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.PropertyCounterPokemonListAdapter;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.PropertyRelation;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class PropertyCounterInfoDialog extends AlertDialog {

    private final List<TeamMember> counters;
    private final PProperty property;
    private final boolean defensive;
    private DataProvider dataProvider;

    public PropertyCounterInfoDialog(@NonNull Context context, DataProvider dataProvider, PProperty prop, List<TeamMember> counters, boolean defensive) {
        super(context);
        this.property = prop;
        this.counters = counters;
        this.defensive = defensive;
        this.dataProvider = dataProvider;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.property_counter_details_dialog);

        TextView heading = findViewById(R.id.teamPerfCounterPopupTitle);
        heading.setText(getContext().getString(defensive ? R.string.team_perf_counter_popup_title_defence : R.string.team_perf_counter_popup_title));

        ListView listView = findViewById(R.id.propertyCounterPokemonListView);
        PropertyRelation.RelationType relationType = defensive ? PropertyRelation.RelationType.BEAT_UP_BY : PropertyRelation.RelationType.STRONG_AGAINST;
        final PropertyCounterPokemonListAdapter adapter = new PropertyCounterPokemonListAdapter(getContext(), property, counters, Boolean.FALSE, relationType, dataProvider);
        listView.setAdapter(adapter);
    }
}
