package de.digitalpenguin.pokeapp.dialog;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.util.Callback;
import de.digitalpenguin.pokeapp.util.TeamNameValidator;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class TeamRenameDialog extends AlertDialog {

    private AppCompatButton buttonConfirm;

    private final Team team;

    private final View.OnClickListener positiveButtonListener;

    public TeamRenameDialog(@NonNull Context context, Team team, View.OnClickListener positiveButtonListener) {
        super(context);
        this.team = team;
        this.positiveButtonListener = positiveButtonListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_rename_team);

        buttonConfirm = findViewById(R.id.changeIconButtonOK);
        toggleConfirmBtn(false);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // save immediate
                XMLHelper.persistTeams(getContext());
                TeamRenameDialog.this.dismiss();
                positiveButtonListener.onClick(buttonConfirm);
            }
        });
        AppCompatButton buttonCancel = findViewById(R.id.changeIconButtonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeamRenameDialog.this.dismiss();
            }
        });

        EditText editNameText = findViewById(R.id.newTeamNameInput);
        final TeamNameValidator validator = new TeamNameValidator(getContext().getResources(), editNameText, team, new Callback() {
            @Override
            public void notify(Object o) {
                toggleConfirmBtn((Boolean) o);
            }
        });
        editNameText.addTextChangedListener(validator);
    }

    private void toggleConfirmBtn(boolean enable) {
        buttonConfirm.setEnabled(enable);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            buttonConfirm.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(enable ? R.color.colorPrimary : R.color.buttonBGDefault)));
        } else {
            buttonConfirm.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
        }
        buttonConfirm.setTextColor(getContext().getResources().getColor(enable ? R.color.colorWhite : R.color.darkerGrey));
    }

}
