package de.digitalpenguin.pokeapp.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.adapter.GenericListAdapter;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class TeamSizeDialog extends AlertDialog {

    private AppCompatButton buttonConfirm;

    private final Team team;

    private final View.OnClickListener positiveButtonListener;

    private AppCompatSpinner sizeSpinner;

    public TeamSizeDialog(@NonNull Context context, Team team, View.OnClickListener positiveButtonListener) {
        super(context);
        this.team = team;
        this.positiveButtonListener = positiveButtonListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_teamsize);

        buttonConfirm = findViewById(R.id.changeIconButtonOK);
        toggleConfirmBtn(false);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    saveAndExit((Integer) sizeSpinner.getSelectedItem(), false);
                }
            }
        });
        AppCompatButton buttonCancel = findViewById(R.id.changeIconButtonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeamSizeDialog.this.dismiss();
            }
        });

        sizeSpinner = findViewById(R.id.teamSizeSpinner);
        List<Integer> values = Arrays.asList(6, 7, 8, 9, 10, 11, 12);
        final GenericListAdapter<Integer> adapter = new GenericListAdapter<Integer>(getContext(), values, R.layout.spinner_layout_default) {
            @Override
            protected void initView(Integer item, View view) {
                TextView text = view.findViewById(R.id.txt);
                text.setGravity(Gravity.END);
                text.setText(Integer.toString(item));
            }
        };
        sizeSpinner.setAdapter(adapter);
        sizeSpinner.setSelection(values.indexOf(team.getTeamSizeMax()));
        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Integer itemAtPosition = adapter.getItemAtPosition(position);
                toggleConfirmBtn(itemAtPosition >= Team.TEAM_MEMBERS_DEFAULT);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void saveAndExit(Integer newSize, boolean trimMembers) {
        team.setTeamSizeMax(newSize);
        List<TeamMember> newMembersList = new ArrayList<>(newSize);
        List<TeamMember> oldMembersList = team.getMembers();
        int i = 0;
        while (oldMembersList.size() > i && newMembersList.size() < newSize) {
            TeamMember tm = oldMembersList.get(i);
            if (tm != null) {
                tm.setSlot(i+1);
                newMembersList.add(tm);
            }
            i++;
        }
        team.setMembers(newMembersList);
        // save immediate
        XMLHelper.persistTeams(getContext());
        TeamSizeDialog.this.dismiss();
        positiveButtonListener.onClick(buttonConfirm);
    }

    private boolean validate() {
        Integer selection = (Integer) sizeSpinner.getSelectedItem();
        if (selection < team.getSize()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getContext().getResources().getString(R.string.team_size_confirm_shrink, selection))
                    .setPositiveButton(R.string.common_save, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            saveAndExit((Integer) sizeSpinner.getSelectedItem(), true);
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton(R.string.common_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            // Create the AlertDialog object and return it
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return false;
        }
        return true;
    }

    private void toggleConfirmBtn(boolean enable) {
        buttonConfirm.setEnabled(enable);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            buttonConfirm.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(enable ? R.color.colorPrimary : R.color.buttonBGDefault)));
        } else {
            buttonConfirm.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
        }
        buttonConfirm.setTextColor(getContext().getResources().getColor(enable ? R.color.colorWhite : R.color.darkerGrey));
    }

}
