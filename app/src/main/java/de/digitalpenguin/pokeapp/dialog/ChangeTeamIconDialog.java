package de.digitalpenguin.pokeapp.dialog;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.flexbox.FlexboxLayout;

import de.digitalpenguin.pokeapp.R;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.util.UIHelper;
import de.digitalpenguin.pokeapp.util.XMLHelper;

public class ChangeTeamIconDialog extends AlertDialog implements View.OnClickListener {

    private FlexboxLayout container;

    private AppCompatButton buttonConfirm;

    private final Team team;

    private String selected;

    public ChangeTeamIconDialog(@NonNull Context context, Team team) {
        super(context);
        this.team = team;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_icon);
        container = findViewById(R.id.changeIconIconsContainer);
        container.removeAllViews();
        for (int i = 0; i < ICON_NAMES.length ; i++) {
            ImageView iv = new ImageView(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    UIHelper.dpToPx(getContext(), 84),
                    UIHelper.dpToPx(getContext(), 84));
            int pad10dp = UIHelper.dpToPx(getContext(), 10);
            iv.setLayoutParams(layoutParams);
            int resId = UIHelper.getDrawableResourceId(getContext(), ICON_NAMES[i]);
            iv.setImageResource(resId);
            iv.setPadding(pad10dp, pad10dp, pad10dp, pad10dp);
            iv.setOnClickListener(this);
            iv.setTag(ICON_NAMES[i]);
            container.addView(iv);
        }

        buttonConfirm = findViewById(R.id.changeIconButtonOK);
        toggleConfirmBtn(false);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                team.setIcon(selected);
                // save immediate
                XMLHelper.persistTeams(getContext(), null);
                ChangeTeamIconDialog.this.dismiss();
            }
        });
        AppCompatButton buttonCancel = findViewById(R.id.changeIconButtonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeTeamIconDialog.this.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        selected = (String) view.getTag();
        int bgClear = Color.parseColor("#80FFFFFF");
        for (int i = 0; i < container.getChildCount(); i++) {
            View icon = container.getChildAt(i);
            icon.setBackgroundColor(bgClear);
        }
        toggleConfirmBtn(true);
        view.setBackgroundColor(getContext().getResources().getColor(R.color.multiSelectionBg));
    }

    private void toggleConfirmBtn(boolean enable) {
        buttonConfirm.setEnabled(enable);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            buttonConfirm.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(enable ? R.color.colorPrimary : R.color.buttonBGDefault)));
        } else {
            buttonConfirm.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
        }
        buttonConfirm.setTextColor(getContext().getResources().getColor(enable ? R.color.colorWhite : R.color.darkerGrey));
    }

    private static final String[] ICON_NAMES = new String[]{
            "icons8_swatch_48",
            "icons8_love_96",
            "icons8_skull_96",
            // meteorological
            "icons8_sun_96",
            "icons8_rainbow_96",
            "icons8_winter_96",
            "icons8_star_filled_96",
            "icons8_moon_and_stars_96",
            // pokemon
            "icons8_pokeball",
            "icons8_star_pokemon_96",
            "icons8_egg_pokemon_96",
            "icons8_bullbasaur_96",
            "icons8_charmander_96",
            "icons8_squirtle_96",
            "icons8_pikachu_pokemon_96",
            // plants
            "icons8_apple_96",
            "icons8_leaf_96",
            "icons8_three_leaf_clover_96",
            "icons8_natural_food_96",
            "icons8_flower_96",
            "icons8_lotus_96",
            "icons8_autumn_96",
            "icons8_razz_berry_96",
            "icons8_sakura_96",
            // properties
            "prop_normal",
            "prop_grass",
            "prop_fire",
            "prop_water",
            "prop_poison",
            "prop_electric",
            "prop_flying",
            "prop_ground",
            "prop_fighting",
            "prop_rock",
            "prop_psychic",
            "prop_bug",
            "prop_ice",
            "prop_ghost",
            "prop_dragon",
            "prop_dark",
            "prop_steel",
            "prop_fairy"
    };
}
