package de.digitalpenguin.pokeapp;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import de.digitalpenguin.pokeapp.data.SQLiteDataProvider;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.persistence.PersistenceManager;
import de.digitalpenguin.pokeapp.util.ILogger;

public class PersistenceActivityBase extends AppCompatActivity implements ILogger {

    private PersistenceManager persistenceManager;

    private SQLiteDataProvider dataProvider;

    public PersistenceManager getPersistenceManager() {
        if (persistenceManager == null) {
            initPersistenceProvider();
        }
        return persistenceManager;
    }

    private void initPersistenceProvider() {
        persistenceManager = new PersistenceManager(this, this);
        Log.i("DEBUG", "PersistenceManager Created");
        dataProvider = new SQLiteDataProvider(persistenceManager);
    }

    @Override
    public void log(String message) {
        Log.i("PERSISTENCE", message);
    }

    @Override
    public void error(String message) {
        Log.e("PERSISTENCE", message);
    }

    public DataProvider getDataProvider() {
        if (persistenceManager == null || persistenceManager.getDatabase() == null || !persistenceManager.getDatabase().isOpen())
            initPersistenceProvider();
        return this.dataProvider;
    }
}
