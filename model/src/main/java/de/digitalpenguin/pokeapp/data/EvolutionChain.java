package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.BaseIdEntity;
import de.digitalpenguin.pokeapp.persistence.Cache;

@Entity
@Table(name = "EVOLUTION_CHAIN")
@Cache
public class EvolutionChain extends BaseIdEntity
{
	private static final long serialVersionUID = 3050871932686111364L;

	@OneToMany(mappedBy = "chainId", targetEntity = EvolutionChainLink.class, cascade = CascadeType.ALL)
	private List<EvolutionChainLink> chainLinks;

	public EvolutionChain()
	{
		//
	}

	public EvolutionChain(List<EvolutionChainLink> links)
	{
		this.chainLinks = links;
	}

	public List<EvolutionChainLink> getChainLinks() {
		return chainLinks;
	}

	public void setChainLinks(List<EvolutionChainLink> chainLinks) {
		this.chainLinks = chainLinks;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(chainLinks).toString();
	}
}
