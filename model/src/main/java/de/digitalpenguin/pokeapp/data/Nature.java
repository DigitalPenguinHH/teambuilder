package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
public class Nature extends LocalizedNamed implements Identifiable {

    private static final long serialVersionUID = 8919453734946462074L;

    @Id
    private int id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names = new LocalizedText();

    @Column(name = "increased")
    @Converter(converterClass = StatType.StatTypeConverter.class, columnType = String.class)
	private StatType increased;

    @Column(name = "decreased")
    @Converter(converterClass = StatType.StatTypeConverter.class, columnType = String.class)
    private StatType decreased;


    public Nature() {
        super();
    }

    public Nature(StatType increased, StatType decreased) {
        this.increased = increased;
        this.decreased = decreased;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    public StatType getIncreased() {
        return increased;
    }

    public StatType getDecreased() {
        return decreased;
    }

    public void setIncreased(StatType increased) {
        this.increased = increased;
    }

    public void setDecreased(StatType decreased) {
        this.decreased = decreased;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
