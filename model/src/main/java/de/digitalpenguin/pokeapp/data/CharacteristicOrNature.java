package de.digitalpenguin.pokeapp.data;

public class CharacteristicOrNature extends NamedObject {

    private static final long serialVersionUID = -7666791562864743581L;
	private final Characteristic characteristic;
    private final Nature nature;

    public CharacteristicOrNature(Characteristic value) {
        this.characteristic = value;
        this.nature = null;
    }

    public CharacteristicOrNature(Nature value) {
        this.characteristic = null;
        this.nature = value;
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public Nature getNature() {
        return nature;
    }

    public boolean isNature() {
        return nature != null;
    }

    public boolean isCharacteristic() {
        return characteristic != null;
    }
}
