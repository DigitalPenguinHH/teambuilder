package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;
import java.text.Normalizer;
import java.util.regex.Pattern;

import de.digitalpenguin.pokeapp.data.util.Named;

public class NamedObject implements Named, Comparable<NamedObject>, Serializable {

    private static final long serialVersionUID = 3070664889457287885L;

    private String name;

    public NamedObject() {
    }

    public NamedObject(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(NamedObject o) {
        if (o == null)
            return 1;
        if (!(o instanceof NamedObject))
            return 1;
        NamedObject no = (NamedObject) o;
        return getNormalizedName().compareTo(no.getNormalizedName());
    }

    private String getNormalizedName() {
        String nfdNormalizedString = Normalizer.normalize(getName(), Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }
}
