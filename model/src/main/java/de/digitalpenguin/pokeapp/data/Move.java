package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;
import de.digitalpenguin.pokeapp.persistence.PPropertyConverter;

@Entity
@Cache
public class Move extends LocalizedNamed implements Identifiable {

	private static final long serialVersionUID = 3170569303590580139L;

	public static final String TAG_ID = "id";

	@Id
	private int id;

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText names = new LocalizedText();

	@Converter(converterClass = PPropertyConverter.class, columnType = String.class)
	private PProperty property;

	@Converter(converterClass = MoveType.MoveTypeConverter.class, columnType = String.class)
	private MoveType type;

	private Integer strength;

	private boolean damageFix = false;

	private boolean damageVariable = false;

	@Converter(converterClass = DamageVarianceModifier.DamageVarianceModifierConverter.class, columnType = String.class)
	private DamageVarianceModifier damageVarianceModifier;

	private Integer accuracy;

	private Integer ap;

	@Converter(converterClass = ContestCategory.ContestCategoryConverter.class, columnType = String.class)
	private ContestCategory contestCategory;

	private int generation;

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText descriptions = new LocalizedText();
	

	public Move()
	{
		//
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public LocalizedText getNames() {
		return names;
	}

	@Override
	public void setNames(LocalizedText names) {
		this.names = names;
	}

	public PProperty getProperty() {
		return property;
	}

	public void setProperty(PProperty property) {
		this.property = property;
	}

	public MoveType getType() {
		return type;
	}

	public void setType(MoveType type) {
		this.type = type;
	}

	public Integer getStrength() {
		return strength;
	}

	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Integer getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Integer accuracy) {
		this.accuracy = accuracy;
	}

	public Integer getAp() {
		return ap;
	}

	public void setAp(Integer ap) {
		this.ap = ap;
	}

	public ContestCategory getContestCategory() {
		return contestCategory;
	}

	public void setContestCategory(ContestCategory contestCategory) {
		this.contestCategory = contestCategory;
	}

	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}

	public boolean isDamageFix() {
		return damageFix;
	}

	public void setDamageFix(boolean damageFix) {
		this.damageFix = damageFix;
	}

	public boolean isDamageVariable() {
		return damageVariable;
	}

	public void setDamageVariable(boolean damageVariable) {
		this.damageVariable = damageVariable;
	}

	public DamageVarianceModifier getDamageVarianceModifier() {
		return damageVarianceModifier;
	}

	public void setDamageVarianceModifier(DamageVarianceModifier damageVarianceModifier) {
		this.damageVarianceModifier = damageVarianceModifier;
	}

	public LocalizedText getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(LocalizedText descriptions) {
		this.descriptions = descriptions;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append(id)
				.append(getNames())
				.append(type)
				.toString();
	}

    public String getDisplayStrength() {
        return strength == null || strength <= 0 ? "-" : strength.toString();
    }

    public String getDisplayAccuracy() {
        return accuracy == null || accuracy <= 0 ? "-" : String.format("%s %%", accuracy.toString());
    }
    
    public boolean doesDamage() {
		return strength != null && strength > 0;
	}

	public boolean getDamageFix() {
		return this.damageFix;
	}

	public boolean getDamageVariable() {
		return this.damageVariable;
	}

	@Override
	public Serializable getIdentifier() {
		return getId();
	}
}
