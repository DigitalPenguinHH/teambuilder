package de.digitalpenguin.pokeapp.data.provider;

import java.util.List;

import de.digitalpenguin.pokeapp.data.Game;

public interface GameProvider {

    List<Game> getGames();

    Game getGameByName(String name);
}
