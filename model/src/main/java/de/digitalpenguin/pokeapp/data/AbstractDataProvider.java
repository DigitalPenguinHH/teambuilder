package de.digitalpenguin.pokeapp.data;

import android.util.SparseArray;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.digitalpenguin.pokeapp.comparator.NamedLexicalComparator;
import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.util.DataUtil;

import static de.digitalpenguin.pokeapp.data.PropertyRelation.RelationType.BEAT_UP_BY;
import static de.digitalpenguin.pokeapp.data.PropertyRelation.RelationType.NO_EFFECT_ON;
import static de.digitalpenguin.pokeapp.data.PropertyRelation.RelationType.STRONG_AGAINST;
import static de.digitalpenguin.pokeapp.data.PropertyRelation.RelationType.WEAK_AGAINST;

public abstract class AbstractDataProvider implements DataProvider {

	private static Map<PProperty, List<PropertyRelation>> propertyRelationsCache = new HashMap<>();

	private Map<String, Object> cache = new HashMap<>();

	protected static String buildKey(String methodName, Object ... params) {
		StringBuilder sb = new StringBuilder();
		sb.append(methodName);
		for (Object o : params) {
			sb.append(o);
			sb.append("#");
		}
		return sb.toString();
	}

	@Override
	public void init() {
		// do nothing
	}

	public <T> List<T> find(final String name)
	{
		return find(name, true, true, true, true, true, true);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> find(final String name, boolean includeForms, boolean includeCharacteristics, boolean includeNatures, boolean includeItems, boolean includeMoves, boolean includeAbilities)
	{
		List<T> result = new LinkedList<>();
		// consider Settings
		// filter name
		if (StringUtils.isNotEmpty(name)) {
			// Pokemon
			List resultsPokemon = findPokemon(name);
			Collections.sort(resultsPokemon, new PokemonComparator(PokemonComparator.SORT_ID));
			result.addAll(resultsPokemon);
			// Forms
			if (includeForms) {
				List formsResult = findPokemonForms(name);
				result.addAll(formsResult);
			}
			// Moves
			if (includeMoves) {
				List movesResult = findMoves(name);
				Collections.sort(movesResult, new NamedLexicalComparator(name));
				result.addAll(movesResult);
			}
			// Abilities
			if (includeAbilities) {
				List abilities = findAbilities(name);
				Collections.sort(abilities, new NamedLexicalComparator(name));
				result.addAll(abilities);
			}
			// Characteristics
			if (includeCharacteristics) {
				List resultCharacteristics = findCharacteristics(name);
				result.addAll(resultCharacteristics);
			}
			// Natures
			if (includeNatures) {
				List resultNatures = findNatures(name);
				result.addAll(resultNatures);
			}
			// Items
			if (includeItems) {
				List resultItems = findItems(name);
				Collections.sort(resultItems, new NamedLexicalComparator(name));
				result.addAll(resultItems);
			}
		}
		return result;
	}

	public List<Pokemon> find(final DataFilter filter)
	{
		List<Pokemon> result = findPokemon(filter);
		Collections.sort(result, new PokemonComparator(PokemonComparator.SORT_ID));
		return result;
	}

    static {
    	// Normal
		propertyRelationsCache.put(PProperty.NORMAL, Arrays.asList(
				new PropertyRelation(PProperty.ROCK, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, NO_EFFECT_ON),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIGHTING, BEAT_UP_BY)
		));
		// Fighting
		propertyRelationsCache.put(PProperty.FIGHTING, Arrays.asList(
				new PropertyRelation(PProperty.NORMAL, STRONG_AGAINST),
				new PropertyRelation(PProperty.FLYING, WEAK_AGAINST),
				new PropertyRelation(PProperty.POISON, WEAK_AGAINST),
				new PropertyRelation(PProperty.ROCK, STRONG_AGAINST),
				new PropertyRelation(PProperty.BUG, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, NO_EFFECT_ON),
				new PropertyRelation(PProperty.STEEL, STRONG_AGAINST),
				new PropertyRelation(PProperty.PSYCHIC, WEAK_AGAINST),
				new PropertyRelation(PProperty.ICE, STRONG_AGAINST),
				new PropertyRelation(PProperty.DARK, STRONG_AGAINST),
				new PropertyRelation(PProperty.FAIRY, WEAK_AGAINST),
				new PropertyRelation(PProperty.FLYING, BEAT_UP_BY),
				new PropertyRelation(PProperty.PSYCHIC, BEAT_UP_BY),
				new PropertyRelation(PProperty.FAIRY, BEAT_UP_BY)
		));
		// Flying
		propertyRelationsCache.put(PProperty.FLYING, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, STRONG_AGAINST),
				new PropertyRelation(PProperty.ROCK, WEAK_AGAINST),
				new PropertyRelation(PProperty.BUG, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, STRONG_AGAINST),
				new PropertyRelation(PProperty.ELECTRIC, WEAK_AGAINST),
				new PropertyRelation(PProperty.ROCK, BEAT_UP_BY),
				new PropertyRelation(PProperty.ELECTRIC, BEAT_UP_BY),
				new PropertyRelation(PProperty.ICE, BEAT_UP_BY)
		));
		// Poison
		propertyRelationsCache.put(PProperty.POISON, Arrays.asList(
				new PropertyRelation(PProperty.POISON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GROUND, WEAK_AGAINST),
				new PropertyRelation(PProperty.ROCK, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, WEAK_AGAINST),
				new PropertyRelation(PProperty.STEEL, NO_EFFECT_ON),
				new PropertyRelation(PProperty.GRASS, STRONG_AGAINST),
				new PropertyRelation(PProperty.FAIRY, STRONG_AGAINST),
				new PropertyRelation(PProperty.GROUND, BEAT_UP_BY),
				new PropertyRelation(PProperty.PSYCHIC, BEAT_UP_BY)
		));
		// Ground
		propertyRelationsCache.put(PProperty.GROUND, Arrays.asList(
				new PropertyRelation(PProperty.FLYING, NO_EFFECT_ON),
				new PropertyRelation(PProperty.POISON, STRONG_AGAINST),
				new PropertyRelation(PProperty.ROCK, STRONG_AGAINST),
				new PropertyRelation(PProperty.BUG, WEAK_AGAINST),
				new PropertyRelation(PProperty.STEEL, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIRE, STRONG_AGAINST),
				new PropertyRelation(PProperty.GRASS, WEAK_AGAINST),
				new PropertyRelation(PProperty.ELECTRIC, STRONG_AGAINST),
				new PropertyRelation(PProperty.WATER, BEAT_UP_BY),
				new PropertyRelation(PProperty.GRASS, BEAT_UP_BY),
				new PropertyRelation(PProperty.ICE, BEAT_UP_BY)
		));
		// Rock
		propertyRelationsCache.put(PProperty.ROCK, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, WEAK_AGAINST),
				new PropertyRelation(PProperty.FLYING, STRONG_AGAINST),
				new PropertyRelation(PProperty.GROUND, WEAK_AGAINST),
				new PropertyRelation(PProperty.BUG, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, STRONG_AGAINST),
				new PropertyRelation(PProperty.ICE, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIGHTING, BEAT_UP_BY),
				new PropertyRelation(PProperty.GROUND, BEAT_UP_BY),
				new PropertyRelation(PProperty.STEEL, BEAT_UP_BY),
				new PropertyRelation(PProperty.WATER, BEAT_UP_BY),
				new PropertyRelation(PProperty.GRASS, BEAT_UP_BY)
		));
		// Bug
		propertyRelationsCache.put(PProperty.BUG, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, WEAK_AGAINST),
				new PropertyRelation(PProperty.FLYING, WEAK_AGAINST),
				new PropertyRelation(PProperty.POISON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, WEAK_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, STRONG_AGAINST),
				new PropertyRelation(PProperty.PSYCHIC, STRONG_AGAINST),
				new PropertyRelation(PProperty.DARK, STRONG_AGAINST),
				new PropertyRelation(PProperty.FAIRY, WEAK_AGAINST),
				new PropertyRelation(PProperty.FLYING, BEAT_UP_BY),
				new PropertyRelation(PProperty.ROCK, BEAT_UP_BY),
				new PropertyRelation(PProperty.FIRE, BEAT_UP_BY)
		));
		// Ghost
		propertyRelationsCache.put(PProperty.GHOST, Arrays.asList(
				new PropertyRelation(PProperty.NORMAL, NO_EFFECT_ON),
				new PropertyRelation(PProperty.GHOST, STRONG_AGAINST),
				new PropertyRelation(PProperty.PSYCHIC, STRONG_AGAINST),
				new PropertyRelation(PProperty.DARK, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, BEAT_UP_BY),
				new PropertyRelation(PProperty.DARK, BEAT_UP_BY)
		));
		// Steel
		propertyRelationsCache.put(PProperty.STEEL, Arrays.asList(
				new PropertyRelation(PProperty.ROCK, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.WATER, WEAK_AGAINST),
				new PropertyRelation(PProperty.ELECTRIC, WEAK_AGAINST),
				new PropertyRelation(PProperty.ICE, STRONG_AGAINST),
				new PropertyRelation(PProperty.FAIRY, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIGHTING, BEAT_UP_BY),
				new PropertyRelation(PProperty.GROUND, BEAT_UP_BY),
				new PropertyRelation(PProperty.FIRE, BEAT_UP_BY)
		));
		// Fire
		propertyRelationsCache.put(PProperty.FIRE, Arrays.asList(
				new PropertyRelation(PProperty.ROCK, WEAK_AGAINST),
				new PropertyRelation(PProperty.BUG, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.WATER, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, STRONG_AGAINST),
				new PropertyRelation(PProperty.ICE, STRONG_AGAINST),
				new PropertyRelation(PProperty.DRAGON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GROUND, BEAT_UP_BY),
				new PropertyRelation(PProperty.ROCK, BEAT_UP_BY),
				new PropertyRelation(PProperty.WATER, BEAT_UP_BY)
		));
		// Water
		propertyRelationsCache.put(PProperty.WATER, Arrays.asList(
				new PropertyRelation(PProperty.GROUND, STRONG_AGAINST),
				new PropertyRelation(PProperty.ROCK, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIRE, STRONG_AGAINST),
				new PropertyRelation(PProperty.WATER, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, WEAK_AGAINST),
				new PropertyRelation(PProperty.DRAGON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, BEAT_UP_BY),
				new PropertyRelation(PProperty.ELECTRIC, BEAT_UP_BY)
		));
		// Grass
		propertyRelationsCache.put(PProperty.GRASS, Arrays.asList(
				new PropertyRelation(PProperty.FLYING, WEAK_AGAINST),
				new PropertyRelation(PProperty.POISON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GROUND, STRONG_AGAINST),
				new PropertyRelation(PProperty.ROCK, STRONG_AGAINST),
				new PropertyRelation(PProperty.BUG, WEAK_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.WATER, STRONG_AGAINST),
				new PropertyRelation(PProperty.GRASS, WEAK_AGAINST),
				new PropertyRelation(PProperty.DRAGON, WEAK_AGAINST),
				new PropertyRelation(PProperty.FLYING, BEAT_UP_BY),
				new PropertyRelation(PProperty.POISON, BEAT_UP_BY),
				new PropertyRelation(PProperty.BUG, BEAT_UP_BY),
				new PropertyRelation(PProperty.FIRE, BEAT_UP_BY),
				new PropertyRelation(PProperty.ICE, BEAT_UP_BY)
		));
		// Electric
		propertyRelationsCache.put(PProperty.ELECTRIC, Arrays.asList(
				new PropertyRelation(PProperty.FLYING, STRONG_AGAINST),
				new PropertyRelation(PProperty.GROUND, NO_EFFECT_ON),
				new PropertyRelation(PProperty.WATER, STRONG_AGAINST),
				new PropertyRelation(PProperty.GRASS, WEAK_AGAINST),
				new PropertyRelation(PProperty.ELECTRIC, WEAK_AGAINST),
				new PropertyRelation(PProperty.DRAGON, WEAK_AGAINST),
				new PropertyRelation(PProperty.GROUND, BEAT_UP_BY)
		));
		// Psychic
		propertyRelationsCache.put(PProperty.PSYCHIC, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, STRONG_AGAINST),
				new PropertyRelation(PProperty.POISON, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.PSYCHIC, WEAK_AGAINST),
				new PropertyRelation(PProperty.DARK, NO_EFFECT_ON),
				new PropertyRelation(PProperty.BUG, BEAT_UP_BY),
				new PropertyRelation(PProperty.GHOST, BEAT_UP_BY),
				new PropertyRelation(PProperty.DARK, BEAT_UP_BY)
		));
		// Ice
		propertyRelationsCache.put(PProperty.ICE, Arrays.asList(
				new PropertyRelation(PProperty.FLYING, STRONG_AGAINST),
				new PropertyRelation(PProperty.GROUND, STRONG_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.WATER, WEAK_AGAINST),
				new PropertyRelation(PProperty.GRASS, STRONG_AGAINST),
				new PropertyRelation(PProperty.ICE, WEAK_AGAINST),
				new PropertyRelation(PProperty.DRAGON, STRONG_AGAINST),
				new PropertyRelation(PProperty.FIGHTING, BEAT_UP_BY),
				new PropertyRelation(PProperty.ROCK, BEAT_UP_BY),
				new PropertyRelation(PProperty.STEEL, BEAT_UP_BY),
				new PropertyRelation(PProperty.FIRE, BEAT_UP_BY)
		));
		// Dragon
		propertyRelationsCache.put(PProperty.DRAGON, Arrays.asList(
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.DRAGON, STRONG_AGAINST),
				new PropertyRelation(PProperty.FAIRY, NO_EFFECT_ON),
				new PropertyRelation(PProperty.ICE, BEAT_UP_BY),
				new PropertyRelation(PProperty.DRAGON, BEAT_UP_BY),
				new PropertyRelation(PProperty.FAIRY, BEAT_UP_BY)
		));
		// Dark
		propertyRelationsCache.put(PProperty.DARK, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, WEAK_AGAINST),
				new PropertyRelation(PProperty.GHOST, STRONG_AGAINST),
				new PropertyRelation(PProperty.PSYCHIC, STRONG_AGAINST),
				new PropertyRelation(PProperty.DARK, WEAK_AGAINST),
				new PropertyRelation(PProperty.FAIRY, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIGHTING, BEAT_UP_BY),
				new PropertyRelation(PProperty.BUG, BEAT_UP_BY),
				new PropertyRelation(PProperty.FAIRY, BEAT_UP_BY)
		));
		// Fairy
		propertyRelationsCache.put(PProperty.FAIRY, Arrays.asList(
				new PropertyRelation(PProperty.FIGHTING, STRONG_AGAINST),
				new PropertyRelation(PProperty.POISON, WEAK_AGAINST),
				new PropertyRelation(PProperty.STEEL, WEAK_AGAINST),
				new PropertyRelation(PProperty.FIRE, WEAK_AGAINST),
				new PropertyRelation(PProperty.DARK, STRONG_AGAINST),
				new PropertyRelation(PProperty.DRAGON, STRONG_AGAINST),
				new PropertyRelation(PProperty.POISON, BEAT_UP_BY),
				new PropertyRelation(PProperty.STEEL, BEAT_UP_BY)
		));
	}

	public List<PropertyRelation> listPropertyRelations(PProperty property) {
		return propertyRelationsCache.get(property);
	}

	public List<PProperty> listCounters(final PProperty defProperty) {
		List<PProperty> result = new ArrayList<>(5);
		for (Map.Entry<PProperty, List<PropertyRelation>> e : propertyRelationsCache.entrySet()) {
			if (CollectionUtils.exists(e.getValue(), new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					PropertyRelation rel = (PropertyRelation) object;
					return rel.getRelationType() == STRONG_AGAINST && rel.getRelationTo() == defProperty;
				}
			})) {
				result.add(e.getKey());
			}
		}
		return result;
	}

	void appendHiddenMachines(Collection<MoveLearned> result, Integer generation) {
    	if (generation == null || generation == 0) {
    		// append all
			SparseArray<MoveLearned> movesDistinct = new SparseArray<>();
			for (Generation gen : getGenerations()) {
				for (HiddenMachine hm : gen.getHms()) {
					if (movesDistinct.indexOfKey(hm.getMoveId()) >= 0)
						continue;
					MoveLearned mlx = newAnonymousMoveLearned(gen, hm, true);
					movesDistinct.put(hm.getMoveId(), mlx);
				}
			}
			result.addAll(DataUtil.getValues(movesDistinct));
		} else if (generation < getGenerations().size()) {
			Generation gen = getGenerations().get(generation - 1);
			for (HiddenMachine hm : gen.getHms()) {
				MoveLearned mlx = newAnonymousMoveLearned(gen, hm, false);
				result.add(mlx);
			}
		}
	}

	private MoveLearned newAnonymousMoveLearned(Generation gen, HiddenMachine hm, boolean anonymousName) {
		MoveLearned mlx = new MoveLearned();
		mlx.setGeneration(gen.getId());
		mlx.setMoveId(hm.getMoveId());
		mlx.setTrigger(MoveLearnTrigger.MACHINE);
		mlx.setMachine(anonymousName ? hm.getName().substring(0, 2) : hm.getName());
		return mlx;
	}

    public Item getItemByName(String value, Locale locale) {
    	if (getItems() == null)
    		return null;
        for (Item item : getItems()) {
            if (StringUtils.equals(item.getName(locale), value))
                return item;
        }
        return null;
    }

	public Pokemon getRandomPokemon() {
		return getRandomPokemon(null);
	}

	public Game getGameByName(String name) {
    	name = name.replaceAll("[\' ]", "");
    	List<Game> games = getGames();
		if (CollectionUtils.isEmpty(games))
			return null;
		for (Game game : games) {
			if (game.getId().equalsIgnoreCase(name))
				return game;
			for (String trans : game.getAllNames()) {
				if (trans.replaceAll("[\' ]", "").equalsIgnoreCase(name))
					return game;
			}
		}
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<PProperty> getDistinctProperties(TeamMember teamMember) {
		PokemonForm form = getPokemonForm(teamMember.getPid(), teamMember.getVariantIdx());
		Pokemon p = form.getPokemon();
		// native properties
		CircularFifoBuffer properties = new CircularFifoBuffer(TeamMember.MAX_MOVES);
		PProperty[] props = form.getActualProperties();
		for (PProperty prop : props) {
			if (prop != null)
				properties.add(prop);
		}
		// selected Abilities
		for (MoveLearned ml : teamMember.getMoves()) {
			Move m = getMove(ml.getMoveId());
			if (m.getProperty() != PProperty.NORMAL && !properties.contains(m.getProperty())) {
				if (m.getStrength() != null && m.getStrength() > 0)
					properties.add(m.getProperty());
			}
		}
		if (properties.isEmpty() && ArrayUtils.contains(p.getProperties(), PProperty.NORMAL))
			properties.add(PProperty.NORMAL);
		return new ArrayList<>(properties);
	}

	protected Object getCached(String cacheKey) {
		return cache.get(cacheKey);
	}

	protected Object cache(String cacheKey, Object object) {
		cache.put(cacheKey, object);
		return object;
	}

	protected boolean isCached(String key) {
		return cache.containsKey(key);
	}

	@Override
	public String getDisplayName(Item item) {
		String itemName = item.getName();
		if (item.getCategory() == ItemCategory.MACHINES) {
			Move move = getMoveForMachine(itemName);
			if (move == null) {
				// VM?
				move = getMoveForMachine(item.getName(Locale.ENGLISH));
			}
			itemName += "     " + move.getName();
		}
		return itemName;
	}
}
