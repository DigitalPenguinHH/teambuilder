package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum MoveLearnTrigger {

	LEVELUP, MACHINE, BREEDING, TUTOR;

	public static class MoveLearnTriggerConverter extends EnumConverter<MoveLearnTrigger> {

		@Override
		protected MoveLearnTrigger from(String value) {
			return MoveLearnTrigger.valueOf(value);
		}
	}
	
}
