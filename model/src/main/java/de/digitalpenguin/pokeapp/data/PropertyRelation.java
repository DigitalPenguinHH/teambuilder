package de.digitalpenguin.pokeapp.data;

public class PropertyRelation {

    public enum RelationType {
        STRONG_AGAINST, WEAK_AGAINST, BEAT_UP_BY, NO_EFFECT_ON
    }

    private PProperty relationTo;

    private RelationType relationType;

    public PropertyRelation(PProperty relationTo, RelationType relationType) {
        this.relationTo = relationTo;
        this.relationType = relationType;
    }

    public PProperty getRelationTo() {
        return relationTo;
    }

    public RelationType getRelationType() {
        return relationType;
    }
}
