package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

public class DataFilter implements Serializable
{

	private static final long serialVersionUID = 5518973044024148279L;

	private String name;
	
	private PProperty property;
	
	private Region region;



	public static DataFilter nameFilter(String name) {
		return new DataFilter(name, null, null);
	}

	public static DataFilter propertyFilter(PProperty property) {
		return new DataFilter(null, property, null);
	}

	public static DataFilter regionFilter(Region region) {
		return new DataFilter(null, null, region);
	}

	private DataFilter(String name, PProperty property, Region region)
	{
		super();
		this.name = name;
		this.property = property;
		this.region = region;
	}

	public PProperty getProperty()
	{
		return property;
	}

	public void setProperty(PProperty property)
	{
		this.property = property;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Region getRegion()
	{
		return region;
	}

	public void setRegion(Region version)
	{
		this.region = version;
	}
}
