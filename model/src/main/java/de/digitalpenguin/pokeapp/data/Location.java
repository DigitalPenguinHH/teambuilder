package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

public class Location extends NamedObject implements Serializable
{
	private static final long serialVersionUID = -3555713786663345882L;

	private String nameEn;

	public Location()
	{
		//
	}

    public Location(String name)
    {
        setName(name);
    }

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
}
