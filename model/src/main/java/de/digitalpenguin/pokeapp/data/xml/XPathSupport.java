package de.digitalpenguin.pokeapp.data.xml;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class XPathSupport {

    private Map<Class<? extends Serializable>, InputStream> streams = new HashMap<>(10);

    public void add(Class<? extends Serializable> type, InputStream is) {
        try {
            streams.put(type, is);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Document getDocument(Class<? extends Serializable> type) {
        InputStream is = streams.get(type);
        DocumentBuilder domParser = null;
        try {
            domParser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return domParser.parse(is);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    Element eval(String expression, Class<? extends Serializable> type) {
        try {
            Node node = (Node) XPathFactory.newInstance().newXPath().compile(expression).evaluate(getDocument(type), XPathConstants.NODE);
            if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
                return (Element) node;
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return null;
    }

    String evalToString(Element onElement, String expression) {
        try {
            Node node = (Node) XPathFactory.newInstance().newXPath().evaluate(expression, onElement, XPathConstants.NODE);
            if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
                return node.getTextContent();
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return null;
    }

    Integer evalToInt(Element onElement, String expression) {
        String val = evalToString(onElement, expression);
        return StringUtils.isNumeric(val) ? Integer.valueOf(val) : null;
    }

    Float evalToFloat(Element onElement, String expression) {
        String val = evalToString(onElement, expression);
        return StringUtils.isNumeric(val) ? Float.parseFloat(val) : null;
    }

    <T extends Serializable> List<Element> evalToList(String expression, Class<T> type) {
        return evalToList(getDocument(type), expression);
    }

    List<Element> evalToList(Object base, String expression) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate(expression, base, XPathConstants.NODESET);
            List<Element> result = new ArrayList<>(nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
                    result.add((Element) node);
                }
            }
            return result;
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    <T extends Serializable> List<T> readAnotherList(Object base, String expression, AbstractValueReader<T> reader) {
        final List<Element> elements = evalToList(base, expression);
        return reader.fromList(elements);
    }
}
