package de.digitalpenguin.pokeapp.data.util;

import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.MachineProperty;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.PropertyRelation;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.provider.AbilityProvider;
import de.digitalpenguin.pokeapp.data.provider.GameProvider;
import de.digitalpenguin.pokeapp.data.provider.ItemProvider;
import de.digitalpenguin.pokeapp.data.provider.MoveProvider;

public interface DataProvider extends ItemProvider, MoveProvider, GameProvider, AbilityProvider {

    void init();

    <T> List<T> find(String name);

    <T> List<T> find(String name, boolean includeForms, boolean includeCharacteristics, boolean includeNatures, boolean includeItems, boolean includeMoves, boolean includeAbilities);

    Pokemon getById(int pokemonId);

    Pokemon getRandomPokemon();

    Pokemon getRandomPokemon(Integer generation);

    PokemonData getPokemonData(int pokemonId);

    PokemonData getPokemonData(Pokemon p);

    List<Pokemon> getPokemonEntries();

    List<Variant> getForms(int pokemonId);

    PokemonForm getPokemonForm(int pid, int variantIdx);

    PokemonForm getPokemonForm(int pid, VariantType form);

    List<Pokemon> find(DataFilter filter);

    List<Pokemon> findPokemon(String name);

    List<PokemonForm> findPokemonForms(String name);

    List<Pokemon> findPokemon(DataFilter filter);

    List<Move> getMoves();

    List<Move> findMoves(String name);

    List<Move> listMoves(MoveType selectedMoveType, PProperty selectedProperty);

    List<MachineProperty> listMachineProperties();

    Move getMoveForMachine(String machineName);

    Map<Integer, List<MoveLearned>> getGenerationsMoveSet(PokemonForm p);

    List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, MoveLearnTrigger trigger);

    List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId);

    List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId, MoveLearnTrigger trigger);

    boolean hasGameSpecificMoves(Pokemon p);

    List<MoveLearned> listMovesLearned(PokemonForm p);

    List<PProperty> getDistinctProperties(TeamMember teamMember);

    EvolutionChain getEvolutionChain(int pokemonId);

    List<EvolutionChain> getEvolutionChains();

    List<Generation> getGenerations();

    List<Region> getRegions();

    Region getRegion(Integer generation);

    List<Characteristic> getCharacteristics();

    List<Characteristic> findCharacteristics(String name);

    List<Nature> getNatures();

    List<Nature> findNatures(String name);

    List<PProperty> listCounters(PProperty defProperty);

    List<PropertyRelation> listPropertyRelations(PProperty property);

    List<MoveLearned> listPokemonMovesLearned(int moveId);

    List<PokemonAbility> listPokemonWithAbility(int abilityId);

    List<PokemonAbility> listAbilities(int pokemonId, VariantType type);

    String getDisplayName(Item item);

    boolean hasMoves(PokemonForm pokemon, int generation, String gameKey);

    int getPokemonEntriesCount();
}
