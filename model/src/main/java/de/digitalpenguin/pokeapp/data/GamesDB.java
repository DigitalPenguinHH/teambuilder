package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class GamesDB {

    private List<Game> games;

    private List<Region> regions;

    private List<Generation> generations;

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public List<Generation> getGenerations() {
        return generations;
    }

    public void setGenerations(List<Generation> generations) {
        this.generations = generations;
    }
}
