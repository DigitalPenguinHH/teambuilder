package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum MoveType {

	PHYSICAL, SPECIAL, STATUS;

	public static MoveType from(String text) {
		text = text.toUpperCase();
		for (MoveType type : values()) {
			if (text.startsWith(type.name().substring(0, 3))) {
				return type;
			}
		}
		return null;
	}

	public static class MoveTypeConverter extends EnumConverter<MoveType>
	{
		@Override
		protected MoveType from(String value) {
			return MoveType.valueOf(value);
		}
	}
	
}
