package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@Cache
public class Game extends LocalizedNamed implements Serializable, Identifiable {

    private static final long serialVersionUID = 1715463933132232842L;

    public static final String PARAM_ID = "gameId";

    public static final String GAMES_KEY_LETSGO_PIKACHU = "LGP";

    @Id
    private String id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names = new LocalizedText();

    private int generation;

    private boolean priority = false;

    @Column(name = "team_member_default")
    private Integer teamMemberDefault = null;

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public String getResourceName() {
        return getName(Locale.ENGLISH).toLowerCase().replaceAll(" ", "");
    }

    public void setHasPriority() {
        this.priority = true;
    }

    public boolean hasPriority() {
        return priority;
    }

    public Integer getTeamMemberDefault() {
        return teamMemberDefault;
    }

    public void setTeamMemberDefault(Integer teamMemberDefault) {
        this.teamMemberDefault = teamMemberDefault;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean getPriority() {
        return this.priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
