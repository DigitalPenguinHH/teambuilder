package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class ItemsDB {

    private List<Item> entries;

    public ItemsDB() {
    }

    public ItemsDB(List<Item> entries) {
        this.entries = entries;
    }

    public List<Item> getEntries() {
        return entries;
    }

    public void setEntries(List<Item> entries) {
        this.entries = entries;
    }
}
