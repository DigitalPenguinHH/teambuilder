package de.digitalpenguin.pokeapp.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@XStreamAlias("ability")
@Cache
public class Ability extends LocalizedNamed implements Identifiable
{
	private static final long serialVersionUID = -8742397527814754671L;

	public static final String TAG_ID = "ability.id";

	@Id
	private int id;

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText names = new LocalizedText();

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText descriptions = new LocalizedText();

	private int sinceGen;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public LocalizedText getNames() {
		return names;
	}

	@Override
	public void setNames(LocalizedText names) {
		this.names = names;
	}

	public LocalizedText getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(LocalizedText descriptions) {
		this.descriptions = descriptions;
	}

	public int getSinceGen() {
		return sinceGen;
	}

	public void setSinceGen(int sinceGen) {
		this.sinceGen = sinceGen;
	}

	@Override
	public String toString() {
		return "Ability [id=" + id + ", names=" + getNames() + ", sinceGen=" + sinceGen + "]";
	}

	@Override
	public Serializable getIdentifier() {
		return getId();
	}
}
