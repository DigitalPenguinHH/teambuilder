package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@Cache
public class Region extends LocalizedNamed implements Identifiable {

    private static final long serialVersionUID = 4842238076490807270L;

    public static final String PARAM_GENERATION = "gen";

    @Id
    private int id;

    private int generation;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names;

    @OneToOne(mappedBy = "region", cascade = CascadeType.ALL)
    private Pokedex pokedex;

    @Transient
    private List<Location> locations;

    public Region() {}

    public Region(int generation) {
        this.generation = generation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Pokedex getPokedex() {
        return pokedex;
    }

    public void setPokedex(Pokedex pokedex) {
        this.pokedex = pokedex;
    }

    @Override
	public String toString() {
		return "Region [generation=" + generation + ", names=" + names + ", pokedex=" + pokedex + "]";
	}

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
