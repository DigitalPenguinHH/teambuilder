package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum StatType {

    HP, ATK, DEF, SPATK, SPDEF, INIT;

    public static class StatTypeConverter extends EnumConverter<StatType> {

        @Override
        protected StatType from(String value) {
            return StatType.valueOf(value);
        }
    }

}
