package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.digitalpenguin.pokeapp.data.util.Named;

public abstract class LocalizedNamed implements Named, Comparable<LocalizedNamed>, Serializable {

    private static final long serialVersionUID = -7152431103503132705L;

    static final String DEFAULT_LANG_CODE = "en";

    public abstract LocalizedText getNames();

    public abstract void setNames(LocalizedText names);

    public String getName(String lang) {
        if (getNames() == null)
            return null;
        String name = getNames().get(lang);
        return StringUtils.isEmpty(name) ? getNames().get(DEFAULT_LANG_CODE) : name;
    }

    public String getName(Locale locale) {
        return getName(locale == null ? DEFAULT_LANG_CODE : locale.getLanguage());
    }

    public void addName(String lang, String value) {
        getNames().addText(lang, value);
    }

    @Override
    public String getName()
    {
        String name = getName(Locale.getDefault().getLanguage());
        return name != null ? name : getName(DEFAULT_LANG_CODE);
    }

    @Override
    public int compareTo(LocalizedNamed o) {
        if (o == null)
            return 1;
        return getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append(getNames())
                .toString();
    }

    public Set<String> getAllNames() {
        return this.getNames().getAllNames();
    }

    public void setNames(Map<String, String> names) {
        for (Map.Entry<String, String > e : names.entrySet()) {
            addName(e.getKey(), e.getValue());
        }
    }

    public Iterable<? extends Map.Entry<String, String>> getNameEntries() {
        return getNames().entrySet();
    }

    public String getNameEn() {
        return getName(Locale.ENGLISH);
    }
}
