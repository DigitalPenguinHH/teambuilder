package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.data.util.Named;
import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum ItemCategory implements Named {

    BERRIES, HOLD_ITEMS, MEDICINE, GENERAL, POKEBALLS, BATTLE_ITEMS, MACHINES;

    public static ItemCategory parse(String str) {
        String id = str.substring(0,2).toUpperCase();
        ItemCategory v = null;
        switch (id) {
            case "BE":
                v = BERRIES;
                break;
            case "HO":
                v = HOLD_ITEMS;
                break;
            case "ME":
                v = MEDICINE;
                break;
            case "MA":
                v = MACHINES;
                break;
            case "GE":
                v = GENERAL;
                break;
            case "BA":
                v = BATTLE_ITEMS;
                break;
            case "PO":
                v = POKEBALLS;
                break;
            default:
                break;
        }
        return v;
    }

    @Override
    public String getName() {
        return name();
    }

    public static class ItemCategoryConverter extends EnumConverter<ItemCategory> {

        @Override
        protected ItemCategory from(String value) {
            return ItemCategory.valueOf(value);
        }
    }

}
