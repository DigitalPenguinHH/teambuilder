package de.digitalpenguin.pokeapp.data.util;

public interface Named {

    String getName();
}
