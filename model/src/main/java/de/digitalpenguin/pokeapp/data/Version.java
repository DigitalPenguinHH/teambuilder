package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum Version
{
	RBY(1),
	SGC(2),
	SRS(3),
	DPP(4),
	SW(5),
	XY(6),
	SM(7),
	LGPE(7),
	SS(8);

	public int generation;

	Version(int generation) {
		this.generation = generation;
	}

	public static Version parse(String str) {
		for (Version v : values()) {
			if (str.toUpperCase().contains(v.name()))
				return v;
		}
		return null;
	}

	public static Version from(int id) {
		for (Version v : values()) {
			if (id == v.ordinal())
				return v;
		}
		return null;
	}

	public static class VersionConverter extends EnumConverter<Version> {

		@Override
		protected Version from(String value) {
			return Version.valueOf(value);
		}
	}
}
