package de.digitalpenguin.pokeapp.data.provider;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.LocalizedNamed;

public class ListDataProvider<T extends LocalizedNamed> implements EntityProvider<T> {

    private final List<T> list;

    public ListDataProvider(List<T> list) {
        this.list = list;
    }

    protected List<T> getList() {
        return Collections.unmodifiableList(list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getByName(final String name, final Locale locale) {
        return (T) CollectionUtils.find(this.list, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                T named = (T) object;
                return name.equalsIgnoreCase(named.getName(locale));
            }
        });
    }

    @Override
    public List<T> getItems() {
        return new ArrayList<T>(this.list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> find(final String name) {
        return (List<T>) CollectionUtils.select(this.list, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                T named = (T) object;
                return named.getAllNames().contains(name);
            }
        });
    }

    public static <T extends LocalizedNamed> EntityProvider<T> of(List<T> entities) {
        return new ListDataProvider<>(entities);
    }
}
