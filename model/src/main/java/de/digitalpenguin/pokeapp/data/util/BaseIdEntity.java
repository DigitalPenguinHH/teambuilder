package de.digitalpenguin.pokeapp.data.util;

import java.io.Serializable;

import javax.persistence.Id;

public class BaseIdEntity implements Identifiable, Serializable {

    @Id
    private int id;

    @Override
    public Serializable getIdentifier() {
        return getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
