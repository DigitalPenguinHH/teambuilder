package de.digitalpenguin.pokeapp.data;

import android.util.SparseArray;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.digitalpenguin.pokeapp.data.util.Named;

public class DataStore extends AbstractDataProvider {

	private static DataStore instance;

	public synchronized static DataStore getInstance() {
		if (instance == null) {
			instance = new DataStore();
		}
		return instance;
	}

	public static void setInstance(DataStore provider) {
		synchronized (instance) {
			instance = provider;
		}
	}
	
    private List<PokemonData> pokemonEntries;

    private SparseArray<PokemonData> pokemonCache = new SparseArray<>(1000);

    private List<Region> regions;

    private List<Game> games;

    private List<Generation> generations;

	private List<EvolutionChain> evolutionChains;

	private List<Move> moves;

	private List<Item> items;

	private List<Characteristic> characteristics;

	private List<Nature> natures;

	private Map<Integer, Move> moveCache = new HashMap<>(500);

	private Map<Integer, Map<Integer, List<MoveLearned>>> pokemonMoveSetCache = new HashMap<>(1000);

    private Map<String, Move> machines;
    
    private List<Ability> abilities;

    public DataStore()
    {
        this.pokemonEntries = new ArrayList<>(1000);
    }

	@Override
	public void init() {
		super.init();
	}

	@Override
    public List<Pokemon> getPokemonEntries()
    {
    	List<Pokemon> simpleList = new ArrayList<>(this.pokemonEntries.size());
		for (Pokemon p : this.pokemonEntries) {
			simpleList.add(p);
		}
        return simpleList;
    }

	public void addPokemonEntries(List<PokemonData> entries)
	{
		this.pokemonEntries.addAll(entries);
		for (PokemonData pokemon : entries) {
			pokemonCache.put(pokemon.getId(), pokemon);
		}
	}

	public Pokemon getRandomPokemon(Integer generation) throws IllegalStateException {
		List<Pokemon> entries;
		if (generation == null || generation <= 0) {
			entries = getPokemonEntries();
		} else {
			entries = find(DataFilter.regionFilter(getRegion(generation)));
		}
		if (entries == null || entries.isEmpty()) {
			throw new IllegalStateException();
		}
		int pid = RandomUtils.nextInt(1, Pokedex.MAX_POKEMON_ID);
		return getById(pid);
	}

    public void initMachineDetails() {
        if (CollectionUtils.isEmpty(this.pokemonEntries) || CollectionUtils.isEmpty(this.moves))
            return;
        machines = new HashMap<>();
        for (PokemonData p : Collections.unmodifiableList(this.pokemonEntries)) {
            for (MoveLearned ml : listMovesLearned(PokemonForm.newDefaultForm(p))) {
                if (ml.getTrigger() == MoveLearnTrigger.MACHINE && !machines.containsKey(ml.getMachine())) {
                    Move m = getMove(ml.getMoveId());
                    machines.put(ml.getMachine(), m);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
	@Override
	public Move getMoveForMachine(final String machineName) {
		Move move = machines.get(machineName);
		if (move == null) {
			// could be VM
			Collection<Generation> res = CollectionUtils.select(this.generations, new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					return ((Generation) object).getId() == 4; // 4 has most VMs
				}
			});
			Generation g = (Generation) res.iterator().next();
			HiddenMachine hm = (HiddenMachine) CollectionUtils.select(g.getHms(), new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					return ((HiddenMachine) object).getNames().contains(machineName);
				}
			}).iterator().next();
			move = getMove(hm.getMoveId());
		}
		return move;
    }

    @Override
	public boolean hasGameSpecificMoves(Pokemon p) {
		for (MoveLearned ml : listMovesLearned(PokemonForm.newDefaultForm(p))) {
			if (ml.getGames() != null && ml.getGames().length > 0)
				return true;
		}
		return false;
	}

	@Override
	public List<Generation> getGenerations() {
		return generations;
	}

	public void setGenerations(List<Generation> generations) {
		this.generations = generations;
	}

	public void clearPokemonDB() {
		this.pokemonEntries = new ArrayList<>(1000);
	}

	@Override
	public List<Ability> getAbilities() {
		return abilities;
	}

	@Override
	public List<Ability> findAbilities(final String name) {
		List<Ability> abilities = getAbilities();
		CollectionUtils.filter(abilities, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return ((Ability) object).getName().toLowerCase().contains(name);
			}
		});
		return abilities;
	}

	public void setAbilities(List<Ability> abilities) {
		this.abilities = abilities;
	}

	private static class NameContainsPredicate implements Predicate
	{

		private final String matchString;

		private NameContainsPredicate(String matchString) {
			this.matchString = matchString;
		}

		@Override
		public boolean evaluate(Object o) {
			if (o instanceof Named) {
				return StringUtils.containsIgnoreCase(((Named) o).getName(), matchString);
			}
			return false;
		}
	}

	private static class NameStartsWithPredicate implements Predicate
	{
		private final String matchString;

		private NameStartsWithPredicate(String matchString) {
			this.matchString = matchString;
		}

		@Override
		public boolean evaluate(Object o) {
			if (o instanceof Named) {
				return StringUtils.startsWithIgnoreCase(((Named) o).getName(), matchString);
			}
			return false;
		}
	}

	@Override
	public List<Item> findItems(String name) {
		return filter(getItems(), new NameStartsWithPredicate(name));
	}

	@Override
	public List<Characteristic> findCharacteristics(String name) {
		return filter(getCharacteristics(), new NameContainsPredicate(name));
	}

	@Override
	public List<Move> findMoves(String name) {
		return filter(Collections.unmodifiableList(moves), new NameContainsPredicate(name));
	}

	@Override
	public List<Move> listMoves(final MoveType selectedMoveType, final PProperty selectedProperty) {
		List<Move> moves = getMoves();
		CollectionUtils.filter(moves, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return selectedMoveType == null || ((Move)object).getType() == selectedMoveType;
			}
		});
		CollectionUtils.filter(moves, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return selectedProperty == null || ((Move)object).getProperty() == selectedProperty;
			}
		});
		return moves;
	}

	@Override
	public List<MachineProperty> listMachineProperties() {
    	return new ArrayList<>();
	}

	@Override
	public List<Nature> findNatures(String name) {
		return filter(getNatures(), new NameContainsPredicate(name));
	}

	@Override
	public Ability getAbility(final int id) {
		return (Ability) CollectionUtils.find(this.abilities, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return ((Ability) object).getId() == id;
			}
		});
	}

	@Override
	public List<Pokemon> findPokemon(final String name) {
		return filter(getPokemonEntries(), new Predicate() {
			@Override
			public boolean evaluate(Object o) {
				Pokemon p = (Pokemon) o;
				return StringUtils.startsWithIgnoreCase(p.getName(Locale.getDefault()), name);
			}
		});
	}

	@Override
	public List<PokemonForm> findPokemonForms(final String name) {
		return (List<PokemonForm>) CollectionUtils.transformedCollection(filter(getPokemonEntries(), new Predicate() {
			@Override
			public boolean evaluate(Object o) {
				Pokemon p = (Pokemon) o;
				return StringUtils.startsWithIgnoreCase(p.getName(Locale.getDefault()), name);
			}
		}), new Transformer() {
			@Override
			public Object transform(Object input) {
				return PokemonForm.newDefaultForm((Pokemon) input);
			}
		});
	}

	@Override
	public List<Pokemon> findPokemon(final DataFilter filter) {
		List<Pokemon> result = new ArrayList<>(getPokemonEntries());
		// filter name
		if (StringUtils.isNotEmpty(filter.getName())) {
			// Pokemon
			CollectionUtils.filter(result, new Predicate() {
				@Override
				public boolean evaluate(Object o) {
					Pokemon p = (Pokemon) o;
					return StringUtils.startsWithIgnoreCase(p.getName(Locale.getDefault()), filter.getName());
				}
			});
		}
		// filter property
		if (filter.getProperty() != null) {
			CollectionUtils.filter(result, new Predicate() {
				@Override
				public boolean evaluate(Object o)
				{
					Pokemon p = (Pokemon) o;
					for (PProperty prop : p.getProperties()) {
						if (prop == filter.getProperty()) {
							return true;
						}
					}
					return false;
				}
			});
		}
		// filter by region
		if (filter.getRegion() != null) {
			CollectionUtils.filter(result, new Predicate() {
				@Override
				public boolean evaluate(Object o) {
					Pokemon p = (Pokemon) o;
					return filter.getRegion().getPokedex().containsNationalDexId(p.getId());
				}
			});
		}
		return result;
	}

	private List filter(List<?> list, Predicate predicate) {
    	List result = new ArrayList<>(list);
    	CollectionUtils.filter(result, predicate);
    	return result;
	}

	@Override
	public Pokemon getById(final int pid) {
		return pokemonCache.get(pid);
	}

	@Override
	public List<Variant> getForms(int pokemonId) {
		return getPokemonData(pokemonId).getForms();
	}

	@Override
	public PokemonForm getPokemonForm(int pid, int variantIdx) {
		Pokemon p = getById(pid);
    	if (variantIdx >= 0) {
			return PokemonForm.newForm(getPokemonData(pid).getForms().get(variantIdx), p);
		} else {
    		return PokemonForm.newDefaultForm(p);
		}
	}

	@Override
	public PokemonForm getPokemonForm(int pid, final VariantType form) {
		PokemonForm found = (PokemonForm) CollectionUtils.find(getPokemonData(pid).getForms(), new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return ((Variant) object).getType() == form;
			}
		});
		if (found == null) {
			return PokemonForm.newDefaultForm(getById(pid));
		} else {
			return found;
		}
	}

	@Override
	public List<Region> getRegions() {
		return regions;
	}

	@Override
	public List<Move> getMoves() {
		return moves;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	@Override
	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public void setEvolutionChains(List<EvolutionChain> evolutionChains) {
		this.evolutionChains = evolutionChains;
	}

	public void setCharacteristics(List<Characteristic> characteristics) {
		Collections.sort(characteristics, new Comparator<Characteristic>() {
			@Override
			public int compare(Characteristic c1, Characteristic c2) {
				return Normalizer.normalize(c1.getName(), Normalizer.Form.NFD).compareTo(Normalizer.normalize(c2.getName(), Normalizer.Form.NFD));
			}
		});
		this.characteristics = characteristics;
	}

	@Override
	public List<Characteristic> getCharacteristics() {
		return characteristics;
	}

	public void setMoves(List<Move> moves) {
        this.moves = moves;
        this.moveCache.clear();
		for (Move m: this.moves) {
			moveCache.put(Integer.valueOf((int) m.getId()), m);
		}
    }

	@Override
	public Map<Integer, List<MoveLearned>> getGenerationsMoveSet(PokemonForm p) {
		Integer key = p.getId();
		Map<Integer, List<MoveLearned>> moveSet = pokemonMoveSetCache.get(key);
		if (moveSet == null) {
			moveSet = initGenerationsMoveSet(getPokemonData(p.getPokemon()));
			pokemonMoveSetCache.put(key, moveSet);
		}
		return moveSet;
	}

	@Override
	public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, MoveLearnTrigger trigger) {
		Set<MoveLearned> result = new HashSet<>();
		Map<Integer, List<MoveLearned>> moveSets = getGenerationsMoveSet(p);
		Integer generationInternal = generation;
		if (generation == null || generation == 0) {
			List<Integer> gens = new ArrayList<>(moveSets.keySet());
			Collections.sort(gens);
			generationInternal = gens.get(Math.max(gens.size() - 1, 0));
		}
		// wenn Pokemon in Generation vorhanden und kennt moves
		if (moveSets.containsKey(generationInternal)) {
			for (MoveLearned ml : moveSets.get(generationInternal)) {
				if (ml.getTrigger() == trigger)
					result.add(ml);
			}
		}
		if (trigger == MoveLearnTrigger.MACHINE)
			appendHiddenMachines(result, generation);
		return new ArrayList<>(result);
	}

	@Override
	public EvolutionChain getEvolutionChain(int pId) {
		for (EvolutionChain ch : getEvolutionChains()) {
			for (EvolutionChainLink l : ch.getChainLinks()) {
				if (l.getPokemonIdFrom() == pId || l.getPokemonIdTo() == pId)
					return ch;
			}
		}
		return null;
	}

	public PokemonData getPokemonData(int pokemonId) {
		return this.pokemonEntries.get(pokemonId);
	}

	public PokemonData getPokemonData(final Pokemon p) {
    	return (PokemonData) CollectionUtils.find(this.pokemonEntries, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return ((PokemonData) object).getId() == p.getId();
			}
		});
	}

	public Map<Integer, List<MoveLearned>> initGenerationsMoveSet(PokemonData p) {
		Map<Integer, List<MoveLearned>> moveSet = new HashMap<>();
		for (MoveLearned m: listMovesLearned(PokemonForm.newDefaultForm(p))) {
			if (m.getGames() != null)
				continue;
			Integer key = Integer.valueOf(m.getGeneration());
			List<MoveLearned> list  = moveSet.get(key);
			if (list == null) {
				list = new LinkedList<>();
				moveSet.put(key, list);
			}
			list.add(m);
		}
		// sort
		for (List<MoveLearned> list: moveSet.values()) {
			Collections.sort(list, new Comparator<MoveLearned>() {
				@Override
				public int compare(MoveLearned m1, MoveLearned m2) {
					if (m1.getLevel() == null && m2.getLevel() != null)
						return -1;
					if (m2.getLevel() == null && m1.getLevel() != null)
						return 1;
					if (m1.getLevel() == null && m2.getLevel() == null)
						return 0;
					return m1.getLevel().compareTo(m2.getLevel());
				}
			});
		}
		return moveSet;
	}

	public List<MoveLearned> getGenerationsMoveList (PokemonForm p, Integer generation, String gamesId) {
		List<MoveLearned> result = new LinkedList<>();
		for (MoveLearned ml : listMovesLearned(p)) {
			if (ml.getGames() == null)
				continue;
			if (ml.getGeneration() == generation.intValue() && StringUtils.join(ml.getGames(), '#').equals(gamesId))
				result.add(ml);
		}
		return result;
	}

	public List<MoveLearned> getGenerationsMoveList (PokemonForm p, Integer generation, String gamesId, MoveLearnTrigger trigger) {
		Set<MoveLearned> result = new HashSet<>();
		for (MoveLearned ml : listMovesLearned(p)) {
			if (ml.getGames() == null)
				continue;
			if (ml.getGeneration() == generation.intValue() && StringUtils.join(ml.getGames(), '#').equals(gamesId) && ml.getTrigger() == trigger)
				result.add(ml);
		}
		return new ArrayList<>(result);
	}

	@Override
	public List<MoveLearned> listPokemonMovesLearned(int moveId) {
		List<MoveLearned> result = new LinkedList<>();
		for (Pokemon p : getPokemonEntries()) {
			for (MoveLearned ml : listMovesLearned(PokemonForm.newDefaultForm(p))) {
				if (ml.getTrigger() == MoveLearnTrigger.LEVELUP && ml.getMoveId() == moveId) {
					result.add(ml);
					break;
				}
			}
		}
		Collections.sort(result, new Comparator<MoveLearned>() {
			@Override
			public int compare(MoveLearned o1, MoveLearned o2) {
				return Integer.valueOf((int) o1.getPokemonId()).compareTo(Integer.valueOf((int) o2.getPokemonId()));
			}
		});
		return result;
	}

	@Override
	public List<PokemonAbility> listPokemonWithAbility(int abilityId) {
		return new LinkedList<>();
	}

	@Override
	public List<PokemonAbility> listAbilities(int pokemonId, VariantType type) {
		return null;
	}

	@Override
	public boolean hasMoves(PokemonForm pokemon, int generation, String gameKey) {
		for (MoveLearned ml : listMovesLearned(pokemon)) {
			if (ml.getGeneration() == generation) {
				if (StringUtils.isNotEmpty(gameKey)
						&& ml.getGames() != null && ml.getGames().length > 0
						&& ArrayUtils.indexOf(ml.getGames(), gameKey) >= 0) {
					return true;
				} else {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int getPokemonEntriesCount() {
		return getPokemonEntries().size();
	}

	@Override
	public Move getMove(int moveId) {
    	return this.moveCache.get(Integer.valueOf(moveId));
	}

	@Override
	public List<MoveLearned> listMovesLearned(PokemonForm p) {
    	// TODO: implement
    	return new ArrayList<>(0);
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public List<Item> getItems() {
		return items;
	}

	@Override
	public List<EvolutionChain> getEvolutionChains() {
		return new ArrayList<>(this.evolutionChains);
	}

	public Item getItemByName(String value, Locale locale) {
    	if (getItems() == null)
    		return null;
        for (Item item : getItems()) {
            if (StringUtils.equals(item.getName(locale), value))
                return item;
        }
        return null;
    }

	@Override
    public Move getMoveByName(String value, Locale locale) {
        for (Move m : this.moves) {
            if (locale.getLanguage().equals("en") && m.getName(Locale.ENGLISH).equals(value)) {
                return m;
            } else if (locale.getLanguage().equals(Locale.GERMAN.getLanguage()) && m.getName(Locale.GERMAN).equals(value)) {
                return m;
            } else if (m.getName().equals(value)) {
                return m;
            }
        }
        return null;
    }

	@Override
    public Region getRegion(Integer generation) {
    	if (getRegions() == null)
    		throw new IllegalStateException();
        for (Region r : getRegions()) {
            if (r.getGeneration() == generation)
                return r;
        }
        return  null;
    }

	@Override
    public List<Item> listItems(ItemCategory category) {
        List<Item> results = new LinkedList<>();
        for (Item it: getItems()) {
            if (it.getCategory() == category)
                results.add(it);
        }
        return results;
    }

	public Game getGameByName(String name) {
    	name = name.replaceAll("[\\' ]", "");
		if (CollectionUtils.isEmpty(getGames()))
			return null;
		for (Game game : getGames()) {
			if (game.getId().equalsIgnoreCase(name))
				return game;
			for (String trans : game.getAllNames()) {
				if (trans.replaceAll("[\\' ]", "").equalsIgnoreCase(name))
					return game;
			}
		}
		return null;
	}

	@Override
	public List<Nature> getNatures() {
		return natures;
	}

	public void setNatures(List<Nature> natures) {
        Collections.sort(natures, new Comparator<Nature>() {
            @Override
            public int compare(Nature n1, Nature n2) {
                return Normalizer.normalize(n1.getName(), Normalizer.Form.NFD).compareTo(Normalizer.normalize(n2.getName(), Normalizer.Form.NFD));
            }
        });
		this.natures = natures;
	}
}
