package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.IntArrayConverter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
public class Characteristic extends LocalizedNamed implements Identifiable {

    private static final long serialVersionUID = -2662859479479050929L;

    @Id
	private int id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names = new LocalizedText();

    @Converter(converterClass = StatType.StatTypeConverter.class, columnType = String.class)
    private StatType highest;

    @Converter(converterClass = IntArrayConverter.class, columnType = String.class)
    @Column(name = "_values")
    private int[] values = new int[7];

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StatType getHighest() {
        return highest;
    }

    public void setHighest(StatType highest) {
        this.highest = highest;
    }

    public int[] getValues() {
        return values;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
