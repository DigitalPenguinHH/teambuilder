package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.BaseIdEntity;
import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.StringArrayConverter;

@Entity
@Table(name = "MOVE_LEARNED")
public class MoveLearned extends BaseIdEntity implements Serializable, Identifiable
{
	private static final long serialVersionUID = -7060158433617464481L;


	private int generation;

	private Integer level;

	@Column(name = "move_id")
	private int moveId;

	@Converter(converterClass = MoveLearnTrigger.MoveLearnTriggerConverter.class, columnType = String.class)
	private MoveLearnTrigger trigger;

	@Column
	private String machine;

	@Converter(converterClass = StringArrayConverter.class, columnType = String.class)
	private String[] games;

	@Column(name = "pokemon_id")
	private int pokemonId;

	@Column(name = "variant_id")
	private Integer variantId;


	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}

	public Integer getLevel() {
		return level;
	}

    public Integer getDisplayLevel() {
        return level == null || level <= 0 ? 1 : level;
    }

	public void setLevel(Integer level) {
		this.level = level;
	}

	public int getMoveId() {
		return moveId;
	}

	public void setMoveId(int moveId) {
		this.moveId = moveId;
	}
	
	public MoveLearnTrigger getTrigger() {
		return trigger;
	}

	public void setTrigger(MoveLearnTrigger trigger) {
		this.trigger = trigger;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public String[] getGames() {
		return games;
	}

	public void setGames(String[] games) {
		this.games = games;
	}

	public int getPokemonId() {
		return pokemonId;
	}

	public void setPokemonId(int pokemonId) {
		this.pokemonId = pokemonId;
	}

	public Integer getVariantId() {
		return variantId;
	}

	public void setVariantId(Integer variantId) {
		this.variantId = variantId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
				.append("generation", generation)
				.append("level", level)
				.append("moveId", moveId)
				.append("trigger", trigger)
				.append("machine", machine)
				.append("pokemonId", pokemonId)
				.append("variantId", variantId)
				.toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(15, 51)
				.append(generation)
				.append(level)
				.append(moveId)
				.append(trigger)
				.append(machine)
				.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof MoveLearned))
			return false;
		MoveLearned other = (MoveLearned) o;
		return new EqualsBuilder()
				.append(generation, other.generation)
				.append(level, other.level)
				.append(moveId, other.moveId)
				.append(trigger, other.trigger)
				.append(machine, other.machine)
				.isEquals();
	}

	public static String stringify(Collection<MoveLearned> movesLearned) {
		List<String> parts = new LinkedList<>();
		for (MoveLearned ml : movesLearned) {
			String genStr = Integer.toString(ml.getGeneration());
			String levelStr = "";
			if (ml.getLevel() != null)
				levelStr = Integer.toString(ml.getLevel());
			String moveIdStr = Integer.toString(ml.getMoveId());
			String triggerStr = ml.getTrigger().name().substring(0, 1);
			String machineStr = "";
			if (ml.getMachine() != null)
				machineStr = ml.getMachine();
			String gamesStr = ml.getGames() != null && ml.getGames().length > 0 ? StringUtils.join(ml.getGames(), '#') : "";
			// join by comma
			String result = StringUtils.join(new String[]{
					genStr,
					levelStr,
					moveIdStr,
					triggerStr,
					machineStr,
					gamesStr}, ',');
			if (result.endsWith(","))
				result = StringUtils.removeEnd(result, ",");
			parts.add(result);
		}
		// parts joined by semicolon
		return StringUtils.join(parts, ';');
	}

	public static List<MoveLearned> destringify(String val, int pokemonId) {
		List<MoveLearned> result = new LinkedList<>();
		String[] movesArr = val.split(";");
		for (String moveStr : movesArr) {
			MoveLearned ml = new MoveLearned();
			ml.setPokemonId(pokemonId);
			String[] inf = moveStr.split(",");
			for (int i = 0; i < inf.length; i++) {
				switch(i) {
					case 0:
						ml.setGeneration(Integer.parseInt(inf[i]));
						break;
					case 1:
						if (StringUtils.isNotEmpty(inf[i]))
							ml.setLevel(Integer.valueOf(inf[i]));
						break;
					case 2:
						ml.setMoveId(Integer.parseInt(inf[i]));
						break;
					case 3:
						for (MoveLearnTrigger v : MoveLearnTrigger.values()) {
							if (inf[i].equals(v.name().substring(0, 1)))
								ml.setTrigger(v);
						}
						break;
					case 4:
						if (StringUtils.isNotEmpty(inf[i]))
							ml.setMachine(inf[i]);
						break;
					case 5:
						if (StringUtils.isNotEmpty(inf[i]))
							ml.setGames(inf[i].split("#"));
						break;
					default:
						break;
				}
			}
			result.add(ml);
		}
		return result;
	}
}
