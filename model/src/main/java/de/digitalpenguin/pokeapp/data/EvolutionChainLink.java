package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.BaseIdEntity;
import de.digitalpenguin.pokeapp.persistence.Cache;

@Entity
@Table(name = "EVOLUTION_CHAIN_LINK")
@Cache
public class EvolutionChainLink extends BaseIdEntity
{
	private static final long serialVersionUID = -2829469702805437565L;

	@Column(name = "pokemon_from")
	private int pokemonIdFrom;

	@Column(name = "pokemon_to")
	private int pokemonIdTo;

	@Column(name = "variant_from")
	private String variantFrom;

	@Column(name = "variant_to")
	private String variantTo;

	@OneToMany(mappedBy = "chainLink", targetEntity = EvolutionCondition.class, cascade = CascadeType.ALL)
	private List<EvolutionCondition<?>> trigger;

	@Column(name = "chain")
	private int chainId;

	public EvolutionChainLink()
	{
		//
	}

	public EvolutionChainLink(int pokemonIdFrom, int pokemonIdTo, List<EvolutionCondition<?>> trigger)
	{
		this.pokemonIdFrom = pokemonIdFrom;
		this.pokemonIdTo = pokemonIdTo;
		this.trigger = trigger;
	}

	public EvolutionChainLink(int pokemonIdFrom, int pokemonIdTo, String fromVariant, String toVariant, List<EvolutionCondition<?>> trigger)
	{
		this(pokemonIdFrom, pokemonIdTo, trigger);
		setVariantFrom(fromVariant);
		setVariantTo(toVariant);
	}

	public int getPokemonIdFrom() {
		return pokemonIdFrom;
	}

	public void setPokemonIdFrom(int pokemonIdFrom) {
		this.pokemonIdFrom = pokemonIdFrom;
	}

	public int getPokemonIdTo() {
		return pokemonIdTo;
	}

	public void setPokemonIdTo(int pokemonIdTo) {
		this.pokemonIdTo = pokemonIdTo;
	}

	public List<EvolutionCondition<?>> getTrigger() {
		return trigger;
	}

	public void setTrigger(List<EvolutionCondition<?>> trigger) {
		this.trigger = trigger;
	}

	public String getVariantFrom() {
		return variantFrom;
	}

	public void setVariantFrom(String variantFrom) {
		this.variantFrom = variantFrom;
	}

	public String getVariantTo() {
		return variantTo;
	}

	public void setVariantTo(String variantTo) {
		this.variantTo = variantTo;
	}

	public int getChainId() {
		return chainId;
	}

	public void setChainId(int chainId) {
		this.chainId = chainId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("P[%d] ", pokemonIdFrom));
		if (trigger != null) {
			for (EvolutionCondition<?> c : trigger) {
				sb.append(" + ");
				sb.append(String.format("%s (%s) ", c.getCondition().name(), c.getObject().toString()));
			}
		}
		sb.append(String.format("=> P[%d]", pokemonIdTo));
		return sb.toString();
	}

    @Override
    public int hashCode() {
        return new HashCodeBuilder(35, 67)
                .append(pokemonIdFrom)
                .append(pokemonIdTo)
				.append(variantFrom)
				.append(variantTo)
                .toHashCode();
    }

    @Override
    public boolean equals(Object o) {
	    if (o == null)
	        return false;
	    if (o == this)
	        return true;
	    if (!(o instanceof EvolutionChainLink))
	        return false;
        EvolutionChainLink other = (EvolutionChainLink) o;
        return new EqualsBuilder()
                .append(this.pokemonIdFrom, other.pokemonIdFrom)
                .append(this.pokemonIdTo, other.pokemonIdTo)
				.append(this.variantFrom, other.variantFrom)
				.append(this.variantTo, other.variantTo)
                .isEquals() && CollectionUtils.isEqualCollection(this.trigger, other.trigger);
    }
}
