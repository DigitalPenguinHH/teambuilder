package de.digitalpenguin.pokeapp.data;

public class Name extends NamedObject {

    private static final long serialVersionUID = 1530579407891255046L;
	private String gameId;

    public Name(String name) {
        super(name);
    }

    public Name(String name, String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
