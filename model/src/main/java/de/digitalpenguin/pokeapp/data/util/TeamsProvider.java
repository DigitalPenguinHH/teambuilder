package de.digitalpenguin.pokeapp.data.util;

import java.util.List;

import de.digitalpenguin.pokeapp.data.Team;

public interface TeamsProvider {

    List<Team> getTeamEntries();

    boolean deleteTeam(Team team);

    Team getTeam(final String teamId);

    Team getTeamByName(CharSequence teamName);

    List<Team> getRecentTeams(int max);

}
