package de.digitalpenguin.pokeapp.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PokemonDB
{
    public static final int MAX_POKEMON_ID = 890;

    private List<PokemonData> entries;
	
	public PokemonDB()
	{
		entries = Collections.emptyList();
	}
	
	public PokemonDB(Collection<PokemonData> content)
	{
		this.entries = new ArrayList<>(content);
	}

	public List<PokemonData> getEntries()
	{
		return entries;
	}
	
	public void setEntries(List<PokemonData> entries)
	{
		this.entries = entries;
	}
}