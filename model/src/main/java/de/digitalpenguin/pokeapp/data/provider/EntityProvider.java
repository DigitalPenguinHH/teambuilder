package de.digitalpenguin.pokeapp.data.provider;

import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.LocalizedNamed;

interface EntityProvider<T extends LocalizedNamed> {

    T getByName(String name, Locale locale);

    List<T> getItems();

    List<T> find(String name);

}
