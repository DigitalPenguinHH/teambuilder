package de.digitalpenguin.pokeapp.data.xml;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;

import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;

class VariantValueReader extends AbstractValueReader<de.digitalpenguin.pokeapp.data.Variant> {

    public VariantValueReader(XPathSupport support) {
        super(support);
    }

    @Override
    public Variant from(Element element) {
        Variant v = new Variant();
        String typeStr = element.getAttribute("type");
        v.setType(StringUtils.isEmpty(typeStr) ? VariantType.OTHER : VariantType.valueOf(typeStr.toUpperCase()));
        v.setSuffix(StringUtils.defaultString(element.getAttribute("suffix"),""));
        setNames(v, element);
        v.setProperties(new PProperty.PropertyArrayConverter().convertToEntityProperty(getString(element, "properties")));
        String trigger = getStringFirstTag(element, "trigger");
        if (trigger != null)
            v.setTrigger(new Item(trigger));
        return v;
    }
}
