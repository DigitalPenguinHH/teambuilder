package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.BaseIdEntity;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.persistence.AbstractStringConverter;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.EnumConverter;

@Entity
@Table(name = "EVOLUTION_CONDITION")
@Cache
public class EvolutionCondition<T> extends BaseIdEntity {

	private static final long serialVersionUID = -9124152003000994531L;

	public enum Type {
		LEVELUP, 
		LOCATION, 
		ABILITY, 
		ITEM, 
		FRIENDSHIP, 
		GENDER,
		BEAUTY, 
		TRADE, 
		TRADE_AGAINST, 
		TRADE_WITH_ITEM, 
		DAY, 
		NIGHT, 
		PROPERTY_IN_TEAM,
		PROPERTY_ABILITY,
		DS_UPSIDE_DOWN, 
		TIME, 
		POKEMON_IN_TEAM, 
		STAT_RELATION,
		FREE_TEAMSLOT,
		WEATHER,
		RANDOM,
        HEARTS_POKEMONAMI,
        HEARTS_POKEREFRESH,
		CANDIES,
		OTHER;

		public static class EvolutionConditionTypeConverter extends EnumConverter<Type> {

			@Override
			protected Type from(String value) {
				return Type.valueOf(value);
			}
		}
	}

	@Column
	@Converter(converterClass = Type.EvolutionConditionTypeConverter.class, columnType = String.class)
	private Type condition;

	@Column
	@Converter(converterClass = GenericObjectToStringConverter.class, columnType = String.class)
	private T object;

	@Converter(converterClass = StringArrayToGamesConverter.class, columnType = String.class)
	private List<Game> games;

	@Column(name = "from_generation")
	private Integer fromGeneration;

	@Column(name = "link")
	private int chainLink;


	public EvolutionCondition()
	{
		//
	}

	public EvolutionCondition(Type condition, T object)
	{
		this.condition = condition;
		this.object = object;
	}

	public Type getCondition() {
		return condition;
	}

	public void setCondition(Type condition) {
		this.condition = condition;
	}

	public T getObject() {
		return object;
	}

	@SuppressWarnings("unchecked")
	public T getReferenceObject(DataProvider dataProvider) {
		switch (getCondition()) {
			case LEVELUP:
			case FRIENDSHIP:
			case CANDIES:
			case HEARTS_POKEMONAMI:
			case HEARTS_POKEREFRESH:
				return object;
			case ITEM:
			case TRADE_WITH_ITEM:
				return (T) dataProvider.getItemByName((String) object, Locale.GERMAN);
			case PROPERTY_ABILITY:
			case PROPERTY_IN_TEAM:
				return (T) PProperty.valueOf((String) object);
			case TRADE_AGAINST:
			case POKEMON_IN_TEAM:
				return (T) dataProvider.getById((Integer) object);
			case ABILITY:
				return (T) dataProvider.getMoveByName((String) object, Locale.GERMAN);
			case LOCATION:
				return (T) new Location((String) object);
		}
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public Integer getFromGeneration() {
		return fromGeneration;
	}

	public void setFromGeneration(Integer fromGeneration) {
		this.fromGeneration = fromGeneration;
	}

	public int getChainLink() {
		return chainLink;
	}

	public void setChainLink(int chainLink) {
		this.chainLink = chainLink;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(27, 85)
				.append(condition)
				.append(object)
				.append(games)
				.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (!(o instanceof EvolutionCondition))
			return false;
		@SuppressWarnings("rawtypes")
		EvolutionCondition other = (EvolutionCondition) o;
		return new EqualsBuilder()
				.append(condition, other.condition)
				.append(object, other.object)
				.isEquals() && CollectionUtils.isEqualCollection(games, other.games);
	}

	public static class StringArrayToGamesConverter extends AbstractStringConverter<List<Game>> {

		@Override
		public List<Game> convertToEntityProperty(String databaseValue) {
			if (databaseValue == null || databaseValue.trim().length() == 0)
				return null;
			StringTokenizer st = new StringTokenizer(databaseValue,",");
			List<Game> resultList = new ArrayList<>(st.countTokens());
			while (st.hasMoreElements()) {
				Game game = new Game();
				LocalizedText names = new LocalizedText();
				names.addText("de", st.nextToken());
				game.setNames(names);
				resultList.add(game);
			}
			return resultList;
		}

		@Override
		public String convertToDatabaseValue(List<Game> entityProperty) {
			if (entityProperty == null || entityProperty.isEmpty())
				return "";
			Collection col = CollectionUtils.collect(entityProperty, new Transformer() {
				@Override
				public Object transform(Object input) {
					Game game = (Game) input;
					return game.getName(Locale.GERMAN);
				}
			});
			return StringUtils.join(col, ',');
		}
	}

	public static class GenericObjectToStringConverter extends AbstractStringConverter<Object> {

		@Override
		public Object convertToEntityProperty(String databaseValue) {
			if (databaseValue == null)
				return null;
			String s = databaseValue;
			if (StringUtils.isEmpty(s)) {
				return null;
			}
			if (StringUtils.isNumeric(s)) {
				return Integer.parseInt(s);
			}
			return s;
		}

		@Override
		public String convertToDatabaseValue(Object entityProperty) {
			if (entityProperty == null) {
				return null;
			}
			if (entityProperty instanceof Item) {
				return ((Item) entityProperty).getName(Locale.GERMAN);
			}
			if (entityProperty instanceof  Integer) {
				return Integer.toString((Integer) entityProperty);
			}
			if (entityProperty instanceof String) {
				return (String) entityProperty;
			}
			if (entityProperty instanceof Location) {
				return ((Location) entityProperty).getName();
			}
			if (entityProperty instanceof Move) {
				return ((Move) entityProperty).getName(Locale.GERMAN);
			}
			if (entityProperty instanceof PProperty) {
				return ((PProperty) entityProperty).name();
			}
			return entityProperty.toString();
		}
	}
}
