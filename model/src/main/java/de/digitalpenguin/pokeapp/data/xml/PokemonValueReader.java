package de.digitalpenguin.pokeapp.data.xml;

import org.w3c.dom.Element;

import de.digitalpenguin.pokeapp.data.LocalizedNamed;
import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;

public class PokemonValueReader extends AbstractValueReader<Pokemon> {

    public PokemonValueReader(XPathSupport support) {
        super(support);
    }

    @Override
    public Pokemon from(Element element) {
        Pokemon p = new Pokemon();
        p.setId(getIntFirstTag(element, "id"));
        setNames(p, element);
        p.setProperties(new PProperty.PropertyArrayConverter().convertToEntityProperty(getString(element, "properties")));
        return p;
    }
}
