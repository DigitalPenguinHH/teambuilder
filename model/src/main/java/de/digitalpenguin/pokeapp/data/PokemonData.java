package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.digitalpenguin.pokeapp.data.util.GenderRatio;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@Table(name = "POKEMON")
@Cache
public class PokemonData extends Pokemon
{
	private static final long serialVersionUID = -5862350888518961015L;


	public static final String PARAM_FORM_IDX = "form.idx";
    public static final String PARAM_FORM = "form.name";

    @Transient
	private Version version;

	private Float height;

	private Float weight;

	@Column(name = "genders")
	@Converter(converterClass = GenderRatio.GenderRatioConverter.class, columnType = String.class)
	private GenderRatio genderRatio = new GenderRatio();

	@Transient
	@OneToMany(mappedBy = "pokemonId", cascade = CascadeType.ALL, targetEntity = MoveLearned.class)
	private List<MoveLearned> movesLearned;

	@Column(name = "basestats")
	@Converter(converterClass = Stats.StatsConverter.class, columnType = String.class)
	private Stats baseStats;

	@Column(name = "dexentries")
	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText dexEntries = new LocalizedText();

	@OneToMany(mappedBy = "pokemonId", cascade = CascadeType.ALL, targetEntity = Variant.class)
	private List<Variant> forms;

	public Version getVersion()
	{
		return version;
	}

	public void setVersion(Version version)
	{
		this.version = version;
	}

	public void setMovesLearned(List<MoveLearned> movesLearned) {
		this.movesLearned = movesLearned;
	}

	public Float getHeight() { return height; }

	public void setHeight(float height) {
		this.height = height;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getMalePercent() {
		return this.genderRatio.getMalePercent();
	}

	public void setMalePercent(float malePercent) {
		this.genderRatio.setMalePercent(malePercent);
	}

	public GenderRatio getGenderRatio() {
		return genderRatio;
	}

	public void setGenderRatio(GenderRatio genderRatio) {
		this.genderRatio = genderRatio;
	}

	public float getFemalePercent() {
		return this.genderRatio.getFemalePercent();
	}

	public void setFemalePercent(float femalePercent) {
		this.genderRatio.setFemalePercent(femalePercent);
	}

	public Stats getBaseStats() {
		return baseStats;
	}

	public void setBaseStats(Stats baseStats) {
		this.baseStats = baseStats;
	}

	public String getDexEntry() {
		String entryText = dexEntries.get(Locale.getDefault().getLanguage());
		entryText = StringUtils.isEmpty(entryText) ? dexEntries.get(LocalizedNamed.DEFAULT_LANG_CODE) : entryText;
		return StringUtils.defaultString(entryText, "");
	}

	public void addDexEntry(String lang, String text) {
		dexEntries.addText(lang, text);
	}

	public LocalizedText getDexEntries() {
		return dexEntries;
	}

	public void setForms(List<Variant> forms) {
		this.forms = forms;
	}

	public boolean hasGameSpecificMoves() {
		for (MoveLearned ml : getMovesLearned()) {
			if (ml.getGames() != null && ml.getGames().length > 0)
				return true;
		}
		return false;
	}

    public static Variant getForm(List<Variant> forms, int index) {
		if (CollectionUtils.isNotEmpty(forms)) {
			return forms.size() > index ? forms.get(index) : null;
		} else {
			return null;
		}
    }

	public void setHeight(Float height) {
		this.height = height;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public List<MoveLearned> getMovesLearned() {
		return movesLearned;
	}

	public void setDexEntries(LocalizedText dexEntries) {
		this.dexEntries = dexEntries;
	}

	public List<Variant> getForms() {
		return forms;
	}

	@Override
	public Serializable getIdentifier() {
		return getId();
	}
}
