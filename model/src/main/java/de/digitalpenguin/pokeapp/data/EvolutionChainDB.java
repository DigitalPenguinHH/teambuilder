package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class EvolutionChainDB
{
	private List<EvolutionChain> chains;

	public EvolutionChainDB()
	{
		//
	}

	public EvolutionChainDB(List<EvolutionChain> chains)
	{
		super();
		this.chains = chains;
	}

	public List<EvolutionChain> getChains() {
		return chains;
	}

	public void setChains(List<EvolutionChain> chains) {
		this.chains = chains;
	}
}
