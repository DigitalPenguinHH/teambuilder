package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "VARIANT")
public class PokemonForm extends Variant implements Comparator<PokemonForm> {

    private static final long serialVersionUID = 3430236494522488808L;

    public static final Comparator<? super PokemonForm> compartor = new Comparator<PokemonForm>() {
        @Override
        public int compare(PokemonForm o1, PokemonForm o2) {
            if (o1.getPokemonId() == o2.getPokemonId()) {
                return Integer.valueOf(o1.getType().ordinal()).compareTo(o2.getType().ordinal());
            }
            return Integer.valueOf(o1.getPokemonId()).compareTo(o2.getPokemonId());
        }
    };

    @OneToOne
    @Column(name = "pokemon_id")
    private Pokemon pokemon;

    @Transient
    private int index = -1;

    public static PokemonForm newDefaultForm(Pokemon pokemon) {
        PokemonForm form = new PokemonForm();
        form.setType(VariantType.DEFAULT);
        form.setPokemon(pokemon);
        form.setPokemonId(pokemon.getId());
        return form;
    }

    public static PokemonForm newForm(Variant v, Pokemon pokemon) {
        if (v == null)
            return newDefaultForm(pokemon);
        PokemonForm form = new PokemonForm();
        form.setId(v.getId());
        form.setProperties(v.getProperties());
        form.setNames(v.getNames());
        form.setSuffix(v.getSuffix());
        form.setType(v.getType());
        form.setTrigger(v.getTrigger());
        form.setPokemon(pokemon);
        form.setPokemonId(pokemon.getId());
        return form;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    @Override
    public  int compare(PokemonForm o1, PokemonForm o2) {
        if (o1.getPokemonId() == o2.getPokemonId()) {
            return Integer.valueOf(o1.getType().ordinal()).compareTo(o2.getType().ordinal());
        }
        return Integer.valueOf(o1.getPokemonId()).compareTo(o2.getPokemonId());
    }

    @Override
    public String toString() {
        if (pokemon == null)
            return super.toString();
        String name = this.getName();
        return pokemon.toString().concat(" " + (StringUtils.isEmpty(name) ? this.getType().name() : name));
    }

    public boolean isDefault() {
        return getType() == null || VariantType.DEFAULT == getType();
    }

    public boolean isRegional() {
        return VariantType.ALOLAN == getType() || VariantType.GALARIAN == getType();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public PProperty[] getActualProperties() {
        PProperty[] thisProps = getProperties();
        return ArrayUtils.isEmpty(thisProps) ? getPokemon().getProperties() : thisProps;
    }
}
