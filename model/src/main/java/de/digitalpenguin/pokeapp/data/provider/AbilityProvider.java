package de.digitalpenguin.pokeapp.data.provider;

import java.util.List;

import de.digitalpenguin.pokeapp.data.Ability;

public interface AbilityProvider {

    Ability getAbility(int id);

    List<Ability> getAbilities();

    List<Ability> findAbilities(String name);

}
