package de.digitalpenguin.pokeapp.data.xml;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.digitalpenguin.pokeapp.data.LocalizedNamed;
import de.digitalpenguin.pokeapp.data.LocalizedText;

public abstract class AbstractValueReader<T extends Serializable> {

    protected XPathSupport support;

    public AbstractValueReader(XPathSupport support) {
        this.support = support;
    }

    public abstract T from(Element element);

    public List<T> fromList(List<Element> elements) {
        List<T> result = new ArrayList<>(elements.size());
        for (Element e : elements) {
            result.add(from(e));
        }
        return result;
    }

    protected String getString(Element el, String expression) {
        return support.evalToString(el, expression);
    }

    protected Integer getInt(Element el, String expression) {
        return support.evalToInt(el, expression);
    }

    protected Float getFloat(Element el, String expression) {
        return support.evalToFloat(el, expression);
    }

    protected String getStringFirstTag(Element el, String tagName) {
        Node node = el.getElementsByTagName(tagName).item(0);
        if (node == null)
            return null;
        return node.getTextContent();
    }

    protected Integer getIntFirstTag(Element el, String tagName) {
        return Integer.valueOf(getStringFirstTag(el, tagName));
    }

    protected String getStringFirstTag(Element el, String tagName, String attribute, String attributeValue) {
        NodeList elements = el.getElementsByTagName(tagName);
        for (int i = 0; i < elements.getLength(); i++) {
            if (elements.item(i).hasAttributes()) {
                Node attrItem = elements.item(i).getAttributes().getNamedItem(attribute);
                if (attrItem != null && attributeValue.equals(attrItem.getTextContent()))
                    return elements.item(i).getTextContent();
            }
        }
        return null;
    }

    protected void setNames(LocalizedNamed n, Element element) {
        n.setNames(new LocalizedText());
        n.addName("de", getStringFirstTag(element, "name", "lang", "de"));
        n.addName("en", getStringFirstTag(element, "name", "lang", "en"));
    }
}
