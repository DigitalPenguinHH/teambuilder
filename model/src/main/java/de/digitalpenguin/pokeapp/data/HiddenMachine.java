package de.digitalpenguin.pokeapp.data;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@Table(name = "HIDDEN_MACHINE")
public class HiddenMachine extends LocalizedNamed implements Identifiable {

    private static final long serialVersionUID = 1271893354464360186L;

    @Id
    private int id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names = new LocalizedText();

    @Column(name = "move_id")
    private int moveId;

    @ManyToOne(targetEntity = Move.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "move_id")
    private Move move;

    @Column
    private int generation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoveId() {
        return moveId;
    }

    public void setMoveId(int moveId) {
        this.moveId = moveId;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
}
