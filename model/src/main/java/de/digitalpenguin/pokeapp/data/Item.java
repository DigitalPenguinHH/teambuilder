package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
public class Item extends LocalizedNamed implements Identifiable
{
	private static final long serialVersionUID = 7260933830322239835L;

    public static final String PARAM_CATEGORY = "itemCategory";

	@Id
	private int id;

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText names = new LocalizedText();

	@Converter(converterClass = ItemCategory.ItemCategoryConverter.class, columnType = String.class)
	private ItemCategory category;

	@Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText descriptions = new LocalizedText();

	@Transient
	private List<GameLocations> findLocations;

	public Item() {
	}

	public Item(String defaultName)
	{
		getNames().addText(Locale.ENGLISH.getLanguage(), defaultName);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ItemCategory getCategory() {
		return category;
	}

	@Override
	public LocalizedText getNames() {
		return names;
	}

	@Override
	public void setNames(LocalizedText names) {
		this.names = names;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public List<GameLocations> getFindLocations() {
		return findLocations;
	}

	public void setFindLocations(List<GameLocations> findLocations) {
		this.findLocations = findLocations;
	}

	public String getDescription() {
		return descriptions != null ? descriptions.getText() : null;
	}

	public LocalizedText getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(LocalizedText descriptions) {
		this.descriptions = descriptions;
	}

	public String getDrawableResourceName() {
		String itemNameEn = getName(Locale.ENGLISH);
		itemNameEn = itemNameEn
                .replaceAll("\\.", "")
                .replaceAll("-", "_")
                .replaceAll("'", "")
                .replaceAll(" ", "_")
                .toLowerCase()
                .replaceAll("special","sp");
		itemNameEn = StringUtils.stripAccents(itemNameEn);
		return itemNameEn;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
				.append(getName())
				.append(category)
				.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Item))
			return false;
		if (obj == this)
			return true;
		Item other = (Item) obj;
		return new EqualsBuilder()
				.append(getName(), other.getName())
				.isEquals();
	}

	@Override
	public Serializable getIdentifier() {
		return getId();
	}
}
