package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum ContestCategory {

	COOLNESS, BEAUTY, CUTENESS, CLEVERNESS, TOUGHNESS;
	
	public static ContestCategory parse(String text) {
		if (text.startsWith("Stärke")) {
			return ContestCategory.TOUGHNESS;
		} else if (text.startsWith("Putzig")) {
			return ContestCategory.CUTENESS;
		} else if (text.startsWith("Schön")) {
			return ContestCategory.BEAUTY;
		} else if (text.startsWith("Cool")) {
			return ContestCategory.COOLNESS;
		} else if (text.startsWith("Klug")) {
			return ContestCategory.CLEVERNESS;
		}
		return null;
	}

	public static class ContestCategoryConverter extends EnumConverter<ContestCategory> {

		@Override
		protected ContestCategory from(String value) {
			return ContestCategory.valueOf(value);
		}
	}
	
}
