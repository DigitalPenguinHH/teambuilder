package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum VariantType {

    DEFAULT, ALOLAN, MEGA, GALARIAN, GIGANTAMAX, OTHER;

    public static class VariantTypeConverter extends EnumConverter<VariantType> {

        @Override
        protected VariantType from(String value) {
            return VariantType.valueOf(value);
        }
    }

}
