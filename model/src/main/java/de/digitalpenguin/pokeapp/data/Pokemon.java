package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
@Table(name = "POKEMON")
@Cache
public class Pokemon extends LocalizedNamed implements Identifiable
{
	private static final long serialVersionUID = 7629661172270517143L;

	public static final String PARAM_ID = "pId";

	@Id
    private int id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
	private LocalizedText names = new LocalizedText();

	@Converter(converterClass = PProperty.PropertyArrayConverter.class, columnType = String.class)
	private PProperty[] properties;

	@Column
	private int generation;


	public int getId() {
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Override
	public LocalizedText getNames() {
		return names;
	}

	@Override
	public void setNames(LocalizedText names) {
		this.names = names;
	}

	public PProperty[] getProperties()
	{
		return properties;
	}

	public PProperty getPrimaryProperty() {
		return this.properties == null ? null : properties[0];
	}

	public void setProperties(PProperty[] properties)
	{
		this.properties = properties;
	}

	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}

	@Override
	public boolean equals(Object o)	{
		if (o == null)
			return false;
		if (!(o instanceof Pokemon))
			return false;
		Pokemon other = (Pokemon) o;
		return new EqualsBuilder()
				.append(id, other.id)
				.isEquals();
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append(id)
                .append(getName(Locale.GERMAN))
                .append(getName(Locale.ENGLISH))
                .append(properties)
                .toString();
    }

    public boolean isMultiProperty() {
		return this.properties.length > 1 && this.properties[1] != null;
	}

	public boolean hasOnlyOneProperty() {
		return this.properties.length == 1 || this.properties[1] == null;
	}

	public PProperty getPrimaryPropertyNotNormal() {
		if (hasOnlyOneProperty())
			return properties[0];
		for (int i = 0; i < properties.length; i++) {
			if (properties[i] != null && properties[i] != PProperty.NORMAL)
				return properties[i];
		}
		return null;
	}

	@Override
	public Serializable getIdentifier() {
		return getId();
	}
}
