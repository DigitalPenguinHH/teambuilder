package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import de.digitalpenguin.pokeapp.persistence.AbstractStringConverter;

public class Stats implements Serializable
{
	private static final long serialVersionUID = -8516208422295710887L;

	private int kp;
	
	private int atk;
	
	private int def;
	
	private int spcAtk;
	
	private int spcDef;
	
	private int init;

	public Stats()
	{
		//
	}

	public Stats(int kp, int atk, int def, int spcAtk, int spcDef, int init)
	{
		this.kp = kp;
		this.atk = atk;
		this.def = def;
		this.spcAtk = spcAtk;
		this.spcDef = spcDef;
		this.init = init;
	}
	
	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getSpcAtk() {
		return spcAtk;
	}

	public void setSpcAtk(int spcAtk) {
		this.spcAtk = spcAtk;
	}

	public int getSpcDef() {
		return spcDef;
	}

	public void setSpcDef(int spcDef) {
		this.spcDef = spcDef;
	}

	public int getInit() {
		return init;
	}

	public void setInit(int init) {
		this.init = init;
	}


	public static class StatsConverter extends AbstractStringConverter<Stats> {

		@Override
		public Stats convertToEntityProperty(String databaseValue) {
			Stats stats = new Stats();
			String statsStr = databaseValue;
			String[] values = statsStr.split("\\,");
			for (int i = 0; i < values.length; i++) {
				switch (i) {
					case 0:
						stats.setKp(Integer.parseInt(values[i]));
						break;
					case 1:
						stats.setAtk(Integer.parseInt(values[i]));
						break;
					case 2:
						stats.setDef(Integer.parseInt(values[i]));
						break;
					case 3:
						stats.setSpcAtk(Integer.parseInt(values[i]));
						break;
					case 4:
						stats.setSpcDef(Integer.parseInt(values[i]));
						break;
					case 5:
						stats.setInit(Integer.parseInt(values[i]));
						break;
					default:
						break;
				}
			}
			return stats;
		}

		@Override
		public String convertToDatabaseValue(Stats entityProperty) {
			if (entityProperty == null)
				return null;
			return StringUtils.join(new String[]{
					Integer.toString(entityProperty.getKp()),
					Integer.toString(entityProperty.getAtk()),
					Integer.toString(entityProperty.getDef()),
					Integer.toString(entityProperty.getSpcAtk()),
					Integer.toString(entityProperty.getSpcDef()),
					Integer.toString(entityProperty.getInit())
			}, ',');
		}
	}
}
