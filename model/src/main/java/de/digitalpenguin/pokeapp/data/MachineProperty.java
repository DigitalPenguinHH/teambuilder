package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.Identifiable;

@Entity
@Table(name = "MACHINE_PROPERTIES")
public class MachineProperty implements Serializable, Identifiable {

    private static final long serialVersionUID = 1283716450016228165L;

    @Id
    @Column(name = "machine")
    private String id;

    @ManyToOne(targetEntity = Move.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "move_id")
    private Move move;

    @Column
    private int generation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
}
