package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.util.Named;

public class TeamMember implements Comparable<TeamMember>, Serializable, Named
{

	private static final long serialVersionUID = 3819120797274744255L;

	public static final int MAX_MOVES = 4;

	public static final String PARAM_ID = "teamMember";


	private int slot = -1;
	
	private int pid;

	private int variantIdx = -1;

	private Team team;

	private List<MoveLearned> moves = new ArrayList<>(MAX_MOVES);

	
	public TeamMember() 
	{
		super();
	}

	public TeamMember(int slot, int pid) {
		super();
		this.slot = slot;
		this.pid = pid;
	}

	public TeamMember(int slot, int pid, int variantIdx) {
		super();
		this.slot = slot;
		this.pid = pid;
		this.variantIdx = variantIdx;
	}

    public TeamMember(int slot, long pid, Team team) {
        super();
        this.slot = slot;
        this.pid = (int) pid;
        this.team = team;
    }

    public int getSlot()
	{
		return slot;
	}

	public void setSlot(int slot)
	{
		this.slot = slot;
	}

	public int getPid()
	{
		return pid;
	}

	public void setPid(int pid)
	{
		this.pid = pid;
	}

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

	public List<MoveLearned> getMoves() {
		return moves;
	}

	public void setMoves(List<MoveLearned> moves) {
		this.moves = moves;
	}

	public void teach(MoveLearned ... moves) {
		Collections.addAll(this.moves, moves);
	}

	public int getVariantIdx() {
		return variantIdx;
	}

	public void setVariantIdx(int variantIdx) {
		this.variantIdx = variantIdx;
	}

	@Override
	public int compareTo(TeamMember o) {
		return Integer.valueOf(slot).compareTo(Integer.valueOf(o.slot));
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(19, 47)
				.append(pid)
				.append(slot)
				.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof TeamMember))
			return false;
		TeamMember other = (TeamMember) o;
		return new EqualsBuilder()
				.append(slot, other.slot)
				.append(pid, other.pid)
				.append(variantIdx, other.variantIdx)
				.isEquals();
	}

	@Override
	public String getName() {
		return getSlot() + " = " + getPid();
	}

	private AtomicInteger primaryPropertyIndex = new AtomicInteger(0);

	public void increasePrimaryPropertyIndex(List<PProperty> distinctProperties) {
		if (distinctProperties != null && !distinctProperties.isEmpty()) {
			if (primaryPropertyIndex.incrementAndGet() >= distinctProperties.size()) {
				setPrimaryPropertyIndex(0);
			}
		}
	}

	public int getPrimaryPropertyIndex() {
		return primaryPropertyIndex.get();
	}

	public void setPrimaryPropertyIndex(Integer index) {
		primaryPropertyIndex.set(index);
	}
}
