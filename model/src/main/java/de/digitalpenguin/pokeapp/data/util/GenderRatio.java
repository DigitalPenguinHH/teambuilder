package de.digitalpenguin.pokeapp.data.util;

import de.digitalpenguin.pokeapp.persistence.AbstractStringConverter;

public class GenderRatio {

    private float malePercent = -1f;

    private float femalePercent = -1f;

    public float getMalePercent() {
        return malePercent;
    }

    public void setMalePercent(float malePercent) {
        this.malePercent = malePercent;
    }

    public float getFemalePercent() {
        return femalePercent;
    }

    public void setFemalePercent(float femalePercent) {
        this.femalePercent = femalePercent;
    }

    @Override
    public String toString() {
        return String.format("M%s#F%s", getMalePercent(), getFemalePercent());
    }

    public static class GenderRatioConverter extends AbstractStringConverter<GenderRatio> {

        @Override
        public GenderRatio convertToEntityProperty(String databaseValue) {
            if (databaseValue == null || !databaseValue.contains("#"))
                return new GenderRatio();
            String[] parts = databaseValue.split("#");
            GenderRatio genderRatio = new GenderRatio();
            genderRatio.setMalePercent  (Float.parseFloat(parts[0].substring(1)));
            genderRatio.setFemalePercent(Float.parseFloat(parts[1].substring(1)));
            return genderRatio;
        }

        @Override
        public String convertToDatabaseValue(GenderRatio entityProperty) {
            return entityProperty.toString();
        }
    }
}
