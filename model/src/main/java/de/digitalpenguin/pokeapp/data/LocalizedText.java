package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class LocalizedText implements Comparable<LocalizedText>, Serializable {

    private static final long serialVersionUID = -4242431108162445020L;

    public static final String DEFAULT_LANG_CODE = "en";

    private static final List<String> SUPPORTED_LANGS = Arrays.asList("de","en","fr");

    private Map<String, String> texts = new HashMap<>(5);

    public String getText() {
        return getText(Locale.getDefault().getLanguage());
    }

    public String getText(Locale l) {
        return getText(l.getLanguage(), true);
    }

    public String getText(String lang) {
        return getText(lang, true);
    }

    public String getText(String lang, boolean useFallback) {
        String value = texts.get(lang);
        if (useFallback || !SUPPORTED_LANGS.contains(lang))
            value = StringUtils.isEmpty(value) ? texts.get(DEFAULT_LANG_CODE) : value;
        return StringUtils.defaultString(value, "");
    }

    public void addText(String lang, String text) {
        if (lang != null && text != null)
            texts.put(lang, text);
    }

    public boolean isEmpty() {
        return texts.isEmpty();
    }

    @Override
    public int compareTo(LocalizedText o) {
        if (o == null)
            return 1;
        return getText().compareTo(o.getText());
    }

    public Iterable<? extends Map.Entry<String, String>> entrySet() {
        return new HashMap<>(texts).entrySet();
    }

    @Override
    public String toString() {
        return "LocalizedText[" + texts + "]";
    }

    public String get(String lang) {
        return getText(lang);
    }

    public boolean contains(final String name) {
        return name != null && !texts.isEmpty() && CollectionUtils.find(this.texts.values(), new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                return name.equals(object);
            }
        }) != null;
    }

    public Set<String> getAllNames() {
        return new HashSet<>(this.texts.values());
    }
}
