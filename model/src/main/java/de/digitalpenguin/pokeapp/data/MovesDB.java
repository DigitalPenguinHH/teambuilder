package de.digitalpenguin.pokeapp.data;

import java.util.List;
import java.util.Locale;

public class MovesDB
{
	private List<Move> moves;

	public MovesDB()
	{ 
		
	}

	public MovesDB(List<Move> moves)
	{
		this.moves = moves;
	}

	public List<Move> getMoves() {
		return moves;
	}

	public void setMoves(List<Move> moves) {
		this.moves = moves;
	}

	public long findMoveIdByName(String name, Locale locale) {
		for (Move move : moves) {
			if (move.getName(locale).equals(name))
				return move.getId();
		}
		return -1;
	}
	
	public long findMoveIdByName(String name) {
		return findMoveIdByName(name, Locale.GERMAN);
	}
}
