package de.digitalpenguin.pokeapp.data.provider;

import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;

public interface ItemProvider {

    Item getItemByName(String name, Locale locale);

    List<Item> getItems();

    List<Item> findItems(String name);

    List<Item> listItems(final ItemCategory category);

}
