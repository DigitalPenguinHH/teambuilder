package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

@Entity
public class Variant extends LocalizedNamed implements Identifiable {

    private static final long serialVersionUID = -3112595209310819706L;

    @Id
    private int id;

    @Converter(converterClass = LocalizedTextConverter.class, columnType = String.class)
    private LocalizedText names = new LocalizedText();

    @Converter(converterClass = PProperty.PropertyArrayConverter.class, columnType = String.class)
    private PProperty[] properties;

    @ManyToOne
    @Column(name = "item_id")
    private Item trigger;

    @Converter(converterClass = VariantType.VariantTypeConverter.class, columnType = String.class)
    private VariantType type;

    private String suffix;

    @Column(name = "pokemon_id")
    private int pokemonId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public LocalizedText getNames() {
        return names;
    }

    @Override
    public void setNames(LocalizedText names) {
        this.names = names;
    }

    public PProperty[] getProperties() {
        return properties;
    }

    public PProperty getPrimaryProperty() {
        return this.properties == null ? null : properties[0];
    }

    public VariantType getType() {
        return type;
    }

    public void setType(VariantType type) {
        this.type = type;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setProperties(PProperty[] properties) {
        this.properties = properties;
    }

    public Item getTrigger() {
        return trigger;
    }

    public void setTrigger(Item trigger) {
        this.trigger = trigger;
    }

    public int getPokemonId() {
        return pokemonId;
    }

    public void setPokemonId(int pokemonId) {
        this.pokemonId = pokemonId;
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append(type)
                .toString();
    }
}
