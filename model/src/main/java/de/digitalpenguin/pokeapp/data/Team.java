package de.digitalpenguin.pokeapp.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class Team implements Comparable<Team>, Serializable
{

	private static final long serialVersionUID = -7002706496709483960L;

	public static final String PARAM_ID   = "tid";
    public static final String PARAM_GAME = "gameKey";

    public static volatile int TEAM_MEMBERS_DEFAULT = 6;

	private static final int FREE = 0; 
	private static final int TAKEN = 1; 

	private String id;
	
	private String name;

	private Game game;
	
	private List<TeamMember> members;

	private String icon;

	private Long lastModified;

	private int teamSizeMax = Team.TEAM_MEMBERS_DEFAULT;

	public static Team newTeam() {
		Team t = new Team();
		t.setGame(null);
		t.setName("");
		t.setLastModified(Calendar.getInstance().getTimeInMillis());
		return t;
	}

	public Team() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void add(int pid)
	{
		add(new TeamMember(nextFreeSlot(), pid, this));
	}

	public void add(Pokemon pokemon)
	{
		add(pokemon, -1);
	}

	public void add(Pokemon pokemon, int variantIdx)
	{
		TeamMember teamMember = new TeamMember(nextFreeSlot(), pokemon.getId(), this);
		teamMember.setVariantIdx(variantIdx);
		add(teamMember);
	}

	public void add(TeamMember teamMember) {
		if (!isFull()) {
			members.add(teamMember);
			modified();
		}
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
		modified();
	}

	public List<TeamMember> getMembers()
	{
		List<TeamMember> list = new ArrayList<>(getMembersSafe());
		Collections.sort(list);
		return list;
	}

	public void removeAllMembers() {
		getMembersSafe().clear();
	}

	public void setMembers(List<TeamMember> members)
	{
		this.members = members;
		modified();
	}

	public int getSize() {
		return this.members == null || this.members.isEmpty() ? 0 : this.members.size();
	}
	
	public void substitute(int slot, int newPid)
	{
		TeamMember existing = get(slot);
		if (existing != null) {
			Iterator<TeamMember> it = getMembersSafe().iterator();
			while (it .hasNext()) {
				TeamMember tm = it.next();
				if (tm.getSlot() == slot) {
					it.remove();
					break;
				}
			}
			add(new TeamMember(slot, newPid, this));
		}
		modified();
	}

	public boolean isFull()
	{
		return getMembersSafe().size() == getTeamSizeMax();
	}

	public int getTeamSizeMax() {
		return this.teamSizeMax;
	}

	public void setTeamSizeMax(int teamSizeMax) {
		this.teamSizeMax = teamSizeMax;
	}

	private List<TeamMember> getMembersSafe() {
		if (members == null) {
			members = new ArrayList<>(TEAM_MEMBERS_DEFAULT);
		}
		return members;
	}
	
	public TeamMember get(int slot)
	{
		for (TeamMember teamMember : getMembersSafe()) {
			if (teamMember.getSlot() == slot)
				return teamMember;
		}
		return null;
	}

	private int nextFreeSlot()
	{
		int[] slots = new int[teamSizeMax];
		for (int i = 0; i < slots.length; i++) {
			slots[i] = FREE;
		}
		for (TeamMember tm : getMembersSafe()) {
			slots[tm.getSlot()-1] = TAKEN;
		}
		int freeIndex = find(slots);
		return freeIndex + 1;
	}

	private int find(int[] arr)
	{
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == FREE)
				return i;
		}
		return -1;
	}
	
//	public List<Pokemon> list(PProperty p) {
//		List<Pokemon> result = new ArrayList<>(teamSizeMax);
//		for (TeamMember teamMember : getMembersSafe()) {
//			Pokemon pokemon = DataStore.getInstance().getById(teamMember.getPid());
//			if (ArrayUtils.indexOf(pokemon.getProperties(), p) >= 0) {
//				result.add(pokemon);
//			}
//		}
//		return result;
//	}

	public List<TeamMember> listCounteringTeamMembers(PProperty prop, DataProvider dataProvider)
	{
		List<PProperty> counterProperties = dataProvider.listCounters(prop);
		List<TeamMember> result = new ArrayList<>(teamSizeMax);
		for (TeamMember teamMember : members) {
			for (PProperty pe : dataProvider.getDistinctProperties(teamMember)) {
				if (counterProperties.contains(pe)) {
					result.add(teamMember);
					break;
				}				
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TeamMember> listTeamMembersWeakAgainst(PProperty prop, DataProvider dataProvider)
	{
		Collection<PropertyRelation> propertyRelations = CollectionUtils.select(dataProvider.listPropertyRelations(prop), new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				PropertyRelation rel = (PropertyRelation) object;
				return rel.getRelationType() == PropertyRelation.RelationType.STRONG_AGAINST;
			}
		});
		Collection<PProperty> propertiesCountered = CollectionUtils.collect(propertyRelations, new Transformer() {
			@Override
			public Object transform(Object object) {
				PropertyRelation rel = (PropertyRelation) object;
				return rel.getRelationTo();
			}
		});
		List<TeamMember> result = new ArrayList<>(teamSizeMax);
		for (TeamMember teamMember : members) {
			for (PProperty pe : dataProvider.getDistinctProperties(teamMember)) {
				if (propertiesCountered.contains(pe)) {
					result.add(teamMember);
					break;
				}
			}
		}
		return result;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(21, 77)
				.append(id)
				.append(name)
				.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Team))
			return false;
		Team other = (Team) o;
		return new EqualsBuilder()
				.append(this.id, other.id)
				.append(this.name, other.name)
				.isEquals();
	}

	@Override
	public int compareTo(Team another)
	{
		return getName().compareTo(another.getName());
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
		modified();
	}

	public void remove(TeamMember member) {
		this.members.remove(member);
		modified();
	}

	public boolean contains(int pId, int formIdx) {
		for (TeamMember tmp : getMembersSafe()) {
			if (tmp.getPid() == pId && ((formIdx < 0 || formIdx == tmp.getVariantIdx())))
				return true;
		}
		return false;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
		modified();
	}

	public Long getLastModified() {
		if (lastModified == null) {
			modified();
		}
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	private void modified() {
		this.lastModified = new Date().getTime();
	}
}
