package de.digitalpenguin.pokeapp.data;

import java.util.ArrayList;
import java.util.List;

public class TeamDB
{
	private List<Team> teams;

	public TeamDB()
	{
		teams = new ArrayList<>(0);
	}

	public TeamDB(List<Team> teamEntries)
	{
		this.teams = teamEntries;
	}

	public List<Team> getTeams()
	{
		return teams;
	}

	public void setTeams(List<Team> teams)
	{
		this.teams = teams;
	}
}
