package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Cache;

@Entity
@Cache
public class Pokedex implements Identifiable, Serializable {

    private static final long serialVersionUID = -6740360300858107739L;

    public static final int MAX_POKEMON_ID = 890;

    @Id
    private int id;

    @OneToOne
    private Region region;

    @OneToMany(mappedBy = "pokedexId", cascade = CascadeType.ALL, targetEntity = PokedexEntry.class)
    private List<PokedexEntry> entries;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEntries(List<PokedexEntry> entries) {
        this.entries = entries;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<PokedexEntry> getEntries() {
        return entries;
    }

    public long[] getNationalDexIds() {
        return nationalDexIds;
    }

    public void setNationalDexIds(long[] nationalDexIds) {
        this.nationalDexIds = nationalDexIds;
    }

    public Map<Integer, Integer> getNatDexToLocalDexMapping() {
        return natDexToLocalDexMapping;
    }

    public void setNatDexToLocalDexMapping(Map<Integer, Integer> natDexToLocalDexMapping) {
        this.natDexToLocalDexMapping = natDexToLocalDexMapping;
    }

    public void addEntry(int id, int natDexId) {
        if (entries == null)
            entries = new LinkedList<>();
        entries.add(new PokedexEntry(id, natDexId));
    }

    @Transient
    private long[] nationalDexIds;

    public boolean containsNationalDexId(long natDexId) {
        if (nationalDexIds == null) {
            nationalDexIds = new long[entries.size()];
            int idx = 0;
            for (PokedexEntry e: getEntries()) {
                nationalDexIds[idx] = e.getNationalDexId();
                idx++;
            }
            Arrays.sort(nationalDexIds);
        }
        return Arrays.binarySearch(nationalDexIds, natDexId) >= 0;
    }

    @Transient
    private Map<Integer, Integer> natDexToLocalDexMapping;

    public Integer getLocalDexId(int natDexId) {
        if (natDexToLocalDexMapping == null) {
            initMapping();
        }
        return natDexToLocalDexMapping.get(Integer.valueOf(natDexId));
    }

    private void initMapping() {
        natDexToLocalDexMapping = new HashMap<>();
        for (PokedexEntry e : getEntries()) {
            natDexToLocalDexMapping.put(e.getNationalDexId(), e.getLocalDexId());
        }
    }

	@Override
	public String toString() {
		return "Pokedex [entries=" + StringUtils.join(entries, ',') + "]";
	}

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
