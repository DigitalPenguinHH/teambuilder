package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class CharacteristicsDB {

    private List<Characteristic> entries;

    public CharacteristicsDB() {
    }

    public CharacteristicsDB(List<Characteristic> entries) {
        this.entries = entries;
    }

    public List<Characteristic> getEntries() {
        return entries;
    }

    public void setEntries(List<Characteristic> entries) {
        this.entries = entries;
    }
}
