package de.digitalpenguin.pokeapp.data.xml;

import org.w3c.dom.Element;

import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Variant;

public class PokemonDataValueReader extends PokemonValueReader {

    public PokemonDataValueReader(XPathSupport support) {
        super(support);
    }

    @Override
    public Pokemon from(Element element) {
        Pokemon base = super.from(element);
        PokemonData p = new PokemonData();
        p.setId(base.getId());
        p.setNames(base.getNames());
        p.setProperties(base.getProperties());
        // custom attributes
        p.setForms(support.readAnotherList(element, "variants/variant", new VariantValueReader(support)));
        return p;
    }

}
