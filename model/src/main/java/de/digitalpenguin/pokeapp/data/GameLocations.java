package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class GameLocations
{
	private Game game;
	
	private List<Location> locations;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
}
