package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.Identifiable;

@Entity
@Table(name = "POKEDEX_ENTRY")
public class PokedexEntry implements Serializable, Identifiable {

    private static final long serialVersionUID = -5853640227265566841L;

    @Id
    private int id;

    @Column(name = "local_dex_id")
    private int localDexId;

    @Column(name = "national_dex_id")
    private int nationalDexId;

    @Column(name = "pokedex_id")
    private int pokedexId;

    public PokedexEntry() {

    }

    public PokedexEntry(int localId, int natDexId) {
        this.localDexId = localId;
        this.nationalDexId = natDexId;
    }

    public PokedexEntry(int id, int localId, int natDexId, int pokedexId) {
        setId(id);
        this.localDexId = localId;
        this.nationalDexId = natDexId;
        this.pokedexId = pokedexId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocalDexId() {
        return localDexId;
    }

    public void setLocalDexId(int localDexId) {
        this.localDexId = localDexId;
    }

    public int getNationalDexId() {
        return nationalDexId;
    }

    public void setNationalDexId(int nationalDexId) {
        this.nationalDexId = nationalDexId;
    }

    public int getPokedexId() {
        return pokedexId;
    }

    public void setPokedexId(int pokedexId) {
        this.pokedexId = pokedexId;
    }

	@Override
	public String toString() {
		return "[LOCAL=" + localDexId + ", NATIONAL=" + nationalDexId + "]";
	}

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
