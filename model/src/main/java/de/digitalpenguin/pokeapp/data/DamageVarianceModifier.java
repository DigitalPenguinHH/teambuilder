package de.digitalpenguin.pokeapp.data;

import de.digitalpenguin.pokeapp.persistence.EnumConverter;

public enum DamageVarianceModifier {
	
	MISC, WEIGHT, KP, LEVEL, AFFECTION, RANDOM;

	public static DamageVarianceModifier parse(String text) {
		text = text.replace("*", "").trim();
		if (text.equals("Variiert")) {
			return MISC;
		} else if (text.contains("Zufall")) {
			return RANDOM;
		} else if (text.equals("Gewicht")) {
			return DamageVarianceModifier.WEIGHT;
		} else if (text.equals("Level")) {
			return DamageVarianceModifier.LEVEL;
		} else if (text.equals("Zuneigung")) {
			return DamageVarianceModifier.AFFECTION;
		} else if (text.equals("KP")) {
			return DamageVarianceModifier.KP;
		}
		return null;
	}

	public static class DamageVarianceModifierConverter extends EnumConverter<DamageVarianceModifier>
	{
		@Override
		protected DamageVarianceModifier from(String value) {
			return DamageVarianceModifier.valueOf(value);
		}
	}

}
