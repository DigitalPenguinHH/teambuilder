package de.digitalpenguin.pokeapp.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import de.digitalpenguin.pokeapp.data.util.Identifiable;

@Entity
public class Generation implements Comparable<Generation>, Serializable, Identifiable {

    private static final long serialVersionUID = -2109451990525150551L;

    public static final int GEN_8_ID = 8;

    @Id
	private int id;

    @OneToMany(mappedBy = "generation", targetEntity = HiddenMachine.class, cascade = CascadeType.ALL)
    private List<HiddenMachine> hms = new ArrayList<>(8);


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<HiddenMachine> getHms() {
        return hms;
    }

    public void setHms(List<HiddenMachine> hms) {
        this.hms = hms;
    }

    @Override
    public int compareTo(Generation gen) {
        return Integer.valueOf(this.id).compareTo(Integer.valueOf(gen.id));
    }

    @Override
    public Serializable getIdentifier() {
        return getId();
    }
}
