package de.digitalpenguin.pokeapp.data.xml;

import android.annotation.SuppressLint;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.AbstractDataProvider;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.MachineProperty;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;

public class XPathDataProvider extends AbstractDataProvider {

    @SuppressLint("UseSparseArrays")
    private Map<Integer, Pokemon> pokemonCache = new HashMap<>(1000);

    private static Map<Class<? extends Serializable>, AbstractValueReader<? extends Serializable>> readerCache = new HashMap<>(10);

    private XPathSupport xpath;

    public XPathDataProvider() {
        xpath = new XPathSupport();
    }

    public void addSupport(Class<? extends Serializable> type, InputStream is) {
        xpath.add(type, new ResetOnCloseInputStream(is));
        try {
            String readerClassName = getClass().getPackage().getName().concat(".").concat(type.getSimpleName()).concat("ValueReader");
            readerCache.put(type, (AbstractValueReader<? extends Serializable>) Class.forName(readerClassName).getConstructor(XPathSupport.class).newInstance(xpath));
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to create ValueReader for Type " + type.getSimpleName());
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <T extends Serializable> AbstractValueReader<T> getValueReader(Class<T> type) {
        return (AbstractValueReader<T>) readerCache.get(type);
    }

    @Override
    public Pokemon getById(int pokemonId) {
        if (!pokemonCache.containsKey(pokemonId)) {
            Pokemon p = getValueReader(Pokemon.class).from(xpath.eval(String.format("./db/entries/pokemon[id='%d']", pokemonId), Pokemon.class));
            pokemonCache.put(pokemonId, p);
        }
        return pokemonCache.get(pokemonId);
    }

    @Override
    public Pokemon getRandomPokemon(Integer generation) {
        return null;
    }

    @Override
    public PokemonData getPokemonData(int pokemonId) {
        return getValueReader(PokemonData.class).from(xpath.eval(String.format("./db/entries/pokemon[id='%d']", pokemonId), Pokemon.class));
    }

    @Override
    public PokemonData getPokemonData(Pokemon p) {
        return null;
    }

    @Override
    public List<Pokemon> getPokemonEntries() {
        return getValueReader(Pokemon.class).fromList(xpath.evalToList("./db/entries/pokemon", Pokemon.class));
    }

    @Override
    public List<Variant> getForms(int pokemonId) {
        return null;
    }

    @Override
    public PokemonForm getPokemonForm(int pid, int variantIdx) {
        return null;
    }

    @Override
    public PokemonForm getPokemonForm(int pid, VariantType form) {
        return null;
    }

    @Override
    public List<Pokemon> findPokemon(String name) {
        return null;
    }

    @Override
    public List<PokemonForm> findPokemonForms(String name) {
        return null;
    }

    @Override
    public List<Pokemon> findPokemon(DataFilter filter) {
        return null;
    }

    @Override
    public Move getMove(int moveId) {
        return null;
    }

    @Override
    public Move getMoveByName(String value, Locale locale) {
        return null;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }

    @Override
    public List<Move> findMoves(String name) {
        return null;
    }

    @Override
    public List<Move> listMoves(MoveType selectedMoveType, PProperty selectedProperty) {
        return null;
    }

    @Override
    public List<MachineProperty> listMachineProperties() {
        return null;
    }

    @Override
    public Move getMoveForMachine(String machineName) {
        return null;
    }

    @Override
    public Map<Integer, List<MoveLearned>> getGenerationsMoveSet(PokemonForm p) {
        return null;
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, MoveLearnTrigger trigger) {
        return null;
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId) {
        return null;
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId, MoveLearnTrigger trigger) {
        return null;
    }

    @Override
    public boolean hasGameSpecificMoves(Pokemon p) {
        return false;
    }

    @Override
    public List<MoveLearned> listMovesLearned(PokemonForm p) {
        return null;
    }

    @Override
    public EvolutionChain getEvolutionChain(int pokemonId) {
        return null;
    }

    @Override
    public List<EvolutionChain> getEvolutionChains() {
        return null;
    }

    @Override
    public List<Item> getItems() {
        return null;
    }

    @Override
    public List<Item> listItems(ItemCategory category) {
        return null;
    }

    @Override
    public List<Item> findItems(String name) {
        return null;
    }

    @Override
    public List<Generation> getGenerations() {
        return null;
    }

    @Override
    public List<Region> getRegions() {
        return null;
    }

    @Override
    public Region getRegion(Integer generation) {
        return null;
    }

    @Override
    public List<Game> getGames() {
        return null;
    }

    @Override
    public List<Characteristic> getCharacteristics() {
        return null;
    }

    @Override
    public List<Characteristic> findCharacteristics(String name) {
        return null;
    }

    @Override
    public List<Nature> getNatures() {
        return null;
    }

    @Override
    public List<Nature> findNatures(String name) {
        return null;
    }

    @Override
    public Ability getAbility(int id) {
        return null;
    }

    @Override
    public List<Ability> getAbilities() {
        return null;
    }

    @Override
    public List<Ability> findAbilities(String name) {
        return null;
    }

    @Override
    public List<MoveLearned> listPokemonMovesLearned(int moveId) {
        return null;
    }

    @Override
    public List<PokemonAbility> listPokemonWithAbility(int abilityId) {
        return null;
    }

    @Override
    public List<PokemonAbility> listAbilities(int pokemonId, VariantType type) {
        return null;
    }

    @Override
    public boolean hasMoves(PokemonForm pokemon, int generation, String gameKey) {
        return false;
    }

    @Override
    public int getPokemonEntriesCount() {
        return 0;
    }
}
