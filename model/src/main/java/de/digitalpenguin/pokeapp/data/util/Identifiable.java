package de.digitalpenguin.pokeapp.data.util;

import java.io.Serializable;

public interface Identifiable {

    Serializable getIdentifier();

}
