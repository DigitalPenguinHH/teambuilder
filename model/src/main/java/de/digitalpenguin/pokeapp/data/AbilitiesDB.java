package de.digitalpenguin.pokeapp.data;

import java.util.LinkedList;
import java.util.List;

public class AbilitiesDB
{
	private List<Ability> entries = new LinkedList<Ability>();

	public AbilitiesDB()
	{
		super();
	}

	public AbilitiesDB(List<Ability> abilities)
	{
		super();
		this.entries = abilities;
	}

	public List<Ability> getEntries() {
		return entries;
	}

	public void setEntries(List<Ability> abilities) {
		this.entries = abilities;
	}
}
