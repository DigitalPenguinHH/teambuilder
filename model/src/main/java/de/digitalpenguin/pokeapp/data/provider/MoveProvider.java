package de.digitalpenguin.pokeapp.data.provider;

import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Move;

public interface MoveProvider {

    Move getMove(int moveId);

    Move getMoveByName(String value, Locale locale);

}
