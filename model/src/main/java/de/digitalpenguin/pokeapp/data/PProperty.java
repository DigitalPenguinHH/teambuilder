package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import de.digitalpenguin.pokeapp.data.util.Named;
import de.digitalpenguin.pokeapp.persistence.AbstractStringConverter;

public enum PProperty implements Named
{
	
	NORMAL,							// 0
	FIRE, 							// 1
	WATER, 							// 2
	GRASS, 							// 3
	POISON, 						// 4
	GROUND, 						// 5
	FLYING, 						// 6
	ELECTRIC, 						// 7
	ROCK, 							// 8
	BUG, 							// 9
	PSYCHIC, 						// 10
	FIGHTING, 						// 11
	GHOST, 							// 12
	ICE, 							// 13
	DRAGON, 						// 14
	STEEL, 							// 15
	DARK, 							// 16
	FAIRY							// 17
	;
	
	public static PProperty from(String s)
	{
		if (s == null || s.isEmpty() || s.trim().startsWith("??"))
			return null;
		s = s.replaceAll("ä", "ae");
		PProperty p;
		String key = s.toUpperCase(Locale.ENGLISH);
		try {
			p = PProperty.valueOf(key.trim());
		} catch (Exception e) {
			p = translate(key.trim());
		}
		return p;
	}
	
	public static PProperty translate(String germanName) {
		if (germanName.equalsIgnoreCase("NORMAL")) {
			return NORMAL;
		} else if (germanName.equalsIgnoreCase("FEUER")) {
			return FIRE;
		} else if (germanName.equalsIgnoreCase("WASSER")) {
			return WATER;
		} else if (germanName.equalsIgnoreCase("PFLANZE")) {
			return GRASS;
		} else if (germanName.equalsIgnoreCase("GIFT")) {
			return POISON;
		} else if (germanName.equalsIgnoreCase("FLUG")) {
			return FLYING;
		} else if (germanName.equalsIgnoreCase("ELEKTRO")) {
			return ELECTRIC;
		} else if (germanName.equalsIgnoreCase("KAMPF")) {
			return FIGHTING;
		} else if (germanName.equalsIgnoreCase("KÄFER")) {
			return BUG;
		} else if (germanName.equalsIgnoreCase("BODEN")) {
			return GROUND;
		} else if (germanName.equalsIgnoreCase("GESTEIN")) {
			return ROCK;
		} else if (germanName.equalsIgnoreCase("PSYCHO")) {
			return PSYCHIC;
		} else if (germanName.equalsIgnoreCase("GEIST")) {
			return GHOST;
		} else if (germanName.equalsIgnoreCase("EIS")) {
			return ICE;
		} else if (germanName.equalsIgnoreCase("DRACHE")) {
			return DRAGON;
		} else if (germanName.equalsIgnoreCase("STAHL")) {
			return STEEL;
		} else if (germanName.equalsIgnoreCase("FEE")) {
			return FAIRY;
		} else if (germanName.equalsIgnoreCase("UNLICHT")) {
			return DARK;
		}
		return null;
	}

	@Override
	public String getName() {
		return this.name();
	}

	public static class PropertyArrayConverter extends AbstractStringConverter<PProperty[]> {

		@Override
		public PProperty[] convertToEntityProperty(String databaseValue) {
			if (databaseValue == null || databaseValue.trim().length() == 0)
				return null;
			StringTokenizer st = new StringTokenizer(databaseValue, ",");
			List<PProperty> resultSet = new LinkedList<>();
			while (st.hasMoreElements()) {
				String s = (String) st.nextElement();
				if (StringUtils.isNotBlank(s))
					resultSet.add(PProperty.valueOf(s));
			}
			return resultSet.toArray(new PProperty[resultSet.size()]);
		}

		@Override
		public String convertToDatabaseValue(PProperty[] entityProperty) {
			return StringUtils.join(entityProperty,',');
		}
	}
}
