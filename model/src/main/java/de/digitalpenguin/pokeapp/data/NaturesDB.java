package de.digitalpenguin.pokeapp.data;

import java.util.List;

public class NaturesDB {

    private List<Nature> entries;

    public NaturesDB() {
    }

    public NaturesDB(List<Nature> entries) {
        this.entries = entries;
    }

    public List<Nature> getEntries() {
        return entries;
    }

    public void setEntries(List<Nature> entries) {
        this.entries = entries;
    }
}
