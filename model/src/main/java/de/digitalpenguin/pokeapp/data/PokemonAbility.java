package de.digitalpenguin.pokeapp.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import de.digitalpenguin.pokeapp.data.util.BaseIdEntity;
import de.digitalpenguin.pokeapp.data.util.Identifiable;
import de.digitalpenguin.pokeapp.persistence.Converter;
import de.digitalpenguin.pokeapp.persistence.StringArrayConverter;

@Entity
@Table(name = "POKEMON_ABILITY")
public class PokemonAbility extends BaseIdEntity
{

	@Column(name = "ability_id")
	private int abilityId;

	@Column(name = "pokemon_id")
	private int pokemonId;

	@OneToOne
	@Column(name = "pokemon_id")
	private Pokemon pokemon;

	@OneToOne
	@Column(name = "ability_id")
	private Ability ability;

	@OneToOne
	@Column(name = "variant_id")
	private Variant variant;

	@Column
	private boolean hidden = false;

	public PokemonAbility() {
	}

	public PokemonAbility(Integer pokemonId, int ablityId) {
		this.pokemonId = pokemonId;
		this.abilityId = ablityId;
	}

	public int getPokemonId() {
		return pokemonId;
	}

	public void setPokemonId(int pokemonId) {
		this.pokemonId = pokemonId;
	}

	public int getAbilityId() {
		return abilityId;
	}

	public void setAbilityId(int abilityId) {
		this.abilityId = abilityId;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public Pokemon getPokemon() {
		return pokemon;
	}

	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}

	public Ability getAbility() {
		return ability;
	}

	public void setAbility(Ability ability) {
		this.ability = ability;
	}

	public Variant getVariant() {
		return variant;
	}

	public void setVariant(Variant variant) {
		this.variant = variant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PokemonAbility that = (PokemonAbility) o;
		return new EqualsBuilder()
				.append(this.pokemonId, that.pokemonId)
				.append(this.abilityId, that.abilityId)
				.append(this.variant, that.variant)
				.append(this.hidden, that.hidden)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 311)
				.append(pokemonId)
				.append(abilityId)
				.append(variant)
				.append(hidden)
				.toHashCode();
	}
}
