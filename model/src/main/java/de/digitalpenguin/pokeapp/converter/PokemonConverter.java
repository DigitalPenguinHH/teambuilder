package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Stats;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.Version;

public class PokemonConverter extends LocalizedNamedConverter
{

	private static final String COMMA = ",";

	private static final String ATTR_ID = "id";

	private static final String ATTR_PROPERTIES = "properties";	

	private static final String ATTR_NAMES = "names";

	private static final String ATTR_VERSION = "version";

	private static final String ATTR_HEIGHT = "height";

	private static final String ATTR_WEIGHT = "weight";

	private static final String ATTR_GENDERS = "genders";

	private static final String ATTR_MOVES_LEARNED = "movesLearned";

	private static final String ATTR_BASE_STATS = "baseStats";

	private static final String ATTR_DEXENTRIES = "dexEntries";

	private static final String ATTR_ENTRY = "entry";

	private static final String ATTR_VARIANTS = "variants";

	private static final String ATTR_VARIANT = "variant";

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
		return PokemonData.class.equals(clazz);
	}

	@Override
	public void marshal(Object src, HierarchicalStreamWriter writer, MarshallingContext context) {
		PokemonData p = (PokemonData) src;
		newIntNode(writer, ATTR_ID, p.getId());
		writeLocalizations(writer, p, true);
		if (CollectionUtils.isNotEmpty(p.getForms())) {
			writer.startNode(ATTR_VARIANTS);
			for (Variant v: p.getForms()) {
				writer.startNode(ATTR_VARIANT);
				context.convertAnother(v);
				writer.endNode();
			}
			writer.endNode();
		}
		newStringNode(writer, ATTR_VERSION, p.getVersion().name());
		newStringNode(writer, ATTR_PROPERTIES, StringUtils.join(p.getProperties(), ','));
		newDecimalNode(writer, ATTR_HEIGHT, p.getHeight());
		newDecimalNode(writer, ATTR_WEIGHT, p.getWeight());
		newStringNode(writer, ATTR_GENDERS, p.getGenderRatio().toString());
		writer.startNode(ATTR_MOVES_LEARNED);
		if (p.getMovesLearned() != null && !p.getMovesLearned().isEmpty()) {
            String joined = MoveLearned.stringify(p.getMovesLearned());
            writer.setValue(joined);
		}
		writer.endNode();
		newStringNode(writer, ATTR_BASE_STATS, compressBaseStats(p));
		writer.startNode(ATTR_DEXENTRIES);
		for (Entry<String,String> e : p.getDexEntries().entrySet()) {
			writer.startNode(ATTR_ENTRY);
			writer.addAttribute(LANG, e.getKey());
			writer.setValue(e.getValue());
			writer.endNode();
		}
		writer.endNode();
	}
	
	private String compressBaseStats(PokemonData p) {
		return StringUtils.join(new String[]{
				Integer.toString(p.getBaseStats().getKp()),
				Integer.toString( p.getBaseStats().getAtk()),
				Integer.toString(p.getBaseStats().getDef()),
				Integer.toString(p.getBaseStats().getSpcAtk()),
				Integer.toString(p.getBaseStats().getSpcDef()),
				Integer.toString(p.getBaseStats().getInit())
		}, ',');
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		PokemonData p;
		try {
			p = new PokemonData();
			p.setMovesLearned(new LinkedList<MoveLearned>());
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				String tag = reader.getNodeName();
				String value = reader.getValue();
				switch (tag) {
					case ATTR_ID:
						p.setId(Integer.parseInt(value));
						break;
					case ATTR_NAMES:
						readNames(reader, p);
						break;
					case ATTR_VARIANTS:
						List<Variant> variants = new LinkedList<>();
						while (reader.hasMoreChildren()) {
							reader.moveDown();
							Variant v = (Variant) context.convertAnother(p, Variant.class);
							variants.add(v);
							reader.moveUp();
						}
						p.setForms(variants);
						break;
					case ATTR_VERSION:
						p.setVersion(Version.valueOf(value));
						break;
					case ATTR_PROPERTIES:
						PProperty[] els = new PProperty[2];
						String propertiesStr = reader.getValue();
						String[] parts = propertiesStr.split(COMMA);
						els[0] = PProperty.valueOf(parts[0]);
						if (parts.length > 1) {
							if (StringUtils.isNotEmpty(parts[1]))
								els[1] = PProperty.valueOf(parts[1]);
						}
						p.setProperties(els);
						break;
					case ATTR_HEIGHT:
						p.setHeight(Float.parseFloat(value));
						break;
					case ATTR_WEIGHT:
						p.setWeight(Float.parseFloat(value));
						break;
					case ATTR_GENDERS:
						if (value.contains("#")) {
							p.setMalePercent(Float.parseFloat(value.split("#")[0].substring(1)));
							p.setFemalePercent(Float.parseFloat(value.split("#")[1].substring(1)));
						}
						break;
					case ATTR_MOVES_LEARNED:
						String val = reader.getValue();
						p.setMovesLearned(MoveLearned.destringify(val, p.getId()));
						break;
					case ATTR_BASE_STATS:
						Stats stats = new Stats();
						String statsStr = reader.getValue();
						String[] values = statsStr.split(COMMA);
						for (int i = 0; i < values.length; i++) {
							switch (i) {
								case 0:
									stats.setKp(Integer.parseInt(values[i]));
									break;
								case 1:
									stats.setAtk(Integer.parseInt(values[i]));
									break;
								case 2:
									stats.setDef(Integer.parseInt(values[i]));
									break;
								case 3:
									stats.setSpcAtk(Integer.parseInt(values[i]));
									break;
								case 4:
									stats.setSpcDef(Integer.parseInt(values[i]));
									break;
								case 5:
									stats.setInit(Integer.parseInt(values[i]));
									break;
								default:
									break;
							}
						}
						p.setBaseStats(stats);
						break;
					case ATTR_DEXENTRIES:
						while (reader.hasMoreChildren()) {
							reader.moveDown();
							String lang = reader.getAttribute("lang");
							String text = reader.getValue();
							p.addDexEntry(lang, text);
							reader.moveUp();
						}
						break;
					default:
						break;
				}
				reader.moveUp();
			}
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
