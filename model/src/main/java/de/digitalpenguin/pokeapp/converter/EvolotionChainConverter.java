package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.LinkedList;

import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;

public class EvolotionChainConverter implements Converter
{

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class type) {
		return EvolutionChain.class.equals(type);
	}

	@Override
	public void marshal(Object src, HierarchicalStreamWriter writer, MarshallingContext context) {
		EvolutionChain chain = (EvolutionChain) src; 
		for (EvolutionChainLink link : chain.getChainLinks()) {
			writer.startNode("link");
			context.convertAnother(link);
			writer.endNode();
		}
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		EvolutionChain chain = new EvolutionChain();
		chain.setChainLinks(new LinkedList<EvolutionChainLink>());
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			EvolutionChainLink chainLink = (EvolutionChainLink) context.convertAnother(context, EvolutionChainLink.class);
			chain.getChainLinks().add(chainLink);
			reader.moveUp();
		}
		return chain;
	}

}
