package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.EvolutionChainLink;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.EvolutionCondition.Type;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.Location;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class EvolotionChainLinkConverter implements Converter
{

	private static final String ATTR_FROM = "from";
	private static final String ATTR_TO = "to";
	private static final String TAG_CONDITION = "condition";
	private static final String ATTR_TYPE = "type";
	private static final String ATTR_GAME = "game";
	private static final String ATTR_GENERATION = "fromGeneration";
	private static final String ATTR_VAR_FROM = "fromVariant";
	private static final String ATTR_VAR_TO = "toVariant";

	private DataProvider dataProvider;

	public EvolotionChainLinkConverter(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class type) {
		return EvolutionChainLink.class.equals(type);
	}

	@Override
	public void marshal(Object src, HierarchicalStreamWriter writer, MarshallingContext context) {
		EvolutionChainLink link = (EvolutionChainLink) src; 
		writer.addAttribute(ATTR_FROM, intToStr(link.getPokemonIdFrom()));
		writer.addAttribute(ATTR_TO, intToStr(link.getPokemonIdTo()));
		for (EvolutionCondition<?> condition : link.getTrigger()) {
			writer.startNode("condition");
			writer.addAttribute("type", condition.getCondition().name());
			if (condition.getGames() != null && !condition.getGames().isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (Game g : condition.getGames()) {
					sb.append(g.getName(Locale.ENGLISH));
					sb.append(',');
				}
				sb.deleteCharAt(sb.length() - 1);
				writer.addAttribute("generation", sb.toString());
			}
			writer.setValue(stringify(condition.getObject()));
			writer.endNode();
		}
	}

	private String stringify(Object o) {
		if (Integer.class.equals(o.getClass())) {
			return intToStr(o);
		} else if (Item.class.equals(o.getClass())) {
			return ((Item) o).getName(); // Item name
		} else if (Move.class.equals(o.getClass())) {
			return ((Move) o).getName(Locale.GERMAN); // Move name
		} else if (String.class.equals(o.getClass())) {
			return (String) o;
		}
		return "";
	}

	private String intToStr(Object o) {
		return ((Integer) o).toString();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		EvolutionChainLink link = new EvolutionChainLink();
		// From/To
		link.setPokemonIdFrom(Integer.parseInt(reader.getAttribute(ATTR_FROM)));
		link.setPokemonIdTo(Integer.parseInt(reader.getAttribute(ATTR_TO)));
		// Variant infos
		String fromVar = reader.getAttribute(ATTR_VAR_FROM);
		if (StringUtils.isNotEmpty(fromVar)) {
			link.setVariantFrom(fromVar);
		}
		String toVar = reader.getAttribute(ATTR_VAR_TO);
		if (StringUtils.isNotEmpty(toVar)) {
			link.setVariantTo(toVar);
		}
		link.setTrigger(new LinkedList<EvolutionCondition<?>>());
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			// Conditions
			if (reader.getNodeName().equals(TAG_CONDITION)) {
				EvolutionCondition<?> c;
				Type type = Type.valueOf(reader.getAttribute(ATTR_TYPE));
				String value = reader.getValue();
				switch (type) {
					case LEVELUP:
					case POKEMON_IN_TEAM:
					case FRIENDSHIP:
                    case HEARTS_POKEMONAMI:
                    case HEARTS_POKEREFRESH:
					case TRADE_AGAINST:
						c = new EvolutionCondition<>(type, StringUtils.isNumeric(value) ? getInteger(value) : null);
						break;
					case ITEM:
					case TRADE_WITH_ITEM:
						c = new EvolutionCondition<>(type, dataProvider.getItemByName(value, Locale.GERMAN));
						break;
					case ABILITY:
						c = new EvolutionCondition<>(type, dataProvider.getMoveByName(value, Locale.GERMAN));
						break;
					case PROPERTY_ABILITY:
						c = new EvolutionCondition<>(type, PProperty.from(value));
						break;
					case LOCATION:
						c = new EvolutionCondition<>(type, new Location(value));
						break;
					case CANDIES:
						c = new EvolutionCondition<>(type, getInteger(value));
						break;
					default: 
						c = new EvolutionCondition<>(type, value);
						break;
				}
				c.setCondition(type);
				// Game specific condition?
				String gamesStr = reader.getAttribute(ATTR_GAME);
				if (gamesStr != null) {
					List<Game> gamesList = new LinkedList<>();
					for (String s : gamesStr.split(",")) {
						Game g = dataProvider.getGameByName(s);
						if (g != null)
							gamesList.add(g);
					}
					c.setGames(gamesList);
				}
				// Generation specific condition?
				String gensStr = reader.getAttribute(ATTR_GENERATION);
				if (gensStr != null) {
					c.setFromGeneration(Integer.parseInt(gensStr));
				}
				//
				link.getTrigger().add(c);
			}
			reader.moveUp();
		}
		return link;
	}

	private Integer getInteger(Object o) {
		return Integer.valueOf((String) o );
	}

}
