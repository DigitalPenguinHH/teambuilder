package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.StatType;

public class NatureConverter extends LocalizedNamedConverter {

    private static final String TAG_INC = "increased";

    private static final String TAG_DEC = "decreased";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        // not implemented
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Nature n = new Nature();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch(nodeName) {
                case TAG_INC:
                    n.setIncreased(StatType.valueOf(reader.getValue().toUpperCase()));
                    break;
                case TAG_DEC:
                    n.setDecreased(StatType.valueOf(reader.getValue().toUpperCase()));
                    break;
                case NAMES:
                    readNames(reader, n);
                    break;
            }
            reader.moveUp();
        }
        // finished
        return n;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Nature.class.equals(type);
    }
}
