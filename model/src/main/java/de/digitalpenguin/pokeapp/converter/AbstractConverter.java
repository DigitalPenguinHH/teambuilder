package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.lang3.tuple.Pair;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public abstract class AbstractConverter implements Converter {

    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n";
    
    @SuppressWarnings("unchecked")
	protected <T> T cast(Object o, Class<T> toType) {
    	return (T) o;
    }

    protected void newStringNode(HierarchicalStreamWriter writer, String name, String value) {
        writer.startNode(name);
        writer.setValue(value);
        writer.endNode();
    }

    protected void newStringNode(HierarchicalStreamWriter writer, String name, String value, Pair<String, String> attribute) {
        writer.startNode(name);
        if (attribute != null) {
			writer.addAttribute(attribute.getKey(), attribute.getValue());
        }
        writer.setValue(value);
        writer.endNode();
    }

    protected void newIntNode(HierarchicalStreamWriter writer, String name, Number value) {
        writer.startNode(name);
        writer.setValue(value == null ? "" : value.toString());
        writer.endNode();
    }

    protected void newBooleanNode(HierarchicalStreamWriter writer, String name, Boolean value) {
        writer.startNode(name);
        writer.setValue(value ? "true" : "false");
        writer.endNode();
    }

    protected void newDecimalNode(HierarchicalStreamWriter writer, String name, Float value) {
        writer.startNode(name);
        writer.setValue(new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.ENGLISH)).format(value));
        writer.endNode();
    }

}
