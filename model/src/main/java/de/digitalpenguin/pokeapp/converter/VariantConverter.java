package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.provider.ItemProvider;

public class VariantConverter extends LocalizedNamedConverter {

    private static final String PROPERTIES = "properties";

    private static final String TRIGGER = "trigger";

    private ItemProvider itemProvider;

    public VariantConverter(ItemProvider itemProvider) {
        this.itemProvider = itemProvider;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        Variant v = (Variant) source;
        if (v.getType() != VariantType.OTHER)
            writer.addAttribute("type", v.getType().name().toLowerCase());
        if (StringUtils.isNotEmpty(v.getSuffix()))
            writer.addAttribute("suffix", v.getSuffix());
        writeLocalizations(writer, v);
        if (v.getProperties() != null && v.getProperties().length > 0) {
            newStringNode(writer, PROPERTIES, StringUtils.join(v.getProperties(), ','));
        }
        if (v.getTrigger() != null) {
            newStringNode(writer, TRIGGER, v.getTrigger().getName(Locale.ENGLISH));
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Variant v = new Variant();
        String typeStr = reader.getAttribute("type");
        v.setType(StringUtils.isEmpty(typeStr) ? VariantType.OTHER : VariantType.valueOf(typeStr.toUpperCase()));
        v.setSuffix(StringUtils.defaultString(reader.getAttribute("suffix"),""));
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String node = reader.getNodeName();
            switch (node) {
                case NAME:
                    readName(reader, v);
                    break;
                case PROPERTIES:
                    LinkedList<PProperty> propsList = new LinkedList<>();
                    String[] propsStr = reader.getValue().split(",");
                    for (String s : propsStr) {
                        propsList.add(PProperty.valueOf(s.toUpperCase()));
                    }
                    v.setProperties(propsList.toArray(new PProperty[0]));
                    break;
                case TRIGGER:
                    if (itemProvider != null) {
                        Item item = itemProvider.getItemByName(reader.getValue(), Locale.ENGLISH);
                        if (item != null) {
                            v.setTrigger(item);
                        } else {
                            v.setTrigger(new Item(reader.getValue()));
                        }
                    } else {
                        v.setTrigger(new Item(reader.getValue()));
                    }
                    break;
                default:
                    break;
            }
            reader.moveUp();
        }
        return v;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Variant.class.equals(type);
    }
}
