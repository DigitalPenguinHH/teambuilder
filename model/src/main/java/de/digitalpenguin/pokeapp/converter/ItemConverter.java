package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.Item;

public class ItemConverter extends LocalizedNamedConverter {

    private static final String TAG_CATEGORY = "category";

    private static final String TAG_DESCRIPTIONS = "descriptions";

    private Map<String, Map<Locale, String>> descriptionTranslations;

    public ItemConverter() {
        //
    }

    public ItemConverter(Map<String, Map<Locale, String>> descTranslations) {
        this.descriptionTranslations = descTranslations;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        if (MapUtils.isEmpty(descriptionTranslations))
            throw new IllegalArgumentException("Cannot serialize Item to xml without description translations map.");

        Item item = (Item) source;
        newStringNode(writer, TAG_CATEGORY, item.getCategory() != null ? item.getCategory().name().substring(0,2).toUpperCase() : "");

        writeLocalizations(writer, item, true);

        /* TODO: Implemetn localized descriptions
        writer.startNode(TAG_DESCRIPTIONS);
        itemTranslations = descriptionTranslations.get(item.getByName());
        String nameEn = nameTranslations.get(item.getByName()).get(Locale.ENGLISH);
        if (itemTranslations == null) {
            // try en
            itemTranslations = descriptionTranslations.get(nameEn);
        }
        if (itemTranslations != null) {
            for (Map.Entry<Locale, String> e : itemTranslations.entrySet()) {
                writer.startNode(TAG_DESCRIPTION);
                writer.addAttribute(ATTR_LANG, e.getKey().getLanguage());
                writer.setValue(e.getValue());
                writer.endNode();
            }
        }
        */

        writer.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Item item = new Item();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch(nodeName) {
                case TAG_CATEGORY:
                    item.setCategory(StringUtils.isNotEmpty(reader.getValue()) ? ItemCategory.parse(reader.getValue()) : null);
                    break;
                case NAMES:
                    readNames(reader, item);
                    break;
                case TAG_DESCRIPTIONS:
                    readDescriptions(reader, item);
                    break;
            }
            reader.moveUp();
        }
        // finished
        return item;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Item.class.equals(type);
    }

    private void readDescriptions(HierarchicalStreamReader reader, Item i) {
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            readDescription(reader, i);
            reader.moveUp();
        }
    }

    private void readDescription(HierarchicalStreamReader reader, Item i) {
        i.getDescriptions().addText(reader.getAttribute(LANG), reader.getValue());
    }
}
