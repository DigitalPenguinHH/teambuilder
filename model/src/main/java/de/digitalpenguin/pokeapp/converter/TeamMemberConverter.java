package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.TeamMember;

public class TeamMemberConverter extends AbstractConverter {

    private static final String TAG_SLOT = "slot";

    private static final String TAG_POKEMON_ID = "pid";

    private static final String TAG_PRIMARY_PROPERTY = "primaryPropertyIndex";

    private static final String TAG_MOVES = "movesLearned";

    private static final String TAG_VARIANT = "variant";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        TeamMember tm = (TeamMember) source;
        newIntNode(writer, TAG_SLOT, tm.getSlot());
        writer.startNode(TAG_POKEMON_ID);
        if (tm.getVariantIdx() >= 0)
            writer.addAttribute(TAG_VARIANT, Integer.toString(tm.getVariantIdx()));
        writer.setValue(Integer.toString(tm.getPid()));
        writer.endNode();
        newIntNode(writer, TAG_PRIMARY_PROPERTY, tm.getPrimaryPropertyIndex());
        if (CollectionUtils.isNotEmpty(tm.getMoves())) {
            writer.startNode(TAG_MOVES);
            String joined = MoveLearned.stringify(tm.getMoves());
            writer.setValue(joined);
            writer.endNode();
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        TeamMember tm = new TeamMember();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch (nodeName) {
                case TAG_SLOT:
                    tm.setSlot(Integer.parseInt(reader.getValue()));
                    break;
                case TAG_POKEMON_ID:
                    String variant = reader.getAttribute(TAG_VARIANT);
                    if (StringUtils.isNotEmpty(variant))
                        tm.setVariantIdx(Integer.parseInt(variant));
                    tm.setPid(Integer.parseInt(reader.getValue()));
                    break;
                case TAG_PRIMARY_PROPERTY:
                    tm.setPrimaryPropertyIndex(Integer.valueOf(reader.getValue()));
                    break;
                case TAG_MOVES:
                    List<MoveLearned> movesLearned = MoveLearned.destringify(reader.getValue(), tm.getPid());
                    tm.setMoves(new ArrayList<>(movesLearned));
                    break;
            }
            reader.moveUp();
        }
        // finished
        return tm;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return TeamMember.class.equals(type);
    }

}
