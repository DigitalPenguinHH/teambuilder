package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.Location;

public class LocationConverter extends AbstractConverter {

    private static final String NAME = "name";

    private static final String LANG = "lang";

    private Map<String, Map<String, String>> translations;

    public LocationConverter(Map<String, Map<String, String>> translations) {
        this.translations = translations;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        if (translations == null || translations.isEmpty())
            throw new IllegalArgumentException("Unable to marchal locations without translations.");
        Location l = (Location) source;
        Map<String, String> translationsForLocations = this.translations.get(l.getName());
        if (translationsForLocations == null)
            return;
        for (Map.Entry<String, String> trans : translationsForLocations.entrySet()) {
            writer.startNode(NAME);
            writer.addAttribute(LANG, trans.getKey());
            writer.setValue(trans.getValue());
            writer.endNode();
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Location l = new Location();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String node = reader.getNodeName();
            switch (node) {
                case NAME:
                    if (Locale.getDefault().getLanguage().equals(reader.getAttribute(LANG))) {
                        l.setName(reader.getValue());
                    } else if (Locale.ENGLISH.getLanguage().equals(reader.getAttribute(LANG))) {
                        l.setNameEn(reader.getValue());
                    }
                    break;
                default:
                    break;
            }
            reader.moveUp();
        }
        return l;
    }

    @SuppressWarnings("rawtypes")
	@Override
    public boolean canConvert(Class type) {
        return Location.class.equals(type);
    }
}
