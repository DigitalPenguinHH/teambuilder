package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;

public class GenerationConverter extends LocalizedNamedConverter {


    private static final String ID = "id";

    private static final String HMS = "hms";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        // no op
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Generation g = new Generation();
        g.setId(Integer.parseInt(reader.getAttribute(ID)));
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String node = reader.getNodeName();
            switch (node) {
                case HMS:
                    readHms(reader, g, context);
                    break;
                default:
                    break;
            }
            reader.moveUp();
        }
        return g;
    }

    private void readHms(HierarchicalStreamReader reader, Generation gen, UnmarshallingContext context) {
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            HiddenMachine hm = (HiddenMachine) context.convertAnother(reader, HiddenMachine.class);
            hm.setGeneration(gen.getId());
            gen.getHms().add(hm);
            reader.moveUp();
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Generation.class.equals(type);
    }
}
