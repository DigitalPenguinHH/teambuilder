package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.DamageVarianceModifier;
import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.PProperty;

public class MoveConverter extends LocalizedNamedConverter
{

	private static final String ID = "id";

	private static final String PROPERTY = "property";

	private static final String TYPE = "type";

	private static final String DAMAGE_FIX = "damageFix";

	private static final String DAMAGE_VARIABLE = "damageVariable";

	private static final String DAMAGE_VAR_MODIFIER = "damageVarianceModifier";

	private static final String STRENGTH = "strength";

	private static final String ACCURACY = "accuracy";

	private static final String AP = "ap";

	private static final String CONTEST_CATEGORY = "contestCategory";

	private static final String GENERATION = "generation";

	private static final String DESCRIPTIONS = "descriptions";


	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
		return Move.class.equals(clazz);
	}

	@Override
	public void marshal(Object src, HierarchicalStreamWriter writer, MarshallingContext context) {
		Move m = (Move) src;
		newIntNode(writer, "id", (int) m.getId());
		writeLocalizations(writer, m, true);
		if (m.getProperty() != null)
			newStringNode(writer, "property", m.getProperty().name());
		if (m.getType() != null)
			newStringNode(writer, "type", m.getType().name());
		newBooleanNode(writer, "damageFix", m.isDamageFix());
		newBooleanNode(writer, "damageVariable", m.isDamageVariable());
		if (m.getDamageVarianceModifier() != null)
			newStringNode(writer, "damageVarianceModifier", m.getDamageVarianceModifier().name());
		newIntNode(writer, "strength", m.getStrength());
		newIntNode(writer, "accuracy", m.getAccuracy());
		newIntNode(writer, "ap", m.getAp());
		if (m.getContestCategory() != null)
			newStringNode(writer, "contestCategory", m.getContestCategory().name());
		newIntNode(writer, "generation", m.getGeneration());
		if (m.getDescriptions() != null && !m.getDescriptions().isEmpty())
			writeDescriptions(writer, m.getDescriptions());
	}

	private void writeDescriptions(HierarchicalStreamWriter writer, LocalizedText descriptions) {
		writer.startNode(DESCRIPTIONS);
		for (Map.Entry<String, String> trans : descriptions.entrySet()) {
			writer.startNode(NAME);
			writer.addAttribute(LANG, trans.getKey());
			writer.setValue(trans.getValue());
			writer.endNode();
		}
		writer.endNode();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Move m = new Move();
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			String nodeName = reader.getNodeName();
			switch(nodeName) {
				case ID:
					m.setId(Integer.parseInt(reader.getValue()));
					break;
				case NAMES:
					readNames(reader, m);
					break;
				case PROPERTY:
					m.setProperty(PProperty.valueOf(reader.getValue()));
					break;
				case TYPE:
					m.setType(MoveType.valueOf(reader.getValue()));
					break;
				case DAMAGE_FIX:
					m.setDamageFix(Boolean.valueOf(reader.getValue()));
					break;
				case DAMAGE_VARIABLE:
					m.setDamageVariable(Boolean.valueOf(reader.getValue()));
					break;
				case DAMAGE_VAR_MODIFIER:
					m.setDamageVarianceModifier(DamageVarianceModifier.valueOf(reader.getValue()));
					break;
				case STRENGTH:
					m.setStrength(getNullOrInteger(reader));
					break;
				case ACCURACY:
					m.setAccuracy(getNullOrInteger(reader));
					break;
				case AP:
					m.setAp(getNullOrInteger(reader));
					break;
				case CONTEST_CATEGORY:
					m.setContestCategory("null".equals(reader.getValue()) || StringUtils.isEmpty(reader.getValue()) ? null : ContestCategory.valueOf(reader.getValue()));
					break;
				case GENERATION:
					m.setGeneration(Integer.valueOf(reader.getValue()));
					break;
				case DESCRIPTIONS:
					readDescriptions(reader, m);
					break;
				default:
					break;
			}
			reader.moveUp();
		}
		return m;
	}

	private void readDescriptions(HierarchicalStreamReader reader, Move m) {
		// names
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			readDescription(reader, m);
			reader.moveUp();
		}
	}

	private void readDescription(HierarchicalStreamReader reader, Move m) {
		m.getDescriptions().addText(reader.getAttribute(LANG), reader.getValue());
	}

	private Integer getNullOrInteger(HierarchicalStreamReader reader) {
		return (StringUtils.isEmpty(reader.getValue()) || "null".equals(reader.getValue())) ? null : Integer.valueOf(reader.getValue());
	}

}
