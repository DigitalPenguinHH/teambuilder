package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamMember;
import de.digitalpenguin.pokeapp.data.provider.GameProvider;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class TeamConverter extends AbstractConverter {

    private static final String TAG_ID = "id";

    private static final String TAG_NAME = "name";

    private static final String TAG_GAME = "game";

    private static final String TAG_MEMBER = "member";

    private static final String TAG_ICON = "icon";

    private static final String TAG_MODIFIED = "modified";

    private static final String TAG_MAXSIZE = "maxSize";


    private final GameProvider dataProvider;

    public TeamConverter(GameProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        Team team = (Team) source;
        newStringNode(writer, TAG_ID, team.getId());
        newStringNode(writer, TAG_NAME, team.getName());
        if (team.getGame() != null)
            newStringNode(writer, TAG_GAME, team.getGame().getName(Locale.ENGLISH));
        if (team.getIcon() != null)
            newStringNode(writer,TAG_ICON, team.getIcon());
        for (TeamMember tm : team.getMembers()) {
            writer.startNode(TAG_MEMBER);
            context.convertAnother(tm);
            writer.endNode();
        }
        if (team.getTeamSizeMax() > Team.TEAM_MEMBERS_DEFAULT)
            newIntNode(writer, TAG_MAXSIZE, team.getTeamSizeMax());
        newStringNode(writer, TAG_MODIFIED, Long.toString(team.getLastModified()));
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Team team = new Team();
        LinkedList<TeamMember> membersList = new LinkedList<>();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch (nodeName) {
                case TAG_ID:
                    team.setId(reader.getValue());
                    break;
                case TAG_NAME:
                    team.setName(reader.getValue());
                    break;
                case TAG_GAME:
                    team.setGame(findGame(reader.getValue()));
                    break;
                case TAG_MEMBER:
                    TeamMember tm = (TeamMember) context.convertAnother(context, TeamMember.class);
                    tm.setTeam(team);
                    membersList.add(tm);
                    break;
                case TAG_ICON:
                    if (StringUtils.isNotEmpty(reader.getValue()))
                        team.setIcon(reader.getValue());
                    break;
                case TAG_MAXSIZE:
                    team.setTeamSizeMax(Integer.parseInt(reader.getValue()));
                    break;
                case TAG_MODIFIED:
                    if (StringUtils.isNotEmpty(reader.getValue())) {
                        team.setLastModified(Long.valueOf(reader.getValue().trim()));
                    } else {
                        team.setLastModified(new Date().getTime());
                    }
                    break;
            }
            reader.moveUp();
        }
        team.setMembers(membersList);
        // finished
        return team;
    }

    private Game findGame(String defaultName) {
        for (Game g : dataProvider.getGames()) {
            if (g.getName(Locale.ENGLISH).equalsIgnoreCase(defaultName))
                return g;
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Team.class.equals(type);
    }

}
