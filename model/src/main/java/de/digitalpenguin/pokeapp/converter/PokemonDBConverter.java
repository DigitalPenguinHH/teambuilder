package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.LinkedList;

import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.PokemonData;

public class PokemonDBConverter implements Converter
{

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
		return PokemonDB.class.equals(clazz);
	}

	@Override
	public void marshal(Object src, HierarchicalStreamWriter writer, MarshallingContext context) {
		PokemonDB db = (PokemonDB) src;
		writer.startNode("entries");
		for (Pokemon p : db.getEntries()) {
			writer.startNode("pokemon");
			context.convertAnother(p);
			writer.endNode();
		}
		writer.endNode();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		PokemonDB db = new PokemonDB();
		db.setEntries(new LinkedList<PokemonData>());
		reader.moveDown();
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			db.getEntries().add((PokemonData) context.convertAnother(db, PokemonData.class));
			reader.moveUp();
		}
		reader.moveUp();
		return db;
	}

}
