package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.StatType;

public class CharacteristicConverter extends LocalizedNamedConverter {

    private static final String TAG_ID = "id";

    private static final String TAG_HIGHEST = "highest";

    private static final String TAG_VALUES = "values";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        // not implemented
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Characteristic c = new Characteristic();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch(nodeName) {
                case TAG_ID:
                    c.setId(Integer.parseInt(reader.getValue()));
                    break;
                case NAMES:
                    readNames(reader, c);
                    break;
                case TAG_VALUES:
                    String[] values = reader.getValue().split("\\,");
                    for (int i = 0; i < values.length; i++) {
                        c.getValues()[i] = Integer.parseInt(values[i]);
                    }
                    break;
                case TAG_HIGHEST:
                    c.setHighest(StatType.valueOf(reader.getValue()));
                    break;
            }
            reader.moveUp();
        }
        // finished
        return c;
    }

    @SuppressWarnings("rawtypes")
	@Override
    public boolean canConvert(Class type) {
        return Characteristic.class.equals(type);
    }
}
