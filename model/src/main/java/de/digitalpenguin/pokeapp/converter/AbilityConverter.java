package de.digitalpenguin.pokeapp.converter;

import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.Ability;

public class AbilityConverter extends AbstractConverter {

    protected static final String LANG = "lang";

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		Ability a = cast(source, Ability.class);
		newIntNode(writer, "id", (int) a.getId());
        writer.startNode("names");
        for (Entry<String, String> e : a.getDescriptions().entrySet()) {
            newStringNode(writer, "text", e.getValue(), Pair.of(LANG, e.getKey()));
        }
		writer.startNode("descriptions");
		for (Entry<String, String> e : a.getDescriptions().entrySet()) {
			newStringNode(writer, "text", e.getValue(), Pair.of(LANG, e.getKey()));
		}
		writer.endNode();
		newIntNode(writer, "sinceGen", a.getSinceGen());
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Ability a = new Ability();
		while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            switch(nodeName) {
                case "id":
                    a.setId(Integer.parseInt(reader.getValue()));
                    break;
                case "names":
                    readNames(reader, a);
                    break;
                case "descriptions":
                    readDescriptions(reader, a);
                    break;
                case "sinceGen":
                    a.setSinceGen(Integer.valueOf(reader.getValue()));
                    break;
            }
            reader.moveUp();
        }
		return a;
	}

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return Ability.class.equals(type);
	}

    private void readNames(HierarchicalStreamReader reader, Ability a) {
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            a.getNames().addText(reader.getAttribute(LANG), reader.getValue());
            reader.moveUp();
        }
    }

    private void readDescriptions(HierarchicalStreamReader reader, Ability a) {
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            a.getDescriptions().addText(reader.getAttribute(LANG), reader.getValue());
            reader.moveUp();
        }
    }
}
