package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.Map;

import de.digitalpenguin.pokeapp.data.LocalizedNamed;

public abstract class LocalizedNamedConverter extends AbstractConverter {

    protected static final String NAME = "name";

    protected static final String LANG = "lang";

    protected static final String NAMES = "names";

    protected void writeLocalizations(HierarchicalStreamWriter writer, LocalizedNamed o) {
        writeLocalizations(writer, o ,false);
    }

    protected void writeLocalizations(HierarchicalStreamWriter writer, LocalizedNamed o, boolean wrapNames) {
        if (wrapNames)
            writer.startNode(NAMES);
        for (Map.Entry<String, String> trans : o.getNames().entrySet()) {
            writer.startNode(NAME);
            writer.addAttribute(LANG, trans.getKey());
            writer.setValue(trans.getValue());
            writer.endNode();
        }
        if (wrapNames)
            writer.endNode();
    }

    protected void readName(HierarchicalStreamReader reader, LocalizedNamed o) {
        o.getNames().addText(reader.getAttribute(LANG), reader.getValue());
    }

    protected void readNames(HierarchicalStreamReader reader, LocalizedNamed o) {
        // names
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            readName(reader, o);
            reader.moveUp();
        }
    }

}
