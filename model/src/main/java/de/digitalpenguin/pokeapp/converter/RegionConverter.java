package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.Location;
import de.digitalpenguin.pokeapp.data.Pokedex;
import de.digitalpenguin.pokeapp.data.PokedexEntry;
import de.digitalpenguin.pokeapp.data.Region;

public class RegionConverter implements Converter {

    public static final String TAG_GENERATION = "generation";

    public static final String TAG_ENTRIES = "entries";

    public static final String ATTR_LANG = "lang";

    private static final String TAG_POKEDEX = "pokedex";

    private static final String TAG_LOCATIONS = "locations";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        Region region = (Region) source;
        newStringNode(writer, TAG_GENERATION, Integer.toString(region.getGeneration()));
        writer.startNode("names");
        for (Map.Entry<String, String> e : region.getNameEntries()) {
            writer.addAttribute(ATTR_LANG, e.getKey());
            writer.setValue(e.getValue());
        }
        writer.endNode();
        if (region.getPokedex() != null) {
            writer.startNode(TAG_POKEDEX);
            writer.startNode("entries");
            StringBuilder sb = new StringBuilder();
            for (PokedexEntry entry : region.getPokedex().getEntries()) {
                sb.append(String.format("%d,%d;", entry.getLocalDexId(), entry.getNationalDexId()));
            }
            if (sb.length() > 0)
                sb.deleteCharAt(sb.length() - 1);
            writer.setValue(sb.toString());
            writer.endNode();
            writer.endNode();
        }
        if (!CollectionUtils.isEmpty(region.getLocations())) {
            writer.startNode(TAG_LOCATIONS);
            for (Location loc : region.getLocations()) {
                writer.startNode("loc");
                context.convertAnother(loc);
                writer.endNode();
            }
            writer.endNode();
        }
    }

    private void newStringNode(HierarchicalStreamWriter writer, String name, String value) {
        writer.startNode(name);
        writer.setValue(value);
        writer.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Region r = new Region();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String node = reader.getNodeName();
            switch (node) {
                case TAG_GENERATION:
                    r.setGeneration(Integer.parseInt(reader.getValue()));
                    break;
                case "name":
                    if (r.getNames() == null)
                        r.setNames(new LocalizedText());
                    r.getNames().addText(reader.getAttribute(ATTR_LANG).toLowerCase(), reader.getValue());
                    break;
                case TAG_POKEDEX:
                    Pokedex dex = new Pokedex();
                    dex.setRegion(r);
                    reader.moveDown();
                    if (reader.getNodeName().equals(TAG_ENTRIES)) {
                        String str = reader.getValue();
                        String[] parts = str.split(";");
                        List<PokedexEntry> entries = new ArrayList<>(parts.length);
                        for (String part : parts) {
                            String[] values = part.split(",");
                            entries.add(new PokedexEntry(Integer.parseInt(values[0]), Integer.parseInt(values[1])));
                        }
                        dex.setEntries(entries);
                    }
                    reader.moveUp();
                    r.setPokedex(dex);
                    break;
                case TAG_LOCATIONS:
                    List<Location> locations = new LinkedList<>();
                    reader.moveDown();
                    while(reader.getNodeName().equals("loc")) {
                        Location loc = (Location) context.convertAnother(r, Location.class);
                        locations.add(loc);
                        reader.moveUp();
                    }
                    r.setLocations(locations);
                    break;
                default:
                    break;
            }
            reader.moveUp();
        }
        return r;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Region.class.equals(type);
    }
}
