package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.Game;

public class GameConverter extends LocalizedNamedConverter {

    private static final String GENERATION = "generation";

    private static final String PRIORITY = "priority";

    private static final String MEMBER_DEFAULTS = "teamMemberDefaults";

    private static final String ID = "id";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        // no op
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Game g = new Game();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String node = reader.getNodeName();
            switch (node) {
                case ID:
                    g.setId(reader.getValue());
                    break;
                case GENERATION:
                    g.setGeneration(Integer.parseInt(reader.getValue()));
                    break;
                case NAME:
                    readName(reader, g);
                    break;
                case PRIORITY:
                    g.setHasPriority();
                    break;
                case MEMBER_DEFAULTS:
                    g.setTeamMemberDefault(getNullOrInteger(reader));
                    break;
                default:
                    break;
            }
            reader.moveUp();
        }
        return g;
    }

    private Integer getNullOrInteger(HierarchicalStreamReader reader) {
        return "null".equals(reader.getValue()) ? null : Integer.valueOf(reader.getValue());
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return Game.class.equals(type);
    }
}
