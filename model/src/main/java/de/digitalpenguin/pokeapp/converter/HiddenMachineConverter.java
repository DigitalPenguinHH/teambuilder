package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import de.digitalpenguin.pokeapp.data.HiddenMachine;

public class HiddenMachineConverter extends LocalizedNamedConverter {

    private static final String MOVE_ID = "moveId";

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        // no op
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        HiddenMachine hm = new HiddenMachine();
        hm.setMoveId(Integer.parseInt(reader.getAttribute(MOVE_ID)));
        readNames(reader, hm);
        return hm;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return HiddenMachine.class.equals(type);
    }
}
