package de.digitalpenguin.pokeapp.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.apache.commons.beanutils.BeanUtils;

import java.util.Locale;

public class NamedWithGenerationConverter<T> implements Converter
{

    private static final String TAG_NAME = "name";

	private static final String TAG_GENERATION = "generation";

	private static final String ATTR_LANG = "lang";
	
	private Class<T> type; 
	
	public NamedWithGenerationConverter(Class<T> type) {
		this.type = type;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
		return type.equals(clazz);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
	    try {
            @SuppressWarnings("unchecked")
			T o = (T) source;
            newStringNode(writer, TAG_GENERATION, BeanUtils.getProperty(o, TAG_GENERATION));
            writer.startNode(TAG_NAME);
            writer.addAttribute(ATTR_LANG, "de");
            writer.setValue(BeanUtils.getProperty(o, TAG_NAME));
            writer.endNode();
            writer.startNode(TAG_NAME);
            writer.addAttribute(ATTR_LANG, "en");
            writer.setValue(BeanUtils.getProperty(o, "nameEn"));
            writer.endNode();
	    } catch (Exception e) {
        }
	}

    private void newStringNode(HierarchicalStreamWriter writer, String name, String value) {
        writer.startNode(name);
        writer.setValue(value);
        writer.endNode();
    }

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		try {
			T o = type.newInstance();
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				String tag = reader.getNodeName();
				String value = reader.getValue();
				switch (tag) {
					case TAG_GENERATION:
						BeanUtils.setProperty(o, TAG_GENERATION, Integer.parseInt(value));
						break;
					case TAG_NAME:
						String lang = reader.getAttribute(ATTR_LANG);
						if (lang.equals(Locale.getDefault().getLanguage())) {
							BeanUtils.setProperty(o, TAG_NAME, value);
						} else if (reader.getAttribute(ATTR_LANG).equals("en")) {
                            BeanUtils.setProperty(o, "nameEn", reader.getValue());
                        }
						break;
					default:
						break;
				}
				reader.moveUp();
			}
			return o;
		} catch (Exception e) {
			return null;
		}
	}

}
