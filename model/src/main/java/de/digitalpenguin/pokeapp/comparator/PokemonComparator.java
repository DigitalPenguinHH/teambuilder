package de.digitalpenguin.pokeapp.comparator;

import java.util.Comparator;

import de.digitalpenguin.pokeapp.data.Pokedex;
import de.digitalpenguin.pokeapp.data.Pokemon;

public class PokemonComparator implements Comparator<Pokemon> {

    public static final int SORT_ID = 1;

    public static final int SORT_NAME = 2;

    public static final int SORT_LOCALDEX = 3;

    private Pokedex pokedex = null;

    private int sort;

    public PokemonComparator(int sort) {
        this.sort = sort;
    }

    public PokemonComparator(Pokedex dex) {
        this.sort = SORT_LOCALDEX;
        this.pokedex = dex;
    }

    @Override
    public int compare(Pokemon p1, Pokemon p2) {
        if (sort == SORT_NAME)
            return p1.getName().compareTo(p2.getName());
        if (sort == SORT_ID)
            return Integer.valueOf(p1.getId()).compareTo(Integer.valueOf(p2.getId()));
        if (sort == SORT_LOCALDEX) {
            Integer localDexId1 =  pokedex.getLocalDexId(p1.getId());
            Integer localDexId2 =  pokedex.getLocalDexId(p2.getId());
            if (localDexId1 == null)
                return localDexId2 == null ? 0 : -1;
            if (localDexId2 == null)
                return 1;
            return localDexId1.compareTo(localDexId2);
        }
        return 0;
    }
}
