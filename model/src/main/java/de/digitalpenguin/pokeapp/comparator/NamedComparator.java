package de.digitalpenguin.pokeapp.comparator;

import java.util.Comparator;

import de.digitalpenguin.pokeapp.data.util.Named;

public class NamedComparator implements Comparator<Named> {

    @Override
    public int compare(Named o1, Named o2) {
        String name1 = o1.getName();
        String name2 = o2.getName();
        if (name1 == null)
            return name2 == null ? 0 : -1;
        if (name2 == null)
            return name1 == null ? 0 : 1;
        return name1.compareTo(name2);
    }

}
