package de.digitalpenguin.pokeapp.comparator;

import java.util.Comparator;

import de.digitalpenguin.pokeapp.data.util.Named;

public class NamedLexicalComparator implements Comparator<de.digitalpenguin.pokeapp.data.util.Named> {

    private final String term;

    public NamedLexicalComparator(String term) {
        this.term = term.toLowerCase();
    }

    @Override
    public int compare(Named o1, Named o2) {
        String localName1 = o1.getName().toLowerCase();
        String localName2 = o2.getName().toLowerCase();
        return compareContains(localName1, localName2);
    }

    private int compareContains(String localName1, String localName2) {
        Integer pos1 = localName1.indexOf(term);
        Integer pos2 = localName2.indexOf(term);
        if (pos1 < 0 && pos2 >= 0)
            return -1;
        if (pos1 >= 0 && pos2 < 0)
            return 1;
        return pos1.compareTo(pos2);
    }

}
