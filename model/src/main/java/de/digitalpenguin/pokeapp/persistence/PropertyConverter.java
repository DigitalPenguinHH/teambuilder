package de.digitalpenguin.pokeapp.persistence;

public interface PropertyConverter<P, D> {

    P convertToEntityProperty(D databaseValue);

    D convertToDatabaseValue(P entityProperty);

    D convert(Object o);
}
