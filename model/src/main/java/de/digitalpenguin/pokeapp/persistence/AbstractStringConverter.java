package de.digitalpenguin.pokeapp.persistence;

public abstract class AbstractStringConverter<P> implements PropertyConverter<P, String> {

    @Override
    public String convert(Object o) {
        return convertToDatabaseValue((P) o);
    }

}
