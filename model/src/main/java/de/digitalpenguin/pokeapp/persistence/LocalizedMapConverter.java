package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class LocalizedMapConverter extends AbstractStringConverter<Map<String, String>> {

    private static final String keyValueSeparator = "\\=";
    private static final String i18nSeperator = "##";

    @Override
    public Map<String, String> convertToEntityProperty(String databaseValue) {
        if (databaseValue == null)
            return new HashMap<>();
        StringTokenizer st = new StringTokenizer(databaseValue,i18nSeperator);
        Map<String, String> result = new HashMap<>(st.countTokens());
        while (st.hasMoreElements()) {
            String loc = (String) st.nextElement();
            String[] parts = loc.split(keyValueSeparator);
            result.put(parts[0], parts[1]);
        }
        return result;
    }

    @Override
    public String convertToDatabaseValue(Map<String, String> entityProperty) {
        if (entityProperty == null || entityProperty.isEmpty())
            return null;
        Set<String> i18nParts = new HashSet<>();
        for (Map.Entry<String, String> e : entityProperty.entrySet()) {
            i18nParts.add(e.getKey().concat(keyValueSeparator).concat(e.getKey()));
        }
        return StringUtils.join(i18nParts, i18nSeperator);
    }
}