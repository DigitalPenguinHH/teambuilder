package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.StringUtils;

import java.util.StringTokenizer;

public class IntArrayConverter extends AbstractStringConverter<int[]> {

    @Override
    public int[] convertToEntityProperty(String databaseValue) {
        if (databaseValue == null || databaseValue.trim().length() == 0)
            return null;
        StringTokenizer st = new StringTokenizer(databaseValue, ",");
        int[] resultArray = new int[st.countTokens()];
        int i = 0;
        while (st.hasMoreElements()) {
            String s = (String) st.nextElement();
            if (StringUtils.isNotBlank(s))
                resultArray[i] = Integer.valueOf(s);
            i++;
        }
        return resultArray;
    }

    @Override
    public String convertToDatabaseValue(int[] entityProperty) {
        return StringUtils.join(entityProperty,',');
    }
}
