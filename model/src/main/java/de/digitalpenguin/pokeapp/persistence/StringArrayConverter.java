package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class StringArrayConverter extends AbstractStringConverter<String[]> {

    @Override
    public String[] convertToEntityProperty(String databaseValue) {
        if (databaseValue == null || databaseValue.trim().length() == 0)
            return null;
        StringTokenizer st = new StringTokenizer(databaseValue,",");
        Set<String> resultSet = new HashSet<>();
        while (st.hasMoreElements()) {
            resultSet.add((String) st.nextElement());
        }
        return resultSet.toArray(new String[resultSet.size()]);
    }

    @Override
    public String convertToDatabaseValue(String[] entityProperty) {
        return StringUtils.join(entityProperty,',');
    }
}