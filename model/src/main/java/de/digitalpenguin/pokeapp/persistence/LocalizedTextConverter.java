package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import de.digitalpenguin.pokeapp.data.LocalizedText;

public class LocalizedTextConverter extends AbstractStringConverter<LocalizedText> {

    private static final String keyValueSeparator = "=";
    private static final String i18nSeperator = "##";

    @Override
    public LocalizedText convertToEntityProperty(String databaseValue) {
        if (databaseValue == null)
            return null;
        StringTokenizer st = new StringTokenizer(databaseValue,i18nSeperator);
        LocalizedText result = new LocalizedText();
        while (st.hasMoreElements()) {
            String loc = (String) st.nextElement();
            String[] parts = loc.split(keyValueSeparator);
            if (parts.length == 2) {
                result.addText(parts[0], parts[1]);
            }
        }
        return result;
    }

    @Override
    public String convertToDatabaseValue(LocalizedText entityProperty) {
        if (entityProperty == null || entityProperty.isEmpty())
            return "";
        Set<String> i18nParts = new HashSet<>();
        for (Map.Entry<String, String> e : entityProperty.entrySet()) {
            i18nParts.add(e.getKey().concat(keyValueSeparator).concat(e.getValue()));
        }
        return StringUtils.join(i18nParts, i18nSeperator);
    }
}