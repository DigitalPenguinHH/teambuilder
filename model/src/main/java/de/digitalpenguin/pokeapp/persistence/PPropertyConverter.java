package de.digitalpenguin.pokeapp.persistence;

import de.digitalpenguin.pokeapp.data.PProperty;

public class PPropertyConverter extends EnumConverter<PProperty> {

    @Override
    protected PProperty from(String value) {
        return PProperty.valueOf(value);
    }
}
