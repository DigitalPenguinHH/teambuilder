package de.digitalpenguin.pokeapp.persistence;

import org.apache.commons.lang3.StringUtils;

import de.digitalpenguin.pokeapp.data.PProperty;

public abstract class EnumConverter<T extends Enum> extends AbstractStringConverter<T> {

    @Override
    public T convertToEntityProperty(String databaseValue) {
        if (StringUtils.isEmpty(databaseValue))
            return null;
        return from(databaseValue);
    }

    protected abstract T from(String value);

    @Override
    public String convertToDatabaseValue(T entityProperty) {
        if (entityProperty == null)
            return null;
        return entityProperty.name();
    }
}
