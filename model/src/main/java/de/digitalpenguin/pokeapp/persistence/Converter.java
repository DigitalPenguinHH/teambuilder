package de.digitalpenguin.pokeapp.persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Converter {

    public Class<? extends PropertyConverter> converterClass();

    public Class<?> columnType();

}
