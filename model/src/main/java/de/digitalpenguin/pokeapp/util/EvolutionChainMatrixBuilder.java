package de.digitalpenguin.pokeapp.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;

public class EvolutionChainMatrixBuilder
{

    private final EvolutionChain chain;

    public EvolutionChainMatrixBuilder(EvolutionChain chain) {
        this.chain = chain;
    }

    public Map<Integer, List<EvolutionChainLink>> getRows() {

        Map<Integer, List<EvolutionChainLink>> rows = new HashMap<>();

        for (EvolutionChainLink link : this.chain.getChainLinks()) {
            Integer idx = 0;
            List<EvolutionChainLink> links = getOrCreate(rows, idx);
            while (containsFrom(links, link.getPokemonIdFrom(), link.getVariantFrom())) {
                idx++;
                links = getOrCreate(rows, idx);
                copyUntil(links, rows.get(idx-1), link);
            }
            while (!canAppend(links, link)) {
                idx++;
                links = getOrCreate(rows, idx);
            }
            links.add(link);
        }


        for (int i = 0; i < rows.size() - 1 ; i++) {
            int next = i + 1;
            if (CollectionUtils.isEqualCollection(rows.get(i), rows.get(next))) {
                rows.remove(next);
            }
        }

        return rows;
    }

    private void copyUntil(List<EvolutionChainLink> toLinks, List<EvolutionChainLink> evolutionChainLinks, EvolutionChainLink link) {
        for (EvolutionChainLink l : evolutionChainLinks) {
            if (l.getPokemonIdTo() == link.getPokemonIdFrom() && (!toLinks.contains(l)))
                toLinks.add(l);
        }
    }

    private boolean canAppend(List<EvolutionChainLink> links, EvolutionChainLink link) {
        if (links.isEmpty())
            return true;
        for (EvolutionChainLink l : links) {
            if (l.getPokemonIdTo() == link.getPokemonIdFrom() && StringUtils.equalsIgnoreCase(link.getVariantFrom(), l.getVariantFrom()))
                return true;
        }
        return false;
    }

    private List<EvolutionChainLink> getOrCreate(Map<Integer, List<EvolutionChainLink>> rows, Integer idx) {
        List<EvolutionChainLink> links = rows.get(idx);
        if (links == null) {
            links = new ArrayList<>(3);
            rows.put(idx, links);
        }
        return links;
    }

    private boolean containsFrom(List<EvolutionChainLink> links, int pokemonIdFrom, String variantFrom) {
        if (links == null)
            return false;
        for (EvolutionChainLink l : links) {
            if (l.getPokemonIdFrom() == pokemonIdFrom && (variantFrom == null || StringUtils.equalsIgnoreCase(l.getVariantFrom(), variantFrom)))
                return true;
        }
        return false;
    }
}