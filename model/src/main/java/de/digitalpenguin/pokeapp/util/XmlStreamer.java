package de.digitalpenguin.pokeapp.util;

import java.io.InputStream;
import java.util.Scanner;

import org.apache.commons.collections.Closure;

import com.thoughtworks.xstream.XStream;

public class XmlStreamer<T>
{	
	private XStream xstream; 	
	
	private boolean tagOpened = false;
	
	private StringBuffer content = new StringBuffer();

	public void stream(InputStream inputStream, String alias, XStream xstream, Closure consumer) {
		Scanner sc = null;
		this.xstream = xstream;
		try {
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				if (isHeader(line))
					continue;
				int startIdx = line.indexOf(startTag(alias));
				if (startIdx >= 0) {
					int endIdx = line.indexOf(endTag(alias));
					if (endIdx >= 0) {
						// <a>...</a>
						content.append(line.substring(startIdx, endIdx + endTag(alias).length()));
					} else {
						// <a>...
						content.append(line.substring(startIdx));
					}
					tagOpened = true;
				} else {
					int endIdx = line.indexOf(endTag(alias));
					if (endIdx >= 0) {
						// ...</a>
						content.append(line.substring(0, endIdx + endTag(alias).length()));
						finalizeObject(consumer);
						tagOpened = false;
					} else {
						// (recording) ...
						if (tagOpened) {
							content.append(line);
						} else {
							// no op
						}
					}
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (sc != null)
				sc.close();
		}
	}

	private boolean isHeader(String line) {
		return line.indexOf("<?xml version=") >= 0;
	}

	private void finalizeObject(Closure consumer) {
		Object o = xstream.fromXML(content.toString());
		consumer.execute(o);
		this.content.setLength(0);
	}

	private String endTag(String alias) {
		StringBuffer sb = new StringBuffer();
		sb.append('<');
		sb.append('/');
		sb.append(alias);
		sb.append('>');
		return sb.toString();
	}

	private String startTag(String alias) {
		StringBuffer sb = new StringBuffer();
		sb.append('<');
		sb.append(alias);
		sb.append('>');
		return sb.toString();
	}
}
