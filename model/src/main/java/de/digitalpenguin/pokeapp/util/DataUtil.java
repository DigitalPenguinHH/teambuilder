package de.digitalpenguin.pokeapp.util;

import android.util.SparseArray;

import org.apache.commons.collections.Transformer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DataUtil {

    public static <T> Collection<T> getValues(SparseArray<T> sa) {
        Collection<T> result = new ArrayList<>(sa.size());
        for (int i = 0; i < sa.size(); i++) {
            result.add(sa.valueAt(i));
        }
        return result;
    }

    public static byte[] convertStreamToByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buff = new byte[10240];
        int i = Integer.MAX_VALUE;
        while ((i = is.read(buff, 0, buff.length)) > 0) {
            baos.write(buff, 0, i);
        }
        return baos.toByteArray();
    }

    public static <K, T> Map<K, List<T>> toMap(Collection<T> col, Transformer keyExtractor) {
        Map<K, List<T>> result = new HashMap<>(10);
        for (T o : col) {
            Object key = keyExtractor.transform(o);
            List<T> valuesForKey = result.get(key);
            if (valuesForKey == null) {
                valuesForKey = new LinkedList<T>();
                result.put((K) key, valuesForKey);
            }
            valuesForKey.add(o);
        }
        return result;
    }

}
