package de.digitalpenguin.pokeapp.util;

import java.util.List;

import org.apache.commons.collections.Closure;

public class AppendListConsumer<T> implements Closure {
	
	protected List<T> theList;

	public AppendListConsumer(List<T> theList) {
		this.theList = theList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Object input) {
		this.theList.add((T) input);
	}
	
}
