package de.digitalpenguin.pokeapp.util;

import com.thoughtworks.xstream.XStream;

import java.io.InputStream;

import de.digitalpenguin.pokeapp.converter.AbilityConverter;
import de.digitalpenguin.pokeapp.converter.CharacteristicConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainLinkConverter;
import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.GenerationConverter;
import de.digitalpenguin.pokeapp.converter.HiddenMachineConverter;
import de.digitalpenguin.pokeapp.converter.ItemConverter;
import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.converter.NatureConverter;
import de.digitalpenguin.pokeapp.converter.PokemonConverter;
import de.digitalpenguin.pokeapp.converter.PokemonDBConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.converter.VariantConverter;
import de.digitalpenguin.pokeapp.data.AbilitiesDB;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.CharacteristicsDB;
import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainDB;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemsDB;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.NaturesDB;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class XStreamHelper {
    
    private static XStreamHelper defaultInstance;

    private XStreamHelper() {

    }

    public static XStream initXStreamPokemon(DataProvider dataProvider)
    {
        try {
        	XStream xstream = new XStream();
            xstream.setClassLoader(PokemonDB.class.getClassLoader());
            xstream.alias("db", PokemonDB.class);
            xstream.alias("pokemon", Pokemon.class);
            xstream.alias("move", MoveLearned.class);
            xstream.alias("variant", Variant.class);
            xstream.registerConverter(new PokemonDBConverter());
            xstream.registerConverter(new PokemonConverter());
            xstream.registerConverter(new MoveConverter());
            xstream.registerConverter(new VariantConverter(dataProvider));
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static XStream initXStreamGamesAndRegions()
    {
        try {
            XStream xstream = new XStream();
            xstream.setClassLoader(GamesDB.class.getClassLoader());
            xstream.alias("db", GamesDB.class);
            xstream.alias("region", Region.class);
            xstream.alias("game", Game.class);
            xstream.alias("generation", Generation.class);
            xstream.alias("hm", HiddenMachine.class);
            xstream.registerConverter(new GameConverter());
            xstream.registerConverter(new RegionConverter());
            xstream.registerConverter(new GameConverter());
            xstream.registerConverter(new GenerationConverter());
            xstream.registerConverter(new HiddenMachineConverter());
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static XStream initXStreamEvochains(DataProvider dataProvider)
    {
        try {
        	XStream xstream = new XStream();
            xstream.setClassLoader(EvolutionChainDB.class.getClassLoader());
            xstream.alias("db", EvolutionChainDB.class);
            xstream.alias("chain", EvolutionChain.class);
            xstream.registerConverter(new EvolotionChainConverter());
            xstream.registerConverter(new EvolotionChainLinkConverter(dataProvider));
            xstream.addImplicitCollection(EvolutionChainDB.class, "chains", EvolutionChain.class);
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
    }

    public static XStream initXStreamMoves()
    {
        try {
        	XStream xstream = new XStream();
            xstream.setClassLoader(MovesDB.class.getClassLoader());
            xstream.alias("db", MovesDB.class);
            xstream.alias("move", Move.class);
            xstream.alias("type", MoveType.class);
            xstream.alias("property", PProperty.class);
            xstream.alias("contestCategory", ContestCategory.class);
            xstream.registerConverter(new MoveConverter());
            xstream.addImplicitCollection(MovesDB.class, "moves");
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
    }

    public static XStream initXStreamItems()
    {
        try {
        	XStream xstream = new XStream();
            xstream.setClassLoader(ItemsDB.class.getClassLoader());
            xstream.alias("db", ItemsDB.class);
            xstream.alias("item", Item.class);
            xstream.addImplicitCollection(ItemsDB.class, "entries", Item.class);
            xstream.registerConverter(new ItemConverter());
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static XStream initXStreamAbilities()
    {
        try {
            XStream xstream = new XStream();
            xstream.setClassLoader(AbilitiesDB.class.getClassLoader());
            xstream.alias("db", AbilitiesDB.class);
            xstream.alias("ability", Ability.class);
            xstream.addImplicitCollection(AbilitiesDB.class, "entries", Ability.class);
            xstream.registerConverter(new AbilityConverter());
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
    }

    public static XStream initXStreamNatures() {
        try {
            XStream xstream = new XStream();
            xstream.setClassLoader(NaturesDB.class.getClassLoader());
            xstream.alias("db", NaturesDB.class);
            xstream.alias("nature", Nature.class);
            xstream.addImplicitCollection(NaturesDB.class, "entries", Nature.class);
            xstream.registerConverter(new NatureConverter());
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static XStream initXStreamCharacteristics() {
        try {
            XStream xstream = new XStream();
            xstream.setClassLoader(CharacteristicsDB.class.getClassLoader());
            xstream.alias("db", CharacteristicsDB.class);
            xstream.alias("characteristic", Characteristic.class);
            xstream.addImplicitCollection(CharacteristicsDB.class, "entries", Characteristic.class);
            xstream.registerConverter(new CharacteristicConverter());
            return xstream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T load(InputStream is, Class<T> resultType) {
    	XStream xstream = null;
        if (resultType.getSimpleName().startsWith("Abi")) {
        	xstream = initXStreamAbilities();
        }
        if (xstream == null) {
        	return null;
        }
        @SuppressWarnings("unchecked")
        T some = (T) xstream.fromXML(is);
        return some;
    }
}
