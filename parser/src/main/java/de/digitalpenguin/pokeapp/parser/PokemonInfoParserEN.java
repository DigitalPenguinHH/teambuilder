package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PokemonInfoParserEN
{
	private static final String URL = "https://www.serebii.net/pokedex-swsh/%s/";
	
	private String dexEntryEn;
	private String generRatioStr;
	
	private NumberFormat nf = new DecimalFormat("0.0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

	public void parse(String nameEn) {
		try {
            // find move names
            Document docNames = Jsoup.connect(String.format(URL, nameEn.toLowerCase().replaceAll("’", "'").replaceAll(" ", ""))).get();
            Elements tableRows = docNames.getElementById("content").getElementsByClass("dextable").get(1).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            int rowIdx = 0;
            for (Element element : tableRows) {
                if (rowIdx == 1) {
                	Elements tds = element.child(3).getElementsByTag("td");
                	if (tds.get(0).text().indexOf("Genderless") >= 0) {
                		generRatioStr = "M-1.0#F-1.0";
                	} else {
						String text = tds.get(2).text();
						String ratioMale = text.substring(0, text.indexOf('%'));
						text = tds.get(4).text();
						String ratioFemale = text.substring(0, text.indexOf('%'));
						// M87.5#F12.5
						generRatioStr = "M" + nf.format(Float.parseFloat(ratioMale)) + "#" + "F" + nf.format(Float.parseFloat(ratioFemale));
                	}
                }
                rowIdx ++;
            }
            tableRows = docNames.getElementById("content").getElementsByClass("dextable").get(7).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            rowIdx = 0;
            for (Element element : tableRows) {
                if (rowIdx == 1) {
                	this.dexEntryEn = element.child(1).text().trim();
                }
                rowIdx ++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) {
		PokemonInfoParserEN parser = new PokemonInfoParserEN();
		parser.parse("Dracozolt");
		System.out.println(parser.getDexEntryEn());
		System.out.println(parser.getGenerRatioStr());
	}

	public String getGenerRatioStr() {
		return generRatioStr;
	}

	public String getDexEntryEn() {
		return dexEntryEn;
	}
}
