package de.digitalpenguin.pokeapp.parser.serializer;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.parser.MovesParser;

public class MoveXmlEntrySerializer
{
	
	private static final String TEMPLATE = 
			"  <move>\r\n" + 
			"    <id>{0}</id>\r\n" + 
			"    <names>\r\n" + 
			"      <name lang=\"de\">{1}</name>\r\n" + 
			"      <name lang=\"en\">{2}</name>\r\n" + 
			"      <name lang=\"fr\">{3}</name>\r\n" + 
			"      <name lang=\"es\">{4}</name>\r\n" + 
			"    </names>\r\n" + 
			"    <property>{5}</property>\r\n" + 
			"    <type>{6}</type>\r\n" + 
			"    <damageFix>{7}</damageFix>\r\n" + 
			"    <damageVariable>{8}</damageVariable>\r\n" + 
			"    <strength>{9}</strength>\r\n" + 
			"    <accuracy>{10}</accuracy>\r\n" + 
			"    <ap>{11}</ap>\r\n" + 
			"    <contestCategory>{12}</contestCategory>\r\n" + 
			"    <generation>{13}</generation>\r\n" + 
			"    <descriptions>\r\n" + 
			"      <name lang=\"en\">{14}</name>\r\n" + 
			"      <name lang=\"de\">{15}</name>\r\n" + 
			"      <name lang=\"fr\">{16}</name>\r\n" + 
			"    </descriptions>\r\n" + 
			"  </move>";
	
	public static void main(String[] args) {
		MovesParser parser = new MovesParser(o -> print((Move) o));
		parser.parse(Range.between(743, 796));
	}

	private static void print(Move m) {
		String result = MessageFormat.format(TEMPLATE,
				StringUtils.leftPad(Integer.toString((int) m.getId()), 3, '0'), // ID
				StringUtils.trimToEmpty(m.getName("de")),
				StringUtils.trimToEmpty(m.getName("en")),
				StringUtils.trimToEmpty(m.getName("fr")),
				StringUtils.trimToEmpty(m.getName("es")),
				m.getProperty().name(),
				m.getType().name(),
				"false",
				"false",
				toString(m.getStrength()),
				toString(m.getAccuracy()),
				toString(m.getAp()), "", "8",
				m.getDescriptions().getText("en"),
				m.getDescriptions().getText("de"),
				m.getDescriptions().getText("fr", false)
		);
		System.out.println(result);
	}
	
	private static String toString(Integer i) {
		return i == null ? "" : Integer.toString(i);
	}

}
