package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.Range;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PokemonStatsParser
{
	private static final String URL = "https://www.serebii.net/swordshield/pokemon.shtml";
	
	private Map<Integer, String> stats = new TreeMap<>();

	public void parse(Range<Integer> idRange) {
		try {
            // find move names
            Document docNames = Jsoup.connect(URL).get();
            Elements nameTableRows = docNames.getElementById("content").getElementsByClass("tab").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            int rowIdx = 0;
            for (Element element : nameTableRows) {
                if (rowIdx >= 2 && element.child(0).html().indexOf('#') >= 0) {
	                String idStr = element.child(0).text().trim().substring(1);
	                Integer idAsInt = Integer.valueOf(idStr);
					if (idAsInt < idRange.getMinimum() || (idAsInt > idRange.getMaximum())) {
						rowIdx++;
	                	continue;
					}
	                StringBuffer sb = new StringBuffer();
	                sb.append(element.child(5).text() + ",");
	                sb.append(element.child(6).text() + ",");
	                sb.append(element.child(7).text() + ",");
	                sb.append(element.child(8).text() + ",");
	                sb.append(element.child(9).text() + ",");
	                sb.append(element.child(10).text());
	                stats.put(idAsInt, sb.toString());
                }
                rowIdx++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public Map<Integer, String> getStats() {
		return stats;
	}

	public static void main(String[] args) {
		PokemonStatsParser parser = new PokemonStatsParser();
		Range<Integer> range = Range.between(810, 890);
		parser.parse(range);
		for (int i = range.getMinimum(); i <= range.getMaximum(); i++) {
			System.out.println(i + ": " + parser.getStats().get(i));
		}
	}
}
