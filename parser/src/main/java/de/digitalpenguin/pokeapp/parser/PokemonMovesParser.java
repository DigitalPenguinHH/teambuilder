package de.digitalpenguin.pokeapp.parser;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.parser.xmlreader.MovesXMLReader;

public class PokemonMovesParser extends AbstractParser
{
	private static final String URL = "https://www.pokewiki.de/%s";
	
	private List<String> movesEncoded = null;

	private Map<String, Integer> movesCache;
	
	public PokemonMovesParser(Map<String, Integer> moves) {
		this.movesCache = moves;
	}

	// Generation,Level,MoveID,Trigger(L|M),Machine-Name,[OPTIONAL]Game-IDs(split by #)
	public void parse(String nameEn) {
		this.movesEncoded = new LinkedList<String>();
		try {
            // find move names
            Document doc = Jsoup.connect(String.format(URL, nameEn.replaceAll("’", "'").replaceAll(" ", "_"))).get();
            iterateRows(doc.getElementById("Durch_Levelaufstieg").parent().parent(), new Consumer<Element>() {
				@Override
				public void accept(Element row) {
					StringBuffer sb = new StringBuffer();
            		sb.append("8,");
            		if (row.child(0).getElementsByTag("a").size() > 0)
            			return;
            		sb.append(row.child(0).text().trim() + ",");
            		String moveName = row.child(1).text().trim();
            		Integer moveId = movesCache.entrySet().stream().filter(e -> e.getKey().contains(moveName)).findFirst().get().getValue();
					sb.append(Integer.toString(moveId) + ",");
					sb.append("L,");
					sb.append(""); // Machine-Name
            		movesEncoded.add(sb.toString());
				}
			}, (e, i) -> e.hasClass("atkrow"));
            Element heading = doc.getElementById("Durch_TM/TP");
            if (heading != null) {
				iterateRows(heading.parent().parent(), new Consumer<Element>() {
					@Override
					public void accept(Element row) {
						StringBuffer sb = new StringBuffer();
	            		sb.append("8,");
	            		sb.append(",");
	            		String moveName = row.child(1).text().trim();
	            		Integer moveId = movesCache.entrySet().stream().filter(e -> e.getKey().contains(moveName)).findFirst().get().getValue();
						sb.append(Integer.toString(moveId) + ",");
						sb.append("M,");
						sb.append(row.child(0).text().trim()); // Machine-Name
	            		movesEncoded.add(sb.toString());
					}
				}, (e, i) -> e.hasClass("atkrow"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) {
		MovesDB db = new MovesXMLReader(new File("D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\moves.xml")).read();
		Map<String, Integer> moveCache = new TreeMap<String, Integer>();
		db.getMoves().forEach(m -> moveCache.put(m.getName("de"), (int) m.getId()));
		
		PokemonMovesParser parser = new PokemonMovesParser(moveCache);
		parser.parse("Zacian");
		System.out.println(StringUtils.join(parser.getMovesEncoded(), ';'));
	}

	public List<String> getMovesEncoded() {
		return movesEncoded;
	}
}
