package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.Range;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.digitalpenguin.pokeapp.data.PProperty;

public class PokemonNamesParser
{
	private static final String URL = "https://www.pokewiki.de/Pok%C3%A9mon-Liste";
	
	private Map<Integer, Map<String, String>> names = new TreeMap<>();
	private Map<Integer, List<PProperty>> properties = new TreeMap<>();

	public void parse(Range<Integer> idRange) {
		try {
            // find move names
            Document docNames = Jsoup.connect(URL).get();
            Elements nameTableRows = docNames.getElementsByClass("pwtable1").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            for (Element element : nameTableRows) {
                if (element.child(0).tagName().equals("th"))
                    continue;
                String idStr = element.child(0).text();
                Integer idAsInt = Integer.valueOf(idStr);
				if (idAsInt < idRange.getMinimum() || (idAsInt > idRange.getMaximum()))
                	continue;
                Map<String, String> localizations = new HashMap<>(6);
                localizations.put("de", element.child(2).text());
                localizations.put("en", element.child(3).text());
                localizations.put("fr", element.child(4).text());
                localizations.put("jp", getForeignName(element.child(5).text()));
                localizations.put("kr", getForeignName(element.child(6).text()));
                localizations.put("cn", getForeignName(element.child(7).text()));
                names.put(idAsInt, localizations);
                List<PProperty> props = new ArrayList<>(2);
                for (Element propLink : element.child(8).getElementsByTag("a")) {
                	String propName = propLink.attr("title").trim();
                	props.add(PProperty.translate(propName));
				}
                properties.put(idAsInt, props);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	private String getForeignName(String content) {
		StringBuffer sb = new StringBuffer();
		char[] chars = content.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (Integer.valueOf(chars[i]) > 1000) {
				sb.append(chars[i]);
			}
		}
		return sb.toString();
	}
	
	public Map<Integer, Map<String, String>> getNames() {
		return names;
	}

	public Map<Integer, List<PProperty>> getProperties() {
		return properties;
	}

	public static void main(String[] args) {
		new PokemonNamesParser().parse(Range.between(1, 9));
	}
}
