package de.digitalpenguin.pokeapp.parser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AbstractParser
{
	
	public static final String XML_DIR = "D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\"; 

	protected Consumer<Object> onRowCompelted;
	
	public AbstractParser() {
		super();
	}
	
	public AbstractParser(Consumer<Object> onRowCompelted) {
		super();
		this.onRowCompelted = onRowCompelted;
	}

	protected void writePersistentObject(Object o, String key) {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(getPersistendObjectFile(key));
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(o);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected Object readPersistentObject(String key, Supplier<Object> s) {
		Object o = null;
		ObjectInputStream objectInputStream = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(getPersistendObjectFile(key));
			objectInputStream = new ObjectInputStream(fileInputStream);
			o = objectInputStream.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		} finally {
			if (objectInputStream != null)
				try {
					objectInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		if (o == null) {
			o = s.get();
			writePersistentObject(o, key);
		}
		return o;
	}

	private String getPersistendObjectFile(String key) {
		return System.getProperty("java.io.tmpdir") + key + ".dat";
	}
	
	protected String toStr(Element el) {
		if (el == null)
			return null;
		return el.text().trim();
	}
	
	protected Integer toInt(Element el) {
		if (el == null || !StringUtils.isNumeric(el.text()))
			return null;
		return Integer.valueOf(toStr(el));
	}
	
	protected Integer toInt(String str) {
		if (!StringUtils.isNumeric(str))
			return null;
		return Integer.valueOf(str);
	}
	
	protected void iterateRows(Element table, Consumer<Element> rowConsumer) {
		iterateRows(table, rowConsumer, (e, i) -> i > 0 && !e.tagName().equals("th"));
	}
	
	protected void iterateRows(Element table, Consumer<Element> rowConsumer, BiFunction<Element, Integer, Boolean> isHeaderFunction) {
		Elements rows = table.getElementsByTag("tbody").get(0).getElementsByTag("tr");
        int rowIdx = 0;
        for (Element row : rows) {
        	if (isHeaderFunction.apply(row, rowIdx)) {
        		rowConsumer.accept(row);
        	}
        	rowIdx++;
        }
	}

	protected void onRowCompleted(Object o) {
		if (onRowCompelted != null)
			onRowCompelted.accept(o);
	}
}
