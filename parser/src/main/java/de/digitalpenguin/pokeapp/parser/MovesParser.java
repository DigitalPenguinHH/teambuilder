package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import de.digitalpenguin.pokeapp.data.Descriptions;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.PProperty;

public class MovesParser extends AbstractParser
{
	private static final String URL_EN = "https://bulbapedia.bulbagarden.net/wiki/List_of_moves";
	
	private List<Move> moves = new LinkedList<Move>();
	
	public MovesParser(Consumer<Object> callback)
	{
		super(callback);
	}

	public void parse(Range<Integer> range) {
		try {
            Document doc = Jsoup.connect(URL_EN).get();
            iterateRows(doc.getElementById("List_of_moves").parent().nextElementSibling().nextElementSibling().children().get(0).getElementsByTag("table").get(0), new Consumer<Element>() {				
				@Override
				public void accept(Element row) {
					
					Integer id = toInt(row.child(0));
					if (id < range.getMinimum() || id > range.getMaximum())
						return;
					
					Move move = new Move();
					move.setId(id);
					
            		final String nameEn = toStr(row.child(1).getElementsByTag("a").get(0));
            		Map<String, String> moveNames = parseOtherNames(nameEn);
            		moveNames.put("en", nameEn);
					move.setNames(moveNames);
					
					move.setProperty(PProperty.valueOf(toStr(row.child(2).getElementsByTag("span").get(0)).toUpperCase()));
					move.setType(MoveType.from(toStr(row.child(3).getElementsByTag("span").get(0)).toUpperCase()));
					move.setAp(toInt(row.child(5)));
					move.setStrength(toInt(row.child(6)));
					String pcStr = row.child(7).text().trim();
					move.setAccuracy(toInt(pcStr.substring(0, pcStr.indexOf('%') > 0 ? pcStr.indexOf('%') : 0)));
					
					Descriptions descs = new Descriptions();
					Map<String, String> moveDesc = parseDescriptions(moveNames);
					if (moveDesc != null)
						moveDesc.entrySet().forEach(e -> descs.addText(e.getKey(), e.getValue()));
					move.setDescriptions(descs);
					
					moves.add(move);					
					onRowCompleted(move);
				}
			});
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private Map<String, String> parseOtherNames(String nameEn) {
		Map<String, String> names = new HashMap<>();
		try {
            Document doc = Jsoup.connect(String.format("https://bulbapedia.bulbagarden.net/wiki/%s_(move)", nameEn.replaceAll(" ", "_"))).get();
            Element startTag = doc.getElementById("In other languages");
            if (startTag == null)
            	startTag = doc.getElementById("In_other_languages");
			Elements tableRows = startTag.parent().nextElementSibling().children().get(0).getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            int rowIdx = 0;
            for (Element row : tableRows) {
            	if (rowIdx > 2 && row.children().size() == 2) {
                	String lang = decodeLang(toStr(row.child(0).getElementsByTag("a").get(1)));
					String value = toStr(row.child(1));
					names.put(lang, value);
            	}
            	rowIdx++;
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
		return names;
	}

	private String decodeLang(String trim) {
		final String id = trim.toUpperCase().substring(0, 3);
		switch (id) {
			case "GER":
				return "de";
			case "ENG":
				return "en";
			case "JAP":
				return "jp";
			case "FRE":
				return "fr";
			case "ITA":
				return "it";
			case "KOR":
				return "kr";
			case "CHI":
				return "cn";
			case "SPA":
				return "es";
			default:
				break;
		}
		return null;
	}
	
	private Map<String, String> parseDescriptions(Map<String, String> moveNames) {
		Map<String, String> descs = new HashMap<>(6);
		try {
			String lang = "en";
            Document doc = Jsoup.connect(String.format("https://bulbapedia.bulbagarden.net/wiki/%s_(move)", moveNames.get("en").replaceAll(" ", "_"))).get();
            Element lastRow = doc.getElementById("Description").parent().nextElementSibling().getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr").last();
			String value = toStr(lastRow.child(1));
			descs.put(lang, value);
			
			lang = "de";
            doc = Jsoup.connect(String.format("https://www.pokewiki.de/%s", moveNames.get("de").replaceAll(" ", "_"))).get();
            Element startTag = doc.getElementById("Beschreibungen");
            if (startTag != null) {
				lastRow = startTag.parent().nextElementSibling().getElementsByTag("tbody").get(0).getElementsByTag("tr").last();
				value = toStr(lastRow.child(1));
				descs.put(lang, value);
            }			
        } catch (IOException x) {
            x.printStackTrace();
        }
		return descs;
	}

	public static void main(String[] args) {
		MovesParser parser = new MovesParser(o -> System.out.println(o));
		parser.parse(Range.between(743, 796));
	}

	public List<Move> getMoves() {
		return moves;
	}

}
