package de.digitalpenguin.pokeapp.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.thoughtworks.xstream.XStream;

import de.digitalpenguin.pokeapp.data.AbilitiesDB;
import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.util.XStreamHelper;

public class AbilitiesParser extends AbstractParser
{
	private static final List<String> SUPPORTED_LANGS = Arrays.asList("de", "en", "fr", "es", "kr");
	
	private static final String URL = "https://bulbapedia.bulbagarden.net/wiki/Ability";

	private static final Map<String, String> mappings = new HashMap<String, String>();
	static {
		mappings.put("Trace", "Fährte");
	}

	private AbilitiesDB abilitiesDb;

	public AbilitiesParser()
	{
		super();
	}

	public AbilitiesParser(Consumer<Object> onRowCompelted)
	{
	}

	public AbilitiesParser(AbilitiesDB db, Consumer<Object> onRowCompelted)
	{
		super(onRowCompelted);
		this.abilitiesDb = db;
	}

	public List<Ability> parse() {
		List<Ability> result = new LinkedList<Ability>();
		try {
            // find move names
            Document doc = Jsoup.connect(URL).get();
            Element table = doc.getElementById("List_of_Abilities").parent().nextElementSibling().children().get(0).getElementsByTag("table").get(0);
            iterateRows(table, row -> {
            	Ability a = new Ability();
            	if (!StringUtils.isNumeric(toStr(row.child(0))))
            		return;
            	a.setId(toInt(row.child(0)));
            	if (this.abilitiesDb != null && this.abilitiesDb.getEntries().stream().anyMatch(other -> a.getId() == other.getId()))
            		return;
            	a.getNames().addText("en", toStr(row.child(1).getElementsByTag("a").get(0)));
            	a.getDescriptions().addText("en", toStr(row.child(2)));
            	a.setSinceGen(romanToInt(row.child(3).text().trim()));
            	parseNamesAndEffectGer(a);
            	parseDescFr(a);
				result.add(a);
				onRowCompleted(a);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
		return result;
	}

	private void parseNamesAndEffectGer(Ability a) {
		String url = "https://www.pokewiki.de/%s";
		try {
            // find move names
			String name = a.getNames().getText().replaceAll(" ", "_");
			if (mappings.containsKey(name)) {
				name = mappings.get(name);
			}
			Document doc = Jsoup.connect(String.format(url, name)).get();
            Element lastRow = doc.getElementById("bodyContent").getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr").last();
            a.getDescriptions().addText("de", toStr(lastRow.child(0)));
            iterateRows(doc.getElementById("In_anderen_Sprachen").parent().nextElementSibling(), row -> {
				String lang = decodeLang(toStr(row.child(0)));
				if (SUPPORTED_LANGS.contains(lang))
					a.getNames().addText(lang, toStr(row.child(1)));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	private void parseDescFr(Ability a) {
		String url = "https://www.pokepedia.fr/%s";
		try {
            // find move names
            Document doc = Jsoup.connect(String.format(url, URLEncoder.encode(a.getNames().getText("fr"), "UTF-8"))).get();
            Element startTag = doc.getElementById("Descriptions");
            if (startTag == null)
            	return;
			a.getDescriptions().addText("fr", toStr(startTag.parent().nextElementSibling().child(1)));
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private String decodeLang(String trim) {
		final String id = trim.toUpperCase().substring(0, 3);
		switch (id) {
			case "DEU":
				return "de";
			case "ENG":
				return "en";
			case "JAP":
				return "jp";
			case "FRA":
				return "fr";
			case "ITA":
				return "it";
			case "KOR":
				return "kr";
			case "CHI":
				return "cn";
			case "SPA":
				return "es";
			default:
				break;
		}
		return null;
	}

	private int romanToInt(String str) {
		switch (str) {
			case "I":
				return 1;
			case "II":
				return 2;
			case "III":
				return 3;
			case "IV":
				return 4;
			case "V":
				return 5;
			case "VI":
				return 6;
			case "VII":
				return 7;
			case "VIII":
				return 8;
			case "IX":
				return 9;
			case "X":
				return 10;
			default:
				break;
		}
		return 0;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		xstream = XStreamHelper.initXStreamAbilities();		
		file = AbstractParser.XML_DIR.concat("abilities.xml");
		try {
			db = (AbilitiesDB) xstream.fromXML(new FileInputStream(file));
			new AbilitiesParser(db, o -> appendToXml(o)).parse();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static XStream xstream;
	private static String file;
	private static AbilitiesDB db;

	private static void appendToXml(Object o) {
		Ability a = (Ability) o;
		System.out.println(a);
		try {
			db.getEntries().add(a);
			xstream.toXML(db, new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}	
	}
	
}
