package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Pokedex;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.parser.xmlreader.GamesXMLReader;
import de.digitalpenguin.pokeapp.parser.xmlreader.MovesXMLReader;
import de.digitalpenguin.pokeapp.parser.xmlreader.PokemonXMLReader;

public class PokemonGen8MovesParser extends AbstractParser
{
	private static final String URL = "https://www.pokewiki.de/%s/Attacken";
	
	public static void main(String[] args) {
		
		PokemonDB db = new PokemonXMLReader(new File(XML_DIR, "pokemon.xml")).read();
		
		GamesDB gamesdb = new GamesXMLReader(new File(XML_DIR, "games.xml")).read();
		Map<Integer, String> namesDeCache = new HashMap<Integer, String>();
		db.getEntries().forEach(p -> namesDeCache.put(p.getId(), p.getName("de")));
		
		MovesDB movesDb = new MovesXMLReader(new File(XML_DIR, "moves.xml")).read();
		Map<String, Integer> movesDeCache = new TreeMap<String, Integer>();
		movesDb.getMoves().forEach(m -> movesDeCache.put(m.getName("de"), m.getId()));
		
		Map<Integer, String> result = new PokemonGen8MovesParser(namesDeCache, movesDeCache,
				gamesdb.getRegions().stream().filter(r -> r.getGeneration() == 8).findFirst().get().getPokedex()
		).parse(Range.between(1, 809));
		
		result.entrySet().forEach(e -> System.out.println(String.format("%03d", e.getKey()) + ": " + e.getValue()));
	}

	private Map<Integer, String> namesDeCache;
	private Map<String, Integer> movesDeCache;
	private Pokedex galardex;
	
	public PokemonGen8MovesParser(Map<Integer, String> namesDeCache, Map<String, Integer> movesDeCache, Pokedex galardex) {
		this.namesDeCache = namesDeCache;
		this.movesDeCache = movesDeCache;
		this.galardex = galardex;
	}
	
	public Map<Integer, String> parse(Range<Integer> range) {
		Map<Integer, String> result = new TreeMap<>();
		for (Entry<Integer, String> pEntry : this.namesDeCache.entrySet()) {
			if (!galardex.containsNationalDexId(pEntry.getKey()) || (pEntry.getKey() < range.getMinimum() || (pEntry.getKey() > range.getMaximum()))) 
				continue;
			List<String> movesEncoded = new LinkedList<String>();
			try {
	            // find move names
	            Document doc = Jsoup.connect(String.format(URL, pEntry.getValue().replaceAll("’", "'").replaceAll(" ", ""))).get();
	            Element start = doc.getElementById("8._Generation");
	            if (start == null)
	            	continue;
				Elements tables = start.parent().nextElementSibling().getElementsByClass("atktable");
				Elements tableRows = tables.get(0).child(0).getElementsByClass("round").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
	            for (Element row : tableRows) {
	                if (row.hasClass("atkrow")) {
	                	StringBuffer sb = new StringBuffer();
	            		sb.append("8,");
	            		if (row.child(0).getElementsByTag("a").size() > 0)
	            			continue;
	            		sb.append(row.child(0).text().trim() + ",");
	            		final String moveName = row.child(1).text().trim();
						Integer moveId = movesDeCache.entrySet().stream().filter(e -> e.getKey().contains(moveName)).findFirst().get().getValue();
						sb.append(Long.toString(moveId) + ",");
						sb.append("L,");
						sb.append(""); // Machine-Name
	            		movesEncoded.add(sb.toString());
	                }
	            }
	            if (tables.size() > 1) {
		            tableRows = tables.get(1).child(0).getElementsByClass("round").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
		            for (Element row : tableRows) {
		                if (row.hasClass("atkrow")) {
		                	StringBuffer sb = new StringBuffer();
		            		sb.append("8,");
		            		sb.append(",");
		            		final String moveName = row.child(1).getElementsByTag("a").get(0).text().trim();
							Integer moveId = movesDeCache.entrySet().stream().filter(e -> e.getKey().contains(moveName)).findFirst().get().getValue();
							sb.append(Long.toString(moveId) + ",");
							sb.append("M,");
							sb.append(row.child(0).text().trim()); // Machine-Name
		            		movesEncoded.add(sb.toString());
		                }
		            }
	            }
	            result.put(pEntry.getKey(), StringUtils.join(movesEncoded, ';'));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		return result;
	}
	
}
