package de.digitalpenguin.pokeapp.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Consumer;

import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class PokemonAbilitiesParser extends AbstractParser
{
	private static final String URL = "https://www.pokewiki.de/{0}";

	private final DataProvider dataProvider;

	private final int startWithId;

	private Set<PokemonAbility> pokemonAbilities = new HashSet<>();

	public PokemonAbilitiesParser(int startWithId, DataProvider dataProvider, Consumer<Object> callback)
	{
		super(callback);
		this.startWithId = startWithId;
		this.dataProvider = dataProvider;
	}

	public void parse() {
		Locale.setDefault(Locale.GERMAN);
		for (Pokemon p : dataProvider.getPokemonEntries()) {
			if (p.getId() < startWithId)
				continue;
			try {
				Document doc = Jsoup.connect(MessageFormat.format(URL, p.getName(Locale.GERMAN).replaceAll("’", "'").replaceAll(" ", ""))).get();
				Element tdContent = doc.getElementsByAttributeValue("href", "/F%C3%A4higkeit").get(0).parent().nextElementSibling();
				for (Element link : tdContent.getElementsByTag("a")) {
					if (link.text().length() < 3)
						continue;
                    List<Ability> abilities = dataProvider.findAbilities(link.text());
                    if (abilities.isEmpty())
                        continue;
                    Ability ability = abilities.iterator().next();
					PokemonAbility pa = new PokemonAbility(p.getId(), ability.getId());
					Element nextElementSibling = link.nextElementSibling();
					if (nextElementSibling != null && nextElementSibling.tagName().equals("sup")) {
						if (nextElementSibling.text().trim().contains("Alola")) {
							pa.setVariant(dataProvider.getPokemonForm(pa.getPokemonId(), VariantType.ALOLAN));
						} else if(nextElementSibling.text().trim().contains("Galar")) {
							pa.setVariant(dataProvider.getPokemonForm(pa.getPokemonId(), VariantType.GALARIAN));
						} else if(nextElementSibling.text().trim().contains("Mega")) {
							pa.setVariant(dataProvider.getPokemonForm(pa.getPokemonId(), VariantType.MEGA));
						}
						if(nextElementSibling.text().trim().contains("VF")) {
							pa.setHidden(true);
						}
					}
					pa.setPokemon(p);
					pa.setPokemonId(p.getId());
					pokemonAbilities.add(pa);
					onRowCompleted(pa);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public List<PokemonAbility> getPokemonAbilities() {
		return new ArrayList<>(this.pokemonAbilities);
	}
}
