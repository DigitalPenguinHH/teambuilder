package de.digitalpenguin.pokeapp.parser.xmlreader;

import java.io.File;

import com.thoughtworks.xstream.XStream;

public abstract class AbstractXmlReader<T>
{
	private File xmlFile;

    private XStream xstream;

	public AbstractXmlReader(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	
	@SuppressWarnings("unchecked")
	public T read() {
		xstream = getXStream();
        T persistentEntity = null;
        persistentEntity = (T) xstream.fromXML(xmlFile);
        return persistentEntity;
	}

	private XStream getXStream() {
		XStream xstream = new XStream();
		initXStream(xstream);
		return xstream;
	}

	protected abstract void initXStream(XStream xstream);
}
