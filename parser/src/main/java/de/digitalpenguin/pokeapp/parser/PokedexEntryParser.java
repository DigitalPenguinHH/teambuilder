package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PokedexEntryParser
{
	
	private static final String URL = "https://www.bisafans.de/pokedex/listen/regionaldex.php?dex=Galar";

	public static void main(String[] args) {
		List<String> pairs = new LinkedList<>();
		 try {
             // find move names
             Document docNames = Jsoup.connect(URL).get();
             Elements nameTableRows = docNames.getElementsByClass("table-responsive").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
             for (Element element : nameTableRows) {
                 int idxId = 0, idxNatDexId = 1;
                 if (element.child(0).tagName().equals("th"))
                     continue;
                 if (element.getElementsByTag("td").size() == 4) {
                     idxId = 1;
                     idxNatDexId = 2;
                 }
                 String idStr = element.child(idxId).text();
                 String href = element.child(idxNatDexId).getElementsByTag("a").get(0).attr("href");
                 String natIdStr = href.substring(href.indexOf(".php")-3, href.indexOf(".php"));
                 pairs.add(idStr.concat(",").concat(natIdStr));
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
		 System.out.println(StringUtils.join(pairs, ';'));
	}

}
