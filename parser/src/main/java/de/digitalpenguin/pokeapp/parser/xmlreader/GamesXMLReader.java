package de.digitalpenguin.pokeapp.parser.xmlreader;

import java.io.File;

import com.thoughtworks.xstream.XStream;

import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.GenerationConverter;
import de.digitalpenguin.pokeapp.converter.HiddenMachineConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Region;

public class GamesXMLReader extends AbstractXmlReader<GamesDB>
{
	public GamesXMLReader(File xmlFile)
	{
		super(xmlFile);
	}

	public static void main(String[] args) {
		GamesDB db = new GamesXMLReader(new File("D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\games.xml")).read();
		db.getGames().forEach(g -> System.out.println(g.getId() + " : " + g.getName()));
		System.out.println();
		db.getRegions().forEach(r -> System.out.println(r.getName() + " : " + r.toString()));
	}

	@Override
	protected void initXStream(XStream xstream) {
        xstream.setClassLoader(GamesDB.class.getClassLoader());
        xstream.alias("db", GamesDB.class);
        xstream.alias("region", Region.class);
        xstream.alias("game", Game.class);
        xstream.alias("generation", Generation.class);
        xstream.alias("hm", HiddenMachine.class);
        xstream.registerConverter(new GameConverter());
        xstream.registerConverter(new RegionConverter());
        xstream.registerConverter(new GameConverter());
        xstream.registerConverter(new GenerationConverter());
        xstream.registerConverter(new HiddenMachineConverter());
	}
}
