package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PokemonInfoParserDE
{
	private static final String URL = "https://www.pokewiki.de/%s";
	
	private float height;
	private float weight;
	private String dexEntryDe;

	public void parse(String nameEn) {
		try {
            // find move names
            Document doc = Jsoup.connect(String.format(URL, nameEn.toLowerCase())).get();
            Elements tableRows = doc.getElementById("bodyContent").getElementsByClass("innerround").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            int rowIdx = 0;
            for (Element row : tableRows) {
            	if (row.children().size() > 1) {
                	String str = row.child(1).text().trim();
    				switch(rowIdx) {
                		case 16:
                        	this.height = Float.parseFloat(str.substring(0, str.indexOf("m") - 1).replaceAll("\\,", "."));
                        	break;
                		case 17:
                        	this.weight = Float.parseFloat(str.substring(0, str.indexOf("kg") - 1).replaceAll("\\,", "."));;
                        	break;
                    	default:
                    		break;
                	}
            	}
            	rowIdx++;
            }
            Element tableDexEntries = doc.getElementById("Pokédex-Einträge").parent();
            do {
            	tableDexEntries = tableDexEntries.nextElementSibling();
            } while (!tableDexEntries.tagName().equals("table"));
            tableRows = tableDexEntries.getElementsByTag("tbody").get(0).getElementsByTag("tr");
            rowIdx = 0;
            for (Element row : tableRows) {
            	if (rowIdx == 1) {
            		this.dexEntryDe = row.child(2).text().trim();
            	}
            	rowIdx++;
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) {
		new PokemonInfoParserDE().parse("Memmeon");
	}

	public float getHeight() {
		return height;
	}

	public float getWeight() {
		return weight;
	}

	public String getDexEntryDe() {
		return dexEntryDe;
	}
}
