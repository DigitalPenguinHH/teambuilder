package de.digitalpenguin.pokeapp.parser.xmlreader;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

import com.thoughtworks.xstream.XStream;

import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.PProperty;

public class MovesXMLReader extends AbstractXmlReader<MovesDB>
{
	public MovesXMLReader(File xmlFile)
	{
		super(xmlFile);
	}

	public static void main(String[] args) {
		MovesDB db = new MovesXMLReader(new File("D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\moves.xml")).read();
		Map<String, Integer> result = new TreeMap<String, Integer>();
		db.getMoves().forEach(m -> result.put(m.getName("de"), (int) m.getId()));
		result.entrySet().forEach(e -> System.out.println(e.getKey() + " : " + e.getValue()));
	}

	@Override
	protected void initXStream(XStream xstream) {
        xstream.setClassLoader(MovesDB.class.getClassLoader());
        xstream.alias("db", MovesDB.class);
        xstream.alias("move", Move.class);
        xstream.alias("type", MoveType.class);
        xstream.alias("property", PProperty.class);
        xstream.registerConverter(new MoveConverter());
        xstream.addImplicitCollection(MovesDB.class, "moves");
	}
}
