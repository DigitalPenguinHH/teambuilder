package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import de.digitalpenguin.pokeapp.data.Descriptions;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.LocalizedText;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.provider.MoveProvider;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class TechnicalRecordsParser extends AbstractParser
{
	private static final String URL_EN = "https://www.serebii.net/swordshield/tr.shtml";

	private final MoveProvider dataProvider;

	private List<HiddenMachine> machines = new LinkedList<HiddenMachine>();

	public TechnicalRecordsParser(MoveProvider dataProvider, Consumer<Object> callback)
	{
		super(callback);
		this.dataProvider = dataProvider;
	}

	public void parse(Range<Integer> range) {
		try {
            Document doc = Jsoup.connect(URL_EN).get();
            iterateRows(doc.getElementById("content").getElementsByTag("table").get(0), new Consumer<Element>() {
				@Override
				public void accept(Element row) {

					String numberStr = toStr(row.child(0)).substring(2);

					if (!range.contains(Integer.valueOf(numberStr)))
						return;

					HiddenMachine hm = new HiddenMachine();
					hm.setGeneration(8);
					LocalizedText names = new LocalizedText();
					names.addText("de", String.format("TP%s", numberStr));
					names.addText("en", String.format("TR%s", numberStr));
					names.addText("es", String.format("DT%s", numberStr));
					names.addText("fr", String.format("DT%s", numberStr));
					names.addText("it", String.format("DT%s", numberStr));
					hm.setNames(names);
					Move m = dataProvider.getMoveByName(toStr(row.child(1)), Locale.ENGLISH);
					hm.setMoveId(m.getId());
					hm.setMove(m);

					machines.add(hm);

					onRowCompleted(hm);
				}
			});
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public List<HiddenMachine> getMachines() {
		return machines;
	}
}
