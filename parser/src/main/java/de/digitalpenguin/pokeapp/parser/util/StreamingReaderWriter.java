package de.digitalpenguin.pokeapp.parser.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;

import de.digitalpenguin.pokeapp.parser.AbstractParser;

public class StreamingReaderWriter
{
	
	private File inFile;
	private File outFile;
	private Map<Integer, String> writeMoves;
	
	public StreamingReaderWriter(File inFile, File outFile, Map<Integer, String> writeMoves)
	{
		this.inFile = inFile;
		this.outFile = outFile;
		this.writeMoves = writeMoves;
	}

	public void readAndWrite() {
		final MutableBoolean active = new MutableBoolean(false);
		final MutableInt current = new MutableInt(0);
		try (Stream<String> stream = Files.lines(Paths.get(inFile.getAbsolutePath()));
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF8"))) {
			stream.forEach(l -> {
				try {
					if (l.contains("<id>")) {
						Integer id = Integer.parseInt(l.substring(l.indexOf("<id>") + 4, l.indexOf("</id>")));
						if (writeMoves.containsKey(id)) {
							active.setTrue();
							current.setValue(id);
						}
					}
					else if (active.isTrue()) {
						int startIdx = l.indexOf("</movesLearned>");
						if (startIdx > 0) {
							l = l.substring(0, startIdx).concat(";").concat(writeMoves.get(current.getValue())).concat(l.substring(startIdx));
							active.setFalse();
							current.setValue(0);
						}
					}
					System.out.println(l);
					out.write(l);
					out.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws IOException {
		List<String> lines = FileUtils.readLines(new File("D:\\Temp\\newmoves_gen8.txt"), Charset.forName("UTF-8"));
		Map<Integer, String> movesEncoded = new HashMap<Integer, String>();
		lines.forEach(l -> {
			String[] parts = l.split("\\: ");
			movesEncoded.put(Integer.parseInt(parts[0]), parts[1]);
		});
		new StreamingReaderWriter(
				new File(AbstractParser.XML_DIR, "pokemon.xml"), 
				new File("D:\\Temp\\pokemon_new.xml"),
				movesEncoded).readAndWrite();
	}
}
