package de.digitalpenguin.pokeapp.parser.xmlreader;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

import com.thoughtworks.xstream.XStream;

import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.converter.PokemonConverter;
import de.digitalpenguin.pokeapp.converter.PokemonDBConverter;
import de.digitalpenguin.pokeapp.converter.VariantConverter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.Variant;

public class PokemonXMLReader extends AbstractXmlReader<PokemonDB>
{
	public PokemonXMLReader(File xmlFile)
	{
		super(xmlFile);
	}

	public static void main(String[] args) {
		PokemonDB db = new PokemonXMLReader(new File("D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\pokemon.xml")).read();
		Map<String, Integer> result = new TreeMap<String, Integer>();
		db.getEntries().forEach(m -> result.put(m.getName("de"), (int) m.getId()));
		result.entrySet().forEach(e -> System.out.println(e.getKey() + " : " + e.getValue()));
	}

	@Override
	protected void initXStream(XStream xstream) {
		xstream.setClassLoader(PokemonDB.class.getClassLoader());
        xstream.alias("db", PokemonDB.class);
        xstream.alias("pokemon", Pokemon.class);
        xstream.alias("move", MoveLearned.class);
        xstream.alias("variant", Variant.class);
        xstream.registerConverter(new PokemonDBConverter());
        xstream.registerConverter(new PokemonConverter());
        xstream.registerConverter(new MoveConverter());
        xstream.registerConverter(new VariantConverter(DataStore.getInstance()));
	}
	
}
