package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.data.util.DataProvider;

public class PokemonMoveLearnedRegionalParser extends AbstractParser
{
	private static final String URL = "https://pokewiki.de/{0}/Attacken";

	private final DataProvider dataProvider;

	private final Range<Integer> pokemonRange;

	public PokemonMoveLearnedRegionalParser(Range<Integer> pokemonRange, DataProvider dataProvider, Consumer<Object> callback)
	{
		super(callback);
		this.pokemonRange = pokemonRange;
		this.dataProvider = dataProvider;
	}

	public void parse() {
		Locale.setDefault(Locale.GERMAN);
		for (Pokemon p : dataProvider.getPokemonEntries()) {
			if (!pokemonRange.contains(p.getId()))
				continue;
			try {
				Document doc = Jsoup.connect(MessageFormat.format(URL, p.getName(Locale.GERMAN).replaceAll("’", "'").replaceAll(" ", ""))).get();
				for (Element heading : doc.getElementsByTag("h2")) {
					VariantType variantType = null;
					Integer generation = null;
					if (heading.children().size() > 0) {
						String attrId = heading.child(0).attr("id");
						if (attrId.contains("Galar")) {
							variantType = VariantType.GALARIAN;
						} else if (attrId.contains("Alola")) {
							variantType = VariantType.ALOLAN;
						}
						generation = Integer.parseInt(attrId.substring(0, 1));
					}
					if (generation == null)
						continue;
					Elements tables = heading.nextElementSibling().getElementsByClass("atktable");
					for (Element table : tables) {
						MoveLearnTrigger trigger = null;
						Element triggerHeadeing = table.parent().child(0);
						int moveNameIdx = 1;
						if (triggerHeadeing.text().contains("Levelaufstieg")) {
							trigger = MoveLearnTrigger.LEVELUP;
						} else if (triggerHeadeing.text().contains("TM/TP") || triggerHeadeing.text().contains("TM/VM")) {
							trigger = MoveLearnTrigger.MACHINE;
						} else if (triggerHeadeing.text().contains("Vererbbarkeit")) {
							trigger = MoveLearnTrigger.BREEDING;
							moveNameIdx = 0;
						} else if (triggerHeadeing.text().contains("Attacken-Lehrer")) {
							trigger = MoveLearnTrigger.TUTOR;
						} else {
							continue;
						}
						int rowIdx = 0;
						String gamesForTable[] = null;
						for (Element row : table.getElementsByClass("round").get(0).getElementsByTag("tr")) {
							if (rowIdx == 0) {
								Elements gamesHeaders = row.getElementsByClass("sk_item");
								if (gamesHeaders.size() > 0) {
									gamesForTable = extractGamesInfo(gamesHeaders);
								}
							}
							if (!row.hasClass("atkrow")) {
								rowIdx++;
								continue;
							}
							MoveLearned ml = new MoveLearned();
							ml.setTrigger(trigger);
							ml.setPokemonId(p.getId());
							if (variantType != null)
								ml.setVariantId(dataProvider.getPokemonForm(p.getId(), variantType).getId());
							ml.setGeneration(generation);
							Element tdName = row.child(moveNameIdx);
							if (tdName.attr("style").replaceAll(" ", "").contains("display:none")) {
								moveNameIdx++;
								tdName = row.child(moveNameIdx);
							}
							String moveName = tdName.child(0).text().trim();
							Move move = dataProvider.getMoveByName(moveName, Locale.GERMAN);
							int moveId = move.getId();
							ml.setMoveId(moveId);
							switch(trigger){
								case LEVELUP:
									ml.setLevel(toInt(row.child(0)));
									break;
								case MACHINE:
									ml.setMachine(toStr(row.child(0)));
									break;
								case TUTOR:
									setGames(row.child(0).getElementsByClass("sk_item"), ml);
									break;
								default:
									break;
							}
							if (trigger != MoveLearnTrigger.TUTOR) {
								Elements gameLinks = tdName.getElementsByClass("sk_item");
								if (gameLinks != null && gameLinks.size() > 0) {
									setGames(gameLinks, ml);
								} else if (gamesForTable != null) {
									ml.setGames(gamesForTable);
								}
							}
							if (ml.getMachine() != null && (ml.getMachine().equalsIgnoreCase("TM") || ml.getMachine().equalsIgnoreCase("VM"))) {
								rowIdx++;
								continue;
							}
							if (moveId == 0) {
								throw new NullPointerException("Move could not be found: " + moveName);
							}
							System.out.println(
									StringUtils.rightPad(Integer.toString(p.getId()), 3) + " " +
									StringUtils.rightPad(p.getName(), 15) + " " +
									StringUtils.rightPad(StringUtils.defaultString(variantType == null ? null : variantType.name()), 8) + " " +
									generation + " " +
									(ml.getGames() == null ? "          " : StringUtils.rightPad(StringUtils.join(ml.getGames(), ','),10) + " ") +
									StringUtils.rightPad(trigger.name(), 8) + " " +
									StringUtils.rightPad(StringUtils.defaultString((ml.getLevel() == null ? ml.getMachine() : Integer.toString(ml.getLevel()))), 5) + " " +
									StringUtils.rightPad(move.getName(), 25));
							onRowCompleted(ml);
							rowIdx++;
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void setGames(Elements links, MoveLearned ml) {
		ml.setGames(extractGamesInfo(links));
	}

	private String[] extractGamesInfo(Elements links) {
		List<String> games = new LinkedList<>();
		for (Element gameLink : links) {
			String gameId = gameLink.text().trim();
			if (gameId.indexOf('Ω') == 0) {
				gameId = "O".concat(gameId.substring(1));
			} else if (gameId.indexOf('α') == 0) {
				gameId = "A".concat(gameId.substring(1));
			}
			games.add(gameId);
		}
		return StringUtils.join(games, '#').split("#");
	}

}
