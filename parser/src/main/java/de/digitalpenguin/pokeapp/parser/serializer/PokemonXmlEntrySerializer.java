package de.digitalpenguin.pokeapp.parser.serializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;

import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.parser.PokemonInfoParserDE;
import de.digitalpenguin.pokeapp.parser.PokemonInfoParserEN;
import de.digitalpenguin.pokeapp.parser.PokemonMovesParser;
import de.digitalpenguin.pokeapp.parser.PokemonNamesParser;
import de.digitalpenguin.pokeapp.parser.PokemonStatsParser;
import de.digitalpenguin.pokeapp.parser.xmlreader.MovesXMLReader;

public class PokemonXmlEntrySerializer
{
	
	private static final String TEMPLATE = 
			"    <pokemon>\r\n" + 
			"      <id>{0}</id>\r\n" + 
			"      <names>\r\n" + 
			"        <name lang=\"de\">{1}</name>\r\n" + 
			"        <name lang=\"kr\">{2}</name>\r\n" + 
			"        <name lang=\"en\">{3}</name>\r\n" + 
			"        <name lang=\"fr\">{4}</name>\r\n" + 
			"      </names>\r\n" + 
			"      <version>SS</version>\r\n" + 
			"      <properties>{5}</properties>\r\n" + 
			"      <height>{6}</height>\r\n" + 
			"      <weight>{7}</weight>\r\n" + 
			"      <genders>{8}</genders>\r\n" + 
			"      <movesLearned>{9}</movesLearned>\r\n" + 
			"      <baseStats>{10}</baseStats>\r\n" + 
			"      <dexEntries>\r\n" + 
			"        <entry lang=\"de\">{11}</entry>\r\n" + 
			"        <entry lang=\"en\">{12}</entry>\r\n" +
			"      </dexEntries>\r\n" + 
			"    </pokemon>\r\n";
	
	private static NumberFormat nf = new DecimalFormat("0.0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

	public static void main(String[] args) {
		
//		Range<Integer> range = Range.between(886, 888);
		Range<Integer> range = Range.between(866, 890);
		
		PokemonNamesParser namesParser = new PokemonNamesParser();
		namesParser.parse(range);
		
		PokemonStatsParser statsParser = new PokemonStatsParser();
		statsParser.parse(range);
		
		PokemonInfoParserDE parserInfoDe = new PokemonInfoParserDE();
		PokemonInfoParserEN parserInfoEn = new PokemonInfoParserEN();
		
		File toFile = new File("D:\\Temp\\pokemon_new.xml");
		if (toFile.exists()) {
			try (PrintWriter writer = new PrintWriter(toFile)) {
				writer.print("");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		Map<String, Integer> movesCache = new TreeMap<String, Integer>();
		MovesDB db = new MovesXMLReader(new File("D:\\dev\\AndroidStudioProjects\\pokeapp\\app\\src\\main\\res\\raw\\moves.xml")).read();
		db.getMoves().forEach(m -> movesCache.put(m.getName("de"), (int) m.getId()));
		PokemonMovesParser movesParser = new PokemonMovesParser(movesCache);
		
		for (int id = range.getMinimum(); id <= range.getMaximum(); id++) {			

			try (BufferedWriter out = new BufferedWriter(
					new OutputStreamWriter(
							new FileOutputStream(toFile, true), StandardCharsets.UTF_8))) {
			
				Map<String, String> names = namesParser.getNames().get(id);
				parserInfoDe.parse(names.get("de"));
				parserInfoEn.parse(names.get("en"));
				movesParser.parse(names.get("en"));
				String result = MessageFormat.format(TEMPLATE, 
						StringUtils.leftPad(Integer.toString(id), 3, '0'), // ID
						StringUtils.trimToEmpty(names.get("de")),
						StringUtils.trimToEmpty(names.get("kr")),
						StringUtils.trimToEmpty(names.get("en")),
						StringUtils.trimToEmpty(names.get("fr")),
						StringUtils.join(namesParser.getProperties().get(id), ','), // Properties
						nf.format(parserInfoDe.getHeight()),
						nf.format(parserInfoDe.getWeight()),
						parserInfoEn.getGenerRatioStr(),
						StringUtils.join(movesParser.getMovesEncoded(), ';'),  // moves
						statsParser.getStats().get(id),   // stats
						parserInfoDe.getDexEntryDe(),
						parserInfoEn.getDexEntryEn()
				);
				System.out.println(result);
				out.append(result);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
