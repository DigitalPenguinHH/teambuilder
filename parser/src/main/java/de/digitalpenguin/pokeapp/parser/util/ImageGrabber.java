package de.digitalpenguin.pokeapp.parser.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.lang3.Range;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ImageGrabber
{
	
	private static final String IMAGE_URL = "https://www.serebii.net/swordshield/pokemon/%d.png";
	
	private static final String URL = "https://www.pokewiki.de/Pokémon-Liste";
	
	private static final String OUTPUT_DIR = "D:\\Temp\\pokeapp-img\\20191117\\";

	public static void main(String[] args) {
		ImageGrabber ig = new ImageGrabber();
		Range<Integer> range = Range.between(810, 890);
		try {
            // find move names
            Document doc = Jsoup.connect(URL).get();
            Elements rows = doc.getElementById("bodyContent").getElementsByClass("pwtable1").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
            int rowIdx = 0;
            for (Element row : rows) {
            	if (rowIdx > 0 && row.child(0).tagName().equals("td")) {
            		Integer pokemonNr = Integer.valueOf(row.child(0).text().trim());
            		if (pokemonNr >= range.getMinimum() && pokemonNr <= range.getMaximum()) {
	            		String fileName = "artw%03d%s.png";
						String nameEn = row.child(3).text().trim().toLowerCase().replaceAll("’", "").replaceAll(" ", "").replaceAll("\\.", "");
						ig.grab(String.format(IMAGE_URL, pokemonNr), new File(OUTPUT_DIR, String.format(fileName, pokemonNr, nameEn)));
            		}
            	}
            	rowIdx++;
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void grab(String urlStr, File toFile) {
		try {
			URL url = new URL(urlStr);
			InputStream in = new BufferedInputStream(url.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] response = out.toByteArray();
			FileOutputStream fos = new FileOutputStream(toFile);
			fos.write(response);
			fos.close();
			System.out.println(urlStr + ": OK");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
