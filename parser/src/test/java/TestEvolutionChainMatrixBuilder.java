
import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.util.EvolutionChainMatrixBuilder;

public class TestEvolutionChainMatrixBuilder extends AbstractResourceBasedTest {


    @Test
    public void testBuild1() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condLvlUp7 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 7);
        EvolutionCondition<?> condLvlUp10 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 10);
        EvolutionCondition<?> condLvlRandom = new EvolutionCondition<>(EvolutionCondition.Type.RANDOM, "");
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(265,266, new ArrayList<>(Arrays.asList(condLvlUp7, condLvlRandom))),
                new EvolutionChainLink(266,267,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condLvlUp10))),
                new EvolutionChainLink(265,268, new ArrayList<>(Arrays.asList(condLvlUp7, condLvlRandom))),
                new EvolutionChainLink(268,269,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condLvlUp10)))));

        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals(2, rows.get(0).size());
        Assert.assertEquals(2, rows.get(1).size());
    }

    @Test
    public void testBuild2() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condLvlUp25 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 25);
        EvolutionCondition<?> condAquaStone = new EvolutionCondition<>(EvolutionCondition.Type.ITEM, new Item("AquaStone"));
        EvolutionCondition<?> condTrandeKingsRock = new EvolutionCondition<>(EvolutionCondition.Type.TRADE_WITH_ITEM, new Item("King's Rock"));
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(60,61,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condLvlUp25))),
                new EvolutionChainLink(61,62,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condAquaStone))),
                new EvolutionChainLink(61,186,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condTrandeKingsRock)))));

        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals(2, rows.get(0).size());
        Assert.assertEquals(2, rows.get(1).size());
        Assert.assertEquals(rows.get(0).get(0), rows.get(1).get(0));
    }

    @Test
    public void testBuild3() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condAquaStone = new EvolutionCondition<>(EvolutionCondition.Type.ITEM, new Item("AquaStone"));
        EvolutionCondition<?> condThunderStone = new EvolutionCondition<>(EvolutionCondition.Type.ITEM, new Item("AquaStone"));
        EvolutionCondition<?> condFireStone = new EvolutionCondition<>(EvolutionCondition.Type.ITEM, new Item("AquaStone"));
        EvolutionCondition<?> condAffection = new EvolutionCondition<>(EvolutionCondition.Type.FRIENDSHIP, 200);
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(133,134,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condAquaStone))),
                new EvolutionChainLink(133,135,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condThunderStone))),
                new EvolutionChainLink(133,136,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condFireStone))),
                new EvolutionChainLink(133,196,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condAffection)))));

        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(4, rows.size());
        Assert.assertEquals(1, rows.get(0).size());
        Assert.assertEquals(1, rows.get(1).size());
        Assert.assertEquals(1, rows.get(2).size());
        Assert.assertEquals(1, rows.get(3).size());
    }

    @Test
    public void testBuild4() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condLvlUp20 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 20);
        EvolutionCondition<?> condFreeteamSlot = new EvolutionCondition<>(EvolutionCondition.Type.FREE_TEAMSLOT, "");
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(290,291,new ArrayList<EvolutionCondition<?>>(Arrays.asList(condLvlUp20))),
                new EvolutionChainLink(290,292,new ArrayList<>(Arrays.asList(condLvlUp20, condFreeteamSlot)))
        ));
        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals(1, rows.get(0).size());
        Assert.assertEquals(1, rows.get(1).size());
    }

    @Test
    public void testBuildGalarianZigzagoon() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condLvlUp20 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 20);
        EvolutionCondition<?> condLvlUp40 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 40);
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(263,264,new ArrayList<>(Arrays.asList(condLvlUp20))),
                new EvolutionChainLink(263,264,"galarian","galarian",new ArrayList<>(Arrays.asList(condLvlUp20))),
                new EvolutionChainLink(264,827,"galarian","galarian",new ArrayList<>(Arrays.asList(condLvlUp40)))
        ));
        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals(1, rows.get(0).size());
        Assert.assertEquals(2, rows.get(1).size());
    }

    @Test
    public void testBuildGalarianPonyta() {
        EvolutionChain chain = new EvolutionChain();
        EvolutionCondition<?> condLvlUp40 = new EvolutionCondition<>(EvolutionCondition.Type.LEVELUP, 40);
        chain.setChainLinks(Arrays.asList(
                new EvolutionChainLink(77,78, new ArrayList<>(Arrays.asList(condLvlUp40))),
                new EvolutionChainLink(77,78,"galarian","galarian",new ArrayList<>(Arrays.asList(condLvlUp40)))
        ));
        Map<Integer, List<EvolutionChainLink>> rows = new EvolutionChainMatrixBuilder(chain).getRows();
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals(1, rows.get(0).size());
        Assert.assertEquals(77, rows.get(0).get(0).getPokemonIdFrom());
        Assert.assertEquals(78, rows.get(0).get(0).getPokemonIdTo());
        Assert.assertEquals(1, rows.get(1).size());
        Assert.assertEquals(77, rows.get(1).get(0).getPokemonIdFrom());
        Assert.assertEquals(78, rows.get(1).get(0).getPokemonIdTo());
        Assert.assertEquals("galarian", rows.get(1).get(0).getVariantFrom());
        Assert.assertEquals("galarian", rows.get(1).get(0).getVariantTo());
    }

}
