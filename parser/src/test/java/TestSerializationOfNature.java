import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Locale;

import de.digitalpenguin.pokeapp.converter.NatureConverter;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.NaturesDB;

public class TestSerializationOfNature extends AbstractResourceBasedTest {

    @Before
    public void setUp() {
        Locale.setDefault(Locale.GERMAN);
        initXStream();
    }

    @Test
    public void testDeserializeDB() throws IOException {
        NaturesDB db = (NaturesDB) xstream.fromXML(getRawResource("natures.xml"));
        Assert.assertEquals(25, db.getEntries().size());
    }

    private XStream xstream;

    private void initXStream()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(NaturesDB.class.getClassLoader());
            xstream.alias("db", NaturesDB.class);
            xstream.alias("nature", Nature.class);
            xstream.addImplicitCollection(NaturesDB.class, "entries", Nature.class);
            xstream.registerConverter(new NatureConverter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
