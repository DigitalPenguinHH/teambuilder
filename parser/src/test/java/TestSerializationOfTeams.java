import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.converter.TeamConverter;
import de.digitalpenguin.pokeapp.converter.TeamMemberConverter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Team;
import de.digitalpenguin.pokeapp.data.TeamDB;
import de.digitalpenguin.pokeapp.data.TeamMember;

import static de.digitalpenguin.pokeapp.converter.AbstractConverter.XML_HEADER;
public class TestSerializationOfTeams extends AbstractResourceBasedTest
{
	private File file = new File("teams_test.xml");
	
	@Before
	public void setUp() {
		Locale.setDefault(Locale.GERMAN);
        if (file.exists())
            file.delete();
	}

    @Test
    public void testSerializeDB() throws IOException {

	    initXStreamGames();
	    GamesDB dbGames = (GamesDB) xstream.fromXML(getRawResource("games.xml"));

	    TeamDB db = new TeamDB();
	    Team t1 = new Team();
	    t1.setId("ABCD-1234");
	    t1.setName("Awesome Team A");
        Game g1 = new Game();
        g1.addName("en","Sapphire");
        t1.setGame(g1);
        t1.setMembers(Arrays.asList(new TeamMember(1,9), new TeamMember(2,44), new TeamMember(3,120)));
        t1.setLastModified(new Date().getTime());
        MoveLearned m1 = new MoveLearned();
        m1.setGeneration(7);
        m1.setMoveId(92);
        m1.setTrigger(MoveLearnTrigger.MACHINE);
        m1.setPokemonId(9);
        m1.setMachine("TM33");
        MoveLearned m2 = new MoveLearned();
        m2.setGeneration(7);
        m2.setLevel(54);
        m2.setMoveId(76);
        m2.setTrigger(MoveLearnTrigger.LEVELUP);
        m2.setPokemonId(9);
        t1.getMembers().get(0).teach(m1,m2);
        Team t2 = new Team();
        t2.setId("DEFG-5678");
        t2.setName("Beautiful Team B");
        Game g2 = new Game();
        g2.addName("en","Ultra Moon");
        t2.setGame(g2);
        t2.setMembers(Arrays.asList(new TeamMember(1,601), new TeamMember(2,680), new TeamMember(3,745)));
        t2.setLastModified(new Date().getTime());
	    db.setTeams(Arrays.asList(t1, t2));

	    initXStream(dbGames);

        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write(XML_HEADER);
        xstream.marshal(db, new PrettyPrintWriter(writer, new char[] { ' ', ' ' }));
        List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
        assertLineExists(lines, "<id>ABCD-1234</id>");
        assertLineExists(lines, "<name>Awesome Team A</name>");
        assertLineExists(lines, "<game>Sapphire</game>");
        assertLineExists(lines, "<member>");
        assertLineContains(lines, "<modified>");
        assertLineContains(lines, "<movesLearned>");
        assertLineContains(lines, "7,54,76,L,");
        assertLineContains(lines, "7,,92,M,TM33");
    }

    private void assertLineExists(List<String> lines, String s) {
	    boolean exists = false;
        for (String l: lines) {
            if (l.trim().equals(s))
                exists = true;
        }
        Assert.assertTrue(exists);
    }

    private void assertLineContains(List<String> lines, String s) {
        boolean contains = false;
        for (String l: lines) {
            if (l.trim().contains(s))
                contains = true;
        }
        Assert.assertTrue(contains);
    }

    @Test
	public void testDeserializeDB() throws IOException {
	    testSerializeDB();
		TeamDB db = (TeamDB) xstream.fromXML(new FileInputStream(file));
		Assert.assertEquals(2, db.getTeams().size());
        //
        Team peek = db.getTeams().get(db.getTeams().size()-1);
        Assert.assertEquals("DEFG-5678", peek.getId());
        Assert.assertEquals("Beautiful Team B", peek.getName());
        Assert.assertEquals("Ultra Mond", peek.getGame().getName());
        Assert.assertEquals(7, peek.getGame().getGeneration());
        Assert.assertEquals(3, peek.getMembers().size());
	}


	private XStream xstream;

	private void initXStream(GamesDB dbGames)
	{
        xstream = new XStream();
        xstream.setClassLoader(TeamDB.class.getClassLoader());
        xstream.alias("db", TeamDB.class);
        xstream.alias("team", Team.class);
        xstream.alias("member", TeamMember.class);
        xstream.registerConverter(new TeamConverter(DataStore.getInstance()));
        xstream.registerConverter(new TeamMemberConverter());
        xstream.addImplicitCollection(TeamDB.class, "teams", Team.class);
        xstream.addImplicitCollection(Team.class, "members", TeamMember.class);
	}

	private void initXStreamGames() {
        xstream = new XStream();
        xstream.setClassLoader(Pokemon.class.getClassLoader());
        xstream.alias("db", GamesDB.class);
        xstream.alias("region", Region.class);
        xstream.alias("game", Game.class);
        xstream.registerConverter(new GameConverter());
        xstream.registerConverter(new RegionConverter());
    }
}
