import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import de.digitalpenguin.pokeapp.converter.AbilityConverter;
import de.digitalpenguin.pokeapp.data.Ability;

public class TestSerializationOfAbility extends AbstractResourceBasedTest
{
	private static String xml = "<ability>" + 
			"  <id>15</id>" + 
			"  <names>" + 
			"    <name lang=\"de\">Sprunghaft</name>" + 
			"    <name lang=\"en\">Bouncy</name>" + 
			"  </names>" + 
			"  <descriptions>" + 
			"    <text lang=\"de\">Eine deutsche Beschreibung.</text>" + 
			"    <text lang=\"en\">An english description.</text>" + 
			"  </descriptions>" + 
			"  <sinceGen>4</sinceGen>" + 
			"</ability>";
	
	@Before
	public void setUp() {
		xstream = new XStream();
		xstream.alias("ability", Ability.class);
		xstream.registerConverter(new AbilityConverter());
	}
	
	@Test
	public void testUnmarshal() {
		
		Ability a = (Ability) xstream.fromXML(xml);
		assertEquals(15, a.getId());
		assertEquals(4, a.getSinceGen());
		assertEquals("Sprunghaft", a.getName("de"));
		assertEquals("An english description.", a.getDescriptions().getText("en"));
	}
	
	@Test
	public void testMarshal() {
		xstream = new XStream();
		xstream.alias("ability", Ability.class);
		xstream.registerConverter(new AbilityConverter());
		Ability ab = new Ability();
		ab.setId(15);
		ab.setSinceGen(4);
		ab.getDescriptions().addText("de", "Eine deutsche Beschreibung.");
		ab.getDescriptions().addText("en", "An english description.");
		ab.addName("de", "Sprunghaft");
		ab.addName("en", "Bouncy");
		String resultXml = xstream.toXML(ab);
		assertEquals(asOneLine(xml), asOneLine(resultXml));
	}

	@Test
	public void testReadDatabaseFromXml() {
		requireAbilitiesDB();
	}
	
	private String asOneLine(String s) {
		return s.replace("\n", "").replace("\r", "");
	}
}
