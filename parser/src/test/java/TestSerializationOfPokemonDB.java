import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.PokedexEntry;

public class TestSerializationOfPokemonDB extends AbstractResourceBasedTest
{
	
	@Before
	public void setUp() {
		Locale.setDefault(Locale.ENGLISH);
		requireGamesDB();
		requirePokemonDB();
	}

    @Test
	public void testDeserialize() {
		List<PokedexEntry> entries = DataStore.getInstance().getRegion(8).getPokedex().getEntries();
		Collections.sort(entries, new Comparator<PokedexEntry>() {
			@Override
			public int compare(PokedexEntry e1, PokedexEntry e2) {
				return Integer.valueOf(e1.getLocalDexId()).compareTo(Integer.valueOf(e2.getLocalDexId()));
			}
		});
		for (PokedexEntry e : entries) {
			System.out.println(String.format("Local.Dex[%03d] National.Dex[%03d] = %s",
					e.getLocalDexId(), e.getNationalDexId(), DataStore.getInstance().getById(e.getNationalDexId()).getName()));
		}
	}
}
