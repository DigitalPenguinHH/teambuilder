import com.thoughtworks.xstream.XStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Locale;

import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Region;

public class TestSerializationOfGames extends AbstractResourceBasedTest
{
	
	@Before
	public void setUp() {
		Locale.setDefault(Locale.GERMAN);
		initXStream();
	}
	
	@Test
	public void testDeserializeDB() throws IOException {
		GamesDB db = (GamesDB) xstream.fromXML(getRawResource("games.xml"));
		Assert.assertEquals(30, db.getGames().size());
		//
		Game peek = db.getGames().get(db.getGames().size()-1);
		Assert.assertEquals(7, peek.getGeneration());
		Assert.assertEquals("Let's Go Evoli", peek.getName());
		Assert.assertEquals("LGE", peek.getId());
		//
        Assert.assertEquals(7, db.getRegions().size());
        Region r = db.getRegions().get(4);
        Assert.assertEquals("Einall", r.getName());
        Assert.assertEquals("Unova", r.getNameEn());
        Assert.assertNotNull(r.getPokedex());
	}

	private XStream xstream;

	private void initXStream()
	{
        try {
			xstream = new XStream();
			xstream.setClassLoader(MovesDB.class.getClassLoader());
			xstream.alias("db", GamesDB.class);
			xstream.alias("game", Game.class);
			xstream.alias("region", Region.class);
            xstream.registerConverter(new RegionConverter());
            xstream.registerConverter(new GameConverter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
