package de.digitalpenguin.pokeapp.parser;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.parser.util.SQLiteHelper;

public class PokemonAbilitiesParserTest {

    public static void main(String[] args) {
        try (SQLiteHelper helper = new SQLiteHelper()) {
            final AtomicInteger idx = new AtomicInteger(0);
            new PokemonAbilitiesParser(1, helper, o -> {
                PokemonAbility pa = (PokemonAbility) o;
                if (pa.getVariant() != null) {
                    helper.insert(String.format("INSERT INTO POKEMON_ABILITY (id,ability_id,pokemon_id,variant_id,hidden) VALUES (%d,%d,%d,%d,%d)",
                            idx.incrementAndGet(), pa.getAbilityId(), pa.getPokemonId(), pa.getVariant().getId(), pa.isHidden() ? 1 : 0));
                } else {
                    helper.insert(String.format("INSERT INTO POKEMON_ABILITY (id,ability_id,pokemon_id,hidden) VALUES (%d,%d,%d,%d)",
                            idx.incrementAndGet(), pa.getAbilityId(), pa.getPokemonId(), pa.isHidden() ? 1 : 0));
                }
                System.out.println("Insert complete for " + pa.getPokemon() + " - [" + pa.getVariant() + "]");
            }).parse();
            helper.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}