package de.digitalpenguin.pokeapp.parser.serializer;

import com.thoughtworks.xstream.XStream;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.DamageVarianceModifier;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.parser.AbstractResourceBasedTest;

public class TestSerializationOfMove extends AbstractResourceBasedTest
{
	private File file = new File("test_lang.xml");
	
	@Before
	public void setUp() {
		initXStream();
		if (file.exists())
			file.delete();
	}
	
	@Test
	public void testSerialize() throws IOException {
		Move m = new Move();
		m.setId(123);
		m.addName("de","Sprung");
		m.addName("en","Jump");
		m.addName("fr","Saut");
		m.addName("es","Salto");
		m.setAccuracy(100);
		m.setStrength(40);
		m.setProperty(PProperty.FIRE);
		m.setType(MoveType.SPECIAL);
		m.setAp(10);
		m.setContestCategory(ContestCategory.CLEVERNESS);
		m.setGeneration(2);
		m.setDamageVariable(true);
		m.setDamageFix(false);
		m.setDamageVarianceModifier(DamageVarianceModifier.KP);
		xstream.toXML(m, new FileWriter(file));
		List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
		Assert.assertTrue(lines.contains("  <name lang=\"de\">Sprung</name>"));
		Assert.assertTrue(lines.contains("  <name lang=\"en\">Jump</name>"));
		Assert.assertTrue(lines.contains("  <name lang=\"fr\">Saut</name>"));
		Assert.assertTrue(lines.contains("  <name lang=\"es\">Salto</name>"));
	}
	
	@Test
	public void testDeserialize() throws IOException {
		testSerialize();
		Move move = (Move) xstream.fromXML(file);
		Assert.assertEquals("Salto", move.getName("es"));
		Assert.assertEquals(123, move.getId());
		Assert.assertEquals(true, move.isDamageVariable());
		Assert.assertEquals(MoveType.SPECIAL, move.getType());
		Assert.assertEquals((Integer) 10, move.getAp());
	}
	
	@Test
	public void testDeserializeDB() throws IOException {
		MovesDB db = (MovesDB) xstream.fromXML(getRawResource("moves.xml"));
		Assert.assertEquals(796, db.getMoves().size());
	}

	private XStream xstream;

	private void initXStream()
	{
        try {
			xstream = new XStream();
			xstream.setClassLoader(MovesDB.class.getClassLoader());
			xstream.alias("db", MovesDB.class);
			xstream.alias("move", Move.class);
			xstream.alias("type", MoveType.class);
			xstream.alias("property", PProperty.class);
			xstream.alias("contestCategory", ContestCategory.class);
			xstream.registerConverter(new MoveConverter());
            xstream.addImplicitCollection(MovesDB.class, "moves");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
