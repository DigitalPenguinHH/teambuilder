package de.digitalpenguin.pokeapp.parser.serializer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.parser.AbstractResourceBasedTest;

public class TestSerializationOfItem extends AbstractResourceBasedTest {

    @Before
    public void setUp() {
        Locale.setDefault(Locale.GERMAN);
        requireItemsDB();
    }

    @Test
    public void testDeserializeDB() {
        Assert.assertEquals(817, DataStore.getInstance().getItems().size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testItemResource() {
        requirePokemonDB();
        requireMoves();
        requireGamesDB();
        DataStore.getInstance().initMachineDetails();
		Collection<Item> items = CollectionUtils.select(DataStore.getInstance().getItems(), new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                Item i = (Item) object;
                return i.getCategory() == ItemCategory.MACHINES;
            }
        });
        for (Object o : items) {
            Item item = (Item) o;
            String itemName = item.getName(Locale.ENGLISH);
            Move move = DataStore.getInstance().getMoveForMachine(itemName);
            Assert.assertNotNull(itemName, move);
        }
    }

}
