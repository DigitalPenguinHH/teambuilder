package de.digitalpenguin.pokeapp.parser.serializer;

import com.thoughtworks.xstream.XStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Locale;

import de.digitalpenguin.pokeapp.converter.CharacteristicConverter;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.CharacteristicsDB;
import de.digitalpenguin.pokeapp.parser.AbstractResourceBasedTest;

public class TestSerializationOfCharacteristics extends AbstractResourceBasedTest {

    @Before
    public void setUp() {
        Locale.setDefault(Locale.GERMAN);
        initXStream();
    }

    @Test
    public void testDeserializeDB() throws IOException {
        CharacteristicsDB db = (CharacteristicsDB) xstream.fromXML(getRawResource("characteristics.xml"));
        Assert.assertEquals(6, db.getEntries().size());
    }

    private XStream xstream;

    private void initXStream()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(CharacteristicsDB.class.getClassLoader());
            xstream.alias("db", CharacteristicsDB.class);
            xstream.alias("characteristic", Characteristic.class);
            xstream.addImplicitCollection(CharacteristicsDB.class, "entries", Characteristic.class);
            xstream.registerConverter(new CharacteristicConverter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
