package de.digitalpenguin.pokeapp.parser.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.digitalpenguin.pokeapp.data.Ability;
import de.digitalpenguin.pokeapp.data.AbstractDataProvider;
import de.digitalpenguin.pokeapp.data.Characteristic;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemCategory;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.Nature;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonAbility;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.PokemonForm;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.VariantType;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

public class SQLiteHelper extends AbstractDataProvider implements AutoCloseable {

    private Connection con = null;

    public SQLiteHelper() {
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:app/src/main/assets/teambuilder");
            con.setAutoCommit(false);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public int maxId(String tableName) {
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT MAX(ID) FROM " + tableName);
            if (result.next()) {
                return (int) result.getLong(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int count(String fromQuery) {
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT COUNT(1) " + fromQuery);
            if (result.next()) {
                return (int) result.getLong(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Move getMove(int moveId) {
        return null;
    }

    private Map<String, Move> moveCache = new HashMap<>(800);

    private static Map<String, String> movesRenamed = new HashMap<>();
    static {
        movesRenamed.put("Pfund", "Klaps");
        movesRenamed.put("Schnabel", "Pikser");
        movesRenamed.put("Konfustrahl", "Konfusstrahl");
        movesRenamed.put("Giftblick", "Schlangenblick");
        movesRenamed.put("Gesichte", "Scharfblick");
        movesRenamed.put("Rollentausch", "Rollenspiel");
        movesRenamed.put("Wertewechsel", "Fähigkeitstausch");
        movesRenamed.put("Pflanzsäulen", "Pflanzensäulen");
        movesRenamed.put("Fauna-Statue", "Flora-Statue");
        movesRenamed.put("Staffette", "Stafette");
        movesRenamed.put("Verwandler", "Wandler");
    }

    @Override
    public Move getMoveByName(String value, Locale locale) {
        String key = locale.getLanguage().concat("=").concat(value);
        if (moveCache.containsKey(key))
            return moveCache.get(key);
        if (locale.equals(Locale.GERMAN) && movesRenamed.containsKey(value)) {
            value = movesRenamed.get(value);
        }
        Move move = new Move();
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT id, names FROM MOVE WHERE names LIKE '%" + (locale.getLanguage() + "=" + value) + "%'");
            if (result.next()) {
                move.setId(result.getInt(1));
                move.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
            }
            moveCache.put(key, move);
            return move;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return move;
    }

    @Override
    public Ability getAbility(int id) {
        return null;
    }

    @Override
    public List<Ability> getAbilities() {
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT id, names, sinceGen FROM ABILITY ORDER BY id");
            List<Ability> abilities = new LinkedList<>();
            while (result.next()) {
                Ability a = new Ability();
                a.setId(result.getInt(1));
                a.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
                a.setSinceGen(result.getInt(3));
                abilities.add(a);
            }
            return abilities;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>(0);
    }

    @Override
    public List<Ability> findAbilities(String name) {
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT id, names FROM ABILITY WHERE names LIKE '%de=" + name + "%' ORDER BY id");
            List<Ability> abilities = new LinkedList<>();
            while (result.next()) {
                Ability a = new Ability();
                a.setId(result.getInt(1));
                a.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
                abilities.add(a);
            }
            CollectionUtils.filter(abilities, new Predicate() {
                @Override
                public boolean evaluate(Object object) {
                    return ((Ability) object).getName(Locale.GERMAN).equals(name);
                }
            });
            return abilities;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>(0);
    }

    public void insert(String sql) {
        try {
            Statement stat = con.createStatement();
            stat.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws IOException {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Pokemon getById(int pokemonId) {
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery(String.format("SELECT id, names, properties FROM POKEMON where id = %d", pokemonId));
            if (result.next()) {
                Pokemon p = new Pokemon();
                p.setId(result.getInt(1));
                p.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
                p.setProperties(new PProperty.PropertyArrayConverter().convertToEntityProperty(result.getString(3)));
                return p;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Pokemon getRandomPokemon(Integer generation) {
        return null;
    }

    @Override
    public PokemonData getPokemonData(int pokemonId) {
        return null;
    }

    @Override
    public PokemonData getPokemonData(Pokemon p) {
        return null;
    }

    @Override
    public List<Pokemon> getPokemonEntries() {
        List<Pokemon> resultList = new LinkedList<>();
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery("SELECT id, names, properties FROM POKEMON ORDER BY id");
            while (result.next()) {
                Pokemon p = new Pokemon();
                p.setId(result.getInt(1));
                p.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
                p.setProperties(new PProperty.PropertyArrayConverter().convertToEntityProperty(result.getString(3)));
                resultList.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public List<Variant> getForms(int pokemonId) {
        return null;
    }

    @Override
    public PokemonForm getPokemonForm(int pid, int variantIdx) {
        return null;
    }

    private Map<String, PokemonForm> formCache = new HashMap<>(300);

    @Override
    public PokemonForm getPokemonForm(int pid, VariantType form) {
        String key = Integer.toString(pid).concat("#").concat(form == null ? "" : form.name());
        if (formCache.containsKey(key))
            return formCache.get(key);
        try {
            Statement stat = con.createStatement();
            ResultSet result = stat.executeQuery(String.format("SELECT id, names, properties, type, suffix, pokemon_id FROM VARIANT where pokemon_id = %d and type = '%s'", pid, form.name()));
            if (result.next()) {
                Variant v = new Variant();
                v.setId(result.getInt(1));
                v.setNames(new LocalizedTextConverter().convertToEntityProperty(result.getString(2)));
                v.setProperties(new PProperty.PropertyArrayConverter().convertToEntityProperty(result.getString(3)));
                v.setType(form);
                v.setSuffix(result.getString(5));
                v.setPokemonId(pid);
                Pokemon pokemon = getById(pid);
                PokemonForm newForm = PokemonForm.newForm(v, pokemon);
                formCache.put(key, newForm);
                return newForm;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Pokemon> findPokemon(String name) {
        return null;
    }

    @Override
    public List<PokemonForm> findPokemonForms(String name) {
        return null;
    }

    @Override
    public List<Pokemon> findPokemon(DataFilter filter) {
        return null;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }

    @Override
    public List<Move> findMoves(String name) {
        return null;
    }

    @Override
    public List<Move> listMoves(MoveType selectedMoveType, PProperty selectedProperty) {
        return null;
    }

    @Override
    public List<HiddenMachine> listHiddenMachineMoves() {
        return null;
    }

    @Override
    public Move getMoveForMachine(String machineName) {
        return null;
    }

    @Override
    public Map<Integer, List<MoveLearned>> getGenerationsMoveSet(PokemonForm p) {
        return null;
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId) {
        return null;
    }

    @Override
    public List<MoveLearned> getGenerationsMoveList(PokemonForm p, Integer generation, String gamesId, MoveLearnTrigger trigger) {
        return null;
    }

    @Override
    public boolean hasGameSpecificMoves(Pokemon p) {
        return false;
    }

    @Override
    public List<MoveLearned> listMovesLearned(PokemonForm p) {
        return null;
    }

    @Override
    public EvolutionChain getEvolutionChain(int pokemonId) {
        return null;
    }

    @Override
    public List<EvolutionChain> getEvolutionChains() {
        return null;
    }

    @Override
    public List<Generation> getGenerations() {
        return null;
    }

    @Override
    public List<Region> getRegions() {
        return null;
    }

    @Override
    public Region getRegion(Integer generation) {
        return null;
    }

    @Override
    public List<Characteristic> getCharacteristics() {
        return null;
    }

    @Override
    public List<Characteristic> findCharacteristics(String name) {
        return null;
    }

    @Override
    public List<Nature> getNatures() {
        return null;
    }

    @Override
    public List<Nature> findNatures(String name) {
        return null;
    }

    @Override
    public List<MoveLearned> listPokemonMovesLearned(int moveId) {
        return null;
    }

    @Override
    public List<PokemonAbility> listPokemonWithAbility(int abilityId) {
        return null;
    }

    @Override
    public List<PokemonAbility> listAbilities(int pokemonId, VariantType type) {
        return null;
    }

    @Override
    public boolean hasMoves(PokemonData pokemon, int generation, String gameKey) {
        return false;
    }

    @Override
    public int getPokemonEntriesCount() {
        return 0;
    }

    @Override
    public List<Game> getGames() {
        return null;
    }

    @Override
    public List<Item> getItems() {
        return null;
    }

    @Override
    public List<Item> findItems(String name) {
        return null;
    }

    @Override
    public List<Item> listItems(ItemCategory category) {
        return null;
    }
}