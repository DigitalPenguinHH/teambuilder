package de.digitalpenguin.pokeapp.parser.serializer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.digitalpenguin.pokeapp.converter.AbstractConverter;
import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.converter.PokemonConverter;
import de.digitalpenguin.pokeapp.converter.PokemonDBConverter;
import de.digitalpenguin.pokeapp.converter.VariantConverter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.PokemonData;
import de.digitalpenguin.pokeapp.data.Stats;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.data.Version;
import de.digitalpenguin.pokeapp.data.provider.ItemProvider;
import de.digitalpenguin.pokeapp.parser.AbstractResourceBasedTest;

public class TestSerializationOfMoveLearned extends AbstractResourceBasedTest
{
	private File file = new File("test_movelearned.xml");
	
	@Before
	public void setUp() {
		requireItemsDB();
		initXStream(DataStore.getInstance());
		if (file.exists())
			file.delete();
	}
	
	@Test
	public void testSerialize() throws IOException {
		PokemonDB db = new PokemonDB();
		PokemonData p1 = new PokemonData();
		p1.setId(585);
		Map<String, String> namesMap = new HashMap<>(2);
		namesMap.put("de", "Sesokitz");
		namesMap.put("en", "Deerling");
		p1.setNames(namesMap);
		p1.setProperties(new PProperty[] { PProperty.NORMAL, PProperty.GRASS });
		p1.setVersion(Version.RBY);
		p1.setHeight(0.8f);
		p1.setWeight(4f);
		p1.setMalePercent(79.5f);
		p1.setFemalePercent(20.5f);
		p1.addDexEntry("de", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
		p1.setBaseStats(new Stats(100,14,12,23,19,40));
		Variant v1 = new Variant();
		v1.getNames().addText("de", "AlphaDE");
		v1.getNames().addText("en", "AlphaEN");
		Variant v2 = new Variant();
		v2.getNames().addText("de", "DeltaDE");
		v2.getNames().addText("en", "DeltaEN");
		v2.setProperties(new PProperty[]{PProperty.FIRE, PProperty.FLYING});
		v2.setTrigger(new Item("Catalyst"));
		p1.setForms(Arrays.asList(v1, v2));
		MoveLearned move1 = new MoveLearned();
		move1.setGeneration(1);
		move1.setLevel(-1);
		move1.setMoveId(45);
		move1.setTrigger(MoveLearnTrigger.LEVELUP);
		MoveLearned move2 = new MoveLearned();
		move2.setGeneration(1);
		move2.setLevel(4);
		move2.setMoveId(33);
		move2.setTrigger(MoveLearnTrigger.LEVELUP);
		MoveLearned move3 = new MoveLearned();
		move3.setGeneration(1);
		move3.setMoveId(78);
		move3.setMachine("TM01");
		move3.setTrigger(MoveLearnTrigger.MACHINE);
		MoveLearned move4 = new MoveLearned();
		move4.setGeneration(1);
		move4.setMoveId(81);
		move4.setMachine("VM01");
		move4.setTrigger(MoveLearnTrigger.MACHINE);
		p1.setMovesLearned(Arrays.asList(move1, move2, move3, move4));
		db.setEntries(Arrays.asList(p1));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
		writer.write(AbstractConverter.XML_HEADER);
		xstream.marshal(db, new PrettyPrintWriter(writer, new char[] { ' ', ' ' }));
		List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
		Assert.assertTrue(lines.contains("      <movesLearned>1,-1,45,L,;1,4,33,L,;1,,78,M,TM01;1,,81,M,VM01</movesLearned>"));
	}
	
	@Test
	public void testDeserialize() throws IOException {
		if (!file.exists() || file.getTotalSpace() <= 1) {
			Assert.fail("Input file doesnt exist");
		}
		testSerialize();
		PokemonDB db = (PokemonDB) xstream.fromXML(file);
		Assert.assertEquals(1, db.getEntries().size());
		PokemonData p = db.getEntries().get(0);
		Assert.assertEquals(2, p.getProperties().length);
		Assert.assertEquals((Float) 0.8f, (Float) p.getHeight());
		Assert.assertEquals((Float) 4.0f, (Float) p.getWeight());
		Assert.assertEquals((Float) 79.5f, (Float) p.getMalePercent());
		Assert.assertEquals((Float) 20.5f, (Float) p.getFemalePercent());
		Assert.assertEquals(2, p.getProperties().length);
		Assert.assertEquals(PProperty.NORMAL, p.getPrimaryProperty());
		Assert.assertEquals(PProperty.GRASS, p.getProperties()[1]);
		Assert.assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", p.getDexEntry());
		Collection<MoveLearned> moves = p.getMovesLearned();
		Assert.assertEquals(4, moves.size());
		MoveLearned peek = moves.iterator().next();
		Assert.assertEquals(1, peek.getGeneration());
		Assert.assertEquals(Integer.valueOf(-1), peek.getLevel());
		Assert.assertEquals(45, peek.getMoveId());
		Assert.assertEquals(MoveLearnTrigger.LEVELUP, peek.getTrigger());
		Assert.assertEquals(2, p.getForms().size());
		Assert.assertEquals("AlphaDE", p.getForms().get(0).getName("de"));
		Assert.assertEquals("AlphaEN", p.getForms().get(0).getName("en"));
		Assert.assertEquals(2, p.getForms().get(1).getProperties().length);
		Assert.assertEquals(new Item("Catalyst"), p.getForms().get(1).getTrigger());
	}

	private XStream xstream;

	private void initXStream(ItemProvider provider)
	{
        try {
			xstream = new XStream();
			xstream.setClassLoader(PokemonDB.class.getClassLoader());
			xstream.alias("db", PokemonDB.class);
            xstream.alias("pokemon", Pokemon.class);
			xstream.alias("move", MoveLearned.class);
			xstream.alias("variant", Variant.class);
            xstream.registerConverter(new PokemonDBConverter());
            xstream.registerConverter(new PokemonConverter());
			xstream.registerConverter(new MoveConverter());
			xstream.registerConverter(new VariantConverter(provider));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
