package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.parser.util.SQLiteHelper;

public class PokemonMoveLearnedRegionalParserTest {

    public static void main(String[] args) {
        SQLiteHelper helper = new SQLiteHelper();
        try {
            final AtomicInteger idx = new AtomicInteger(helper.maxId("MOVE_LEARNED"));
            new PokemonMoveLearnedRegionalParser(Range.between(132, 890), helper, o -> {
                MoveLearned ml = (MoveLearned) o;
                helper.insert(String.format("INSERT INTO MOVE_LEARNED (id,generation,level,move_id,trigger,machine,games,pokemon_id,variant_id) VALUES (%d,%d,%d,%d,'%s',%s,%s,%d,%d)",
                        idx.incrementAndGet(), ml.getGeneration(), ml.getLevel(), ml.getMoveId(), ml.getTrigger().name(),
                        ml.getMachine() == null ? "null" : ("'" + ml.getMachine() + "'"),
                        ml.getGames() == null ? "null" : ("'" + StringUtils.join(ml.getGames(), ',') + "'"),
                        ml.getPokemonId(), ml.getVariantId()));
//                System.out.println("Insert complete for " + ml + "   =  " + helper.getMove(ml.getMoveId()).getName());
                if (idx.get()%100==0)
                    helper.commit();
            }).parse();
            helper.commit();
        } catch (Exception e) {
            helper.rollback();
            e.printStackTrace();
        }
    }
}