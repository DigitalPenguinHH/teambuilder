package de.digitalpenguin.pokeapp.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.junit.Assert;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

import de.digitalpenguin.pokeapp.converter.AbstractConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainLinkConverter;
import de.digitalpenguin.pokeapp.converter.GameConverter;
import de.digitalpenguin.pokeapp.converter.GenerationConverter;
import de.digitalpenguin.pokeapp.converter.HiddenMachineConverter;
import de.digitalpenguin.pokeapp.converter.ItemConverter;
import de.digitalpenguin.pokeapp.converter.MoveConverter;
import de.digitalpenguin.pokeapp.converter.PokemonConverter;
import de.digitalpenguin.pokeapp.converter.PokemonDBConverter;
import de.digitalpenguin.pokeapp.converter.RegionConverter;
import de.digitalpenguin.pokeapp.converter.VariantConverter;
import de.digitalpenguin.pokeapp.data.AbilitiesDB;
import de.digitalpenguin.pokeapp.data.ContestCategory;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainDB;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.GamesDB;
import de.digitalpenguin.pokeapp.data.Generation;
import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.ItemsDB;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.MoveLearned;
import de.digitalpenguin.pokeapp.data.MoveType;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PokemonDB;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.Region;
import de.digitalpenguin.pokeapp.data.Variant;
import de.digitalpenguin.pokeapp.util.XStreamHelper;

public class AbstractResourceBasedTest {

    private static final String RAW_RES_PATH = "..\\app\\src\\main\\res\\raw\\";

    protected FileInputStream getRawResource(String fileName) throws FileNotFoundException {
        return new FileInputStream(RAW_RES_PATH + fileName);
    }

    protected File getRawFile(String fileName) {
        return new File(RAW_RES_PATH + fileName);
    }

    protected  void writeXml(File toFile, Object o) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(toFile));
        writer.write(AbstractConverter.XML_HEADER);
        xstream.marshal(o, new PrettyPrintWriter(writer, new char[] { ' ', ' ' }));
    }

    protected void requirePokemonDB() {
        initXStreamPokemon();
        DataStore.getInstance().clearPokemonDB();
        PokemonDB db;
        try {
            db = (PokemonDB) xstream.fromXML(getRawResource("pokemon.xml"));
            DataStore.getInstance().addPokemonEntries(db.getEntries());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        Assert.assertEquals(890, DataStore.getInstance().getPokemonEntries().size());
    }

    protected void requireGamesDB() {
        initXStreamGamesAndRegions();
        GamesDB db = null;
        try {
            db = (GamesDB) xstream.fromXML(getRawResource("games.xml"));
            DataStore.getInstance().setGames(db.getGames());
            DataStore.getInstance().setRegions(db.getRegions());
            DataStore.getInstance().setGenerations(db.getGenerations());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        Assert.assertNotNull(db.getGames());
        Assert.assertEquals(32, db.getGames().size());
    }

    protected void requireItemsDB() {
        initXStreamItems();
        ItemsDB db = null;
        try {
            db = (ItemsDB) xstream.fromXML(getRawResource("items.xml"));
            DataStore.getInstance().setItems(db.getEntries());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        Assert.assertNotNull(db.getEntries());
    }

    protected void requireEvolutionchains() {
        initXStreamEvochains();
        EvolutionChainDB db = null;
        try {
            db = (EvolutionChainDB) xstream.fromXML(getRawResource("evochains.xml"));
            DataStore.getInstance().setEvolutionChains(db.getChains());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        Assert.assertNotNull(db.getChains());
        Assert.assertTrue(db.getChains().size() > 100);
    }

    protected void requireMoves() {
        initXStreamMoves();
        MovesDB db = null;
        try {
            db = (MovesDB) xstream.fromXML(getRawResource("moves.xml"));
            DataStore.getInstance().setMoves(db.getMoves());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        Assert.assertNotNull(db.getMoves());
    }

    protected void requireAbilitiesDB() {
    	AbilitiesDB db = null;
    	try {
			db = XStreamHelper.load(new FileInputStream(RAW_RES_PATH.concat("abilities.xml")), AbilitiesDB.class);
			DataStore.getInstance().setAbilities(db.getEntries());
	        Assert.assertNotNull(db);
		} catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
		}
    }

    protected XStream xstream;

    private void initXStreamPokemon()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(PokemonDB.class.getClassLoader());
            xstream.alias("db", PokemonDB.class);
            xstream.alias("pokemon", Pokemon.class);
            xstream.alias("move", MoveLearned.class);
            xstream.alias("variant", Variant.class);
            xstream.registerConverter(new PokemonDBConverter());
            xstream.registerConverter(new PokemonConverter());
            xstream.registerConverter(new MoveConverter());
            xstream.registerConverter(new VariantConverter(DataStore.getInstance()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initXStreamGamesAndRegions()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(GamesDB.class.getClassLoader());
            xstream.alias("db", GamesDB.class);
            xstream.alias("region", Region.class);
            xstream.alias("game", Game.class);
            xstream.alias("generation", Generation.class);
            xstream.alias("hm", HiddenMachine.class);
            xstream.registerConverter(new GameConverter());
            xstream.registerConverter(new RegionConverter());
            xstream.registerConverter(new GameConverter());
            xstream.registerConverter(new GenerationConverter());
            xstream.registerConverter(new HiddenMachineConverter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initXStreamEvochains()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(EvolutionChainDB.class.getClassLoader());
            xstream.alias("db", EvolutionChainDB.class);
            xstream.alias("chain", EvolutionChain.class);
            xstream.registerConverter(new EvolotionChainConverter());
            xstream.registerConverter(new EvolotionChainLinkConverter(DataStore.getInstance()));
            xstream.addImplicitCollection(EvolutionChainDB.class, "chains", EvolutionChain.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initXStreamMoves()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(MovesDB.class.getClassLoader());
            xstream.alias("db", MovesDB.class);
            xstream.alias("move", Move.class);
            xstream.alias("type", MoveType.class);
            xstream.alias("property", PProperty.class);
            xstream.alias("contestCategory", ContestCategory.class);
            xstream.registerConverter(new MoveConverter());
            xstream.addImplicitCollection(MovesDB.class, "moves");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initXStreamItems()
    {
        try {
            xstream = new XStream();
            xstream.setClassLoader(ItemsDB.class.getClassLoader());
            xstream.alias("db", ItemsDB.class);
            xstream.alias("item", Item.class);
            xstream.addImplicitCollection(ItemsDB.class, "entries", Item.class);
            xstream.registerConverter(new ItemConverter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
