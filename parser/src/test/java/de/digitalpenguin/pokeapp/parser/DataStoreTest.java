package de.digitalpenguin.pokeapp.parser;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.MoveLearnTrigger;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.PProperty;
import de.digitalpenguin.pokeapp.data.PokemonForm;

public class DataStoreTest extends AbstractResourceBasedTest
{
	@Before
	public void setUp() {
		requirePokemonDB();
		requireGamesDB();
	}

	@Test
	public void testGetGenerationsMoveSetAll() throws Exception {
		for (Pokemon p : DataStore.getInstance().getPokemonEntries()) {
			for (int gen = 1; gen <= 7 ; gen++) {
				Assert.assertNotNull(DataStore.getInstance().getGenerationsMoveList(PokemonForm.newDefaultForm(p), gen, MoveLearnTrigger.LEVELUP));
				Assert.assertNotNull(DataStore.getInstance().getGenerationsMoveList(PokemonForm.newDefaultForm(p), gen, MoveLearnTrigger.MACHINE));
			}
		}
	}

	@Test
	public void testListCounters() throws Exception {
		List<PProperty> properties = null;
		// Bug
		properties = DataStore.getInstance().listCounters(PProperty.BUG);
		Assert.assertEquals(3, properties.size());
		Assert.assertTrue(properties.contains(PProperty.FLYING));
		Assert.assertTrue(properties.contains(PProperty.FIRE));
		Assert.assertTrue(properties.contains(PProperty.ROCK));
		// Gross
		properties = DataStore.getInstance().listCounters(PProperty.GRASS);
		Assert.assertEquals(5, properties.size());
		Assert.assertTrue(properties.contains(PProperty.FLYING));
		Assert.assertTrue(properties.contains(PProperty.FIRE));
		Assert.assertTrue(properties.contains(PProperty.POISON));
		Assert.assertTrue(properties.contains(PProperty.BUG));
		Assert.assertTrue(properties.contains(PProperty.ICE));
	}

}
