package de.digitalpenguin.pokeapp.parser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collections;
import java.util.List;

import de.digitalpenguin.pokeapp.comparator.PokemonComparator;
import de.digitalpenguin.pokeapp.data.DataFilter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Pokemon;
import de.digitalpenguin.pokeapp.data.Region;

public class PokedexSorterTest extends AbstractResourceBasedTest
{

    @Before
    public void setUp() {
        requireGamesDB();
        requirePokemonDB();
    }

    @Test
    public void testSortGen6() throws IOException {
        int generation = 6;
        Region region = DataStore.getInstance().getRegion(generation);
        List<Pokemon> entries = DataStore.getInstance().find(DataFilter.regionFilter(region));
        Collections.sort(entries, new PokemonComparator(DataStore.getInstance().getRegion(generation).getPokedex()));
        Assert.assertEquals(650, entries.get(0).getId());
        Assert.assertEquals(651, entries.get(1).getId());
        Assert.assertEquals(652, entries.get(2).getId());
    }

    void initConnection() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

}
