package de.digitalpenguin.pokeapp.parser;

import org.apache.commons.lang3.Range;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import de.digitalpenguin.pokeapp.data.HiddenMachine;
import de.digitalpenguin.pokeapp.data.Move;
import de.digitalpenguin.pokeapp.data.provider.MoveProvider;
import de.digitalpenguin.pokeapp.parser.util.SQLiteHelper;
import de.digitalpenguin.pokeapp.persistence.LocalizedTextConverter;

public class TechnicalRecordsParserTest {

    public static void main(String[] args) {
        try (SQLiteHelper helper = new SQLiteHelper()) {
            final AtomicInteger idx = new AtomicInteger(40);
            new TechnicalRecordsParser(helper, o -> {
                HiddenMachine hm = (HiddenMachine) o;
                helper.insert(String.format("INSERT INTO HIDDEN_MACHINE (id,names,move_id,generation) VALUES (%d,'%s',%d,%d)",
                        idx.incrementAndGet(), new LocalizedTextConverter().convertToDatabaseValue(hm.getNames()), hm.getMoveId(), hm.getGeneration()));
            }).parse(Range.between(0, 99));
            helper.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}