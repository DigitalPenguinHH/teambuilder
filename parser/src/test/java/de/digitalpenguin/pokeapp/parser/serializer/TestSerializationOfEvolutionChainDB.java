package de.digitalpenguin.pokeapp.parser.serializer;

import com.thoughtworks.xstream.XStream;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import de.digitalpenguin.pokeapp.converter.EvolotionChainConverter;
import de.digitalpenguin.pokeapp.converter.EvolotionChainLinkConverter;
import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.EvolutionChain;
import de.digitalpenguin.pokeapp.data.EvolutionChainDB;
import de.digitalpenguin.pokeapp.data.EvolutionChainLink;
import de.digitalpenguin.pokeapp.data.EvolutionCondition;
import de.digitalpenguin.pokeapp.data.EvolutionCondition.Type;
import de.digitalpenguin.pokeapp.data.Game;
import de.digitalpenguin.pokeapp.data.Item;
import de.digitalpenguin.pokeapp.data.MovesDB;
import de.digitalpenguin.pokeapp.data.util.DataProvider;
import de.digitalpenguin.pokeapp.parser.AbstractResourceBasedTest;

public class TestSerializationOfEvolutionChainDB extends AbstractResourceBasedTest
{
	private File file = new File("test_chaindb.xml");
	
	@Before
	public void setUp() {
		requireMoves();
		requireItemsDB();
		requireGamesDB();
		initXStream(DataStore.getInstance());
		if (file.exists())
			file.delete();
	}
	
	@Test
	public void testSerialize() throws IOException {
		
		EvolutionChain chain1 = new EvolutionChain();
		EvolutionChainLink linkA1 = new EvolutionChainLink(1,2,
				Arrays.asList(
                        new EvolutionCondition<>(Type.LEVELUP, Integer.valueOf(16))));
		EvolutionChainLink linkA2 = new EvolutionChainLink(2,3,
				Arrays.asList(
                        new EvolutionCondition<>(Type.LEVELUP, Integer.valueOf(32))));
		chain1.setChainLinks(Arrays.asList(linkA1, linkA2));
		
		EvolutionChain chain2 = new EvolutionChain();
		EvolutionChainLink linkB1 = new EvolutionChainLink(54,55,
				Arrays.asList(
                        new EvolutionCondition<>(Type.LEVELUP, Integer.valueOf(33)),
                        new EvolutionCondition<>(Type.ITEM, "Donnerstein")));
		EvolutionChainLink linkB2 = new EvolutionChainLink(306,307,
				Arrays.asList(
                        new EvolutionCondition<>(Type.NIGHT, "")));
		linkB2.getTrigger().get(0).setGames(Arrays.asList(new Game()));
		chain2.setChainLinks(Arrays.asList(linkB1, linkB2));
		
		EvolutionChainDB db = new EvolutionChainDB(Arrays.asList(chain1, chain2));
		xstream.toXML(db, new FileWriter(file));
		List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
		Assert.assertTrue(lines.contains("<db>"));
		Assert.assertTrue(lines.contains("  <chain>"));
		Assert.assertTrue(lines.contains("    <link from=\"1\" to=\"2\">"));
		Assert.assertTrue(lines.contains("      <condition type=\"LEVELUP\">32</condition>"));
		Assert.assertTrue(lines.contains("    <link from=\"54\" to=\"55\">"));
		Assert.assertTrue(lines.contains("      <condition type=\"ITEM\">Donnerstein</condition>"));
		Assert.assertTrue(lines.contains("      <condition type=\"NIGHT\" generation=\"6\"/></link>"));
	}
	
	@Test
	public void testDeserialize() throws IOException {
		EvolutionChainDB db = (EvolutionChainDB) xstream.fromXML(file);
		Assert.assertEquals(2, db.getChains().size());
		EvolutionChain chain = db.getChains().get(1);
		Assert.assertEquals(2, chain.getChainLinks().size());
		EvolutionChainLink link1 = chain.getChainLinks().get(0);
		Assert.assertEquals(54, link1.getPokemonIdFrom());
		Assert.assertEquals(55, link1.getPokemonIdTo());
		Assert.assertEquals(2, link1.getTrigger().size());
		Assert.assertEquals(Type.LEVELUP, link1.getTrigger().get(0).getCondition());
		Assert.assertEquals(Integer.valueOf(33), link1.getTrigger().get(0).getObject());
		Assert.assertEquals(Type.ITEM, link1.getTrigger().get(1).getCondition());
		Assert.assertEquals(new Item("Donnerstein"), link1.getTrigger().get(1).getObject());
	}


	@Test
	public void testDeserializeAll() {
		requireEvolutionchains();
	}

	private XStream xstream;

	private void initXStream(DataProvider dp)
	{
        try {
			xstream = new XStream();
			xstream.setClassLoader(MovesDB.class.getClassLoader());
			xstream.alias("db", EvolutionChainDB.class);
			xstream.alias("chain", EvolutionChain.class);
			xstream.registerConverter(new EvolotionChainConverter());
			xstream.registerConverter(new EvolotionChainLinkConverter(dp));
            xstream.addImplicitCollection(EvolutionChainDB.class, "chains", EvolutionChain.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
