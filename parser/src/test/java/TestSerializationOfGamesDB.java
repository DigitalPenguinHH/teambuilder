import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Locale;

import de.digitalpenguin.pokeapp.data.DataStore;
import de.digitalpenguin.pokeapp.data.Generation;

public class TestSerializationOfGamesDB extends AbstractResourceBasedTest
{
	
	@Before
	public void setUp() {
		Locale.setDefault(Locale.ENGLISH);
	}

    @Test
	public void testDeserialize() {
		requireGamesDB();

		List<Generation> generations = DataStore.getInstance().getGenerations();
		assertNotNull(generations);
		// Anzahl Generationen (Sonne/Mond ohne VMs) = 6
		assertEquals(6, generations.size());
		assertEquals(5, generations.get(0).getHms().size());
		assertEquals("VM03", generations.get(2).getHms().get(2).getName(Locale.GERMAN));
		assertEquals("HM05", generations.get(4).getHms().get(4).getName(Locale.ENGLISH));
	}
}
